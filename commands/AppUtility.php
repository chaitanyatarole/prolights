<?php
/**
 * Created by PhpStorm.
 * User: Chaitanya
 * Date: 6/14/2015
 * Time: 12:13 PM
 */

namespace app\commands;


class AppUtility
{
    public static function dump($a, $isJson = false)
    {
        if ($isJson) {
            print json_encode(print_r($a, true));
        } else {
            echo '<pre>';
            print_r($a);
            echo '</pre>';
        }
        exit ();
    }

    public static function jsonResponse($response)
    {
        echo json_encode($response);
    }
}