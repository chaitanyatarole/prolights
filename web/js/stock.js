/**
 * Created by chaitanya on 19/11/15.
 */
var app = angular.module('proLightStock', ['ngSanitize', 'ui-notification']);

app.controller('notificationController', ['$scope', '$http', 'Notification', function ($scope, $http, Notification) {
    $http({
        method: 'GET',
        url: baseUrl + '/stock/getpendingworkorderlist'
    }).success(function (response) {
        var length = response.length;
        var count = length;

        Notification.success({
            message: 'Total Pending Work Orders : ' + count + '<br><a href="http://localhost/prolights/web/stock/receiveaudioorder">Click Here</a>',
            title: 'Pending Work Orders'
        });
        //for (var i = 0; i < length; i++) {
        //var eventName = response[i].eventName;
        //var invoiceNumber = response[i].invoice_no;
        //
        //    Notification.success({
        //        message: 'Event Name : ' + eventName + '<br>Invoice No: ' + invoiceNumber + '<br><a href="http://localhost/prolights/web/stock/receiveorder">Check Order</a>',
        //        title: 'Pending Work Order'
        //    });
        //
        //}
    });

    $http({
        method: 'GET',
        url: baseUrl + '/stock/getpendingpurchaseorderlist'
    }).success(function (response) {
        var length = response.length;
        var count = length;
        //for (var i = 0; i < length; i++) {
        //    var vendorName = response[i].client_name;
        //    var invoiceNumber = response[i].purchase_order_no;

        Notification.warning({
            message: 'Total Pending Purchase Orders : ' + count + '<br><a href="http://localhost/prolights/web/stock/receiveaudiopurchaseorder">Check Order</a>',
            title: 'Pending Purchase Order',
            positionY: 'bottom',
            positionX: 'right'
        });

        //}
    });
}]);

app.controller('receivedOrderCtrl', ['$scope', '$http', function ($scope, $http) {

    $scope.isDisabled = false;

    $scope.acceptOrder = function () {
        smoke.confirm("Are you sure you want to accept order?", function (e) {
            if (e) {
                $scope.isDisabled = true;
                var invoiceNumber = $("#invoiceNumber").val();
                var invoice = $.param({invoice: invoiceNumber});
                $http({
                    url: baseUrl + '/stock/acceptaudioorder',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response) {
                        smoke.alert("Order Accepted");
                        location.href = 'http://localhost/prolights/web/stock/showaudioworkorder';
                    }
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    };

    $scope.cancelOrder = function () {

        smoke.confirm("Are you sure you want to reject order This cant be undone?", function (e) {
            if (e) {
                var invoiceNumber = $("#invoiceNumber").val();
                var invoice = $.param({invoice: invoiceNumber});
                $http({
                    url: baseUrl + '/stock/rejectorder',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response) {
                        smoke.alert("Order Rejected");
                        location.reload();
                    }
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    }
}]);

app.controller('receivedBalajiOrderCtrl', ['$scope', '$http', function ($scope, $http) {

    $scope.acceptOrder = function () {
        smoke.confirm("Are you sure you want to accept order?", function (e) {
            if (e) {
                var invoiceNumber = $("#invoiceNumber").val();
                var invoice = $.param({invoice: invoiceNumber});
                $http({
                    url: baseUrl + '/stock/acceptbalajiorder',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response) {
                        smoke.alert("Order Accepted");
                        location.href = 'http://localhost/prolights/web/stock/showbalajiworkorder';
                    }
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    };

    $scope.cancelOrder = function () {

        smoke.confirm("Are you sure you want to reject order This cant be undone?", function (e) {
            if (e) {
                var invoiceNumber = $("#invoiceNumber").val();
                var invoice = $.param({invoice: invoiceNumber});
                $http({
                    url: baseUrl + '/stock/rejectbalajiorder',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response) {
                        smoke.alert("Order Rejected");
                        location.reload();
                    }
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    }
}]);

app.controller('stockReturnCtrl', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {

    $scope.getDetails = function () {
        if ($scope.formInfo.invoiceNumber != '') {
            var invoiceNumber = $scope.formInfo.invoiceNumber;
            var invoice = $.param({invoice: invoiceNumber});
            $http({
                url: baseUrl + '/stock/getaudioorderdetails',
                method: 'POST',
                data: invoice,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                var length = response.length;
                var address = response[0].address;
                var date = response[0].date;
                var clientName = response[0].client_name;
                var clientCode = response[0].clientcode;
                var email = response[0].email;
                var mobile = response[0].phone_no;
                var eventName = response[0].eventName;
                var eventLocation = response[0].eventLocation;
                var invoiceNumber = response[0].invoice_no;
                var total = response[0].total_amount;

                $scope.body = '<div class="form-group"> <label class="control-label col-sm-2">Client Code</label> <div class="col-sm-1"> <input type="text" class="form-control" readonly value="' + clientCode + '"></div><label class="control-label col-sm-1">Date</label><div class="col-sm-2"><input type="date" readonly class="form-control"  value="' + date + '" ></div><label class="control-label col-sm-2">Invoice Number</label> <div class="col-sm-2"> <input type="text" readonly id="invoiceNumber" class="form-control" value="' + invoiceNumber + '" ></div></div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Client Name</label> <div class="col-sm-6"> <input type="text" readonly class="form-control"value="' + clientName + '"></div></div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Address</label> <div class="col-sm-6"> <textarea readonly class="form-control">' + address + '</textarea> </div> </div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Mobile Number</label> <div class="col-sm-3"> <input type="text" class="form-control" readonly value="' + mobile + '"> </div> <label class="control-label col-sm-2">Email</label> <div class="col-sm-3"> <input type="text" readonly class="form-control" value="' + email + '"> </div> </div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Event Name</label> <div class="col-sm-3"> <input type="text" class="form-control" readonly value="' + eventName + '"> </div> <label class="control-label col-sm-2">Event Location</label> <div class="col-sm-3"> <input type="text" readonly class="form-control" value="' + eventLocation + '"> </div> </div>';
                $scope.body += '<div class=""><table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10"> <thead> <th>Sr.No </th> <th>Item </th><th>Quantity</th></thead> <tbody id="taxTable">';

                for (var i = 0; i < length; i++) {
                    var serialNumber = i + 1;
                    var particular = response[i].particularName;
                    var quantity = response[i].quantity;
                    //var rate = response[i].rate;
                    //var amount = response[i].amount;

                    $scope.body += '<tr><td style="width: 60px;">' + serialNumber + '</td><td style="width: 430px;"><input type="text" readonly class="form-control" value="' + particular + '"></td><td style="width: 216px;padding-right: 10px;"><input type="text" class="form-control" readonly value="' + quantity + '"></td></tr>';
                    //$scope.body += '<tr><td style="width: 60px;">' + serialNumber + '</td><td style="width: 430px;"><input type="text" readonly class="form-control" value="' + particular + '"></td><td style="width: 216px;padding-right: 10px;"><input type="text" class="form-control" readonly value="' + quantity + '"></td><td style="width: 200px;padding-right: 10px;"><input type="text" readonly class="form-control" value="' + rate + '"></td><td style="width: 200px;"><input type="text" readonly class="form-control" value="' + amount + '" ></td></tr>';


                }
                //$scope.body += '</tbody><tbody><tr><td style="width: 5px;"></td><td style="width: 200px;"></td><td style="width: 10px;"> </td><td style="width: 60px;"><strong>Total Amount</strong></td><td style="width: 30px;;"><input type="text" readonly class="form-control" value="' + total + '" ></td></tr></tbody></table></div>';
                $scope.body += '</tbody></table></div>';

                $scope.body += '<div class="form-actions"><div class="row"><div class="col-md-5"></div><div class="col-md-7"><button type="button" class="btn green" id="submit" ng-click="addReturnedStock()">Submit</button><button type="button" class="btn default" onclick="location.reload();">Cancel</button></div></div></div>';

                $scope.renderHtml = function (htmlCode) {
                    return $sce.trustAsHtml(htmlCode);
                };

            });
        } else {
            $scope.body = '';
        }
    };

    $scope.addReturnedStock = function () {

        smoke.confirm("Are you sure you want to add returned stock? This can't be undone", function (e) {
            if (e) {
                var invoiceNumber = $("#invoiceNumber").val();
                var invoice = $.param({invoice: invoiceNumber});
                $http({
                    url: baseUrl + '/stock/addreturnedstock',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    smoke.alert("Stock Updated");
                    location.reload();
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    }
}]);

app.controller('balajiStockReturnCtrl', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {

    $scope.getDetails = function () {
        if ($scope.formInfo.invoiceNumber != '') {
            var invoiceNumber = $scope.formInfo.invoiceNumber;
            var invoice = $.param({invoice: invoiceNumber});
            $http({
                url: baseUrl + '/stock/getbalajiorderdetails',
                method: 'POST',
                data: invoice,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                var length = response.length;
                var address = response[0].address;
                var date = response[0].date;
                var clientName = response[0].client_name;
                var clientCode = response[0].clientcode;
                var email = response[0].email;
                var mobile = response[0].phone_no;
                var eventName = response[0].eventName;
                var eventLocation = response[0].eventLocation;
                var invoiceNumber = response[0].invoice_no;
                var total = response[0].total_amount;

                $scope.body = '<div class="form-group"> <label class="control-label col-sm-2">Client Code</label> <div class="col-sm-1"> <input type="text" class="form-control" readonly value="' + clientCode + '"></div><label class="control-label col-sm-1">Date</label><div class="col-sm-2"><input type="date" readonly class="form-control"  value="' + date + '" ></div><label class="control-label col-sm-2">Invoice Number</label> <div class="col-sm-2"> <input type="text" readonly id="invoiceNumber" class="form-control" value="' + invoiceNumber + '" ></div></div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Client Name</label> <div class="col-sm-6"> <input type="text" readonly class="form-control"value="' + clientName + '"></div></div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Address</label> <div class="col-sm-6"> <textarea readonly class="form-control">' + address + '</textarea> </div> </div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Mobile Number</label> <div class="col-sm-3"> <input type="text" class="form-control" readonly value="' + mobile + '"> </div> <label class="control-label col-sm-2">Email</label> <div class="col-sm-3"> <input type="text" readonly class="form-control" value="' + email + '"> </div> </div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Event Name</label> <div class="col-sm-3"> <input type="text" class="form-control" readonly value="' + eventName + '"> </div> <label class="control-label col-sm-2">Event Location</label> <div class="col-sm-3"> <input type="text" readonly class="form-control" value="' + eventLocation + '"> </div> </div>';
                $scope.body += '<div class=""><table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10"> <thead> <th>Sr.No </th> <th>Item </th><th>Quantity</th></thead> <tbody id="taxTable">';

                var totalAmount = 0;
                for (var i = 0; i < length; i++) {
                    var serialNumber = i + 1;
                    var particular = response[i].particularName;
                    var quantity = response[i].quantity;
                    //var rate = response[i].rate;
                    //var amount = response[i].amount;
                    //totalAmount += parseFloat(amount);

                    //$scope.body += '<tr><td style="width: 60px;">' + serialNumber + '</td><td style="width: 430px;"><input type="text" readonly class="form-control" value="' + particular + '"></td><td style="width: 216px;padding-right: 10px;"><input type="text" class="form-control" readonly value="' + quantity + '"></td><td style="width: 200px;padding-right: 10px;"><input type="text" readonly class="form-control" value="' + rate + '"></td><td style="width: 200px;"><input type="text" readonly class="form-control" value="' + amount + '" ></td></tr>';
                    $scope.body += '<tr><td style="width: 60px;">' + serialNumber + '</td><td style="width: 430px;"><input type="text" readonly class="form-control" value="' + particular + '"></td><td style="width: 216px;padding-right: 10px;"><input type="text" class="form-control" readonly value="' + quantity + '"></td></tr>';


                }
                $scope.body += '</tbody></table></div>';
                //$scope.body += '</tbody><tbody><tr><td style="width: 5px;"></td><td style="width: 200px;"></td><td style="width: 10px;"> </td><td style="width: 60px;"><strong>Total Amount</strong></td><td style="width: 30px;;"><input type="text" readonly class="form-control" value="' + totalAmount + '" ></td></tr></tbody></table></div>';


                $scope.body += '<div class="form-actions"><div class="row"><div class="col-md-5"></div><div class="col-md-7"><button type="button" class="btn green" id="submit" ng-click="addReturnedStock()">Submit</button><button type="button" class="btn default" onclick="location.reload();">Cancel</button></div></div></div>';

                $scope.renderHtml = function (htmlCode) {
                    return $sce.trustAsHtml(htmlCode);
                };

            });
        } else {
            $scope.body = '';
        }
    };

    $scope.addReturnedStock = function () {

        smoke.confirm("Are you sure you want to add returned stock? This can't be undone", function (e) {
            if (e) {
                var invoiceNumber = $("#invoiceNumber").val();
                var invoice = $.param({invoice: invoiceNumber});
                $http({
                    url: baseUrl + '/stock/addbalajireturnedstock',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    smoke.alert("Stock Updated");
                    location.reload();
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    }
}]);

app.controller('receivedPurchaseOrderCtrl', ['$scope', '$http', '$compile', function ($scope, $http, $compile) {


    $scope.isDisabled = false;

    $scope.acceptOrder = function () {
        smoke.confirm("Are you sure you want to accept order?", function (e) {
            if (e) {
                $scope.isDisabled = true;
                var amountArray = new Array();
                var rateArray = new Array();
                var quantityArray = new Array();
                var particularArray = new Array();
                var particularNameArray = new Array();


                var table = document.getElementById("taxTable");
                var rowCount = table.rows.length;

                var totamt1 = 0;
                for (var i = 0; i < rowCount; i++) {
                    var a = document.getElementsByName("amount[]")[i].value;
                    var rate = document.getElementsByName("rate[]")[i].value;
                    var quantity = document.getElementsByName("quantity[]")[i].value;
                    var particular = document.getElementsByName("particularID[]")[i].value;
                    var particularName = document.getElementsByName("particular[]")[i].value;

                    if (!isNaN(parseFloat(a))) {
                        amountArray[i] = a;
                        rateArray[i] = rate;
                        quantityArray[i] = quantity;
                        particularArray[i] = particular;
                        particularNameArray[i] = particularName;
                        totamt1 = totamt1 + parseFloat(a);
                    }
                }
                document.getElementById("totamt").value = totamt1.toFixed(2);

                var totalAmount = totamt1;

                var invoiceNumber = $("#invoiceNumber").val();
                var invoice = $.param({
                    entireData: $scope.formInfo,
                    amountArray: amountArray,
                    rateArray: rateArray,
                    quantityArray: quantityArray,
                    particularArray: particularArray,
                    particularNameArray: particularNameArray,
                    total: totalAmount
                });
                $http({
                    url: baseUrl + '/stock/acceptaudiopurchaseorder',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response) {
                        smoke.alert("Order Accepted");
                        location.href = 'http://localhost/prolights/web/stock/receiveaudiopurchaseorder';
                    }
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    };

    $scope.cancelOrder = function () {

        smoke.confirm("Are you sure you want to reject order This cant be undone?", function (e) {
            if (e) {
                var invoiceNumber = $("#invoiceNumber").val();
                var invoice = $.param({invoice: invoiceNumber});
                $http({
                    url: baseUrl + '/stock/rejectaudiopurchaseorder',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response) {
                        smoke.alert("Order Rejected");
                        location.reload();
                    }
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    };
}]);

app.controller('receivedBalajiPurchaseOrderCtrl', ['$scope', '$http', '$compile', function ($scope, $http, $compile) {


    $scope.acceptOrder = function () {
        smoke.confirm("Are you sure you want to accept order?", function (e) {
            if (e) {
                var amountArray = new Array();
                var rateArray = new Array();
                var quantityArray = new Array();
                var particularArray = new Array();
                var particularNameArray = new Array();


                var table = document.getElementById("taxTable");
                var rowCount = table.rows.length;

                var totamt1 = 0;
                for (var i = 0; i < rowCount; i++) {
                    var a = document.getElementsByName("amount[]")[i].value;
                    var rate = document.getElementsByName("rate[]")[i].value;
                    var quantity = document.getElementsByName("quantity[]")[i].value;
                    var particular = document.getElementsByName("particularID[]")[i].value;
                    var particularName = document.getElementsByName("particular[]")[i].value;

                    if (!isNaN(parseFloat(a))) {
                        amountArray[i] = a;
                        rateArray[i] = rate;
                        quantityArray[i] = quantity;
                        particularArray[i] = particular;
                        particularNameArray[i] = particularName;
                        totamt1 = totamt1 + parseFloat(a);
                    }
                }
                document.getElementById("totamt").value = totamt1.toFixed(2);

                var totalAmount = totamt1;

                var invoiceNumber = $("#invoiceNumber").val();
                var invoice = $.param({
                    entireData: $scope.formInfo,
                    amountArray: amountArray,
                    rateArray: rateArray,
                    quantityArray: quantityArray,
                    particularArray: particularArray,
                    particularNameArray: particularNameArray,
                    total: totalAmount
                });
                $http({
                    url: baseUrl + '/site/acceptbalajipurchaseorder',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response) {
                        smoke.alert("Order Accepted");
                        location.href ='http://localhost/prolights/web/site/receivebalajipurchaseorder';
                    }
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    };

    $scope.cancelOrder = function () {

        smoke.confirm("Are you sure you want to reject order This cant be undone?", function (e) {
            if (e) {
                var invoiceNumber = $("#invoiceNumber").val();
                var invoice = $.param({invoice: invoiceNumber});
                $http({
                    url: baseUrl + '/site/rejectbalajipurchaseorder',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response) {
                        smoke.alert("Order Rejected");
                        location.reload();
                    }
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    };
}]);

app.controller('returnVendorItems', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {

    $scope.getData = function () {
        var purchaseNumber = $scope.invoiceNumber;
        if (purchaseNumber != '') {
            var invoice = $.param({invoice: purchaseNumber});
            $http({
                url: baseUrl + '/stock/getaudiovendorstockdetails',
                method: 'POST',
                data: invoice,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                var length = response.length;
                var date = response[0].date;
                var returnDate = response[0].returnDate;
                var clientName = response[0].vendorName;
                var clientCode = response[0].vendorId;
                var invoiceNumber = response[0].purchase_order_no;

                $scope.body = '<div class="form-group"> <label class="control-label col-sm-2">Vendor Code</label> <div class="col-sm-1"> <input type="text" class="form-control" readonly value="' + clientCode + '"></div><label class="control-label col-sm-1">Recv. On</label><div class="col-sm-2"><input type="date" readonly class="form-control"  value="' + date + '" ></div><label class="control-label col-sm-2">Invoice Number</label> <div class="col-sm-2"> <input type="text" readonly id="invoiceNumber" class="form-control" value="' + invoiceNumber + '" ></div></div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Vendor Name</label> <div class="col-sm-4"> <input type="text" readonly class="form-control"value="' + clientName + '"></div><label class="control-label col-sm-2">End Date</label> <div class="col-sm-2"> <input type="text" readonly id="invoiceNumber" class="form-control" value="' + returnDate + '" ></div></div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Returning On</label> <div class="col-sm-2"> <input type="date" class="form-control" value="" ng-model="returningOn"></div></div></div>';
                $scope.body += '<div class=""><table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10"> <thead> <th>Sr.No </th> <th>Item </th><th>Quantity</th></thead> <tbody id="taxTable">';

                for (var i = 0; i < length; i++) {
                    var serialNumber = i + 1;
                    var particular = response[i].particularName;
                    var quantity = response[i].quantity;

                    $scope.body += '<tr><td style="width: 60px;">' + serialNumber + '</td><td style="width: 530px;"><input type="text" readonly class="form-control" value="' + particular + '"></td><td style="width: 316px;padding-right: 10px;"><input type="text" class="form-control" readonly value="' + quantity + '"></td></tr>';


                }
                $scope.body += '</tbody></table></div>';

                $scope.body += '<div class="form-actions"><div class="row"><div class="col-md-5"></div><div class="col-md-7"><button type="button" class="btn green" id="submit" ng-click="clearVendorStock()">Submit</button><button type="button" class="btn default" onclick="location.reload();">Cancel</button></div></div></div>';

                $scope.renderHtml = function (htmlCode) {
                    return $sce.trustAsHtml(htmlCode);
                };
            });
        }
    };
    $scope.clearVendorStock = function () {

        smoke.confirm("Are you sure you want to return items. This cant be undone?", function (e) {
            if (e) {
                var purchaseNumber = $scope.invoiceNumber;
                var returningOn = $scope.returningOn;

                var invoice = $.param({invoice: purchaseNumber, returningOn: returningOn});
                $http({
                    url: baseUrl + '/stock/clearaudiovendorstock',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response) {
                        smoke.alert("Order Cleared");
                        location.reload();
                    }
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    };

}]);


app.controller('returnBalajiVendorItems', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {

    $scope.getData = function () {
        var purchaseNumber = $scope.invoiceNumber;
        if (purchaseNumber != '') {
            var invoice = $.param({invoice: purchaseNumber});
            $http({
                url: baseUrl + '/stock/getbalajivendorstockdetails',
                method: 'POST',
                data: invoice,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                var length = response.length;
                var date = response[0].date;
                var returnDate = response[0].returnDate;
                var clientName = response[0].vendorName;
                var clientCode = response[0].vendorId;
                var invoiceNumber = response[0].purchase_order_no;

                $scope.body = '<div class="form-group"> <label class="control-label col-sm-2">Vendor Code</label> <div class="col-sm-1"> <input type="text" class="form-control" readonly value="' + clientCode + '"></div><label class="control-label col-sm-1">Recv. On</label><div class="col-sm-2"><input type="date" readonly class="form-control"  value="' + date + '" ></div><label class="control-label col-sm-2">Invoice Number</label> <div class="col-sm-2"> <input type="text" readonly id="invoiceNumber" class="form-control" value="' + invoiceNumber + '" ></div></div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Vendor Name</label> <div class="col-sm-4"> <input type="text" readonly class="form-control"value="' + clientName + '"></div><label class="control-label col-sm-2">End Date</label> <div class="col-sm-2"> <input type="text" readonly id="invoiceNumber" class="form-control" value="' + returnDate + '" ></div></div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Returning On</label> <div class="col-sm-2"> <input type="date" class="form-control" value="" ng-model="returningOn"></div></div></div>';
                $scope.body += '<div class=""><table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10"> <thead> <th>Sr.No </th> <th>Item </th><th>Quantity</th></thead> <tbody id="taxTable">';

                for (var i = 0; i < length; i++) {
                    var serialNumber = i + 1;
                    var particular = response[i].particularName;
                    var quantity = response[i].quantity;

                    $scope.body += '<tr><td style="width: 60px;">' + serialNumber + '</td><td style="width: 530px;"><input type="text" readonly class="form-control" value="' + particular + '"></td><td style="width: 316px;padding-right: 10px;"><input type="text" class="form-control" readonly value="' + quantity + '"></td></tr>';


                }
                $scope.body += '</tbody></table></div>';

                $scope.body += '<div class="form-actions"><div class="row"><div class="col-md-5"></div><div class="col-md-7"><button type="button" class="btn green" id="submit" ng-click="clearVendorStock()">Submit</button><button type="button" class="btn default" onclick="location.reload();">Cancel</button></div></div></div>';

                $scope.renderHtml = function (htmlCode) {
                    return $sce.trustAsHtml(htmlCode);
                };
            });
        }
    };
    $scope.clearVendorStock = function () {

        smoke.confirm("Are you sure you want to return items. This cant be undone?", function (e) {
            if (e) {
                var purchaseNumber = $scope.invoiceNumber;
                var returningOn = $scope.returningOn;

                var invoice = $.param({invoice: purchaseNumber, returningOn: returningOn});
                $http({
                    url: baseUrl + '/stock/clearbalajivendorstock',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response) {
                        smoke.alert("Order Cleared");
                        location.reload();
                    }
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    };

}]);

app.directive('compile', ['$compile', function ($compile) {
    return function (scope, element, attrs) {
        scope.$watch(
            function (scope) {
                // watch the 'compile' expression for changes
                return scope.$eval(attrs.compile);
            },
            function (value) {
                // when the 'compile' expression changes
                // assign it into the current DOM
                element.html(value);

                // compile the new DOM and link it to the current
                // scope.
                // NOTE: we only compile .childNodes so that
                // we don't get into infinite loop compiling ourselves
                $compile(element.contents())(scope);
            }
        );
    };
}]);
//
//app.directive('ngInitial', function() {
//    return {
//        restrict: 'A',
//        controller: [
//            '$scope', '$element', '$attrs', '$parse', function($scope, $element, $attrs, $parse) {
//                var getter, setter, val;
//                val = $attrs.ngInitial || $attrs.value;
//                getter = $parse($attrs.ngModel);
//                setter = getter.assign;
//                setter($scope, val);
//            }
//        ]
//    };
//});
