/**
 * Created by chaitanya on 19/11/15.
 */
var app = angular.module('proLightBilling', ['ngSanitize']);

app.controller('audioInvoice', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {

    $scope.formInfo = {};

    $scope.getDetails = function () {
        var invoiceNumber = $scope.selectWorkOrder;

        var invoice = $.param({invoice: invoiceNumber});
        $http({
            url: baseUrl + '/billing/getorderdetails',
            method: 'POST',
            data: invoice,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response) {
            if (response) {
                console.log(response);
                var length = response.length;
                $scope.formInfo.address = response[0].address;
                $scope.formInfo.clientName = response[0].client_name;
                $scope.formInfo.clientCode = response[0].clientcode;
                $scope.formInfo.email = response[0].email;
                $scope.formInfo.number = response[0].phone_no;
                $scope.formInfo.eventName = response[0].eventName;
                $scope.formInfo.eventLocation = response[0].eventLocation;
                $scope.body = '<table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10"><thead><th>Sr.No</th><th>Item</th><th>Qty.No</th><th>Rate</th><th>Amount</th></thead>';
                $scope.body += '<tbody id="taxTable">';
                for (var i = 0; i < length; i++) {
                    var serailNumber = i + 1;
                    var particular = response[i].particularName;
                    var quantity = response[i].quantity;
                    //var rate = response[i].rate;
                    //var amount = response[i].amount;
                    var totalAmount = response[i].total_amount;
                    var particularID = response[i].particular;
                    $scope.body += '<tr><td style="width: 60px;">' + serailNumber + '</td><td style="width: 430px;"><input type="hidden" value="' + particularID + '" name="description[]"><input type="text" readonly id="paticular-' + serailNumber + '" name="particular[]" class="form-control" value="' + particular + '"></td><td style="width: 216px;padding-right: 10px;"><input type="text" onkeyup="calculationInTaxSimple(' + serailNumber + ');" onkeypress="return isNumber(event);" id="quantity-' + serailNumber + '" name="quantity[]" readonly class="form-control" value="' + quantity + '"></td><td style="width: 200px;padding-right: 10px;"><input type="text" onkeyup="calculationInTaxSimple(' + serailNumber + ');" onkeypress="return isNumber(event);" id="rate-' + serailNumber + '" name="rate[]" class="form-control" value=""></td><td style="width: 200px;"><input type="text" readonly id="amount-' + serailNumber + '" name="amount[]" class="form-control" value=""></td></tr>';
                }
                var serviceTax = ((parseFloat(totalAmount) * 14.5) / 100);
                var grandTotal = parseFloat(serviceTax) + parseFloat(totalAmount);
                $scope.body += '</tbody><tbody><tr><td style="width: 5px;"></td><td style="width: 10px;"></td><td style="width: 10px;"></td><td style="width: 60px;"><strong>Total Amount</strong></td><td style="width: 30px;;"><input type="text" readonly name="totamt" id="totamt" class="form-control" value=""></td></tr><tr><td style="width: 5px;"></td><td style="width: 200px;"></td><td style="width: 10px;"></td><td style="width: 60px;"><strong>Service Tax 14.5%</strong></td><td style="width: 30px;"><input type="text" name="vat" id="vat" class="form-control" value=""></td></tr><tr><td style="width: 5px;"></td><td style="width: 200px;"></td><td style="width: 10px;"></td><td style="width: 60px;"><strong>Grand Total</strong></td><td style="width: 30px;"><input type="text" name="grandtot" id="grandtot" class="form-control" value=""></td></tr></tbody></table>';
                $scope.renderHtml = function (htmlCode) {
                    return $sce.trustAsHtml(htmlCode);
                };
            }
        });
        console.log(invoiceNumber);
    };
}]);

app.controller('balajiInvoice', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {

    $scope.formInfo = {};

    $scope.getDetails = function () {
        var invoiceNumber = $scope.selectWorkOrder;

        var invoice = $.param({invoice: invoiceNumber});
        $http({
            url: baseUrl + '/billing/getbalajiorderdetails',
            method: 'POST',
            data: invoice,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response) {
            if (response) {
                console.log(response);
                var length = response.length;
                $scope.formInfo.address = response[0].address;
                $scope.formInfo.clientName = response[0].clientName;
                $scope.formInfo.clientCode = response[0].clientCode;
                $scope.formInfo.email = response[0].email;
                $scope.formInfo.number = response[0].mobile;
                $scope.formInfo.eventName = response[0].eventName;
                $scope.formInfo.eventLocation = response[0].eventLocation;
                $scope.formInfo.totalBudget = response[0].totalBudget;
                $scope.formInfo.availableBudget = response[0].availableBudget;
                $scope.body = '<table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10"><thead><th>Sr.No</th><th>Item</th><th>Notes</th><th>Qty.No</th><th>Rate</th><th>Amount</th></thead>';
                $scope.body += '<tbody id="taxTable">';
                var total = 0;
                for (var i = 0; i < length; i++) {
                    var serailNumber = i + 1;
                    var particularID = response[i].particular;
                    var particular = response[i].particularName;
                    var notes = response[i].notes;
                    var quantity = response[i].quantity;
                    var rate = response[i].rate;
                    var amount = response[i].amount;
                    //var rate = response[i].rate;
                    //var amount = response[i].amount;
                    //total += parseFloat(amount);
                    var particularID = response[i].particular;
                    $scope.body += '<tr><td style="width: 60px;">' + serailNumber + '</td><td style="width: 430px;"><input type="hidden" value="' + particularID + '" name="description[]"><input type="text" readonly id="paticular-' + serailNumber + '" name="particular[]" class="form-control" value="' + particular + '"></td><td style="width: 216px;padding-right: 10px;"><input type="text"  name="notes[]" class="form-control" value="' + notes + '" id="notes-' + serailNumber + '"></td><td style="width: 216px;padding-right: 10px;"><input type="text" onkeyup="calculationInTaxSimple(' + serailNumber + ');" onkeypress="return isNumber(event);" id="quantity-' + serailNumber + '" name="quantity[]" readonly class="form-control" value="' + quantity + '"></td><td style="width: 200px;padding-right: 10px;"><input type="text" onkeyup="calculationInTaxSimple(' + serailNumber + ');" onkeypress="return isNumber(event);" id="rate-' + serailNumber + '" name="rate[]" class="form-control" value="' + rate + '"></td><td style="width: 200px;"><input type="text" readonly id="amount-' + serailNumber + '" name="amount[]" class="form-control" value="' + amount + '"></td></tr>';
                }
                var serviceTax = ((parseFloat(total) * 14.5) / 100);
                var grandTotal = parseFloat(serviceTax) + parseFloat(total);
                $scope.body += '</tbody><tbody><tr><td style="width: 5px;"></td><td style="width: 10px;"></td><td style="width: 10px;"></td><td style="width: 10px;"></td><td style="width: 60px;"><strong>Total Amount</strong></td><td style="width: 30px;;"><input type="text" readonly name="totamt" id="totamt" class="form-control" value=""></td></tr><tr><td style="width: 5px;"></td><td style="width: 200px;"><td style="width: 200px;"></td><td style="width: 10px;"></td><td style="width: 60px;"><strong>Service Tax 14.5%</strong></td><td style="width: 30px;;"><input type="text" name="vat" id="vat" class="form-control" value=""></td></tr><tr><td style="width: 5px;"></td><td style="width: 200px;"><td style="width: 200px;"></td><td style="width: 10px;"></td><td style="width: 60px;"><strong>Grand Total</strong></td><td style="width: 30px;"><input type="text" name="grandtot" id="grandtot" class="form-control" value=""></td></tr></tbody></table>';
                $scope.renderHtml = function (htmlCode) {
                    return $sce.trustAsHtml(htmlCode);
                };
            }
        });
    };
}]);

app.directive('compile', ['$compile', function ($compile) {
    return function (scope, element, attrs) {
        scope.$watch(
            function (scope) {
                // watch the 'compile' expression for changes
                return scope.$eval(attrs.compile);
            },
            function (value) {
                // when the 'compile' expression changes
                // assign it into the current DOM
                element.html(value);

                // compile the new DOM and link it to the current
                // scope.
                // NOTE: we only compile .childNodes so that
                // we don't get into infinite loop compiling ourselves
                $compile(element.contents())(scope);
            }
        );
    };
}]);