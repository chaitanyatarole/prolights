/**
 * Created by chaitanya on 19/11/15.
 */
var app = angular.module('proLights', ['ngSanitize', 'ui-notification']);

app.controller('stockReturnCtrl', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {

    $scope.getDetails = function () {

        if ($scope.formInfo.invoiceNumber != '') {
            var invoiceNumber = $scope.formInfo.invoiceNumber;
            var invoice = $.param({invoice: invoiceNumber});
            $http({
                url: baseUrl + '/site/getaudioorderdetails',
                method: 'POST',
                data: invoice,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                var length = response.length;
                var address = response[0].address;
                var date = response[0].date;
                var clientName = response[0].client_name;
                var clientCode = response[0].clientcode;
                var email = response[0].email;
                var mobile = response[0].phone_no;
                var eventName = response[0].eventName;
                var eventLocation = response[0].eventLocation;
                var invoiceNumber = response[0].invoice_no;
                var total = response[0].total_amount;

                $scope.body = '<div class="form-group"> <label class="control-label col-sm-2">Client Code</label> <div class="col-sm-1"> <input type="text" class="form-control" readonly value="' + clientCode + '"></div><label class="control-label col-sm-1">Date</label><div class="col-sm-2"><input type="date" readonly class="form-control"  value="' + date + '" ></div><label class="control-label col-sm-2">Invoice Number</label> <div class="col-sm-2"> <input type="text" readonly id="invoiceNumber" class="form-control" value="' + invoiceNumber + '" ></div></div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Client Name</label> <div class="col-sm-6"> <input type="text" readonly class="form-control"value="' + clientName + '"></div></div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Address</label> <div class="col-sm-6"> <textarea readonly class="form-control">' + address + '</textarea> </div> </div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Mobile Number</label> <div class="col-sm-3"> <input type="text" class="form-control" readonly value="' + mobile + '"> </div> <label class="control-label col-sm-2">Email</label> <div class="col-sm-3"> <input type="text" readonly class="form-control" value="' + email + '"> </div> </div>';
                $scope.body += '<div class="form-group"> <label class="control-label col-sm-2">Event Name</label> <div class="col-sm-3"> <input type="text" class="form-control" readonly value="' + eventName + '"> </div> <label class="control-label col-sm-2">Event Location</label> <div class="col-sm-3"> <input type="text" readonly class="form-control" value="' + eventLocation + '"> </div> </div>';
                $scope.body += '<div class=""><table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10"> <thead> <th>Sr.No </th> <th>Item </th><th>Quantity</th></thead> <tbody id="taxTable">';

                for (var i = 0; i < length; i++) {
                    var serialNumber = i + 1;
                    var particular = response[i].particularName;
                    var quantity = response[i].quantity;
                    //var rate = response[i].rate;
                    //var amount = response[i].amount;

                    $scope.body += '<tr><td style="width: 60px;">' + serialNumber + '</td><td style="width: 430px;"><input type="text" readonly class="form-control" value="' + particular + '"></td><td style="width: 216px;padding-right: 10px;"><input type="text" class="form-control" readonly value="' + quantity + '"></td></tr>';
                    //$scope.body += '<tr><td style="width: 60px;">' + serialNumber + '</td><td style="width: 430px;"><input type="text" readonly class="form-control" value="' + particular + '"></td><td style="width: 216px;padding-right: 10px;"><input type="text" class="form-control" readonly value="' + quantity + '"></td><td style="width: 200px;padding-right: 10px;"><input type="text" readonly class="form-control" value="' + rate + '"></td><td style="width: 200px;"><input type="text" readonly class="form-control" value="' + amount + '" ></td></tr>';


                }
                //$scope.body += '</tbody><tbody><tr><td style="width: 5px;"></td><td style="width: 200px;"></td><td style="width: 10px;"> </td><td style="width: 60px;"><strong>Total Amount</strong></td><td style="width: 30px;;"><input type="text" readonly class="form-control" value="' + total + '" ></td></tr></tbody></table></div>';
                $scope.body += '</tbody></table></div>';

                $scope.body += '<div class="form-actions"><div class="row"><div class="col-md-5"></div><div class="col-md-7"><button type="button" class="btn green" id="submit" onclick="addStock()">Submit</button><button type="button" class="btn default" onclick="location.reload();">Cancel</button></div></div></div>';

                $scope.renderHtml = function (htmlCode) {
                    return $sce.trustAsHtml(htmlCode);
                };

            });
        } else {
            $scope.body = '';
        }
    };

    //$scope.addReturnedStock = function () {
    //
    //    console.log("in add return stock method");
    //
    //    smoke.confirm("Are you sure you want to add returned stock? This can't be undone", function (e) {
    //        if (e) {
    //            var invoiceNumber = $("#invoiceNumber").val();
    //            var invoice = $.param({invoice: invoiceNumber});
    //            $http({
    //                url: baseUrl + '/site/addreturnedstock',
    //                method: 'POST',
    //                data: invoice,
    //                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    //            }).success(function (response) {
    //                smoke.alert("Stock Updated");
    //                location.reload();
    //            });
    //        }
    //    }, {
    //        ok: "Yes",
    //        cancel: "No",
    //        classname: "custom-class",
    //        reverseButtons: true
    //    });
    //}

}]);

app.controller('modifyItemCtrl', ['$scope', function ($scope) {
    $scope.formInfo = {};

    $scope.getData = function () {
        var category = $scope.formInfo.category;

        $.ajax({
            url: baseUrl + '/site/getitems',
            type: 'POST',
            data: {
                category: category
            },
            dataType: 'json',
            success: function (response) {

                var length = response.length;
                var result = '<div class="form-group"><div class="col-sm-12" style="text-align: center;"><label class="control-label">Item</label></div>';
                for (var i = 0; i < length; i++) {
                    var recordId = response[i].id;
                    var name = response[i].name;

                    result += '<div class="form-group"><input type="hidden" name="recordId[]" value="' + recordId + '"><div class="col-sm-4"></div><div class="col-sm-4"><input type="text" style="width: 100%;"class="form-control" name="name[]" value="' + name + '"></div> </div> ';
                }

                result += '<div class="form-actions"> <div class="row"> <div class="col-sm-6"> <button type="submit" name="submitButton" style="float: right;" class="btn green"id="">Submit </button> </div> <div class="col-sm-6"> <button type="button" class="btn default" onclick="location.reload();">Cancel </button> </div></div></div></div>';
                $("#adddynamicitems").html(result);
            },
            error: function (request, error) {
            }
        });
    }

}]);

app.controller('quotationCtrl', ['$scope', '$http', function ($scope, $http) {

    $scope.addRowInQuotation = function () {

        var rows = $("#quotationTable tr").length;
        rows = rows - 1;
        var slno = parseInt(rows) + 2;

        $http({
            method: 'GET',
            url: baseUrl + '/site/getproducts'
        }).success(function (response) {
            $scope.$apply(function () {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;"><strong>' + slno + '</strong></td> ';
                result += '<td style="width: 400px;"><select required required id="productCode-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="particular[]" style="width: 400px !important;" onchange="" class="form-control select2-offscreen addProduct-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productCode = response[i].name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productCode + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" required name="quantity[]" ng-change="updateDynamicRowAmount()" ng-model="formInfo.quantity' + slno + '" id="quantity-' + slno + '" class="form-control" onkeyup="calculationInQuotation(' + slno + ');"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInQuotation(' + slno + ');" onkeypress="return isNumber1(event);" ng-model="formInfo.rate' + slno + '" name="rate[]" value="" class="form-control" id="rate-' + slno + '"> ';
                result += '</td> <td style="width: 250px;;"> <input type="text" class="form-control" readonly value="" ng-model="formInfo.amount' + slno + '" name="amount[]" id="amount-' + slno + '"> </td> </tr>';
                $("#quotationDataTable").append(result);
                $(".addProduct-" + slno).select2();
                $("#counter").val(slno);
                console.log($scope.formInfo.rate2);
            });
        });
    };

    $scope.updateAmount = function () {
        console.log($scope.formInfo);
    };
    $scope.updateDynamicRowAmount = function () {
        alert("ok");
        $scope.$apply();
        console.log($scope.formInfo);
    }
}]);

app.controller("wagesController", ['$scope', '$http', function ($scope, $http) {
    $scope.formInfo = {};

    $scope.isDisabled = false;

    $scope.generateCode = function () {

        var client = $scope.wages.clientNameCode;
        var cname = $scope.wages.employeeName;


        if ($scope.wages.clientNameCode == undefined) {

            $scope.wages.ccode = '';
            $scope.wages.ccodeno = '';

            var words = $scope.wages.employeeName.split(' ');

            for (var i = 0; i < words.length; i++) {
                if (i < 2) {
                    var ss = words[i].charAt(0);
                    var cc = $scope.wages.ccode;
                    var ccno = $scope.wages.ccodeno;
                    var fstr = cc.concat(ss.toUpperCase());
                    if (fstr != '') {
                        $scope.wages.ccode = fstr;
                        var newCode = fstr + '0' + ccno;
                        $scope.wages.employeeCode = newCode;
                    }
                }
            }
        }

        if (cname == '') {
            $scope.wages.ccode = "";
            $scope.wages.clientNameCode = "";
            $scope.wages.employeeCode = "";
            $scope.wages.address = "";
            $scope.wages.email = "";
            $scope.wages.number = "";
        }
    };

    $scope.saveData = function () {

        $scope.isDisabled = true;

        var entireData = $scope.wages;
        $scope.wages.ccodeno = $("#ccodeno").val();
        var invoice = $.param({entireData: entireData});
        $http({
            url: baseUrl + '/site/savewages',
            method: 'POST',
            data: invoice,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response) {
            if (response) {
                smoke.alert("Wages Saved");
                location.reload();
            }
        });
    };

    $scope.getDetails = function () {

        var invoice = $.param({clientCode: $scope.wages.clientNameCode});
        $http({
            url: baseUrl + '/site/getemployeedetails',
            method: 'POST',
            data: invoice,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response) {
            if (response) {
                $scope.wages.ccode = response[0].ccode;
                $scope.wages.ccodeno = response[0].ccodeno;
                $scope.wages.employeeCode = response[0].clientcode;
                $scope.wages.employeeName = response[0].client_name;
                $scope.wages.email = response[0].email;
                $scope.wages.number = response[0].phone_no;
                $scope.wages.address = response[0].address;
            }
        });
    }

}]);

app.controller('audioWorkOrder', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {

    $scope.formInfo = {};

    $scope.getDetails = function () {

        var invoiceNumber = $scope.selectQuotationNumber;

        if (invoiceNumber != '') {

            var invoice = $.param({invoice: invoiceNumber});
            $http({
                url: baseUrl + '/site/getorderdetails',
                method: 'POST',
                data: invoice,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (response) {
                    var length = response.length;
                    $scope.formInfo.address = response[0].address;
                    $scope.formInfo.clientName = response[0].client_name;
                    $scope.formInfo.clientCode = response[0].clientcode;
                    $scope.formInfo.email = response[0].email;
                    $scope.formInfo.eventName = response[0].eventName;
                    $scope.formInfo.totalBudget = response[0].totalBudget;
                    $scope.formInfo.eventLocation = response[0].eventLocation;
                    $scope.formInfo.number = response[0].phone_no;
                    $scope.body = '<table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10"><thead><th>Sr.No</th><th>Item</th><th>Available Stock</th><th>Qty.No</th></thead>';
                    $scope.body += '<tbody id="taxTable">';
                    for (var i = 0; i < length; i++) {
                        var serailNumber = i + 1;
                        var particular = response[i].particularName;
                        var quantity = response[i].quantityInQuotation;
                        var availableStock = response[i].availableStock;
                        var particularID = response[i].particular;
                        $scope.body += '<tr><td style="width: 60px;">' + serailNumber + '</td><td style="width: 430px;"><input type="hidden" value="' + particularID + '" name="description[]"><input type="text" readonly class="form-control" value="' + particular + '"></td><td style="width: 200px;padding-right: 10px;"><input type="text" readonly class="form-control" value="' + availableStock + '"></td><td style="width: 216px;padding-right: 10px;"><input type="text" onkeypress="return isNumber(event);" id="quantity-' + serailNumber + '" name="quantity[]" class="form-control" value="' + quantity + '"></td><td><button type="button" class="btn green" onclick="addRowInWorkOrder();">Add Row</button><td><button type="button" class="btn default" onclick="deleteRowInWorkOrder();">Delete Row</button></td></tr></tbody>';
                    }
                    //$scope.body += '</tbody><tbody id="miscellaneuosTable"><tr><td style="width: 60px;"></td><td style="width: 430px;"><input type="text" name="description[]" placeholder="miscellaneous item" class="form-control" value=""></td><td style="width: 200px;padding-right: 10px;"><input type="text" readonly class="form-control" value="===================="></td><td style="width: 216px;padding-right: 10px;"><input type="text" onkeypress="return isNumber(event);" name="quantity[]" class="form-control" value=""></td><td><button type="button" class="btn green" onclick="addMiscellaneousRowInWorkOrder();">Add Row</button></td><td><button type="button" class="btn default" onclick="deleteMiscellaneousRowInWorkOrder();">Delete Row</button></td></tr></tbody></table>';
                    $scope.renderHtml = function (htmlCode) {
                        return $sce.trustAsHtml(htmlCode);
                    };
                }
            });
        } else {
            $scope.formInfo = {};
            $scope.tableData = {};
        }
    };

}]);

app.controller('balajiWorkOrder', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {

    $scope.formInfo = {};

    $scope.getDetails = function () {

        var invoiceNumber = $scope.selectQuotationNumber;

        if (invoiceNumber != '') {

            var invoice = $.param({invoice: invoiceNumber});
            $http({
                url: baseUrl + '/site/getorderdetailsforbalaji',
                method: 'POST',
                data: invoice,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function (response) {
                if (response) {
                    var length = response.length;
                    $scope.formInfo.address = response[0].address;
                    $scope.formInfo.clientName = response[0].client_name;
                    $scope.formInfo.clientCode = response[0].clientcode;
                    $scope.formInfo.eventName = response[0].eventName;
                    $scope.formInfo.totalBudget = response[0].totalBudget;
                    $scope.formInfo.email = response[0].email;
                    $scope.formInfo.number = response[0].phone_no;
                    $scope.body = '<table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10"><thead><th>Sr.No</th><th>Item</th><th>Available Stock</th><th>Qty.No</th></thead>';
                    $scope.body += '<tbody id="taxTable">';
                    for (var i = 0; i < length; i++) {
                        var serailNumber = i + 1;
                        var particular = response[i].particularName;
                        var quantity = response[i].quantityInQuotation;
                        var availableStock = response[i].availableStock;
                        var particularID = response[i].particular;
                        var rate = response[i].rate;
                        var amount = response[i].amount;
                        var totalAmount = response[i].totalAmount;

                        $scope.body += '<tr><td style="width: 60px;">' + serailNumber + '</td><td style="width: 430px;"><input type="hidden" value="' + particularID + '" name="description[]"><input type="hidden" value="' + rate + '" name="rate[]"><input type="hidden" value="' + amount + '" name="amount[]"><input type="hidden" value="' + totalAmount + '" name="totalAmount[]"><input type="text" readonly class="form-control" value="' + particular + '"></td><td style="width: 200px;padding-right: 10px;"><input type="text" readonly class="form-control" value="' + availableStock + '"></td><td style="width: 216px;padding-right: 10px;"><input type="text" onkeypress="return isNumber(event);" id="quantity-' + serailNumber + '" name="quantity[]" class="form-control" value="' + quantity + '"></td><td><button type="button" class="btn green" onclick="addRowInWorkOrder();">Add Row</button><td><button type="button" class="btn default" onclick="deleteRowInWorkOrder();">Delete Row</button></td></tr>';
                    }
                    $scope.body += '</tbody><tbody id="miscellaneuosTable"><tr><td style="width: 60px;"></td><td style="width: 430px;"><input type="text" name="description[]" placeholder="miscellaneous item" class="form-control" value=""></td><td style="width: 200px;padding-right: 10px;"><input type="text" readonly class="form-control" value="===================="></td><td style="width: 216px;padding-right: 10px;"><input type="text" onkeypress="return isNumber(event);" name="quantity[]" class="form-control" value=""></td><td><button type="button" class="btn green" onclick="addMiscellaneousRowInWorkOrder();">Add Row</button></td><td><button type="button" class="btn default" onclick="deleteMiscellaneousRowInWorkOrder();">Delete Row</button></td></tr></tbody></table>';
                    $scope.renderHtml = function (htmlCode) {
                        return $sce.trustAsHtml(htmlCode);
                    };
                }
            });
        } else {
            $scope.formInfo = {};
            $scope.tableData = {};
        }
    };

}]);

app.controller('receivedBalajiPurchaseOrderCtrl', ['$scope', '$http', '$compile', function ($scope, $http, $compile) {


    $scope.acceptOrder = function () {
        smoke.confirm("Are you sure you want to accept order?", function (e) {
            if (e) {
                var amountArray = new Array();
                var rateArray = new Array();
                var quantityArray = new Array();
                var particularArray = new Array();
                var particularNameArray = new Array();


                var table = document.getElementById("taxTable");
                var rowCount = table.rows.length;

                var totamt1 = 0;
                for (var i = 0; i < rowCount; i++) {
                    var a = document.getElementsByName("amount[]")[i].value;
                    var rate = document.getElementsByName("rate[]")[i].value;
                    var quantity = document.getElementsByName("quantity[]")[i].value;
                    var particular = document.getElementsByName("particularID[]")[i].value;
                    var particularName = document.getElementsByName("particular[]")[i].value;

                    if (!isNaN(parseFloat(a))) {
                        amountArray[i] = a;
                        rateArray[i] = rate;
                        quantityArray[i] = quantity;
                        particularArray[i] = particular;
                        particularNameArray[i] = particularName;
                        totamt1 = totamt1 + parseFloat(a);
                    }
                }
                document.getElementById("totamt").value = totamt1.toFixed(2);

                var totalAmount = totamt1;

                var invoiceNumber = $("#invoiceNumber").val();
                var invoice = $.param({
                    entireData: $scope.formInfo,
                    amountArray: amountArray,
                    rateArray: rateArray,
                    quantityArray: quantityArray,
                    particularArray: particularArray,
                    particularNameArray: particularNameArray,
                    total: totalAmount
                });
                $http({
                    url: baseUrl + '/site/acceptbalajipurchaseorder',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response) {
                        smoke.alert("Order Accepted");
                        location.href = 'http://localhost/prolights/web/site/receivebalajipurchaseorder';
                    }
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    };

    $scope.cancelOrder = function () {

        smoke.confirm("Are you sure you want to reject order This cant be undone?", function (e) {
            if (e) {
                var invoiceNumber = $("#invoiceNumber").val();
                var invoice = $.param({invoice: invoiceNumber});
                $http({
                    url: baseUrl + '/site/rejectbalajipurchaseorder',
                    method: 'POST',
                    data: invoice,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (response) {
                    if (response) {
                        smoke.alert("Order Rejected");
                        location.reload();
                    }
                });
            }
        }, {
            ok: "Yes",
            cancel: "No",
            classname: "custom-class",
            reverseButtons: true
        });
    };
}]);

app.controller('audioInvoice', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {

    $scope.formInfo = {};

    $scope.getDetails = function () {
        var invoiceNumber = $scope.selectWorkOrder;

        var invoice = $.param({invoice: invoiceNumber});
        $http({
            url: baseUrl + '/billing/getorderdetails',
            method: 'POST',
            data: invoice,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (response) {
            if (response) {
                console.log(response);
                var length = response.length;
                $scope.formInfo.address = response[0].address;
                $scope.formInfo.clientName = response[0].client_name;
                $scope.formInfo.clientCode = response[0].clientcode;
                $scope.formInfo.email = response[0].email;
                $scope.formInfo.number = response[0].phone_no;
                $scope.formInfo.eventName = response[0].eventName;
                $scope.formInfo.eventLocation = response[0].eventLocation;
                $scope.body = '<table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10"><thead><th>Sr.No</th><th>Item</th><th>Qty.No</th><th>Rate</th><th>Amount</th></thead>';
                $scope.body += '<tbody id="taxTable">';
                for (var i = 0; i < length; i++) {
                    var serailNumber = i + 1;
                    var particular = response[i].particularName;
                    var quantity = response[i].quantity;
                    //var rate = response[i].rate;
                    //var amount = response[i].amount;
                    var totalAmount = response[i].total_amount;
                    var particularID = response[i].particular;
                    $scope.body += '<tr><td style="width: 60px;">' + serailNumber + '</td><td style="width: 430px;"><input type="hidden" value="' + particularID + '" name="description[]"><input type="text" readonly id="paticular-' + serailNumber + '" name="particular[]" class="form-control" value="' + particular + '"></td><td style="width: 216px;padding-right: 10px;"><input type="text" onkeyup="calculationInTaxSimple(' + serailNumber + ');" onkeypress="return isNumber(event);" id="quantity-' + serailNumber + '" name="quantity[]" readonly class="form-control" value="' + quantity + '"></td><td style="width: 200px;padding-right: 10px;"><input type="text" onkeyup="calculationInTaxSimple(' + serailNumber + ');" onkeypress="return isNumber(event);" id="rate-' + serailNumber + '" name="rate[]" class="form-control" value=""></td><td style="width: 200px;"><input type="text" readonly id="amount-' + serailNumber + '" name="amount[]" class="form-control" value=""></td></tr>';
                }
                var serviceTax = ((parseFloat(totalAmount) * 14.5) / 100);
                var grandTotal = parseFloat(serviceTax) + parseFloat(totalAmount);
                $scope.body += '</tbody><tbody><tr><td style="width: 5px;"></td><td style="width: 10px;"></td><td style="width: 10px;"></td><td style="width: 60px;"><strong>Total Amount</strong></td><td style="width: 30px;;"><input type="text" readonly name="totamt" id="totamt" class="form-control" value=""></td></tr><tr><td style="width: 5px;"></td><td style="width: 200px;"></td><td style="width: 10px;"></td><td style="width: 60px;"><strong>Service Tax 14.5%</strong></td><td style="width: 30px;"><input type="text" name="vat" id="vat" class="form-control" value=""></td></tr><tr><td style="width: 5px;"></td><td style="width: 200px;"></td><td style="width: 10px;"></td><td style="width: 60px;"><strong>Grand Total</strong></td><td style="width: 30px;"><input type="text" name="grandtot" id="grandtot" class="form-control" value=""></td></tr></tbody></table>';
                $scope.renderHtml = function (htmlCode) {
                    return $sce.trustAsHtml(htmlCode);
                };
            }
        });
        console.log(invoiceNumber);
    };
}]);


