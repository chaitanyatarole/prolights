/**
 * Created by chaitanya on 18/8/15.
 */
var baseUrl = $("#baseUrl").val();
var autoCompleteProforma = new Array();
$(document).ready(function () {

    displayStock();

    $("#clientNameCode").change(function () {

        var id = $(this).find(":selected").val();

        $.post(baseUrl + '/site/getdetails', {
                clientCode: id
            }, function (response) {
                if (response) {
                    var clientCode = response[0].clientcode;
                    var clientName = response[0].client_name;
                    var address = response[0].address;
                    var email = response[0].email;
                    var mobile = response[0].phone_no;
                    var vat_no = response[0].vat_no;
                    var cst_no = response[0].cst_no;

                    $("#clientcode").val(clientCode);
                    $("#cname").val(clientName);
                    $("#address").val(address);
                    $("#number").val(mobile);
                    $("#email").val(email);
                    $("#vat_no").val(vat_no);
                    $("#cst_no").val(cst_no);
                }
            }, 'json'
        )
    });

    $("#vendorNameCode").change(function () {

        var id = $(this).find(":selected").val();

        $.post(baseUrl + '/site/getvendordetails', {
                clientCode: id
            }, function (response) {
                if (response) {
                    var clientCode = response[0].clientcode;
                    var clientName = response[0].client_name;
                    var address = response[0].address;
                    var email = response[0].email;
                    var mobile = response[0].phone_no;
                    var vat_no = response[0].vat_no;
                    var cst_no = response[0].cst_no;

                    $("#clientcode").val(clientCode);
                    $("#cname").val(clientName);
                    $("#address").val(address);
                    $("#number").val(mobile);
                    $("#email").val(email);
                    $("#vat_no").val(vat_no);
                    $("#cst_no").val(cst_no);
                }
            }, 'json'
        )
    });

    $("#quotationNameCode").change(function () {

        var id = $(this).find(":selected").val();

        $.post(baseUrl + '/site/getquotationeventname', {
                selectQuotationNumber: id
            }, function (response) {
                if (response) {
                    var eventName = response[0].eventName;

                    $("#eventName").val(eventName);
                }
            }, 'json'
        )
    });

    $("#audioquotationNameCode").change(function () {

        var id = $(this).find(":selected").val();

        $.post(baseUrl + '/site/getaudioquotationeventname', {
                selectQuotationNumber: id
            }, function (response) {
                if (response) {
                    var eventName = response[0].eventName;

                    $("#eventName").val(eventName);
                }
            }, 'json'
        )
    });

    $("#number_of_subcategories").change(function () {

        var category = $("#selected_categories").find(":selected").val();


        if (category == 'select') {
            smoke.alert("Please select category first");
            return false;
        }
        $("#adddynamicitems").html('');
        var count = $(this).find(":selected").val();

        if (count == 'select') {
            $("#adddynamicitems").html('');
        } else {
            var result = '<div class="form-group"><div class="col-sm-12" style="text-align: center;"><label class="control-label">Category</label></div></div>';
            for (var i = 0; i < count; i++) {
                result += '<div class="form-group"><div class="col-sm-3"></div><div class="col-sm-6" style="text-align: center;"><input type="text" style="width: 100%;" class="form-control" value="" name="category[]" id="product-' + i + '"></div> </div>';
            }
            result += '<div class="form-actions"> <div class="row"> <div class="col-sm-6"> <button type="submit" name="submitButton" style="float: right;" class="btn green" id="submit">Submit </button> </div> <div class="col-sm-6"> <button type="button" class="btn default" onclick="location.reload();">Cancel </button> </div> </div> </div>';

            result += '<input type="hidden" id="count" value="' + count + '">';

            $("#adddynamicitems").append(result);
        }
    });

    $("#number_of_items").change(function () {

        var category = $("#selected_categories").find(":selected").val();


        if (category == 'select') {
            smoke.alert("Please select category first");
            return false;
        }
        $("#adddynamicitems").html('');
        var count = $(this).find(":selected").val();

        if (count == 'select') {
            $("#adddynamicitems").html('');
        } else {
            var result = '<div class="form-group"><div class="col-sm-12" style="text-align: center;"><label class="control-label">Item</label></div></div>';
            for (var i = 0; i < count; i++) {
                result += '<div class="form-group"><div class="col-sm-3"></div><div class="col-sm-6" style="text-align: center;"><input type="text" style="width: 100%;" class="form-control" value="" name="item[]" id="product-' + i + '"></div> </div>';
            }
            result += '<div class="form-actions"> <div class="row"> <div class="col-sm-6"> <button type="submit" name="submitButton" style="float: right;" class="btn green" id="submit">Submit </button> </div> <div class="col-sm-6"> <button type="button" class="btn default" onclick="location.reload();">Cancel </button> </div> </div> </div>';

            result += '<input type="hidden" id="count" value="' + count + '">';

            $("#adddynamicitems").append(result);
        }
    });

    /* function for Sai Fabrication   */

    $("#number_of_materials").change(function () {

        $("#adddynamicitems").html('');
        var count = $(this).find(":selected").val();

        if (count == 'select') {
            $("#adddynamicitems").html('');
        } else {
            var result = '<div class="form-group"><div class="col-sm-4" style="text-align: center;"><label class="control-label"><strong>Material</strong></label></div><div class="col-sm-4" style="text-align: center;"><label class="control-label"><strong>Dimension One</strong></label></div><div class="col-sm-4" style="text-align: center"><label class="control-label"><strong>Dimension Two</strong></label></div>';
            for (var i = 0; i < count; i++) {
                result += '<div class="form-group"><div class="col-sm-4"><input type="text" style="width: 100%;" class="form-control" value="" name="product[]" id="product-' + i + '"></div><div class="col-sm-4"> <input type="text"  style="width: 100%;" class="form-control" value="" name="dimensionOne[]" id="description-' + i + '"> </div><div class="col-sm-4"><input type="text" style="width: 100%;"  class="form-control" value="" name="dimensionTwo[]" id="rate-' + i + '"></div> </div>';
            }
            result += '<div class="form-actions"> <div class="row"> <div class="col-sm-6"> <button type="submit" style="float: right;" class="btn green" id="submit">Submit </button> </div> <div class="col-sm-6"> <button type="button" class="btn default" onclick="location.reload();">Cancel </button> </div> </div> </div>';

            result += '<input type="hidden" id="count" value="' + count + '">';

            $("#adddynamicitems").append(result);
        }
    });

    /* End function for Sai Fabrication  */
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function individualDiscount(id) {
    var discount = $("#disc-" + id).val();
    var currentAmount = $("#rate-" + id).val();
    var newAmount = ((parseInt(currentAmount) * parseInt(discount)) / 100);
    var newProductAmount = parseInt(currentAmount) - parseInt(newAmount);
    if (!isNaN(newProductAmount)) {
        $("#productAmount-" + id).val(newProductAmount);
    }
    calculateTransportDiscount();
    calculateTotalInReceivedInvoice();
}

function calculationInReceivedInvoice(id) {

    var productRate = $("#rate-" + id).val();
    var quantity = $("#receivedQuantity-" + id).val();
    var productAmount = parseFloat(productRate) * parseFloat(quantity);
    productAmount = parseFloat(productAmount);
    $("#productAmount-" + id).val(productAmount);
    calculateTransportDiscount();
    calculateTotalInReceivedInvoice();

}

function calculateTransportDiscount() {
    var transportRate = $("#transRate").val();
    var counter = $("#counter").val();
    var totalAmount = 0
    var productAmount = new Array();
    var seperateAmounts = 0;

    var forIncrement = 0;
    for (var i = 0; i < counter; i++) {
        forIncrement = forIncrement + 1;
        seperateAmounts = $("#productAmount-" + forIncrement).val();
        seperateAmounts = parseFloat(seperateAmounts);
        totalAmount += parseFloat(seperateAmounts);
    }
    totalAmount = parseFloat(totalAmount);
    var transAmount = ((parseFloat(totalAmount) * transportRate) / 100);
    transAmount = parseFloat(transAmount).toFixed(2);
    $("#transAmount").val(transAmount);

    var exciseAmount = $("#exciseAmount").val();

    var newTotal = parseFloat(totalAmount) + parseFloat(exciseAmount) + parseFloat(transAmount);

    var vatAmount = ((parseFloat(newTotal) * 12.5) / 100);
    vatAmount = parseFloat(vatAmount).toFixed(2);
    $("#vatAmount").val(vatAmount);
    var grandTotal = parseFloat(newTotal) + parseFloat(vatAmount);
    if (!isNaN(grandTotal)) {
        grandTotal = parseFloat(grandTotal).toFixed(2);
        $("#totalAmount").val(grandTotal);
        $("#hiddenTotal").val(grandTotal);
    }
}

function calculateTotalInReceivedInvoice() {
    var counter = $("#counter").val();
    var totalAmount = 0
    var productAmount = new Array();
    var seperateAmounts = 0;

    var forIncrement = 0;
    for (var i = 0; i < counter; i++) {
        forIncrement = forIncrement + 1;
        seperateAmounts = $("#productAmount-" + forIncrement).val();
        seperateAmounts = parseFloat(seperateAmounts);
        totalAmount += parseFloat(seperateAmounts);
    }
    var exciseReduction = ((totalAmount * 12.5) / 100);
    exciseReduction = parseFloat(exciseReduction).toFixed(2);
    $("#exciseAmount").val(exciseReduction);
    var newTotal = parseFloat(totalAmount) - parseFloat(exciseReduction);
    var vatReduction = ((newTotal * 12.5) / 100);
    vatReduction = parseFloat(vatReduction).toFixed(2);
    var totalAfterVAT = parseFloat(newTotal) - parseFloat(vatReduction);
    totalAfterVAT = parseFloat(totalAfterVAT);
    $("#vatAmount").val(vatReduction);

    if (!isNaN(totalAfterVAT)) {
        totalAfterVAT = parseFloat(totalAfterVAT).toFixed(2);
        $("#totalAmount").val(totalAfterVAT);
        $("#hiddenTotal").val(totalAfterVAT);
    }
}

function calculateNewTotal() {
    var totalAmount = $("#hiddenTotal").val();
    var roundOff = $("#roundOff").val();
    var newTotal = parseFloat(totalAmount) - parseFloat(roundOff);
    if (!isNaN(parseFloat(newTotal))) {
        newTotal = parseFloat(newTotal).toFixed(2);
        $("#totalAmount").val(newTotal);
    }
}

function isNumber1(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function isNumber2(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function cap() {
    var table = document.getElementById("dataTable");
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var txtFirst = document.getElementsByName('item_code[]')[i].value;
        var fstr = txtFirst.toUpperCase();
        if (fstr != '') {
            document.getElementsByName('item_code[]')[i].value = fstr;
        }
    }
}


function addRow() {

    var rows = $("#dataTable tr").length;

    rows = rows - 2;
    var slno = parseInt(rows) + 1;


    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 5px;padding-top: 16px;"><strong>' + slno + '</strong></td> ';
                result += '<td style="width: 200px;"><select id="purchaseproductCode-' + slno + '" data-placeholder="Select..." tabindex="-1" title="" name="purchaseproductCode[]" onchange="displayRateOfProduct(' + slno + ');" class="form-control input-medium select2-offscreen addProduct-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productCode = response[i].description;
                    var productId = $.trim(response[i].uniqueID);
                    result += '<option value="' + productId + '">';
                    result += '' + productCode + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 10px;"> <input type="text" onkeyup="calculationInPurchase();" onkeypress="return isNumber(event);" name="productQuantity[]" class="form-control"> </td> ' +
                    '<td style="width: 60px;"> <input type="text" name="customerName[]" class="form-control"> </td>' +
                    '<td style="width: 30px;;"> <input type="text" name="deliveryLocation[]" class="form-control"> </td> ' +
                    '<td style="width: 30px;"> <input type="text" name="customer_PO[]" value="N/A" class="form-control"> </td> ' +
                    '<td style="width: 30px;"> <input type="text" name="bank_code[]" value="N/A" class="form-control"> </td> ' +
                    '<td style="width: 30px;"> <input type="text" name="master_key_number[]" value="N/A" class="form-control"> </td> ' +
                    '<td style="width: 30px;"> <input type="text" name="locker_sr_number[]" value="N/A" class="form-control"> </td> ' +
                    '<td style="width: 30px;"> <input type="text" name="gpil_enquiry_number[]" value="N/A" class="form-control"> </td> ' +
                    '<td style="width: 30px;"> <input type="text" onkeyup="calculationInPurchase();" onkeypress="return isNumber1(event);" name="productUnit[]" class="form-control" id="productAmount-' + slno + '"> </td> ' +
                    '<td style="width: 30px;"> <input type="text" name="productAmount[]" class="form-control"></td> </tr>';
                $("#dataTable").append(result);
                $(".addProduct-" + slno).select2();
            }
        },
        'json')
}

function addRowInProforma() {

    var rows = $("#proformaTable tr").length;
    rows = rows - 1;

    var slno = parseInt(rows) + 2;


    var result = '<tr> <td style="width: 5px;">' + slno + '</td> ' +
        '<td style="width: 200px;"> <input type="text" name="description[]" class="form-control"> </td> ' +
        '<td style="width: 10px;"> <input type="text" onkeyup="calculationInProforma();" onkeypress="return isNumber(event);" name="quantity[]" class="form-control"> </td> ' +
        '<td style="width: 60px;"> <input type="text" onkeyup="calculationInProforma();" onkeypress="return isNumber(event);" name="rate[]" class="form-control"> </td> ' +
        '<td style="width: 30px;;"> <input type="text" readonly name="amount[]" class="form-control"> </td> </tr>';
    $("#proformaTable").append(result);

}

function addRowInService() {

    var rows = $("#serviceTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;


    var result = '<tr> <td style="width: 5px;">' + slno + '</td> ' +
        '<td style="width: 200px;"> <input type="text" name="description[]" class="form-control"> </td> ' +
        '<td style="width: 10px;"> <input type="text" onkeyup="calculationInService();" onkeypress="return isNumber(event);" name="quantity[]" class="form-control"> </td> ' +
        '<td style="width: 60px;"> <input type="text" onkeyup="calculationInService();" onkeypress="return isNumber(event);" name="rate[]" class="form-control"> </td> ' +
        '<td style="width: 30px;;"> <input type="text" readonly name="amount[]" class="form-control"> </td> </tr>';
    $("#serviceTable").append(result);

}

function addRowInDebit() {
    var rows = $("#serviceTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;


    var result = '<tr> <td style="width: 5px;">' + slno + '</td> ' +
        '<td style="width: 200px;"> <input type="text" name="description[]" class="form-control"> </td> ' +
        '<td style="width: 10px;"> <input type="text" onkeyup="calculationInDebit();" onkeypress="return isNumber(event);" name="quantity[]" class="form-control"> </td> ' +
        '<td style="width: 60px;"> <input type="text" onkeyup="calculationInDebit();" onkeypress="return isNumber(event);" name="rate[]" class="form-control"> </td> ' +
        '<td style="width: 30px;;"> <input type="text" readonly name="amount[]" class="form-control"> </td> </tr>';
    $("#serviceTable").append(result);
}

function deleteRow() {
    $("#dataTable tr:last").remove();
}
function deleteRowInProforma() {

    $("#proformaTable tr:last").remove();
}
function deleteRowInService() {

    $("#serviceTable tr:last").remove();
}
function deleteRowInDebit() {

    $("#serviceTable tr:last").remove();
}

function cal() {
    var table = document.getElementById("dataTable");
    var rowCount = table.rows.length;

    document.getElementById("count").value = rowCount;
    var totamt1 = 0;
    //alert(rowCount);
    for (var i = 0; i < rowCount; i++) {
        var qty = document.getElementsByName("quantity[]")[i].value;
        var rat = document.getElementsByName("rate[]")[i].value;

        var amt = qty * rat;
        document.getElementsByName("amount[]")[i].value = amt.toFixed(2);

        var a = document.getElementsByName("amount[]")[i].value;
        totamt1 = totamt1 + parseInt(a);
    }

    document.getElementById("totamt").value = totamt1.toFixed(0);
}


function Validation1() {

    var name = document.getElementById("cname").value;
    var addre = document.getElementById("address").value;
    var dat = document.getElementById("date").value;
    var m21 = document.getElementById("date").checked;
    if (!m21) {
        var k = confirm("Press CANCEL to check  Date                     Press OK to continue  ");
        if (!k) {
            return false;
        }
    }

    var table = document.getElementById("dataTable");
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var txtFirst = document.getElementsByName('item_code[]')[i].value;
        //var txtSecond = document.getElementsByName('unit[]')[i].value;
        var txtThird = document.getElementsByName('quantity[]')[i].value;
        var txtThird2 = document.getElementsByName('rate[]')[i].value;

        if (txtFirst == '') {
            alert("Please Enter Description ");
            return false;
        }
        else if (txtThird == '') {
            alert("Please Enter Quantity");
            return false;
        }
        else if (txtThird2 == '') {
            alert("Please Enter Rate");
            return false;
        }
    }


    if (name == '') {
        alert("Please Enter Client Name ");
        return false;
    }
    else if (dat == '') {
        alert("Please Select Date ");
        return false;
    }

    else if (addre == '') {
        alert("Please Enter Address ");
        return false;
    }
    else {
        document.getElementById("Submit").disabled = true;
        document.form1.action = "purchorderentry.php";
        document.form1.submit();             // Submit the page
        return true;
    }

}


function gencodevendor(str) {
    var client = document.getElementById("vendorNameCode").value;
    var cname = $("#cname").val();

    if (client == '') {
        document.getElementById("ccode").value = "";
        document.getElementById("clientcode").value = "";
        var words = str.split(' ');

        for (var i = 0; i < words.length; i++) {
            if (i < 2) {
                var ss = words[i].charAt(0);
                var cc = document.getElementById("ccode").value;
                var ccno = document.getElementById("ccodeno").value;
                var fstr = cc.concat(ss.toUpperCase());
                if (fstr != '') {
                    document.getElementById("ccode").value = fstr;
                    document.getElementById("clientcode").value = fstr + '0' + ccno;
                }
            }
        }
    }

    if (cname == '') {
        document.getElementById("ccode").value = "";
        document.getElementById("vendorNameCode").value = "";
        document.getElementById("clientcode").value = "";
        document.getElementById("address").value = "";
        document.getElementById("email").value = "";
        //document.getElementById("number").value = "+91";

    }


}

function validateData() {
    var date = $("#date").val();
    var clientName = $("#cname").val();
    var number = $("#number").val();

    if (date == '') {
        smoke.alert("Please select date");
        return false;
    }
    if (clientName == '') {
        smoke.alert("Please enter client name");
        return false;
    }
    if (number == '') {
        smoke.alert("Please enter mobile number");
        return false;
    }
    if (date != '' && clientName != '' && number != '') {
        $("#submit").prop("type", "submit");
    }


}

function calculationInProforma() {
    var table = document.getElementById("proformaTable");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var qty = document.getElementsByName("quantity[]")[i].value;
        var rat = document.getElementsByName("rate[]")[i].value;

        var amt = qty * rat;
        document.getElementsByName("amount[]")[i].value = amt.toFixed(2);

        var a = document.getElementsByName("amount[]")[i].value;
        totamt1 = totamt1 + parseFloat(a);
    }

    document.getElementById("totamt").value = totamt1.toFixed(2);

    var tott = document.getElementById("totamt").value;
    var vat2 = document.getElementById("vat").value;
    var cst = (tott * vat2) / 100;
    if (!isNaN(cst)) {
        document.getElementById("vatamt").value = cst.toFixed(2);
    }

    var cstn = document.getElementById("vatamt").value;
    var net = parseFloat(tott) + parseFloat(cstn);
    net = Math.round(net);
    if (!isNaN(net)) {
        document.getElementById("netamt").value = net.toFixed(2);
    }


}
function calculationInService() {
    var table = document.getElementById("serviceTable");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var qty = document.getElementsByName("quantity[]")[i].value;
        var rat = document.getElementsByName("rate[]")[i].value;

        var amt = qty * rat;
        document.getElementsByName("amount[]")[i].value = amt.toFixed(2);

        var a = document.getElementsByName("amount[]")[i].value;
        totamt1 = totamt1 + parseFloat(a);
    }

    document.getElementById("totamt").value = totamt1.toFixed(2);
}

function calculationInDebit() {
    var table = document.getElementById("serviceTable");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var qty = document.getElementsByName("quantity[]")[i].value;
        var rat = document.getElementsByName("rate[]")[i].value;

        var amt = qty * rat;
        document.getElementsByName("amount[]")[i].value = amt.toFixed(2);

        var a = document.getElementsByName("amount[]")[i].value;
        totamt1 = totamt1 + parseFloat(a);
    }

    document.getElementById("totamt").value = totamt1.toFixed(2);
}

function calculationInPurchase() {
    var table = document.getElementById("dataBody");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var qty = document.getElementsByName("productQuantity[]")[i].value;
        var rat = document.getElementsByName("productUnit[]")[i].value;

        var amt = qty * rat;
        document.getElementsByName("productAmount[]")[i].value = amt.toFixed(2);

        var a = document.getElementsByName("productAmount[]")[i].value;
        totamt1 = totamt1 + parseFloat(a);
    }

    document.getElementById("totalAmount").value = totamt1.toFixed(2);
}

function validateProformaData() {
    var date = $("#date").val();
    var clientName = $("#cname").val();
    var number = $("#number").val();
    var vat = $("#vat").val();

    if (date == '') {
        smoke.alert("Please select date");
        return false;
    }
    if (clientName == '') {
        smoke.alert("Please enter client name");
        return false;
    }
    if (number == '') {
        smoke.alert("Please enter mobile number");
        return false;
    }

    var table = document.getElementById("proformaTable");
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var txtFirst = document.getElementsByName('description[]')[i].value;
        //var txtSecond = document.getElementsByName('unit[]')[i].value;
        var txtThird = document.getElementsByName('quantity[]')[i].value;
        var txtThird2 = document.getElementsByName('rate[]')[i].value;

        if (txtFirst == '') {
            smoke.alert("Please Enter Description ");
            return false;
        }
        else if (txtThird == '') {
            smoke.alert("Please Enter Quantity");
            return false;
        }
        else if (txtThird2 == '') {
            smoke.alert("Please Enter Rate");
            return false;
        }
    }

    if (vat == '') {
        smoke.alert("Please enter vat");
        return false;
    }

    if (date != '' && clientName != '' && number != '' && vat != '') {
        $("#submit").prop("type", "submit");
    }
}

function deleteRowInOtherInvoice() {
    $("#taxTable tr:last").remove();
}

function validateTaxData() {
    var date = $("#date").val();
    var clientName = $("#cname").val();
    var number = $("#number").val();

    if (date == '') {
        smoke.alert("Please select date");
        return false;
    }
    if (clientName == '') {
        smoke.alert("Please enter client name");
        return false;
    }
    if (number == '') {
        smoke.alert("Please enter mobile number");
        return false;
    }

    var table = document.getElementById("taxTable");
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var txtFirst = document.getElementsByName('description[]')[i].value;
        var txtThird = document.getElementsByName('quantity[]')[i].value;
        var txtThird2 = document.getElementsByName('rate[]')[i].value;

        var enteredStock = document.getElementsByName('quantity[]')[i].value;
        var availablStock = document.getElementsByName('stock[]')[i].value;
        enteredStock = parseInt(enteredStock);
        availablStock = parseInt(availablStock);

        if (enteredStock > availablStock) {
            smoke.alert("Qauntity cannot be greather than available stock");
            return false;
        }
        else if (txtThird == '') {
            smoke.alert("Please Enter Quantity");
            return false;
        }
        else if (txtThird2 == '') {
            smoke.alert("Please Enter Rate");
            return false;
        }
    }
    if (date != '' && clientName != '' && number != '') {
        $("#submit").prop("type", "submit");
    }
}

function validateModifyTaxData() {
    var date = $("#date").val();
    var clientName = $("#cname").val();
    var number = $("#number").val();
    var vat = $("#vat").val();
    var octroi = $("#del").val();


    if (date == '') {
        smoke.alert("Please select date");
        return false;
    }
    if (clientName == '') {
        smoke.alert("Please enter client name");
        return false;
    }
    if (number == '') {
        smoke.alert("Please enter mobile number");
        return false;
    }

    var table = document.getElementById("taxTable");
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var txtFirst = document.getElementsByName('description[]')[i].value;
        var txtThird = document.getElementsByName('quantity[]')[i].value;
        var txtThird2 = document.getElementsByName('rate[]')[i].value;

        if (txtFirst == '') {
            smoke.alert("Please Enter Description ");
            return false;
        }
        else if (txtThird == '') {
            smoke.alert("Please Enter Quantity");
            return false;
        }
        else if (txtThird2 == '') {
            smoke.alert("Please Enter Rate");
            return false;
        }
    }

    if (vat == '') {
        smoke.alert("Please enter vat");
        return false;
    }
    if (octroi == '') {
        smoke.alert("Please enter octroi");
        return false;
    }

    if (date != '' && clientName != '' && number != '' && vat != '' && octroi != '') {
        $("#submit").prop("type", "submit");
    }
}


function validateOtherInvoiceData() {
    var date = $("#date").val();
    var clientName = $("#cname").val();
    var number = $("#number").val();
    var vat = $("#vat").val();
    var octroi = $("#del").val();


    if (date == '') {
        smoke.alert("Please select date");
        return false;
    }
    if (clientName == '') {
        smoke.alert("Please enter client name");
        return false;
    }
    if (number == '') {
        smoke.alert("Please enter mobile number");
        return false;
    }

    var table = document.getElementById("taxTable");
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var txtFirst = document.getElementsByName('description[]')[i].value;
        var txtThird = document.getElementsByName('quantity[]')[i].value;
        var txtThird2 = document.getElementsByName('rate[]')[i].value;

        var enteredStock = document.getElementsByName('quantity[]')[i].value;
        var availablStock = document.getElementsByName('stock[]')[i].value;
        enteredStock = parseInt(enteredStock);
        availablStock = parseInt(availablStock);


        if (txtFirst == '') {
            smoke.alert("Please Enter Description ");
            return false;
        }
        else if (txtThird == '') {
            smoke.alert("Please Enter Quantity");
            return false;
        }
        else if (txtThird2 == '') {
            smoke.alert("Please Enter Rate");
            return false;
        }
    }

    if (vat == '') {
        smoke.alert("Please enter vat");
        return false;
    }
    if (octroi == '') {
        smoke.alert("Please enter octroi");
        return false;
    }

    if (date != '' && clientName != '' && number != '' && vat != '' && octroi != '') {
        $("#submit").prop("type", "submit");
    }
}


function gencode(str) {
    var client = document.getElementById("clientNameCode").value;
    var cname = document.getElementById("cname").value;


    if (client == '') {
        document.getElementById("ccode").value = "";
        document.getElementById("clientcode").value = "";
        var words = str.split(' ');

        for (var i = 0; i < words.length; i++) {
            if (i < 2) {
                var ss = words[i].charAt(0);
                var cc = document.getElementById("ccode").value;
                var ccno = document.getElementById("ccodeno").value;
                var fstr = cc.concat(ss.toUpperCase());
                if (fstr != '') {
                    document.getElementById("ccode").value = fstr;
                    document.getElementById("clientcode").value = fstr + '0' + ccno;
                }
            }
        }
    }

    if (cname == '') {
        document.getElementById("ccode").value = "";
        document.getElementById("clientNameCode").value = "";
        document.getElementById("clientcode").value = "";
        document.getElementById("address").value = "";
        document.getElementById("email").value = "";
        //document.getElementById("number").value = "+91";

    }


}
function validateCategory() {

    var count = $("#count").val();
    var maxId = $("#maxId").val();
    var generatedId = Array();
    var category = Array();

    for (var i = 0; i < count; i++) {
        category[i] = $("#category-" + i).val();
        if (category[i] == '') {
            smoke.alert("Please enter category");
            return false;
        }
        if (category[i] != '') {
            maxId = parseInt(maxId) + 1;
            uniqueID = category[i].toLowerCase();
            uniqueID = category[i].replace(/(^\s+|[^a-zA-Z0-9 ]+|\s+$)/g, "");   //this one
            uniqueID = category[i].replace(/\s+/g, "-");
            generatedId[i] = maxId + uniqueID + Math.floor((Math.random() * 100) + 1);

        }
    }
    $.post(baseUrl + '/site/savecategory', {
            category: category,
            generatedId: generatedId
        }, function (response) {
            if (response) {
                console.log(response);
                smoke.alert("category added");
                location.reload();
            }
        }, 'json'
    )
}


function gencodeClientEntry(str) {
    var client = '';
    var cname = document.getElementById("cname").value;

    if (client == '') {
        document.getElementById("ccode").value = "";
        document.getElementById("clientcode").value = "";
        var words = str.split(' ');

        for (var i = 0; i < words.length; i++) {
            if (i < 2) {
                var ss = words[i].charAt(0);
                var cc = document.getElementById("ccode").value;
                var ccno = document.getElementById("ccodeno").value;
                var fstr = cc.concat(ss.toUpperCase());
                if (fstr != '') {
                    document.getElementById("ccode").value = fstr;
                    document.getElementById("clientcode").value = fstr + '0' + ccno;
                }
            }
        }
    }

    if (cname == '') {
        document.getElementById("ccode").value = "";
        document.getElementById("ClientNameCode").value = "";
        document.getElementById("clientcode").value = "";
        document.getElementById("address").value = "";
        document.getElementById("email").value = "";
        //document.getElementById("number").value = "+91";

    }


}

function validateClientData() {
    var clientName = $("#cname").val();
    var address = $("#address").val();
    var number = $("#number").val();
    var email = $("#email").val();

    if (clientName == '') {
        smoke.alert("Please enter client name");
        return false;
    }
    if (address == '') {
        smoke.alert("Please enter address");
        return false;
    }
    if (number == '') {
        smoke.alert("Please enter mobile number");
        return false;
    }
    if (email == '') {
        smoke.alert("Please enter email");
        return false;
    }
    if (clientName != '' && address != '' && number != '' && email != '') {
        $("#submit").prop("type", "submit");
    }
}

function validateServiceData() {
    var date = $("#date").val();
    var clientName = $("#cname").val();
    var number = $("#number").val();

    if (date == '') {
        smoke.alert("Please select date");
        return false;
    }
    if (clientName == '') {
        smoke.alert("Please enter client name");
        return false;
    }
    if (number == '') {
        smoke.alert("Please enter mobile number");
        return false;
    }

    var table = document.getElementById("serviceTable");
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var txtFirst = document.getElementsByName('description[]')[i].value;
        var txtThird = document.getElementsByName('quantity[]')[i].value;
        var txtThird2 = document.getElementsByName('rate[]')[i].value;

        if (txtFirst == '') {
            smoke.alert("Please Enter Description ");
            return false;
        }
        else if (txtThird == '') {
            smoke.alert("Please Enter Quantity");
            return false;
        }
        else if (txtThird2 == '') {
            smoke.alert("Please Enter Rate");
            return false;
        }
    }
    if (date != '' && clientName != '' && number != '') {
        $("#submit").prop("type", "submit");
    }
}

function validateDebitData() {
    var date = $("#date").val();
    var clientName = $("#cname").val();
    var number = $("#number").val();
    var location = $("#location").val();
    var additionalInformation = $("#additionalInformation").val();

    if (date == '') {
        smoke.alert("Please select date");
        return false;
    }
    if (clientName == '') {
        smoke.alert("Please enter client name");
        return false;
    }
    if (number == '') {
        smoke.alert("Please enter mobile number");
        return false;
    }
    if (location == '') {
        smoke.alert("Please enter work location");
        return false;
    }
    if (additionalInformation == '') {
        smoke.alert("Please enter additional information");
        return false;
    }

    var table = document.getElementById("serviceTable");
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var txtFirst = document.getElementsByName('description[]')[i].value;
        var txtThird = document.getElementsByName('quantity[]')[i].value;
        var txtThird2 = document.getElementsByName('rate[]')[i].value;

        if (txtFirst == '') {
            smoke.alert("Please Enter Description ");
            return false;
        }
        else if (txtThird == '') {
            smoke.alert("Please Enter Quantity");
            return false;
        }
        else if (txtThird2 == '') {
            smoke.alert("Please Enter Rate");
            return false;
        }
    }
    if (date != '' && clientName != '' && number != '' && location != '' && additionalInformation != '') {
        $("#submit").prop("type", "submit");
    }
}

function validateReceivedInvoice() {
    var date = $("#date").val();
    var clientName = $("#cname").val();
    var number = $("#number").val();
    var invoiceNumber = $("#invoiceNumber").val();
    var purchaseOrder = $("#buyersOrder").val();

    if (purchaseOrder == '') {
        smoke.alert("Please select purchase order");
        return false;
    }

    if (date == '') {
        smoke.alert("Please select date");
        return false;
    }
    if (invoiceNumber == '') {
        smoke.alert("Please enter invoice number");
        return false;
    }
    if (clientName == '') {
        smoke.alert("Please enter client name");
        return false;
    }
    if (number == '') {
        smoke.alert("Please enter mobile number");
        return false;
    }

    if (date != '' && clientName != '' && number != '') {
        $("#submit").prop("type", "submit");
    }
}

function validateTaxNumber() {
    var invoiceNumber = $("#selectTaxNumber").val();

    if (invoiceNumber == '') {
        smoke.alert("Please select tax invoice number");
        return false;
    }

    if (invoiceNumber != '') {
        $("#submit").prop("type", "submit");
    }
}

function validateDebitNumber() {
    var invoiceNumber = $("#selectDebitNumber").val();

    if (invoiceNumber == '') {
        smoke.alert("Please select debit note number");
        return false;
    }

    if (invoiceNumber != '') {
        $("#submit").prop("type", "submit");
    }
}


function validateModifyTaxNumber() {
    var invoiceNumber = $("#selectTaxNumber").val();

    if (invoiceNumber == '') {
        smoke.alert("Please select tax invoice number");
        return false;
    }

    if (invoiceNumber != '') {

        $("#submit").prop("type", "submit");
    }
}

function validateModifyProformaNumber() {
    var proformaNumber = $("#selectProformaNumber").val();

    if (proformaNumber == '') {
        smoke.alert("Please select proforma invoice number");
        return false;
    }

    if (proformaNumber != '') {

        $("#submit").prop("type", "submit");
    }
}

function validateModifyServiceNumber() {
    var serviceNumber = $("#selectServiceNumber").val();

    if (serviceNumber == '') {
        smoke.alert("Please select service invoice number");
        return false;
    }

    if (serviceNumber != '') {

        $("#submit").prop("type", "submit");
    }
}
function validateModifyPurchaseNumber() {
    var purchaseNumber = $("#selectPurchaseNumber").val();

    if (purchaseNumber == '') {
        smoke.alert("Please select purchase invoice number");
        return false;
    }

    if (purchaseNumber != '') {

        $("#submit").prop("type", "submit");
    }
}

function validateModifyPurchaseOrderNumber() {
    var receivedTaxNumber = $("#selectTaxNumber").val();

    if (receivedTaxNumber == '') {
        smoke.alert("Please select purchase number");
        return false;
    }

    if (receivedTaxNumber != '') {

        $("#submit").prop("type", "submit");
    }
}
function validatePurchasePayment() {
    var purchaseNumber = $("#selectPurchaseNumber").val();

    var date = $("#date").val();

    if (purchaseNumber == '') {
        smoke.alert("Please select purchase order number");
        return false;
    }
    if (date == '') {
        smoke.alert("Please select date");
        return false;
    }

    if (purchaseNumber != '' && date != '') {


        $.post(baseUrl + '/site/getpaymentdetails', {
            purchaseNumber: purchaseNumber
        }, function (response) {
            if (response) {
                $(".showPaymentData").html('');
                response = JSON.parse(response);
                var grandTotal = response.grandTotal;
                var remainingAmount = response.remainingAmount;
                var result = '<div class="form-group"> <label class="control-label col-sm-4" style="text-align: right!important;">Grand Total</label> <div class="col-sm-4"> <input type="text" readonly class="form-control" id="grandTotal" name="grandTotal" value="' + grandTotal + '" > </div></div>';
                result += '<div class="form-group"> <label class="control-label col-sm-4" style="text-align: right!important;">Remaining Amount</label> <div class="col-sm-4"> <input type="text" readonly class="form-control" id="remainingAmount" name="remainingAmount" value="' + remainingAmount + '" > </div> </div>';
                result += '<div class="form-group"> <label class="control-label col-sm-4" style="text-align: right!important;">Part Payment</label> <div class="col-sm-4"> <input type="text" onkeypress="return isNumber2(event);" onkeyup="checkValue();newRemainingAmount();" required class="form-control" id="partPayment" name="partPayment" value=""></div> </div>';
                result += '<input type="hidden" id="actualRemainingAmount" name="actualRemainingAmount" value="' + remainingAmount + '">';
                $(".showPaymentData").append(result);
            }
        });
    }
}

function isNumber2(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
        return false;

    return true;
}

function checkValue() {
    var grandTotal = $("#grandTotal").val();
    var partPayment = $("#partPayment").val();

    grandTotal = parseInt(grandTotal);
    partPayment = parseInt(partPayment);

    if (partPayment > grandTotal) {
        smoke.alert("Entered part payment is greater then grand total");
        $("#submit").prop('type', 'button');
        return false;
    } else {
        $("#submit").prop('type', 'submit');
    }
}
function newRemainingAmount() {
    var remainingAmount = $("#remainingAmount").val();
    var partPayment = $("#partPayment").val();
    partPayment = parseInt(partPayment);
    remainingAmount = parseInt(remainingAmount);
    if (partPayment > remainingAmount) {
        smoke.alert("Part payment is greater than remaining amount");
        $("#submit").prop('type', 'button');
        return false;
    }
}

function validateTaxPartPayment() {
    var taxNumber = $("#selectTaxNumber").val();

    var date = $("#date").val();


    if (taxNumber == '') {
        smoke.alert("Please select tax invoice number");
        return false;
    }

    if (date == '') {
        smoke.alert("Please select date");
        return false;
    }

    if (taxNumber != '' && date != '') {


        $.post(baseUrl + '/site/gettaxpaymentdetails', {
            taxNumber: taxNumber
        }, function (response) {
            if (response) {
                $(".showTaxPaymentData").html('');
                response = JSON.parse(response);
                var grandTotal = response.grandTotal;
                var remainingAmount = response.remainingAmount;
                var result = '<div class="form-group"> <label class="control-label col-sm-4" style="text-align: right!important;">Grand Total</label> <div class="col-sm-4"> <input type="text" readonly class="form-control" id="grandTotal" name="grandTotal" value="' + grandTotal + '" > </div> </div>';
                result += '<div class="form-group"> <label class="control-label col-sm-4" style="text-align: right!important;">Remaining Amount</label> <div class="col-sm-4"> <input type="text" readonly class="form-control" id="remainingAmount" name="remainingAmount" value="' + remainingAmount + '" > </div> </div>';
                result += '<div class="form-group"> <label class="control-label col-sm-4" style="text-align: right!important;">Part Payment</label> <div class="col-sm-4"> <input type="text" onkeypress="return isNumber2(event);" onkeyup="checkValue();newRemainingAmount();" required class="form-control" id="partPayment" name="partPayment" value=""></div> </div>';
                result += '<input type="hidden" id="actualRemainingAmount" name="actualRemainingAmount" value="' + remainingAmount + '">';
                $(".showTaxPaymentData").append(result);
            }
        });
    }
}

function validatePurchasePaymentReport() {
    var purchaseNumber = $("#selectPurchaseNumber").val();

    if (purchaseNumber == '') {
        smoke.alert("Please select purchase invoice number");
        return false;
    }

    if (purchaseNumber != '') {

        $("#submit").prop("type", "submit");
    }
}

function validateTaxPaymentReport() {
    var taxNumber = $("#selectTaxNumber").val();

    if (taxNumber == '') {
        smoke.alert("Please select tax invoice number");
        return false;
    }

    if (taxNumber != '') {

        $("#submit").prop("type", "submit");
    }
}
function displayRateOfProduct(id) {
    var productId = $("#purchaseproductCode-" + id).val();
    $.post(baseUrl + '/site/getprice', {
            productID: productId
        }, function (response) {
            if (response) {
                response = JSON.parse(response)
                $("#productAmount-" + id).val(response);
            }
        }, 'json'
    )
}

function validateStockData() {
    var numberOfItems = $("#number_of_subcategories").val();
    if (numberOfItems == '') {
        smoke.alert("Please select number of products");
        return false;
    }
    if (numberOfItems != '') {
        $.post(baseUrl + '/site/getproducts', {}, function (response) {
                if (response) {
                    $("#displayData").html('');
                    var length = response.length;
                    var result = '<div class="col-sm-12 form-group"><div class="col-sm-1">Sr.No</div><div class="col-sm-4">Product</div><div class="col-sm-4">Quantity</div> </div>';
                    for (var j = 0; j < numberOfItems; j++) {
                        var serialNumber = j + 1;
                        result += '<div class="col-sm-12 form-group"><div class=col-sm-1>' + serialNumber + '</div>';
                        result += '<div class=col-sm-4><select id="purchaseproductCode" data-placeholder="Select..." tabindex="-1" title="" name="productCode[]" class="form-control input-large select2-offscreen addProduct"><option value=""> </option>';
                        for (var i = 0; i < length; i++) {
                            var productCode = response[i].description;
                            var productId = $.trim(response[i].uniqueID);
                            result += '<option value="' + productId + '">';
                            result += '' + productCode + '</option>';
                        }
                        result += '</select></div>';
                        result += '<div class=col-sm-4><input type="text" onkeypress="return isNumber(event);" name="productQuantity[]" class="form-control"></div></div>';
                    }
                    $("#displayData").append(result);
                    $(".addProduct").select2();
                    $("#submit").prop('type', 'submit');
                }
            },
            'json')
    }
}

function validateModifiedStockData() {
    var numberOfItems = $("#number_of_subcategories").val();
    if (numberOfItems == '') {
        smoke.alert("Please select number of products");
        return false;
    }
    if (numberOfItems != '') {
        $.post(baseUrl + '/site/getproducts', {}, function (response) {
                if (response) {
                    $("#displayData").html('');
                    var length = response.length;
                    var result = '<div class="col-sm-12 form-group"><div class="col-sm-1">Sr.No</div><div class="col-sm-4">Product</div><div class="col-sm-4">Quantity</div> </div>';
                    for (var j = 0; j < numberOfItems; j++) {
                        var serialNumber = j + 1;
                        result += '<div class="col-sm-12 form-group"><div class=col-sm-1>' + serialNumber + '</div>';
                        result += '<div class=col-sm-4><select data-placeholder="Select..." tabindex="-1" title="" name="productCode[]" class="form-control input-large select2-offscreen addProduct" id="productCode-' + serialNumber + '"  onchange="displayAvailableStock(' + serialNumber + ');"><option value=""> </option>';
                        for (var i = 0; i < length; i++) {
                            var productCode = response[i].description;
                            var productId = $.trim(response[i].uniqueID);
                            result += '<option value="' + productId + '">';
                            result += '' + productCode + '</option>';
                        }
                        result += '</select></div>';
                        result += '<div class=col-sm-4><input type="text" onkeypress="return isNumber(event);" name="productQuantity[]" class="form-control" id="productQuantity-' + serialNumber + '"></div></div>';
                    }
                    $("#displayData").append(result);
                    $(".addProduct").select2();
                    $("#submit").prop('type', 'submit');

                }
            },
            'json')
    }
}

function displayAvailableStock(id) {

    var productCode = $("#productCode-" + id).val();

    $.post(baseUrl + '/site/getstock', {
            productCode: productCode
        },
        function (response) {
            if (response) {
                console.log(response);
                var stock = response[0].stock;
                $("#productQuantity-" + id).val(stock);
            }
        }, 'json'
    )

}

function validateEntry() {
    var quantity = $("#productQuantity-1").val();
    var product = $("#productCode-1").val();
    if (quantity != '' && product != '') {
        $("#submit").prop('type', 'submit');
    }
}
function checkEnteredValue(id) {
    var availableStock = $("#stock-" + id).val();
    var enteredStock = $("#quantity-" + id).val();
    availableStock = parseInt(availableStock);
    enteredStock = parseInt(enteredStock);
    if (enteredStock > availableStock) {
        smoke.alert("Qauntity cannot be greather than available stock");
        return false;
    }
}

/*  functions for Sai Fabrications   */



/*  Quotation related functions start here*/


function addRowInQuotation() {
    var rows = $("#quotationTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
                result += '<td style="width: 400px;"><select required required id="productCode-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="particular[]" style="width: 400px !important;" onchange="calculateTotalInQuotation();" class="form-control select2-offscreen addProduct-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productCode = response[i].name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productCode + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" required name="quantity[]" ng-model="formInfo.quantity' + slno + '" ng-change="updateAmount()" id="quantity-' + slno + '" class="form-control" onkeyup="calculationInQuotation(' + slno + ');"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInQuotation(' + slno + ');" onkeypress="return isNumber1(event);" ng-model="formInfo.rate' + slno + '" name="rate[]" class="form-control" id="rate-' + slno + '"> ';
                result += '</td> <td style="width: 250px;;"> <input type="text" readonly ng-model="formInfo.amount' + slno + '" name="amount[]" id="amount-' + slno + '" class="form-control"> </td> </tr>';
                $("#quotationDataTable").append(result);
                $(".addProduct-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

function addRowInExtra() {
    var rows = $("#quotationTableExt tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
                //result += '<td style="width: 400px;"><select required required id="productCodeExt-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="particularExt[]" style="width: 400px !important;" onchange="calculateTotalInQuotationExt();" class="form-control select2-offscreen addProductExt-' + slno + '"><option value=""> </option>';
                //for (var i = 0; i < length; i++) {
                //    var productCode = response[i].name;
                //    var productId = response[i].id;
                //    result += '<option value="' + productId + '">';
                //    result += '' + productCode + '</option>';
                //}
                //result += '</select></td> ';
                result += '<td><input type="text" style="width: 400px;" class="form-control" name="particularExt[]" id="productCodeExt-' + slno + '" onchange="calculationInQuotationExt()"></td>';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" name="notesExt[]" ng-model="formInfo.quantity' + slno + '" id="notesExt-' + slno + '" class="form-control"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" required name="quantityExt[]" ng-model="formInfo.quantity' + slno + '" ng-change="updateAmount()" id="quantityExt-' + slno + '" class="form-control" onkeyup="calculationInQuotationExt(' + slno + ');"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInQuotationExt(' + slno + ');" onkeypress="return isNumber1(event);" ng-model="formInfo.rate' + slno + '" name="rateExt[]" class="form-control" id="rateExt-' + slno + '"> ';
                result += '</td> <td style="width: 250px;;"> <input type="text" readonly ng-model="formInfo.amount' + slno + '" name="amountExt[]" id="amountExt-' + slno + '" class="form-control"> </td> </tr>';
                $("#quotationDataTableExt").append(result);
                $(".addProductExt-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

function addRowInOtr() {
    var rows = $("#quotationTableOtr tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
                //result += '<td style="width: 400px;"><select required required id="productCodeOtr-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="particularOtr[]" style="width: 400px !important;" onchange="calculateTotalInQuotationOtr();" class="form-control select2-offscreen addProductOtr-' + slno + '"><option value=""> </option>';
                //for (var i = 0; i < length; i++) {
                //    var productCode = response[i].name;
                //    var productId = response[i].id;
                //    result += '<option value="' + productId + '">';
                //    result += '' + productCode + '</option>';
                //}
                //result += '</select></td> ';
                result += '<td><input type="text" class="form-control" id="productCodeOtr-' + slno + '" name="particularOtr[]" onchange="calculationInQuotationOtr()" style="width: 400px;"></td>';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" name="notesOtr[]" ng-model="formInfo.quantity' + slno + '" id="notesOtr-' + slno + '" class="form-control"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" required name="quantityOtr[]" ng-model="formInfo.quantity' + slno + '" ng-change="updateAmount()" id="quantityOtr-' + slno + '" class="form-control" onkeyup="calculationInQuotationOtr(' + slno + ');"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInQuotationOtr(' + slno + ');" onkeypress="return isNumber1(event);" ng-model="formInfo.rate' + slno + '" name="rateOtr[]" class="form-control" id="rateOtr-' + slno + '"> ';
                result += '</td> <td style="width: 250px;;"> <input type="text" readonly ng-model="formInfo.amount' + slno + '" name="amountOtr[]" id="amountOtr-' + slno + '" class="form-control"> </td> </tr>';
                $("#quotationDataTableOtr").append(result);
                $(".addProductOtr-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

function addRowInSpecial() {
    var rows = $("#quotationTableSpc tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
                //result += '<td style="width: 400px;"><select required required id="productCodeSpc-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="particularSpc[]" style="width: 400px !important;" onchange="calculateTotalInQuotationSpc();" class="form-control select2-offscreen addProductSpc-' + slno + '"><option value=""> </option>';
                //for (var i = 0; i < length; i++) {
                //    var productCode = response[i].name;
                //    var productId = response[i].id;
                //    result += '<option value="' + productId + '">';
                //    result += '' + productCode + '</option>';
                //}
                //result += '</select></td> ';
                result += '<td><input type="text" class="form-control" id="productCodeSpc-' + slno + '" name="particularSpc[]" onchange="calculationInQuotationSpc()" style="width: 400px;"></td>';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" name="notesSpc[]" ng-model="formInfo.quantity' + slno + '" id="notesSpc-' + slno + '" class="form-control"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" required name="quantitySpc[]" ng-model="formInfo.quantity' + slno + '" ng-change="updateAmount()" id="quantitySpc-' + slno + '" class="form-control" onkeyup="calculationInQuotationSpc(' + slno + ');"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInQuotationSpc(' + slno + ');" onkeypress="return isNumber1(event);" ng-model="formInfo.rate' + slno + '" name="rateSpc[]" class="form-control" id="rateSpc-' + slno + '"> ';
                result += '</td> <td style="width: 250px;;"> <input type="text" readonly ng-model="formInfo.amount' + slno + '" name="amountSpc[]" id="amountSpc-' + slno + '" class="form-control"> </td> </tr>';
                $("#quotationDataTableSpc").append(result);
                $(".addProductSpc-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

function addRowInTechnical() {
    var rows = $("#quotationTableTech tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
                result += '<td style="width: 400px;"><select required required id="productCodeTech-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="particularTech[]" style="width: 400px !important;" onchange="calculateTotalInTech();" class="form-control select2-offscreen addProductTech-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productCode = response[i].name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productCode + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" name="notesTech[]" ng-model="formInfo.quantity' + slno + '" id="notesTech-' + slno + '" class="form-control"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" required name="quantityTech[]" ng-model="formInfo.quantity' + slno + '" ng-change="updateAmount()" id="quantityTech-' + slno + '" class="form-control" onkeyup="calculationInQuotationTech(' + slno + ');"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInQuotationTech(' + slno + ');" onkeypress="return isNumber1(event);" ng-model="formInfo.rate' + slno + '" name="rateTech[]" class="form-control" id="rateTech-' + slno + '"> ';
                result += '</td> <td style="width: 250px;;"> <input type="text" readonly ng-model="formInfo.amount' + slno + '" name="amountTech[]" id="amountTech-' + slno + '" class="form-control"> </td> </tr>';
                $("#quotationDataTableTech").append(result);
                $(".addProductTech-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

function addRowInLight() {
    var rows = $("#quotationTableLight tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
                result += '<td style="width: 400px;"><select required required id="productCodeLight-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="particularLight[]" style="width: 400px !important;" onchange="calculateTotalInQuotationLight();" class="form-control select2-offscreen addProductLight-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productCode = response[i].name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productCode + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" name="notesLight[]" ng-model="formInfo.quantity' + slno + '" id="notesLight-' + slno + '" class="form-control"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" required name="quantityLight[]" ng-model="formInfo.quantity' + slno + '" ng-change="updateAmount()" id="quantityLight-' + slno + '" class="form-control" onkeyup="calculationInQuotationLight(' + slno + ');"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInQuotationLight(' + slno + ');" onkeypress="return isNumber1(event);" ng-model="formInfo.rate' + slno + '" name="rateLight[]" class="form-control" id="rateLight-' + slno + '"> ';
                result += '</td> <td style="width: 250px;;"> <input type="text" readonly ng-model="formInfo.amount' + slno + '" name="amountLight[]" id="amountLight-' + slno + '" class="form-control"> </td> </tr>';
                $("#quotationDataTableLight").append(result);
                $(".addProductLight-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

function addRowInTent() {
    var rows = $("#quotationTableTent tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
                result += '<td style="width: 400px;"><select required required id="productCodeTent-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="particularTent[]" style="width: 400px !important;" onchange="calculateTotalInQuotationTent();" class="form-control select2-offscreen addProductTent-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productCode = response[i].name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productCode + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" name="notesTent[]" ng-model="formInfo.quantity' + slno + '" id="notesTent-' + slno + '" class="form-control"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" required name="quantityTent[]" ng-model="formInfo.quantity' + slno + '" ng-change="updateAmount()" id="quantityTent-' + slno + '" class="form-control" onkeyup="calculationInQuotationTent(' + slno + ');"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInQuotationTent(' + slno + ');" onkeypress="return isNumber1(event);" ng-model="formInfo.rate' + slno + '" name="rateTent[]" class="form-control" id="rateTent-' + slno + '"> ';
                result += '</td> <td style="width: 250px;;"> <input type="text" readonly ng-model="formInfo.amount' + slno + '" name="amountTent[]" id="amountTent-' + slno + '" class="form-control"> </td> </tr>';
                $("#quotationDataTableTent").append(result);
                $(".addProductTent-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

var serialNumberStage = 1;
function addRowInStage() {
    var rows = $("#quotationTableStage tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
                result += '<td style="width: 400px;"><select  required id="productCodeStage-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="particularStage[]" style="width: 400px !important;" onchange="calculateTotalInStage();" class="form-control select2-offscreen addProductStage-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productCode = response[i].name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productCode + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" name="notesStage[]" ng-model="formInfo.quantity' + slno + '" id="notesStage-' + slno + '" class="form-control"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" required name="quantityStage[]" ng-model="formInfo.quantity' + slno + '" ng-change="updateAmount()" id="quantityStage-' + slno + '" class="form-control" onkeyup="calculationInQuotationStage(' + slno + ');"> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInQuotationStage(' + slno + ');" onkeypress="return isNumber1(event);" ng-model="formInfo.rate' + slno + '" name="rateStage[]" class="form-control" id="rateStage-' + slno + '"> ';
                result += '</td> <td style="width: 250px;;"> <input type="text" readonly ng-model="formInfo.amount' + slno + '" name="amountStage[]" id="amountStage-' + slno + '" class="form-control"> </td> </tr>';
                $("#quotationDataTableStage").append(result);
                $(".addProductStage-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

function deleteRowInQuotation() {
    $("#quotationTable tr:last").remove();
    var counter = $("#counter").val();
    counter = counter - 1;
    $("#counter").val(counter);
    calculateTotalInQuotation();
}

function deleteRowInExtra() {
    $("#quotationTableExt tr:last").remove();
    var counter = $("#counter").val();
    counter = counter - 1;
    $("#counter").val(counter);
    calculateTotalInQuotationExt();
}

function deleteRowInOtr() {
    $("#quotationTableOtr tr:last").remove();
    var counter = $("#counter").val();
    counter = counter - 1;
    $("#counter").val(counter);
    calculateTotalInQuotationOtr();
}

function deleteRowInSpc() {
    $("#quotationTableSpc tr:last").remove();
    var counter = $("#counter").val();
    counter = counter - 1;
    $("#counter").val(counter);
    calculateTotalInQuotationSpc();
}

function deleteRowInTech() {
    $("#quotationTableTech tr:last").remove();
    var counter = $("#counter").val();
    counter = counter - 1;
    $("#counter").val(counter);
    calculateTotalInTech();
}

function deleteRowInLight() {
    $("#quotationTableLight tr:last").remove();
    var counter = $("#counter").val();
    counter = counter - 1;
    $("#counter").val(counter);
    calculateTotalInQuotationLight();
}

function deleteRowInTent() {
    $("#quotationTableTent tr:last").remove();
    var counter = $("#counter").val();
    counter = counter - 1;
    $("#counter").val(counter);
    calculateTotalInQuotationTent();
}

function deleteRowInStage() {
    $("#quotationTableStage tr:last").remove();
    var counter = $("#counter").val();
    counter = counter - 1;
    $("#counter").val(counter);
    calculateTotalInStage();
}

function getDimensions(id) {

    var recordID = $("#productCode-" + id).val();

    $.post(baseUrl + '/site/getdimension', {
            recordID: recordID
        }, function (response) {
            if (response) {
                var dimensionOne = response[0].dimensionOne;
                var dimensionTwo = response[0].dimensionTwo;
                $("#dimensionOne-" + id).val(dimensionOne);
                $("#dimensionTwo-" + id).val(dimensionTwo);
                calculationInQuotation(id);
            }
        },
        'json')
}

function getDimensionsForDelivery(id) {

    var recordID = $("#productCode-" + id).val();

    $.post(baseUrl + '/site/getdimension', {
            recordID: recordID
        }, function (response) {
            if (response) {
                var dimensionOne = response[0].dimensionOne;
                var dimensionTwo = response[0].dimensionTwo;
                $("#dimensionOne-" + id).val(dimensionOne);
                $("#dimensionTwo-" + id).val(dimensionTwo);

                var sqft = parseInt(dimensionOne) * parseInt(dimensionTwo);
                sqft = sqft / 144;
                sqft = parseFloat(sqft);
                if (!isNaN(sqft)) {
                    sqft = sqft.toFixed(2);
                    $("#persqft-" + id).val(sqft);
                }
            }
        },
        'json')
}

function calculationInQuotationTech(id) {
    var total = '';
    var totalAmount = '';
    var quantity = $("#quantityTech-" + id).val();
    quantity = parseInt(quantity);
    var rate = $("#rateTech-" + id).val();
    if (!isNaN(quantity)) {
        if (!isNaN(rate)) {
            totalAmount = parseFloat(rate) * parseFloat(quantity);
        }
    }
    if (!isNaN(parseFloat(totalAmount))) {
        totalAmount = totalAmount.toFixed(2);
        $("#amountTech-" + id).val(totalAmount);
    }
    calculateTotalInTech();
}

function calculationInQuotationExt(id) {
    var total = '';
    var totalAmount = '';
    var quantity = $("#quantityExt-" + id).val();
    quantity = parseInt(quantity);
    var rate = $("#rateExt-" + id).val();
    if (!isNaN(quantity)) {
        if (!isNaN(rate)) {
            totalAmount = parseFloat(rate) * parseFloat(quantity);
        }
    }
    if (!isNaN(parseFloat(totalAmount))) {
        totalAmount = totalAmount.toFixed(2);
        $("#amountExt-" + id).val(totalAmount);
    }
    calculateTotalInQuotationExt();
}

function calculationInQuotationOtr(id) {
    var total = '';
    var totalAmount = '';
    var quantity = $("#quantityOtr-" + id).val();
    quantity = parseInt(quantity);
    var rate = $("#rateOtr-" + id).val();
    if (!isNaN(quantity)) {
        if (!isNaN(rate)) {
            totalAmount = parseFloat(rate) * parseFloat(quantity);
        }
    }
    if (!isNaN(parseFloat(totalAmount))) {
        totalAmount = totalAmount.toFixed(2);
        $("#amountOtr-" + id).val(totalAmount);
    }
    calculateTotalInQuotationOtr();
}

function calculationInQuotationSpc(id) {
    var total = '';
    var totalAmount = '';
    var quantity = $("#quantitySpc-" + id).val();
    quantity = parseInt(quantity);
    var rate = $("#rateSpc-" + id).val();
    if (!isNaN(quantity)) {
        if (!isNaN(rate)) {
            totalAmount = parseFloat(rate) * parseFloat(quantity);
        }
    }
    if (!isNaN(parseFloat(totalAmount))) {
        totalAmount = totalAmount.toFixed(2);
        $("#amountSpc-" + id).val(totalAmount);
    }
    calculateTotalInQuotationSpc();
}


function calculationInQuotationLight(id) {
    var total = '';
    var totalAmount = '';
    var quantity = $("#quantityLight-" + id).val();
    quantity = parseInt(quantity);
    var rate = $("#rateLight-" + id).val();
    if (!isNaN(quantity)) {
        if (!isNaN(rate)) {
            totalAmount = parseFloat(rate) * parseFloat(quantity);
        }
    }
    if (!isNaN(parseFloat(totalAmount))) {
        totalAmount = totalAmount.toFixed(2);
        $("#amountLight-" + id).val(totalAmount);
    }
    calculateTotalInQuotationLight();
}

function calculationInQuotationTent(id) {
    var total = '';
    var totalAmount = '';
    var quantity = $("#quantityTent-" + id).val();
    quantity = parseInt(quantity);
    var rate = $("#rateTent-" + id).val();
    if (!isNaN(quantity)) {
        if (!isNaN(rate)) {
            totalAmount = parseFloat(rate) * parseFloat(quantity);
        }
    }
    if (!isNaN(parseFloat(totalAmount))) {
        totalAmount = totalAmount.toFixed(2);
        $("#amountTent-" + id).val(totalAmount);
    }
    calculateTotalInQuotationTent();
}

function calculationInQuotation(id) {
    var total = '';
    var totalAmount = '';
    var quantity = $("#quantity-" + id).val();
    quantity = parseInt(quantity);
    var rate = $("#rate-" + id).val();
    if (!isNaN(quantity)) {
        if (!isNaN(rate)) {
            totalAmount = parseFloat(rate) * parseFloat(quantity);
        }
    }
    if (!isNaN(parseFloat(totalAmount))) {
        totalAmount = totalAmount.toFixed(2);
        $("#amount-" + id).val(totalAmount);
    }
    calculateTotalInQuotation();
}


function calculationInQuotationStage(id) {
    var total = '';
    var totalAmount = '';
    var quantity = $("#quantityStage-" + id).val();
    quantity = parseInt(quantity);
    var rate = $("#rateStage-" + id).val();
    if (!isNaN(quantity)) {
        if (!isNaN(rate)) {
            totalAmount = parseFloat(rate) * parseFloat(quantity);
        }
    }
    if (!isNaN(parseFloat(totalAmount))) {
        totalAmount = totalAmount.toFixed(2);
        $("#amountStage-" + id).val(totalAmount);
    }
    calculateTotalInStage();
}

function calculatePersqft(id) {

    var dimensionOne = $("#dimensionOne-" + id).val();
    var dimensionTwo = $("#dimensionTwo-" + id).val();
    var sqft = parseInt(dimensionOne) * parseInt(dimensionTwo);
    sqft = sqft / 144;
    sqft = parseFloat(sqft);
    if (!isNaN(sqft)) {
        sqft = sqft.toFixed(2);
        $("#persqft-" + id).val(sqft);
    }
    calculationInQuotation(id);
}

function calculateTotalInQuotation() {
    var table = document.getElementById("quotationTable");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var a = document.getElementsByName("amount[]")[i].value;
        if (!isNaN(parseFloat(a))) {
            totamt1 = totamt1 + parseFloat(a);
        }
    }
    document.getElementById("totamt").value = totamt1.toFixed(2);
}

function calculateTotalInQuotationExt() {
    var table = document.getElementById("quotationTableExt");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var a = document.getElementsByName("amountExt[]")[i].value;
        if (!isNaN(parseFloat(a))) {
            totamt1 = totamt1 + parseFloat(a);
        }
    }
    document.getElementById("totamtExt").value = totamt1.toFixed(2);
}

function calculateTotalInQuotationOtr() {
    var table = document.getElementById("quotationTableOtr");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var a = document.getElementsByName("amountOtr[]")[i].value;
        if (!isNaN(parseFloat(a))) {
            totamt1 = totamt1 + parseFloat(a);
        }
    }
    document.getElementById("totamtOtr").value = totamt1.toFixed(2);
}

function calculateTotalInQuotationSpc() {
    var table = document.getElementById("quotationTableSpc");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var a = document.getElementsByName("amountSpc[]")[i].value;
        if (!isNaN(parseFloat(a))) {
            totamt1 = totamt1 + parseFloat(a);
        }
    }
    document.getElementById("totamtSpc").value = totamt1.toFixed(2);
}

function calculateTotalInTech() {
    var table = document.getElementById("quotationTableTech");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var a = document.getElementsByName("amountTech[]")[i].value;
        if (!isNaN(parseFloat(a))) {
            totamt1 = totamt1 + parseFloat(a);
        }
    }
    document.getElementById("totamtTech").value = totamt1.toFixed(2);
}

function calculateTotalInQuotationLight() {
    var table = document.getElementById("quotationTableLight");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var a = document.getElementsByName("amountLight[]")[i].value;
        if (!isNaN(parseFloat(a))) {
            totamt1 = totamt1 + parseFloat(a);
        }
    }
    document.getElementById("totamtLight").value = totamt1.toFixed(2);
}

function calculateTotalInQuotationTent() {
    var table = document.getElementById("quotationTableTent");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var a = document.getElementsByName("amountTent[]")[i].value;
        if (!isNaN(parseFloat(a))) {
            totamt1 = totamt1 + parseFloat(a);
        }
    }
    document.getElementById("totamttent").value = totamt1.toFixed(2);
}

function calculateTotalInStage() {
    var table = document.getElementById("quotationTableStage");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var a = document.getElementsByName("amountStage[]")[i].value;
        if (!isNaN(parseFloat(a))) {
            totamt1 = totamt1 + parseFloat(a);
        }
    }
    document.getElementById("totamtstage").value = totamt1.toFixed(2);
}

function validateModifyQuotationNumber() {
    var invoiceNumber = $("#selectTaxNumber").val();

    if (invoiceNumber == '') {
        smoke.alert("Please select quotation number");
        return false;
    }

    if (invoiceNumber != '') {
        $("#submit").prop("type", "submit");
    }
}


/*  Quotation related functions end here*/

/*  Delivery  related functions start here*/


function addRowInDelivery() {
    var rows = $("#deliveryTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getmaterials', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;"><strong>' + slno + '</strong></td> ';
                result += '<td style="width: 300px;"><select required id="productCode-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="particular[]" style="width: 300px !important;" onchange="getDimensionsForDelivery(' + slno + ');" class="form-control select2-offscreen addProduct-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productCode = response[i].name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productCode + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 180px;padding-right: 10px;"> <input type="text" required class="form-control" name="dimensionOne[]" onkeyup="calculatePersqftInDelivery(' + slno + ');" id="dimensionOne-' + slno + '"></td>';
                result += '<td style="width: 180px;padding-right: 10px;"> <input type="text" required class="form-control" name="dimensionTwo[]" onkeyup="calculatePersqftInDelivery(' + slno + ');" id="dimensionTwo-' + slno + '"></td>';
                result += '<td style="width: 180px;padding-right: 10px;"> <input type="text" class="form-control" readonly onkeypress="return isNumber(event);" required name="persqft[]" id="persqft-' + slno + '"> </td>';
                result += '<td style="width: 180px;padding-right: 10px;"> <input type="text" required class="form-control" onkeypress="return isNumber(event);" required name="quantity[]" onkeyup="calculatePersqftInDelivery(' + slno + ')" id="quantity-' + slno + '"> </td> ';
                result += '<td style="width: 180px;padding-right: 10px;"> <input type="text" readonly onkeypress="return isNumber(event);" required name="totalsqft[]" id="totalsqft-' + slno + '" class="form-control"> </td></tr>';
                $("#deliveryTable").append(result);
                $(".addProduct-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

function deleteRowInDelivery() {

    $("#deliveryTable tr:last").remove();
    var counter = $("#counter").val();
    counter = counter - 1;
    $("#counter").val(counter);
    calculateTotalInDelivery();

}

function calculatePersqftInDelivery(id) {
    var dimensionOne = $("#dimensionOne-" + id).val();
    var dimensionTwo = $("#dimensionTwo-" + id).val();
    var totalSqft = '';
    var sqft = parseFloat(dimensionOne) * parseFloat(dimensionTwo);
    sqft = sqft / 144;
    sqft = parseFloat(sqft);
    if (!isNaN(sqft)) {
        sqft = sqft.toFixed(2);
        $("#persqft-" + id).val(sqft);
    }
    var quantity = $("#quantity-" + id).val();
    totalSqft = parseFloat(sqft) * parseFloat(quantity);
    if (!isNaN(parseFloat(totalSqft))) {
        totalSqft = totalSqft.toFixed(2);
        $("#totalsqft-" + id).val(totalSqft);
    }

}

function validateModifyDeliveryNumber() {
    var invoiceNumber = $("#selectTaxNumber").val();

    if (invoiceNumber == '') {
        smoke.alert("Please select delivery challan number");
        return false;
    }

    if (invoiceNumber != '') {
        $("#submit").prop("type", "submit");
    }
}

/*  Delivery  related functions end here*/

/*  Measurement List  related functions start here*/

function addRowInMeasurement() {
    var rows = $("#measurementTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getmaterials', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;"><strong>' + slno + '</strong></td> ';
                result += '<td style="width: 400px;"><select required id="productCode-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="particular[]" style="width: 400px !important;" onchange="getDimensions(' + slno + ');" class="form-control select2-offscreen addProduct-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productCode = response[i].name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productCode + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required class="form-control" name="dimensionOne[]" onkeyup="calculatePersqftInDelivery(' + slno + ');" id="dimensionOne-' + slno + '"></td>';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required class="form-control" name="dimensionTwo[]" onkeyup="calculatePersqftInDelivery(' + slno + ');" id="dimensionTwo-' + slno + '"></td>';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" readonly onkeypress="return isNumber(event);" required name="persqft[]" id="persqft-' + slno + '" class="form-control"> </td>';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" onkeypress="return isNumber(event);" required onkeyup="calculatePersqftInDelivery(' + slno + ');" name="quantity[]" id="quantity-' + slno + '" class="form-control" onkeyup=""> </td> ';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" readonly onkeypress="return isNumber(event);" required name="totalsqft[]" id="totalsqft-' + slno + '" class="form-control"> </td>';
                result += '<td style="width: 250px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber1(event);" name="rft[]" id="rft-' + slno + '" class="form-control"> </td></tr>';
                $("#measurementTable").append(result);
                $(".addProduct-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

function deleteRowInMeasurement() {

    $("#measurementTable tr:last").remove();
    var counter = $("#counter").val();
    counter = counter - 1;
    $("#counter").val(counter);
}

/*  Measurement List  related functions end here*/

/*  Labour  related functions starts here*/

function addRowInLabour() {
    var rows = $("#labourTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    var result = '<tr> <td style="width: 60px;padding-top: 16px;"><strong>' + slno + '</strong></td> ';
    result += '<td style="width: 700px;padding-right: 10px;"> <input type="text" class="form-control" name="particular[]" id="particular-' + slno + '"></td>';
    result += '<td style="width: 400px;padding-right: 10px;"> <input type="text" class="form-control" name="amount[]" onkeyup="calculationInLabour();" id="amount-' + slno + '"></td></tr>';
    $("#labourTable").append(result);
    $(".addProduct-" + slno).select2();
    $("#counter").val(slno);
}

function deleteRowInLabour() {

    $("#labourTable tr:last").remove();
    var counter = $("#counter").val();
    counter = counter - 1;
    $("#counter").val(counter);
}

function calculationInLabour() {
    var counter = $("#counter").val();
    var totalAmount = 0;
    for (var i = 0; i < counter; i++) {
        var key = i + 1;
        totalAmount += parseFloat($("#amount-" + key).val());
    }
    if (!isNaN(parseFloat(totalAmount))) {
        $("#totalAmount").val(totalAmount);
    }
}

function validateModifyLabourNumber() {
    var invoiceNumber = $("#selectTaxNumber").val();

    if (invoiceNumber == '') {
        smoke.alert("Please select invoice number");
        return false;
    }

    if (invoiceNumber != '') {
        $("#submit").prop("type", "submit");
    }
}

function calculateBalance(id) {
    var totalAmount = $("#totalAmount-" + id).val();
    if (!isNaN(parseFloat(totalAmount))) {
        var amount = $("#amount-" + id).val();
        if (!isNaN(parseFloat(amount))) {
            var remainingAmount = parseFloat(totalAmount) - parseFloat(amount);
        }
    }
    remainingAmount = parseFloat(remainingAmount);
    remainingAmount = remainingAmount.toFixed(2);
    if (!isNaN(remainingAmount)) {
        $("#balance-" + id).val(remainingAmount);
    }
}

/*  Labour  related functions end here*/

/*  Tax  related functions start here*/

function addRowInTax() {
    var rows = $("#taxTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;"><strong>' + slno + '</strong></td> ';
                result += '<td style="width: 300px;"><select required id="productCode-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="particular[]" style="width: 300px !important;" onchange="" class="form-control select2-offscreen addProduct-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productName = response[i].product_name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productName + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 150px;padding-right: 10px;"> <input type="text" required class="form-control" name="dimensionOne[]" onkeyup="calculationInTax(' + slno + ');" id="dimensionOne-' + slno + '"></td>';
                result += '<td style="width: 150px;padding-right: 10px;"> <input type="text" required class="form-control" name="dimensionTwo[]" onkeyup="calculationInTax(' + slno + ');" id="dimensionTwo-' + slno + '"></td>';
                result += '<td style="width: 150px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" name="quantity[]" id="quantity-' + slno + '" class="form-control" onkeyup="calculationInTax(' + slno + ');"> </td> ';
                result += '<td style="width: 150px;padding-right: 10px;"> <input type="text" onkeypress="return isNumber1(event);" readonly name="persqft[]" id="persqft-' + slno + '" class="form-control"> </td> ';
                result += '<td style="width: 150px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInTax(' + slno + ');" onkeypress="return isNumber(event);" name="rate[]" class="form-control" id="rate-' + slno + '"> ';
                result += '</td> <td style="width: 150px;;"> <input type="text" readonly name="amount[]" id="amount-' + slno + '" class="form-control"> </td> </tr>';
                $("#taxTable").append(result);
                $(".addProduct-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

function deleteRowInTax() {
    $("#taxTable tr:last").remove();
    calculateTotalInTax();
}

function calculationInTax(id) {

    var total = '';
    var totalAmount = '';
    var persqft = '';
    var quantity = $("#quantity-" + id).val();
    var dimensionOne = $("#dimensionOne-" + id).val();
    var dimensionTwo = $("#dimensionTwo-" + id).val();
    var sqft = parseInt(dimensionOne) * parseInt(dimensionTwo);
    sqft = sqft / 144;
    quantity = parseInt(quantity);
    persqft = parseInt(quantity) * parseFloat(sqft);
    if (!isNaN(sqft)) {
        persqft = persqft.toFixed(2);
        $("#persqft-" + id).val(persqft);
    }
    var rate = $("#rate-" + id).val();
    if (!isNaN(quantity)) {
        if (!isNaN(rate)) {
            totalAmount = parseFloat(rate) * parseFloat(persqft);
        }
    }
    if (!isNaN(parseFloat(totalAmount))) {
        totalAmount = totalAmount.toFixed(2);
        $("#amount-" + id).val(totalAmount);
    }
    calculateTotalInTax();
}

function calculatePersqftintaxinvoice(id) {

    var dimensionOne = $("#dimensionOne-" + id).val();
    var dimensionTwo = $("#dimensionTwo-" + id).val();
    var sqft = parseInt(dimensionOne) * parseInt(dimensionTwo);
    sqft = sqft / 144;
    sqft = parseFloat(sqft);
    if (!isNaN(sqft)) {
        sqft = sqft.toFixed(2);
        $("#persqft-" + id).val(sqft);
    }
    calculationInTax(id);
}

function calculateTotalInTax() {

    var table = document.getElementById("taxTable");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var a = document.getElementsByName("amount[]")[i].value;
        if (!isNaN(parseFloat(a))) {
            totamt1 = totamt1 + parseFloat(a);
        }
    }
    document.getElementById("totamt").value = totamt1.toFixed(2);

    var vatAmount = ((parseFloat(totamt1) * 12.5) / 100);
    vatAmount = parseFloat(vatAmount).toFixed(2);
    $("#vat").val(vatAmount);
    var grandTotal = parseFloat(totamt1) + parseFloat(vatAmount);
    if (!isNaN(grandTotal)) {
        grandTotal = parseFloat(grandTotal).toFixed(2);
        $("#grandtot").val(grandTotal);
    }

}
function calculateTotalInDelivery() {
    var table = document.getElementById("deliveryTable");
    var rowCount = table.rows.length;
    var counter = $("#counter").val();

    var total = 0;
    for (var i = 0; i < rowCount; i++) {
        var key = i + 1;
        total += parseFloat($("#amount-" + key).val());
    }
    total = parseFloat(total);

    if (!isNaN(total)) {
        total = total.toFixed(2);
        $("#totamt").val(total);

        var vatAmount = ((parseFloat(total) * 12.5) / 100);
        vatAmount = parseFloat(vatAmount).toFixed(2);
        $("#vat").val(vatAmount);
        var grandTotal = parseFloat(total) + parseFloat(vatAmount);
        if (!isNaN(grandTotal)) {
            grandTotal = parseFloat(grandTotal).toFixed(2);
            $("#totamt").val(grandTotal);
        }
    }

}

function showtextbox() {
    var mode = $("#PaymentMode").val();

    if (mode == 'cash') {
        $("#creditnumber").prop('disabled', true);
    }
    if (mode == 'none') {
        $("#creditnumber").prop('disabled', true);
    }
    if (mode == 'credit') {
        $("#creditnumber").prop('disabled', false);
    }
}

function calculateRemainingBalance() {
    var grandTotal = $("#grandtot").val();
    var amountReceived = $("#paymentReceived").val();
    var remainingBalance = parseFloat(grandTotal) - parseFloat(amountReceived);
    remainingBalance = parseFloat(remainingBalance);
    if (!isNaN(remainingBalance)) {
        remainingBalance = remainingBalance.toFixed(2);
        $("#balanceAmount").val(remainingBalance);
    }
    if (remainingBalance != 0) {
        var result = '<label class="control-label col-sm-2">Clearance Date</label> <div class="col-sm-2"> <input type="date" required="" class="form-control" name="amountClearanceDate" value=""id="amountClearanceDate"> </div>';
        $("#addDatePicker").html(result);
    }
    if (remainingBalance == 0) {
        $("#addDatePicker").html('');
    }
}

/*  Tax  related functions end here*/


/*  Tax Simple  related functions start here*/

function addRowInTaxSimple() {
    var rows = $("#taxTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;


    var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
    result += '<td style="width: 202px;padding-right: 10px;"> <input type="text" name="particular[]" id="productCode-' + slno + '" class="form-control"> </td>';
    result += '<td style="width: 216px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" name="quantity[]" id="quantity-' + slno + '" class="form-control" onkeyup="calculationInTaxSimple(' + slno + ');"> </td> ';
    result += '<td style="width: 200px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInTaxSimple(' + slno + ');" onkeypress="return isNumber1(event);" name="rate[]" class="form-control" id="rate-' + slno + '"> ';
    result += '</td> <td style="width: 200px;;"> <input type="text" readonly name="amount[]" id="amount-' + slno + '" class="form-control"> </td> </tr>';
    $("#taxTable").append(result);
    $(".addProduct-" + slno).select2();
    $("#counter").val(slno);


}

function deleteRowInTaxSimple() {
    $("#taxTable tr:last").remove();
}

function calculationInTaxSimple(id) {

    var total = '';
    var totalAmount = '';
    var quantity = $("#quantity-" + id).val();
    quantity = parseInt(quantity);
    var rate = $("#rate-" + id).val();
    if (!isNaN(quantity)) {
        if (!isNaN(rate)) {
            totalAmount = parseFloat(rate) * parseFloat(quantity);
        }
    }
    if (!isNaN(parseFloat(totalAmount))) {
        totalAmount = totalAmount.toFixed(2);
        $("#amount-" + id).val(totalAmount);
    }
    calculateTotalInTaxSimple();
}

function calculateTotalInTaxSimple() {

    var table = document.getElementById("taxTable");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var a = document.getElementsByName("amount[]")[i].value;
        if (!isNaN(parseFloat(a))) {
            totamt1 = totamt1 + parseFloat(a);
        }
    }
    document.getElementById("totamt").value = totamt1.toFixed(2);

    var vatAmount = ((parseFloat(totamt1) * 12.5) / 100);
    vatAmount = parseFloat(vatAmount).toFixed(2);
    $("#vat").val(vatAmount);
    var grandTotal = parseFloat(totamt1) + parseFloat(vatAmount);
    if (!isNaN(grandTotal)) {
        grandTotal = parseFloat(grandTotal).toFixed(2);
        $("#grandtot").val(grandTotal);
    }

}

/*  Tax Simple  related functions ends here*/

/*  Pending Amount related functions start here*/
function getData() {
    var combinedData = $("#selectInvoiceNumber").val();

    $.ajax({
        url: baseUrl + '/site/getdata',
        type: 'POST',
        data: {
            combinedData: combinedData
        },
        dataType: 'json',
        success: function (response) {
            var length = response.length;
            var result = '<table class="table" cellpadding="10" cellspacing="10"><thead><th>Invoice Number</th><th>Client Name</th><th>Grand Total</th><th>Paid Amount</th><th>Balance Amount</th><th>Received Amount</th></thead><tbody>';
            for (var i = 0; i < length; i++) {
                var invoiceNumber = response[i].invoice_no;
                var clientName = response[i].client_name;
                var grandTotal = response[i].grandTotal;
                var paidAmount = response[i].paidAmount;
                var remainingAmount = response[i].remainingAmount;
                result += '<tr><td><input type="text" name="invoiceNumber" class="form-control" readonly value="' + invoiceNumber + '"></td><td><input type="text" name="clientName" class="form-control" readonly value="' + clientName + '"></td><td><input type="text" name="grandTotal" class="form-control" readonly value="' + grandTotal + '"></td><td><input type="text" name="paidAmount" class="form-control" readonly value="' + paidAmount + '"></td><td><input type="text" readonly name="remainingAmount" class="form-control" value="' + remainingAmount + '"></td><td><input type="text" class="form-control" required name="receivedAmount"></td></tr>';
            }
            result += '</tbody></table><div class="form-group"> <label class="control-label col-sm-2">Payment Mode</label> <div class="col-sm-4"> <select id="PaymentMode" required name="paymentMode"class="form-control input-large select2me select2-offscreen"data-placeholder="Select..." tabindex="-1" title=""style="width: 300px!important;"onchange="showtextbox();"> <option value="">------------------------Select------------------------ </option> <option value="cash">Cash</option><option value="credit">Card</option> <option value="cheque">Cheque</option> </select> </div> <label class="control-label col-sm-2">Date</label> <div class="col-sm-2"> <input type="date" required="" class="form-control" name="date" value="" id="date"> </div> </div>   <div class="form-group"> <label class="control-label col-sm-2">Reciept No./Cheque No.</label> <div class="col-sm-3"> <input type="text" style="" required class="form-control" value=""name="creditnumber"id="creditnumber"> </div> </div>';
            $("#appendData").html(result);
            $("#PaymentMode").select2();

        },
        error: function (request, error) {
        }
    });
}

function getPendingAmountData() {

    var combinedData = $("#selectInvoiceNumber").val();

    if (combinedData == '') {
        $("#appendData").html('');
        return false;
    }
    $.ajax({
        url: baseUrl + '/site/getpendingamountdata',
        type: 'POST',
        data: {
            combinedData: combinedData
        },
        dataType: 'json',
        success: function (response) {
            var length = response.length;
            var invoiceNumber = response[0].invoice_no;
            var clientName = response[0].client_name;
            var clientCode = response[0].clientcode;

            var result = '<div class="form-group"> <label class="control-label col-sm-2">Invoice Number</label> <div class="col-sm-1"> <input type="text" class="form-control" name="invoiceNumber" readonly value="' + invoiceNumber + '"></div><label class="control-label col-sm-2">Client Name</label> <div class="col-sm-2"> <input type="text" readonly class="form-control" name="" value="' + clientName + '"> <input type="hidden" name="clientcode" value="' + clientCode + '"></div></div><table class="table" cellpadding="10" cellspacing="10"><thead><th>Date (D-M-Y)</th><th>Mode Of Payment</th><th>Reference Number</th><th>Grand Total</th><th>Paid Amount</th><th>Balance Amount</th></thead><tbody>';
            for (var i = 0; i < length; i++) {
                var date = response[i].clearanceDate;
                var grandTotal = response[i].grandTotal;
                var paidAmount = response[i].paidAmount;
                var remainingAmount = response[i].remainingAmount;
                var paymentMode = response[i].paymentMode;
                var referenceNumber = response[i].referenceNumber;

                dteSplit = date.split("-");
                yr = dteSplit[0][2] + dteSplit[0][3]; //special yr format, take last 2 digits
                month = dteSplit[1];
                day = dteSplit[2];

                finalDate = day + "-" + month + "-" + yr

                result += '<tr><td><input type="text" name="" class="form-control" readonly value="' + finalDate + '"></td><td><input type="text" name="clientName" class="form-control" readonly value="' + paymentMode + '"></td><td><input type="text" name="clientName" class="form-control" readonly value="' + referenceNumber + '"></td><td><input type="text" name="grandTotal" class="form-control" readonly value="' + grandTotal + '"></td><td><input type="text" name="paidAmount" class="form-control" readonly value="' + paidAmount + '"></td><td><input type="text" readonly name="remainingAmount" class="form-control" value="' + remainingAmount + '"></td></tr>';
            }
            result += '</tbody></table><div class="form-group"><div class="col-md-12"><button class="btn green" type="submit">View Report</button></div></div>';
            $("#appendData").html(result);
        },
        error: function (request, error) {
        }
    });
}
/*  Pending Amount functions end here*/

/*  Cancel Invoice, Labour functions starts here*/

function confirmCancel() {

    smoke.confirm("Are you sure?", function (e) {
        if (e) {
            changeStatus();
        } else {

        }
    }, {
        ok: "Yes",
        cancel: "No",
        classname: "custom-class",
        reverseButtons: true
    });
}

function changeStatus() {
    $("#submit").prop("type", "submit");
    //$.LoadingOverlay('show');
}
/*  Cancel Invoice, Labour functions end here*/

/*  Work Order functions start here*/


function displayStock(id) {
    id = 1;
    $("#productCode").change(function () {

        var productCode = $("#productCode").find(":selected").val();
        $.post(baseUrl + '/site/getstock', {
                productCode: productCode
            },
            function (response) {
                if (response) {
                    console.log(response);
                    var stock = response[0].stock;
                    var rate = response[0].rate;
                    $("#stock-" + id).val(stock);
                    $("#rate-" + id).val(rate);

                }
            }, 'json'
        )
    });
}

function displayStockForAdditionalRows(id) {
    var productCode = $("#productCode-" + id).val();

    $.post(baseUrl + '/site/getstock', {
            productCode: productCode
        },
        function (response) {
            if (response) {
                console.log(response);
                var stock = response[0].stock;
                var rate = response[0].rate;
                $("#stock-" + id).val(stock);
                $("#rate-" + id).val(rate);
            }
        }, 'json'
    )

}

function addRowInWorkOrder() {
    var rows = $("#taxTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
                result += '<td style="width: 430px;"><select required id="productCode-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="description[]" style="width: 420px !important;" onchange="displayStockForAdditionalRows(' + slno + ')" class="form-control select2-offscreen addProduct-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productName = response[i].name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productName + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 202px;padding-right: 10px;"> <input type="text" readonly id="stock-' + slno + '" class="form-control"> </td> ';
                result += '<td style="width: 216px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" name="quantity[]" id="quantity-' + slno + '" class="form-control"> </td> ';
                result += '<td><button type="button" class="btn green" onclick="addRowInWorkOrder();">Add Row</button></td><td><button type="button" class="btn default" onclick="deleteRowInWorkOrder();">Delete Row</button></td></tr>';
                $("#taxTable").append(result);
                $(".addProduct-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

function addMiscellaneousRowInWorkOrder() {
    var rows = $("#miscellaneuosTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;
    var content = '<tr><td style="width: 60px;"></td><td style="width: 430px;"><input type="text" name="description[]" placeholder="miscellaneous item" class="form-control" value=""></td><td style="width: 200px;padding-right: 10px;"><input type="text" readonly class="form-control" value="===================="></td><td style="width: 216px;padding-right: 10px;"><input type="text" onkeypress="return isNumber(event);" name="quantity[]" class="form-control" value=""></td><td><button type="button" class="btn green" onclick="addMiscellaneousRowInWorkOrder();">Add Row</button></td><td><button type="button" class="btn default" onclick="deleteRowInWorkOrder();">Delete Row</button></td></tr>';
    $("#miscellaneuosTable").append(content);
}

function deleteRowInWorkOrder() {
    $("#taxTable tr:last").remove();
}

function deleteMiscellaneousRowInWorkOrder() {
    $("#miscellaneuosTable tr:last").remove();
}

function calculationInWorkOrder(id) {
    var total = '';
    var totalAmount = '';
    var quantity = $("#quantity-" + id).val();
    quantity = parseInt(quantity);
    var rate = $("#rate-" + id).val();
    if (!isNaN(quantity)) {
        if (!isNaN(rate)) {
            totalAmount = parseFloat(rate) * parseFloat(quantity);
        }
    }
    if (!isNaN(parseFloat(totalAmount))) {
        totalAmount = totalAmount.toFixed(2);
        $("#amount-" + id).val(totalAmount);
    }
    calculateTotalInWorkOrder();
}

function calculateTotalInWorkOrder() {
    var table = document.getElementById("taxTable");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var a = document.getElementsByName("amount[]")[i].value;
        if (!isNaN(parseFloat(a))) {
            totamt1 = totamt1 + parseFloat(a);
        }
    }
    document.getElementById("totamt").value = totamt1.toFixed(2);
}

function validateOrderData() {
    var date = $("#date").val();
    var clientName = $("#cname").val();
    var number = $("#number").val();

    if (date == '') {
        smoke.alert("Please select date");
        return false;
    }
    if (clientName == '') {
        smoke.alert("Please enter client name");
        return false;
    }
    if (number == '') {
        smoke.alert("Please enter mobile number");
        return false;
    }

    var table = document.getElementById("taxTable");
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var txtThird = document.getElementsByName('quantity[]')[i].value;
        //var txtThird2 = document.getElementsByName('rate[]')[i].value;

        var enteredStock = document.getElementsByName('quantity[]')[i].value;
        //var availablStock = document.getElementsByName('stock[]')[i].value;
        //enteredStock = parseInt(enteredStock);
        //availablStock = parseInt(availablStock);

        //if (enteredStock > availablStock) {
        //    smoke.alert("Qauntity cannot be greather than available stock");
        //    return false;
        //}
        if (txtThird == '') {
            smoke.alert("Please Enter Quantity");
            return false;
        }
        //else if (txtThird2 == '') {
        //    smoke.alert("Please Enter Rate");
        //    return false;
        //}
    }
    if (date != '' && clientName != '' && number != '') {
        $("#submit").prop("type", "submit");
    }
}

function validateOrderList() {
    var invoiceNumber = $("#selectTaxNumber").val();

    if (invoiceNumber == '') {
        smoke.alert("Please select order number");
        return false;
    }

    if (invoiceNumber != '') {
        $("#submit").prop("type", "submit");
    }
}

/*  Work Order functions end here*/

/*  Purchase Order functions start here*/


function addRowInPurchaseOrderBO() {
    var rows = $("#taxTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
                result += '<td style="width: 530px;"><select required id="productCode-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="description[]" style="width: 530px !important;"  class="form-control select2-offscreen addProduct-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productName = response[i].name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productName + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 300px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" name="quantity[]" id="quantity-' + slno + '" class="form-control"></td> ';
                result += '<td style="width: 200px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInWorkOrder(' + slno + ');" onkeypress="return isNumber1(event);" name="rate[]" class="form-control" id="rate-' + slno + '"> ';
                result += '</td> <td style="width: 200px;;"> <input type="text" readonly name="amount[]" id="amount-' + slno + '" class="form-control"> </td> </tr>';
                $("#taxTable").append(result);
                $(".addProduct-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')


    //var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
    //result += '<td style="width: 530px;"><input type="text" name="item[]" id="item-' + slno + '" class="form-control">';
    //result += '</td> ';
    //result += '<td style="width: 300px;padding-right: 10px;"><input type="text" required onkeypress="return isNumber(event);" name="quantity[]" id="quantity-' + slno + '" class="form-control"></td></tr>';
    //$("#taxTable").append(result);
    //$("#counter").val(slno);
}


function addRowInPurchaseOrder() {
    var rows = $("#taxTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
                result += '<td style="width: 530px;"><select required id="productCode-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="description[]" style="width: 530px !important;"  class="form-control select2-offscreen addProduct-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productName = response[i].name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productName + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 300px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" name="quantity[]" id="quantity-' + slno + '" class="form-control"></td> ';
                $("#taxTable").append(result);
                $(".addProduct-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')


    //var result = '<tr> <td style="width: 6px;padding-top: 16px;">' + slno + '</td> ';
    //result += '<td style="width: 530px;"><input type="text" name="item[]" id="item-' + slno + '" class="form-control">';
    //result += '</td> ';
    //result += '<td style="width: 300px;padding-right: 10px;"><input type="text" required onkeypress="return isNumber(event);" name="quantity[]" id="quantity-' + slno + '" class="form-control"></td></tr>';
    //$("#taxTable").append(result);
    //$("#counter").val(slno);
}

function validatePurchaseOrderData() {
    var date = $("#date").val();
    var clientName = $("#cname").val();
    var number = $("#number").val();

    if (date == '') {
        smoke.alert("Please select date");
        return false;
    }
    if (clientName == '') {
        smoke.alert("Please enter vendor name");
        return false;
    }
    if (number == '') {
        smoke.alert("Please enter mobile number");
        return false;
    }

    var table = document.getElementById("taxTable");
    var rowCount = table.rows.length;

    if (date != '' && clientName != '' && number != '') {
        $("#submit").prop("type", "submit");
    }
}

/*  Purchase Order functions end here*/

/*  Simple Tax Invoice functions start here*/

function addRowInTaxInvoiceSimple() {
    var rows = $("#taxTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;"><strong>' + slno + '</strong></td> ';
                result += '<td style="width: 430px;"><select required id="productCode-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="description[]" style="width: 420px !important;" onchange="displayStockForAdditionalRows(' + slno + ')" class="form-control select2-offscreen addProduct-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productName = response[i].name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productName + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 216px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" name="quantity[]" id="quantity-' + slno + '" class="form-control" onkeyup="calculationInWorkOrder(' + slno + ');"> </td> ';
                result += '<td style="width: 200px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInWorkOrder(' + slno + ');" onkeypress="return isNumber1(event);" name="rate[]" class="form-control" id="rate-' + slno + '"> ';
                result += '</td> <td style="width: 200px;;"> <input type="text" readonly name="amount[]" id="amount-' + slno + '" class="form-control"> </td> </tr>';
                $("#taxTable").append(result);
                $(".addProduct-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

function addRowInTaxInvoice() {
    var rows = $("#taxTable tr").length;
    rows = rows - 1;
    var slno = parseInt(rows) + 2;

    $.post(baseUrl + '/site/getproducts', {}, function (response) {
            if (response) {
                var length = response.length;
                var result = '<tr> <td style="width: 6px;padding-top: 16px;"><strong>' + slno + '</strong></td> ';
                result += '<td style="width: 430px;"><select required id="productCode-' + slno + '" data-placeholder="Select..." tabindex="-1"  name="description[]" style="width: 420px !important;" onchange="" class="form-control select2-offscreen addProduct-' + slno + '"><option value=""> </option>';
                for (var i = 0; i < length; i++) {
                    var productName = response[i].name;
                    var productId = response[i].id;
                    result += '<option value="' + productId + '">';
                    result += '' + productName + '</option>';
                }
                result += '</select></td> ';
                result += '<td style="width: 216px;padding-right: 10px;"> <input type="text" required onkeypress="return isNumber(event);" name="quantity[]" id="quantity-' + slno + '" class="form-control" onkeyup="calculationInTaxSimple(' + slno + ');"> </td> ';
                result += '<td style="width: 200px;padding-right: 10px;"> <input type="text" required onkeyup="calculationInTaxSimple(' + slno + ');" onkeypress="return isNumber1(event);" name="rate[]" class="form-control" id="rate-' + slno + '"> ';
                result += '</td> <td style="width: 200px;;"> <input type="text" readonly name="amount[]" id="amount-' + slno + '" class="form-control"> </td> </tr>';
                $("#taxTable").append(result);
                $(".addProduct-" + slno).select2();
                $("#counter").val(slno);
            }
        },
        'json')
}

function validateSimpleTaxData() {
    var date = $("#date").val();
    var clientName = $("#cname").val();
    var number = $("#number").val();

    if (date == '') {
        smoke.alert("Please select date");
        return false;
    }
    if (clientName == '') {
        smoke.alert("Please enter client name");
        return false;
    }
    if (number == '') {
        smoke.alert("Please enter mobile number");
        return false;
    }

    var table = document.getElementById("taxTable");
    var rowCount = table.rows.length;

    for (var i = 0; i < rowCount; i++) {
        var txtThird = document.getElementsByName('quantity[]')[i].value;
        var txtThird2 = document.getElementsByName('rate[]')[i].value;

        if (txtThird == '') {
            smoke.alert("Please Enter Quantity");
            return false;
        }
        else if (txtThird2 == '') {
            smoke.alert("Please Enter Rate");
            return false;
        }
    }
    if (date != '' && clientName != '' && number != '') {
        $("#submit").prop("type", "submit");
    }
}

function calculationInTaxSimple(id) {

    var total = '';
    var totalAmount = '';
    var quantity = $("#quantity-" + id).val();
    quantity = parseInt(quantity);
    var rate = $("#rate-" + id).val();
    if (!isNaN(quantity)) {
        if (!isNaN(rate)) {
            totalAmount = parseFloat(rate) * parseFloat(quantity);
        }
    }
    if (!isNaN(parseFloat(totalAmount))) {
        totalAmount = totalAmount.toFixed(2);
        $("#amount-" + id).val(totalAmount);
    }
    calculateTotalInTaxSimple();
}

function calculateTotalInTaxSimple() {

    var table = document.getElementById("taxTable");
    var rowCount = table.rows.length;

    var totamt1 = 0;
    for (var i = 0; i < rowCount; i++) {
        var a = document.getElementsByName("amount[]")[i].value;
        if (!isNaN(parseFloat(a))) {
            totamt1 = totamt1 + parseFloat(a);
        }
    }
    document.getElementById("totamt").value = totamt1.toFixed(2);

    var vat = $("#selectVAT").val();
    vat = parseFloat(vat);

    var vatAmount = ((parseFloat(totamt1) * vat) / 100);
    vatAmount = parseFloat(vatAmount).toFixed(2);
    $("#vat").val(vatAmount);
    var grandTotal = parseFloat(totamt1) + parseFloat(vatAmount);
    if (!isNaN(grandTotal)) {
        grandTotal = parseFloat(grandTotal).toFixed(2);
        $("#grandtot").val(grandTotal);
    }

}

function validateModifyWorkOrderNumber() {
    var invoiceNumber = $("#selectTaxNumber").val();

    if (invoiceNumber == '') {
        smoke.alert("Please select order number");
        return false;
    }

    if (invoiceNumber != '') {
        $("#submit").prop("type", "submit");
    }
}

function addStock() {

    smoke.confirm("Are you sure you want to add returned stock? This can't be undone", function (e) {
        if (e) {
            var invoiceNumber = $("#invoiceNumber").val();
            //var invoice = $.param({invoice: invoiceNumber});
            //$http({
            //    url: baseUrl + '/site/addreturnedstock',
            //    method: 'POST',
            //    data: invoice,
            //    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            //}).success(function (response) {
            //    smoke.alert("Stock Updated");
            //    location.reload();
            //});
            $.ajax({
                url: baseUrl + '/site/addreturnedstock',
                type: 'POST',
                data: {
                    invoice: invoiceNumber
                },
                dataType: 'json',
                success: function (response) {
                    smoke.alert("Stock Updated");
                    location.reload();
                },
                error: function (request, error) {
                }
            });
        }
    }, {
        ok: "Yes",
        cancel: "No",
        classname: "custom-class",
        reverseButtons: true
    });
}


/*  Simple Tax Invoice functions end here*/




