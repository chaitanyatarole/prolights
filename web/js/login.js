/**
 * Created by chaitanya on 18/8/15.
 */
$(document).ready(function () {
    //getBillNotifications();
});

function getBillNotifications() {

    var baseUrl = $('#baseUrl').val();

    $.post(baseUrl + '/site/getpendingbills',
        function (response) {
            if (response) {
                console.log(response);
                var clientName = new Array();
                var number = new Array();
                var amount = new Array();
                var clientCode = new Array();
                var grandTotal = new Array();
                var combinedData = new Array();
                var length = response.length;
                for (var i = 0; i < length; i++) {
                    clientName[i] = response[i].client_name;
                    grandTotal[i] = response[i].grandTotal;
                    amount[i] = response[i].remainingAmount;
                    clientCode[i] = response[i].clientcode;
                    combinedData[i] = response[i].combinedData;
                    notifyUserAboutBill(clientName[i], number[i], amount[i], grandTotal[i], combinedData[i]);
                }
            }
        },
        'json'
    )
}


function notifyUserAboutBill(clientName, number, amount, grandTotal, combinedData) {

    var baseUrl = $('#baseUrl').val();

    var url = baseUrl + "/site/getreport?id=" + combinedData;


    var n = noty({
        text: 'Client Name: ' + clientName + '<br>' + 'Mobile Number: ' + '1234' + '<br>' + 'Total: ' + grandTotal + '<br>' + 'Pending Amount: ' + amount + "<br><a target='_blank' href='" + url + "'" +
        ">Click Here</a>",
        layout: 'topRight',
        type: 'success',
        animation: {
            open: {height: 'toggle'}, // jQuery animate function property object
            close: {height: 'toggle'}, // jQuery animate function property object
            easing: 'swing', // easing
            speed: 500 // opening & closing animation speed
        }
    });
}