<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $category_id
 * @property string $name
 * @property double $stock
 * @property string $date
 * @property string $status
 */
class BaseProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'name'], 'required'],
            [['stock'], 'number'],
            [['date'], 'safe'],
            [['category_id', 'name'], 'string', 'max' => 200],
            [['status'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'name' => 'Name',
            'stock' => 'Stock',
            'date' => 'Date',
            'status' => 'Status',
        ];
    }
}