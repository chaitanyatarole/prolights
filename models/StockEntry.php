<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 5/9/15
 * Time: 1:03 PM
 */

namespace app\models;


use app\commands\AppUtility;

class StockEntry extends BaseStockEntry
{
    public function addData($data)
    {
        date_default_timezone_set("Asia/Calcutta");
        $productCode = $data['productCode'];
        $query = "select max(entryNumber) as entryNumber from stockEntry";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $entryNumber = isset($result[0]['entryNumber']) ? $result[0]['entryNumber'] : 0;
        $entryNumber = $entryNumber + 1;
        foreach ($productCode as $key => $value) {
            $queryForName = "select name from product where uniqueID = '$value'";
            $nameList = \Yii::$app->db->createCommand($queryForName)->queryAll();
            $name = $nameList[0]['name'];
            $stockEntry = new StockEntry();
            $stockEntry->productID = $value;
            $stockEntry->productName = $name;
            $stockEntry->entryNumber = $entryNumber;
            $stockEntry->quantity = $data['productQuantity'][$key];
            $stockEntry->entryType = 'Fresh Entry';
            $stockEntry->date = date('Y-m-d');
            $stockEntry->time = date('H:i:s');
            $stockEntry->save();
        }

        return true;
    }

    public function getData($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from stockEntry where date BETWEEN '$fromDate' AND '$toDate'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

}