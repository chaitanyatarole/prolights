<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 19/10/15
 * Time: 11:53 AM
 */

namespace app\models;


class MaterialList extends BaseMaterialList
{
    public function getMaxInvoiceNumber()
    {
        $query = "select max(invoiceNumber) as invoiceNumber from materialList";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $challanNumber = isset($result[0]['invoiceNumber']) ? $result[0]['invoiceNumber'] : 0;
        $challanNumber = $challanNumber + 1;
        return $challanNumber;
    }

    public function getInvoiceNumbers()
    {
        $query = "select distinct(invoiceNumber) as invoiceNumber,clientName from materialList";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetails($data)
    {
        $invoiceNumber = $data['selectQuotationNumber'];
        $query = "select * from materialList where invoiceNumber = '$invoiceNumber'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function saveData($data)
    {
        $clientName = $data['cname'];
        $cliendCode = $data['clientcode'];
        $invoiceNumber = $data['invoiceNumber'];
        $date = $data['date'];
        $address = isset($data['address']) ? $data['address'] : '';
        $number = isset($data['number']) ? $data['number'] : '';
        $email = isset($data['email']) ? $data['email'] : '';
        $particular = $data['particular'];
        foreach ($particular as $key => $value) {
            $query = "select name from material where id = '$value'";
            $result = \Yii::$app->db->createCommand($query)->queryAll();
            $name = $result[0]['name'];
            $dimensionOne = isset($data['dimensionOne'][$key]) ? $data['dimensionOne'][$key] : '';
            $dimensionTwo = isset($data['dimensionTwo'][$key]) ? $data['dimensionTwo'][$key] : '';
            $quantity = $data['quantity'][$key];
            $persqft = $data['persqft'][$key];
            $rate = $data['rate'][$key];
            $amount = $data['amount'][$key];
            $totalAmount = $data['totamt'];
            $quotation = new MaterialList();
            $quotation->invoiceNumber = $invoiceNumber;
            $quotation->date = $date;
            $quotation->clientName = $clientName;
            $quotation->clientCode = $cliendCode;
            $quotation->address = $address;
            $quotation->email = $email;
            $quotation->mobile = $number;
            $quotation->particular = $value;
            $quotation->particularName = $name;
            $quotation->dimensionOne = $dimensionOne;
            $quotation->dimensionTwo = $dimensionTwo;
            $quotation->quantity = $quantity;
            $quotation->persqft = $persqft;
            $quotation->rate = $rate;
            $quotation->amount = $amount;
            $quotation->totalAmount = $totalAmount;
            $quotation->save();
        }
        return true;
    }

    public function updateData($data)
    {
        $clientName = $data['cname'];
        $cliendCode = $data['clientcode'];
        $invoiceNumber = $data['invoiceNumber'];
        $date = $data['date'];
        $address = isset($data['address']) ? $data['address'] : '';
        $number = isset($data['number']) ? $data['number'] : '';
        $email = isset($data['email']) ? $data['email'] : '';
        $particular = $data['particularID'];

        foreach ($particular as $key => $value) {
            $dimensionOne = isset($data['dimensionOne'][$key]) ? $data['dimensionOne'][$key] : '';
            $dimensionTwo = isset($data['dimensionTwo'][$key]) ? $data['dimensionTwo'][$key] : '';
            $quantity = $data['quantity'][$key];
            $persqft = $data['persqft'][$key];
            $rate = $data['rate'][$key];
            $amount = $data['amount'][$key];
            $totalAmount = $data['totamt'];

            $updateQuery = "update materialList set date = '$date',clientName = '$clientName', clientCode = '$cliendCode',address ='$address',mobile = '$number',
                            email = '$email', dimensionOne = '$dimensionOne',dimensionTwo = '$dimensionTwo', quantity = '$quantity', persqft = '$persqft',rate = '$rate',
                            amount = '$amount',totalAmount = '$totalAmount' WHERE particular = '$value' AND invoiceNumber = '$invoiceNumber' ";
            \Yii::$app->db->createCommand($updateQuery)->execute();
        }
        return true;
    }

    public function getData($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from materialList WHERE date BETWEEN '$fromDate' AND '$toDate'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

}