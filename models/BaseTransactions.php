<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transactions".
 *
 * @property integer $id
 * @property string $reason
 * @property double $amountAdded
 * @property double $amountDeducted
 * @property string $date
 * @property string $time
 */
class BaseTransactions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amountAdded', 'amountDeducted'], 'number'],
            [['date', 'time'], 'safe'],
            [['reason'], 'string', 'max' => 1000]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reason' => 'Reason',
            'amountAdded' => 'Amount Added',
            'amountDeducted' => 'Amount Deducted',
            'date' => 'Date',
            'time' => 'Time',
        ];
    }
}