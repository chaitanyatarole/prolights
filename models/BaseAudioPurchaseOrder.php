<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "audioPurchaseOrder".
 *
 * @property integer $id
 * @property string $date
 * @property string $returnDate
 * @property integer $purchase_order_no
 * @property integer $quotationNumber
 * @property string $eventName
 * @property string $client_name
 * @property string $clientcode
 * @property string $address
 * @property string $phone_no
 * @property string $email
 * @property string $particular
 * @property string $particularName
 * @property double $quantity
 * @property string $delivery_location
 * @property double $rate
 * @property double $amount
 * @property double $total_amount
 * @property string $status
 */
class BaseAudioPurchaseOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audioPurchaseOrder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'purchase_order_no', 'client_name', 'clientcode'], 'required'],
            [['date', 'returnDate'], 'safe'],
            [['purchase_order_no', 'quotationNumber'], 'integer'],
            [['quantity', 'rate', 'amount', 'total_amount'], 'number'],
            [['eventName', 'client_name', 'clientcode', 'email', 'status'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 500],
            [['phone_no'], 'string', 'max' => 200],
            [['particular', 'particularName', 'delivery_location'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'returnDate' => 'Return Date',
            'purchase_order_no' => 'Purchase Order No',
            'quotationNumber' => 'Quotation Number',
            'eventName' => 'Event Name',
            'client_name' => 'Client Name',
            'clientcode' => 'Clientcode',
            'address' => 'Address',
            'phone_no' => 'Phone No',
            'email' => 'Email',
            'particular' => 'Particular',
            'particularName' => 'Particular Name',
            'quantity' => 'Quantity',
            'delivery_location' => 'Delivery Location',
            'rate' => 'Rate',
            'amount' => 'Amount',
            'total_amount' => 'Total Amount',
            'status' => 'Status',
        ];
    }
}