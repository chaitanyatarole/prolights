<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 29/11/15
 * Time: 5:47 PM
 */

namespace app\models;


use app\commands\AppUtility;

class BalajiQuotation extends BaseBalajiQuotation
{
    public function getMaxQuotationNumber()
    {
        $query = "select max(quotationNumber) as quotationNumber from balajiQuotation";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $quotationNumber = isset($result[0]['quotationNumber']) ? $result[0]['quotationNumber'] : 0;
        $quotationNumber = $quotationNumber + 1;
        return $quotationNumber;
    }

    public function getData($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from balajiQuotation WHERE date BETWEEN '$fromDate' AND '$toDate'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function saveData($data)
    {
//        AppUtility::dump($data);
        $clientName = $data['cname'];
        $cliendCode = $data['clientcode'];
        $quotationNumber = $data['quotationNumber'];
        $date = $data['date'];
        $address = isset($data['address']) ? $data['address'] : '';
        $number = isset($data['number']) ? $data['number'] : '';
        $totalBudget = isset($data['totalBudget']) ? $data['totalBudget'] : '';
        $email = isset($data['email']) ? $data['email'] : '';
        $eventName = isset($data['eventName']) ? $data['eventName'] : '';
        $eventDate = isset($data['eventDate']) ? $data['eventDate'] : '';
        $eventLocation = isset($data['eventLocation']) ? $data['eventLocation'] : '';
        $particularStage = isset($data['particularStage']) ? $data['particularStage'] : '';
        $particularTent = isset($data['particularTent']) ? $data['particularTent'] : '';
        $particularLight = isset($data['particularLight']) ? $data['particularLight'] : '';
        $particularTech = isset($data['particularTech']) ? $data['particularTech'] : '';
        $particularSpc = isset($data['particularSpc']) ? $data['particularSpc'] : '';
        $particularOtr = isset($data['particularOtr']) ? $data['particularOtr'] : '';
        $particularExt = isset($data['particularExt']) ? $data['particularExt'] : '';


        $totalAmountStage = isset($data['totamtStage']) ? $data['totamtStage'] : 0;
        $totalAmountTent = isset($data['totamtTent']) ? $data['totamtTent'] : 0;
        $totalAmountLight = isset($data['totamtLight']) ? $data['totamtLight'] : 0;
        $totalAmountTech = isset($data['totamtTech']) ? $data['totamtTech'] : 0;
        $totalAmountSpc = isset($data['totamtSpc']) ? $data['totamtSpc'] : 0;
        $totalAmountOtr = isset($data['totamtOtr']) ? $data['totamtOtr'] : 0;
        $totalAmountExt = isset($data['totamtExt']) ? $data['totamtExt'] : 0;

//        $totalBudget = $totalAmountStage + $totalAmountTent + $totalAmountLight + $totalAmountTech + $totalAmountSpc + $totalAmountOtr + $totalAmountExt;


        if (!empty($particularStage)) {
            foreach ($particularStage as $key => $value) {
                $queryStage = "select name from product where id = '$value'";
                $resultStage = \Yii::$app->db->createCommand($queryStage)->queryAll();
                $name = $resultStage[0]['name'];
                $quantity = $data['quantityStage'][$key];
                $rate = $data['rateStage'][$key];
                $amount = $data['amountStage'][$key];
                $notes = $data['notesStage'][$key];
                $totalAmount = $data['totamtStage'];
                $quotationStage = new BalajiQuotation();
                $quotationStage->quotationNumber = $quotationNumber;
                $quotationStage->date = $date;
                $quotationStage->clientName = $clientName;
                $quotationStage->clientCode = $cliendCode;
                $quotationStage->address = $address;
                $quotationStage->email = $email;
                $quotationStage->mobile = $number;
                $quotationStage->eventName = $eventName;
                $quotationStage->eventLocation = $eventLocation;
                $quotationStage->eventDate = $eventDate;
                $quotationStage->totalBudget = $totalBudget;
                $quotationStage->notes = $notes;
                $quotationStage->category = $data['categoryStage'];
                $quotationStage->particular = $value;
                $quotationStage->particularName = $name;
                $quotationStage->quantity = $quantity;
                $quotationStage->rate = $rate;
                $quotationStage->amount = $amount;
                $quotationStage->totalAmount = $totalAmount;
                $quotationStage->save();
            }
        }

        if (!empty($particularTent)) {
            foreach ($particularTent as $key => $value) {
                $queryTent = "select name from product where id = '$value'";
                $resultTent = \Yii::$app->db->createCommand($queryTent)->queryAll();
                $name = $resultTent[0]['name'];
                $quantity = $data['quantityTent'][$key];
                $rate = $data['rateTent'][$key];
                $amount = $data['amountTent'][$key];
                $notes = $data['notesTent'][$key];
                $totalAmount = $data['totamtTent'];
                $quotationTent = new BalajiQuotation();
                $quotationTent->quotationNumber = $quotationNumber;
                $quotationTent->date = $date;
                $quotationTent->clientName = $clientName;
                $quotationTent->clientCode = $cliendCode;
                $quotationTent->address = $address;
                $quotationTent->email = $email;
                $quotationTent->mobile = $number;
                $quotationTent->eventName = $eventName;
                $quotationTent->eventDate = $eventDate;
                $quotationTent->eventLocation = $eventLocation;
                $quotationTent->totalBudget = $totalBudget;
                $quotationTent->notes = $notes;
                $quotationTent->category = $data['categoryTent'];
                $quotationTent->particular = $value;
                $quotationTent->particularName = $name;
                $quotationTent->quantity = $quantity;
                $quotationTent->rate = $rate;
                $quotationTent->amount = $amount;
                $quotationTent->totalAmount = $totalAmount;
                $quotationTent->save();
            }
        }

        if (!empty($particularLight)) {
            foreach ($particularLight as $key => $value) {
                $queryLight = "select name from product where id = '$value'";
                $resultLight = \Yii::$app->db->createCommand($queryLight)->queryAll();
                $name = $resultLight[0]['name'];
                $quantity = $data['quantityLight'][$key];
                $rate = $data['rateLight'][$key];
                $amount = $data['amountLight'][$key];
                $notes = $data['notesLight'][$key];
                $totalAmount = $data['totamtLight'];
                $quotationLight = new BalajiQuotation();
                $quotationLight->quotationNumber = $quotationNumber;
                $quotationLight->date = $date;
                $quotationLight->clientName = $clientName;
                $quotationLight->clientCode = $cliendCode;
                $quotationLight->address = $address;
                $quotationLight->email = $email;
                $quotationLight->mobile = $number;
                $quotationLight->eventName = $eventName;
                $quotationLight->eventDate = $eventDate;
                $quotationLight->eventLocation = $eventLocation;
                $quotationLight->totalBudget = $totalBudget;
                $quotationLight->notes = $notes;
                $quotationLight->category = $data['categoryLight'];
                $quotationLight->particular = $value;
                $quotationLight->particularName = $name;
                $quotationLight->quantity = $quantity;
                $quotationLight->rate = $rate;
                $quotationLight->amount = $amount;
                $quotationLight->totalAmount = $totalAmount;
                $quotationLight->save();
            }
        }

        if (!empty($particularTech)) {
            foreach ($particularTech as $key => $value) {
                $queryTech = "select name from product where id = '$value'";
                $resultTech = \Yii::$app->db->createCommand($queryTech)->queryAll();
                $name = $resultTech[0]['name'];
                $quantity = $data['quantityTech'][$key];
                $rate = $data['rateTech'][$key];
                $amount = $data['amountTech'][$key];
                $notes = $data['notesTech'][$key];
                $totalAmount = $data['totamtTech'];
                $quotationTech = new BalajiQuotation();
                $quotationTech->quotationNumber = $quotationNumber;
                $quotationTech->date = $date;
                $quotationTech->clientName = $clientName;
                $quotationTech->clientCode = $cliendCode;
                $quotationTech->address = $address;
                $quotationTech->email = $email;
                $quotationTech->mobile = $number;
                $quotationTech->eventName = $eventName;
                $quotationTech->eventDate = $eventDate;
                $quotationTech->eventLocation = $eventLocation;
                $quotationTech->totalBudget = $totalBudget;
                $quotationTech->notes = $notes;
                $quotationTech->category = $data['categoryTech'];
                $quotationTech->particular = $value;
                $quotationTech->particularName = $name;
                $quotationTech->quantity = $quantity;
                $quotationTech->rate = $rate;
                $quotationTech->amount = $amount;
                $quotationTech->totalAmount = $totalAmount;
                $quotationTech->save();
            }
        }

        if (!empty($particularSpc)) {
            foreach ($particularSpc as $key => $value) {
//                $querySpc = "select name from product where id = '$value'";
//                $resultSpc = \Yii::$app->db->createCommand($querySpc)->queryAll();
//                $name = $resultSpc[0]['name'];
                $quantity = $data['quantitySpc'][$key];
                $rate = $data['rateSpc'][$key];
                $amount = $data['amountSpc'][$key];
                $notes = $data['notesSpc'][$key];
                $totalAmount = $data['totamtSpc'];
                $quotationSpc = new BalajiQuotation();
                $quotationSpc->quotationNumber = $quotationNumber;
                $quotationSpc->date = $date;
                $quotationSpc->clientName = $clientName;
                $quotationSpc->clientCode = $cliendCode;
                $quotationSpc->address = $address;
                $quotationSpc->email = $email;
                $quotationSpc->mobile = $number;
                $quotationSpc->eventName = $eventName;
                $quotationSpc->eventDate = $eventDate;
                $quotationSpc->eventLocation = $eventLocation;
                $quotationSpc->totalBudget = $totalBudget;
                $quotationSpc->notes = $notes;
                $quotationSpc->category = $data['categorySpc'];
                $quotationSpc->particular = $value;
                $quotationSpc->particularName = $value;
                $quotationSpc->quantity = $quantity;
                $quotationSpc->rate = $rate;
                $quotationSpc->amount = $amount;
                $quotationSpc->totalAmount = $totalAmount;
                $quotationSpc->save();
            }
        }

        if (!empty($particularOtr)) {
            foreach ($particularOtr as $key => $value) {
//                $queryOtr = "select name from product where id = '$value'";
//                $resultOtr = \Yii::$app->db->createCommand($queryOtr)->queryAll();
//                $name = $resultOtr[0]['name'];
                $quantity = $data['quantityOtr'][$key];
                $rate = $data['rateOtr'][$key];
                $amount = $data['amountOtr'][$key];
                $notes = $data['notesOtr'][$key];
                $totalAmount = $data['totamtOtr'];
                $quotationOtr = new BalajiQuotation();
                $quotationOtr->quotationNumber = $quotationNumber;
                $quotationOtr->date = $date;
                $quotationOtr->clientName = $clientName;
                $quotationOtr->clientCode = $cliendCode;
                $quotationOtr->address = $address;
                $quotationOtr->email = $email;
                $quotationOtr->mobile = $number;
                $quotationOtr->eventName = $eventName;
                $quotationOtr->eventDate = $eventDate;
                $quotationOtr->eventLocation = $eventLocation;
                $quotationOtr->totalBudget = $totalBudget;
                $quotationOtr->notes = $notes;
                $quotationOtr->category = $data['categoryOther'];
                $quotationOtr->particular = $value;
                $quotationOtr->particularName = $value;
                $quotationOtr->quantity = $quantity;
                $quotationOtr->rate = $rate;
                $quotationOtr->amount = $amount;
                $quotationOtr->totalAmount = $totalAmount;
                $quotationOtr->save();
            }
        }

        if (!empty($particularExt)) {
            foreach ($particularExt as $key => $value) {
//                $queryExt = "select name from product where id = '$value'";
//                $resultExt = \Yii::$app->db->createCommand($queryExt)->queryAll();
//                $name = $resultExt[0]['name'];
                $quantity = $data['quantityExt'][$key];
                $rate = $data['rateExt'][$key];
                $amount = $data['amountExt'][$key];
                $notes = $data['notesExt'][$key];
                $totalAmount = $data['totamtExt'];
                $quotationExt = new BalajiQuotation();
                $quotationExt->quotationNumber = $quotationNumber;
                $quotationExt->date = $date;
                $quotationExt->clientName = $clientName;
                $quotationExt->clientCode = $cliendCode;
                $quotationExt->address = $address;
                $quotationExt->email = $email;
                $quotationExt->mobile = $number;
                $quotationExt->eventName = $eventName;
                $quotationExt->eventDate = $eventDate;
                $quotationExt->eventLocation = $eventLocation;
                $quotationExt->totalBudget = $totalBudget;
                $quotationExt->notes = $notes;
                $quotationExt->category = $data['categoryExt'];
                $quotationExt->particular = $value;
                $quotationExt->particularName = $value;
                $quotationExt->quantity = $quantity;
                $quotationExt->rate = $rate;
                $quotationExt->amount = $amount;
                $quotationExt->totalAmount = $totalAmount;
                $quotationExt->save();
            }
        }

//        AppUtility::dump($data);

        return true;
    }

    public function getQuotationNumbers()
    {
        $query = "select distinct(quotationNumber) as quotationNumber,clientName from balajiQuotation";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetails($data)
    {
        $invoiceNumber = $data['selectQuotationNumber'];
        $query = "select DISTINCT(category),clientName,quotationNumber,eventName,eventDate,totalBudget,date from balajiQuotation where quotationNumber = '$invoiceNumber'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetailsForModification($data)
    {
        $invoiceNumber = $data['selectQuotationNumber'];
        $query = "select * from balajiQuotation where quotationNumber = '$invoiceNumber'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function updateData($data)
    {
        $clientName = $data['cname'];
        $cliendCode = $data['clientcode'];
        $quotationNumber = $data['quotationNumber'];
        $date = $data['date'];
        $address = isset($data['address']) ? $data['address'] : '';
        $number = isset($data['number']) ? $data['number'] : '';
        $email = isset($data['email']) ? $data['email'] : '';
        $eventName = isset($data['eventName']) ? $data['eventName'] : '';
        $eventLocation = isset($data['eventLocation']) ? $data['eventLocation'] : '';
        $eventDate = isset($data['eventDate']) ? $data['eventDate'] : '';
        $totalBudget = isset($data['totalBudget']) ? $data['totalBudget'] : '';
        $particular = $data['particularID'];

        foreach ($particular as $key => $value) {
            $quantity = $data['quantity'][$key];
            $rate = $data['rate'][$key];
            $amount = $data['amount'][$key];
            $totalAmount = $data['totamt'];

            $updateQuery = "update balajiQuotation set date = '$date',eventName = '$eventName',eventLocation = '$eventLocation',eventDate = '$eventDate',totalBudget = '$totalBudget',clientName = '$clientName', clientCode = '$cliendCode',address ='$address',mobile = '$number',
                            email = '$email', quantity = '$quantity',rate = '$rate',
                            amount = '$amount',totalAmount = '$totalAmount' WHERE particular = '$value' AND quotationNumber = '$quotationNumber' ";
            \Yii::$app->db->createCommand($updateQuery)->execute();
        }
        return true;
    }

    public function getQuotationNumbersForWorkOrder()
    {
        $query = "select DISTINCT(quotationNumber) as quotationNumber,clientName from balajiQuotation where status = 'pending'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;

    }

    public function getDetailsForInvoice($data)
    {
        $invoice = $data['invoice'];
        $query = "select quantity,particular,particularName,eventName,totalBudget,clientName,clientCode,address,mobile,email,rate,amount,totalAmount from balajiQuotation WHERE quotationNumber = '$invoice' AND status = 'pending' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $quantity = $value['quantity'];
                $rate = $value['rate'];
                $amount = $value['amount'];
                $totalAmount = $value['totalAmount'];
                $particular = $value['particular'];
                $eventName = $value['eventName'];
                $totalBudget = $value['totalBudget'];
                $particularName = $value['particularName'];
                $clientName = $value['clientName'];
                $clientCode = $value['clientCode'];
                $address = $value['address'];
                $mobile = $value['mobile'];
                $email = $value['email'];

                $queryForAvailableStock = "select stock from product WHERE id='$particular'";
                $resultOfQuery = \Yii::$app->db->createCommand($queryForAvailableStock)->queryAll();
                if (!empty($resultOfQuery)) {
                    $availableStock = $resultOfQuery[0]['stock'];
                    $entireData[] = ['availableStock' => $availableStock, 'quantityInQuotation' => $quantity, 'client_name' => $clientName, 'clientcode' => $clientCode, 'phone_no' => $mobile, 'address' => $address, 'email' => $email, 'particularName' => $particularName, 'particular' => $particular, 'rate' => $rate, 'amount' => $amount, 'totalAmount' => $totalAmount, 'eventName' => $eventName, 'totalBudget' => $totalBudget];
                }
                if (empty($resultOfQuery)) {
                    $entireData[] = ['availableStock' => 'not related to stock', 'quantityInQuotation' => $quantity, 'client_name' => $clientName, 'clientcode' => $clientCode, 'phone_no' => $mobile, 'address' => $address, 'email' => $email, 'particularName' => $particularName, 'particular' => $particular, 'rate' => $rate, 'amount' => $amount, 'totalAmount' => $totalAmount, 'eventName' => $eventName, 'totalBudget' => $totalBudget];
                }
            }
        }
        return $entireData;
    }

}