<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 23/11/15
 * Time: 1:01 PM
 */

namespace app\models;


use app\commands\AppUtility;

class EmployeeWages extends BaseEmployeeWages
{
    public function getMaxOrderNumber()
    {
        $query = "SELECT recordNumber as invoice_no FROM employeeWages ORDER BY invoice_no DESC  ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $orderNumber = isset($result[0]['invoice_no']) ? $result[0]['invoice_no'] : '';
        $orderNumber = $orderNumber + 1;
        return $orderNumber;
    }

    public function saveData($data)
    {
        $query = "SELECT recordNumber as invoice_no FROM employeeWages ORDER BY invoice_no DESC  ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $orderNumber = isset($result[0]['invoice_no']) ? $result[0]['invoice_no'] : '';
        $orderNumber = $orderNumber + 1;

        $employeeName = isset($data['entireData']['employeeName']) ? $data['entireData']['employeeName'] : '';
        $employeeCode = isset($data['entireData']['employeeCode']) ? $data['entireData']['employeeCode'] : '';
        $address = isset($data['entireData']['address']) ? $data['entireData']['address'] : '';
        $mobileNumber = isset($data['entireData']['number']) ? $data['entireData']['number'] : '';
        $email = isset($data['entireData']['email']) ? $data['entireData']['email'] : '';
        $month = isset($data['entireData']['month']) ? $data['entireData']['month'] : '';
        $wage = isset($data['entireData']['wage']) ? $data['entireData']['wage'] : '';
        $remark = isset($data['entireData']['remark']) ? $data['entireData']['remark'] : '';

        $date = isset($data['entireData']['date']) ? $data['entireData']['date'] : '';
        $date = date('Y-m-d', strtotime($date));


        $wages = new EmployeeWages();
        $wages->recordNumber = $orderNumber;
        $wages->employeeId = $employeeCode;
        $wages->employeeName = $employeeName;
        $wages->address = $address;
        $wages->mobile = $mobileNumber;
        $wages->email = $email;
        $wages->wage = $wage;
        $wages->remark = $remark;
        $wages->wageForMonth = $month;
        $wages->wageReceivedOn = $date;
        $wages->save();

        date_default_timezone_set("Asia/Calcutta");
        $transaction = new Transactions();
        $transaction->reason = 'Employee Wage Amount Deducted';
        $transaction->amountAdded = 0;
        $transaction->amountDeducted = $wage;
        $transaction->date = date('Y-m-d');
        $transaction->time = date("H:i:s");
        $transaction->save();

        return true;
    }

    public function getPendingList()
    {
        $query = "select DISTINCT(invoice_no) as invoice_no,eventName from workOrder where status = 'pending'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getData($data)
    {
        $invoiceNumber = $data['selectQuotationNumber'];
        $query = "select * from workOrder WHERE invoice_no = '$invoiceNumber' AND status = 'pending'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function updateOrder($data)
    {
        AppUtility::dump($data);

    }

    public function acceptOrder($data)
    {
        $invoice = $data['invoice'];
        $query = "update workOrder set status = 'accepted' where invoice_no = '$invoice' ";
        \Yii::$app->db->createCommand($query)->execute();

        $queryToFetch = "select quantity,particular from workOrder WHERE invoice_no = '$invoice'";
        $result = \Yii::$app->db->createCommand($queryToFetch)->queryAll();

        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $id = $value['particular'];
                $quantity = $value['quantity'];

                $updateQuery = "update product set stock = stock - '$quantity' WHERE id = '$id' ";
                \Yii::$app->db->createCommand($updateQuery)->execute();
            }
        }

        return true;
    }

    public function rejectOrder($data)
    {
        $invoice = $data['invoice'];
        $query = "update workOrder set status = 'rejected' where invoice_no = '$invoice' ";
        \Yii::$app->db->createCommand($query)->execute();
        return true;
    }

    public function getAcceptedList()
    {
        $query = "select DISTINCT(invoice_no) as invoice_no,eventName from workOrder where status = 'accepted'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetails($data)
    {
        $invoice = $data['invoice'];
        $query = "select * from workOrder WHERE invoice_no = '$invoice' AND status = 'accepted' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function addReturnedStock($data)
    {
        $invoice = $data['invoice'];
        $query = "select * from workOrder WHERE invoice_no = '$invoice' AND status = 'accepted' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $id = $value['particular'];
                $quantity = $value['quantity'];

                $updateQuery = "update product set stock = stock + '$quantity' WHERE  id='$id'";
                \Yii::$app->db->createCommand($updateQuery)->execute();
            }
        }

        $updateWorkOrder = "update workOrder set status = 'completed' WHERE invoice_no = '$invoice'";
        \Yii::$app->db->createCommand($updateWorkOrder)->execute();

        return true;
    }

    public function getDataForReport($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from employeeWages WHERE wageReceivedOn BETWEEN '$fromDate' AND '$toDate'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDataOfEmployee($data)
    {
        $employeeCode = $data['employeeNameCode'];
        $query = "select * from employeeWages WHERE employeeId = '$employeeCode'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

}