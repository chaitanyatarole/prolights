<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employeeData".
 *
 * @property string $client_name
 * @property string $clientcode
 * @property string $ccode
 * @property integer $ccodeno
 * @property string $address
 * @property string $phone_no
 * @property string $email
 */
class BaseEmployeeData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employeeData';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_name', 'clientcode', 'ccode', 'ccodeno', 'address', 'phone_no'], 'required'],
            [['ccodeno'], 'integer'],
            [['client_name', 'clientcode', 'ccode', 'email'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 500],
            [['phone_no'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_name' => 'Client Name',
            'clientcode' => 'Clientcode',
            'ccode' => 'Ccode',
            'ccodeno' => 'Ccodeno',
            'address' => 'Address',
            'phone_no' => 'Phone No',
            'email' => 'Email',
        ];
    }
}