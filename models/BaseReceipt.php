<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "receipt".
 *
 * @property integer $id
 * @property integer $challanNumber
 * @property string $date
 * @property string $clientName
 * @property string $clientCode
 * @property string $address
 * @property string $email
 * @property string $mobile
 * @property string $particular
 * @property double $totalAmount
 * @property double $amount
 * @property double $balanceAmount
 */
class BaseReceipt extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'receipt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['challanNumber', 'date', 'clientName', 'clientCode', 'particular', 'totalAmount', 'amount', 'balanceAmount'], 'required'],
            [['challanNumber'], 'integer'],
            [['date'], 'safe'],
            [['address'], 'string'],
            [['totalAmount', 'amount', 'balanceAmount'], 'number'],
            [['clientName', 'clientCode', 'email', 'particular'], 'string', 'max' => 100],
            [['mobile'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'challanNumber' => 'Challan Number',
            'date' => 'Date',
            'clientName' => 'Client Name',
            'clientCode' => 'Client Code',
            'address' => 'Address',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'particular' => 'Particular',
            'totalAmount' => 'Total Amount',
            'amount' => 'Amount',
            'balanceAmount' => 'Balance Amount',
        ];
    }
}