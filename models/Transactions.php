<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 1/12/15
 * Time: 7:24 PM
 */

namespace app\models;


use app\commands\AppUtility;
use Faker\Provider\DateTime;

class Transactions extends BaseTransactions
{

    public function getAmountDeducted($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];

        $dates = array();
        $start = $current = strtotime($fromDate);
        $end = strtotime($toDate);

        while ($current <= $end) {
            $dates[] = date('d/m/Y', $current);
            $current = strtotime('+1 days', $current);
        }

        $query = "select amountDeducted from transactions WHERE date BETWEEN  '$fromDate' AND '$toDate' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $entireData = array();
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                array_push($entireData, (float)$value['amountDeducted']);
            }
        }

        $queryForAmountAdded = "select amountAdded from transactions WHERE date BETWEEN  '$fromDate' AND '$toDate'";
        $resultOfQuery = \Yii::$app->db->createCommand($queryForAmountAdded)->queryAll();
        $amountAddedArray = array();
        if (!empty($resultOfQuery)) {
            foreach ($resultOfQuery as $key => $value) {
                array_push($amountAddedArray, (float)$value['amountAdded']);
            }
        }

        $newArray = $entireData;
        $newAmountAddedArray = $amountAddedArray;
        $combinedData[] = ['amountDeducted' => $newArray, 'amountAdded' => $newAmountAddedArray, 'dates' => $dates];
        $combinedData = json_encode($combinedData);
        return $combinedData;
    }
}