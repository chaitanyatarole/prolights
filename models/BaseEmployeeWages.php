<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employeeWages".
 *
 * @property integer $id
 * @property integer $recordNumber
 * @property string $employeeId
 * @property string $employeeName
 * @property string $address
 * @property string $mobile
 * @property string $email
 * @property double $wage
 * @property string $remark
 * @property string $wageForMonth
 * @property string $wageReceivedOn
 * @property string $status
 */
class BaseEmployeeWages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employeeWages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recordNumber'], 'integer'],
            [['wage'], 'number'],
            [['wageReceivedOn'], 'safe'],
            [['employeeId', 'employeeName'], 'string', 'max' => 200],
            [['address', 'remark'], 'string', 'max' => 500],
            [['mobile'], 'string', 'max' => 20],
            [['email', 'wageForMonth', 'status'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'recordNumber' => 'Record Number',
            'employeeId' => 'Employee ID',
            'employeeName' => 'Employee Name',
            'address' => 'Address',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'wage' => 'Wage',
            'remark' => 'Remark',
            'wageForMonth' => 'Wage For Month',
            'wageReceivedOn' => 'Wage Received On',
            'status' => 'Status',
        ];
    }
}