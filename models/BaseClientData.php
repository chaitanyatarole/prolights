<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientdata".
 *
 * @property string $client_name
 * @property string $clientcode
 * @property string $ccode
 * @property integer $ccodeno
 * @property string $address
 * @property string $phone_no
 * @property string $email
 * @property string $vat_no
 * @property string $cst_no
 */
class BaseClientdata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clientdata';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_name', 'clientcode', 'ccode', 'ccodeno'], 'required'],
            [['ccodeno'], 'integer'],
            [['client_name', 'clientcode', 'ccode', 'email', 'vat_no', 'cst_no'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 500],
            [['phone_no'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_name' => 'Client Name',
            'clientcode' => 'Clientcode',
            'ccode' => 'Ccode',
            'ccodeno' => 'Ccodeno',
            'address' => 'Address',
            'phone_no' => 'Phone No',
            'email' => 'Email',
            'vat_no' => 'Vat No',
            'cst_no' => 'Cst No',
        ];
    }
}