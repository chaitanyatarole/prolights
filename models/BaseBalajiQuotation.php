<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "balajiQuotation".
 *
 * @property integer $id
 * @property integer $quotationNumber
 * @property string $date
 * @property string $clientName
 * @property string $clientCode
 * @property string $mobile
 * @property string $address
 * @property string $email
 * @property string $eventName
 * @property string $eventLocation
 * @property string $eventDate
 * @property string $notes
 * @property string $category
 * @property string $particular
 * @property string $particularName
 * @property double $quantity
 * @property double $rate
 * @property double $amount
 * @property double $totalAmount
 * @property double $totalBudget
 * @property double $availableBudget
 * @property string $status
 */
class BaseBalajiQuotation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balajiQuotation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quotationNumber', 'date', 'clientName', 'clientCode', 'particular'], 'required'],
            [['quotationNumber'], 'integer'],
            [['date', 'eventDate'], 'safe'],
            [['quantity', 'rate', 'amount', 'totalAmount', 'totalBudget', 'availableBudget'], 'number'],
            [['clientName', 'clientCode', 'email', 'particular', 'status'], 'string', 'max' => 100],
            [['mobile'], 'string', 'max' => 20],
            [['address'], 'string', 'max' => 600],
            [['eventName', 'eventLocation', 'notes'], 'string', 'max' => 500],
            [['category'], 'string', 'max' => 1000],
            [['particularName'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quotationNumber' => 'Quotation Number',
            'date' => 'Date',
            'clientName' => 'Client Name',
            'clientCode' => 'Client Code',
            'mobile' => 'Mobile',
            'address' => 'Address',
            'email' => 'Email',
            'eventName' => 'Event Name',
            'eventLocation' => 'Event Location',
            'eventDate' => 'Event Date',
            'notes' => 'Notes',
            'category' => 'Category',
            'particular' => 'Particular',
            'particularName' => 'Particular Name',
            'quantity' => 'Quantity',
            'rate' => 'Rate',
            'amount' => 'Amount',
            'totalAmount' => 'Total Amount',
            'totalBudget' => 'Total Budget',
            'availableBudget' => 'Available Budget',
            'status' => 'Status',
        ];
    }
}