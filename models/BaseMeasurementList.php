<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "measurementList".
 *
 * @property integer $id
 * @property integer $challanNumber
 * @property string $poNumber
 * @property string $yourChallanNumber
 * @property string $date
 * @property string $clientName
 * @property string $clientCode
 * @property string $address
 * @property string $mobile
 * @property string $particular
 * @property string $particularName
 * @property integer $dimensionOne
 * @property integer $dimensionTwo
 * @property integer $quantity
 * @property double $persqft
 * @property double $totalsqft
 * @property string $rft
 */
class BaseMeasurementList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'measurementList';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['challanNumber', 'date', 'clientName', 'clientCode', 'particular', 'particularName', 'quantity', 'persqft', 'totalsqft'], 'required'],
            [['challanNumber', 'dimensionOne', 'dimensionTwo', 'quantity'], 'integer'],
            [['date'], 'safe'],
            [['address'], 'string'],
            [['persqft', 'totalsqft'], 'number'],
            [['poNumber', 'yourChallanNumber', 'clientName', 'clientCode', 'particular', 'particularName'], 'string', 'max' => 100],
            [['mobile'], 'string', 'max' => 20],
            [['rft'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'challanNumber' => 'Challan Number',
            'poNumber' => 'Po Number',
            'yourChallanNumber' => 'Your Challan Number',
            'date' => 'Date',
            'clientName' => 'Client Name',
            'clientCode' => 'Client Code',
            'address' => 'Address',
            'mobile' => 'Mobile',
            'particular' => 'Particular',
            'particularName' => 'Particular Name',
            'dimensionOne' => 'Dimension One',
            'dimensionTwo' => 'Dimension Two',
            'quantity' => 'Quantity',
            'persqft' => 'Persqft',
            'totalsqft' => 'Totalsqft',
            'rft' => 'Rft',
        ];
    }
}
