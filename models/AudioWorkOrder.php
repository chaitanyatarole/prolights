<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 20/11/15
 * Time: 7:47 PM
 */

namespace app\models;


use app\commands\AppUtility;

class AudioWorkOrder extends BaseAudioWorkOrder
{
    public function getMaxOrderNumber()
    {
        $query = "SELECT invoice_no FROM audioWorkOrder ORDER BY invoice_no DESC  ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $orderNumber = isset($result[0]['invoice_no']) ? $result[0]['invoice_no'] : '';
        $orderNumber = $orderNumber + 1;
        return $orderNumber;
    }

    public function getDataForReport($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from audioWorkOrder WHERE date BETWEEN '$fromDate' AND '$toDate'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDataForStockReport($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from audioWorkOrder WHERE date BETWEEN '$fromDate' AND '$toDate' AND status = 'completed' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }


    public function saveData($data)
    {
        $taxNumber = $data['taxNumber'];
        $clientName = isset($data['cname']) ? $data['cname'] : '';
        $eventName = isset($data['eventName']) ? $data['eventName'] : '';
        $eventLocation = isset($data['eventLocation']) ? $data['eventLocation'] : '';
        $clientCode = isset($data['clientcode']) ? $data['clientcode'] : '';
        $clientAddress = isset($data['address']) ? $data['address'] : '';
        $clientMobileNumber = isset($data['number']) ? $data['number'] : '';
        $clientEmail = isset($data['email']) ? $data['email'] : '';
        $date = isset($data['date']) ? $data['date'] : '';
//        $totalBudget = isset($data['totalBudget']) ? $data['totalBudget'] : '';
//        $availableBudget = isset($data['availableBudget']) ? $data['availableBudget'] : '';

        $particular = $data['description'];

        foreach ($particular as $key => $value) {
            $particular = $value;
            $query = "select name from product where id = '$particular'";
            $name = \Yii::$app->db->createCommand($query)->queryAll();
            $productName = isset($name[0]['name']) ? $name[0]['name'] : '';

            $productQuantity = isset($data['quantity'][$key]) ? $data['quantity'][$key] : '';
            $productRate = isset($data['rate'][$key]) ? $data['rate'][$key] : '';
            $productAmount = isset($data['amount'][$key]) ? $data['amount'][$key] : '';
            $totalAmount = isset($data['totamt']) ? $data['totamt'] : '';

            $workOrder = new AudioWorkOrder();
            $workOrder->date = $date;
            $workOrder->invoice_no = $taxNumber;
            $workOrder->client_name = $clientName;
            $workOrder->clientcode = $clientCode;
            $workOrder->eventName = $eventName;
            $workOrder->eventLocation = $eventLocation;
            $workOrder->address = $clientAddress;
            $workOrder->phone_no = $clientMobileNumber;
            $workOrder->email = $clientEmail;
//            $workOrder->totalBudget = $totalBudget;
//            $workOrder->availableBudget = $availableBudget;
            if (empty($productName)) {
                $workOrder->particularName = $value;
            }
            if (!empty($productName)) {
                $workOrder->particularName = $productName;
                $updateQuery = "update product set stock = stock - '$productQuantity' WHERE id = '$particular' ";
                \Yii::$app->db->createCommand($updateQuery)->execute();
            }
            $workOrder->particular = $value;
            $workOrder->quantity = $productQuantity;
            $workOrder->rate = $productRate;
            $workOrder->amount = $productAmount;
            $workOrder->total_amount = $totalAmount;
            $workOrder->status = 'accepted';
            $workOrder->save();

        }

//        $quotationNumber = $data['quotationList'];
//        $queryForQuotation = "update audioQuotation set status = 'completed' where quotationNumber = '$quotationNumber' ";
//        \Yii::$app->db->createCommand($queryForQuotation)->execute();

        return true;
    }

    public function getPendingList()
    {
        $query = "select DISTINCT(invoice_no) as invoice_no,eventName from audioWorkOrder";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getData($data)
    {
        $invoiceNumber = $data['selectQuotationNumber'];
        $query = "select * from audioWorkOrder WHERE invoice_no = '$invoiceNumber'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function updateOrder($data)
    {
        AppUtility::dump($data);

    }

    public function acceptOrder($data)
    {
        $invoice = $data['invoice'];
        $query = "update audioWorkOrder set status = 'accepted' where invoice_no = '$invoice' ";
        \Yii::$app->db->createCommand($query)->execute();

        $queryToFetch = "select quantity,particular,total_amount from audioWorkOrder WHERE invoice_no = '$invoice'";
        $result = \Yii::$app->db->createCommand($queryToFetch)->queryAll();

        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $id = $value['particular'];
                $quantity = $value['quantity'];

                $bool = (!is_int($id) ? (ctype_digit($id)) : true);
                if (($bool)) {
                    $updateQuery = "update product set stock = stock - '$quantity' WHERE id = '$id' ";
                    \Yii::$app->db->createCommand($updateQuery)->execute();
                }
            }
        }

        return true;
    }

    public function rejectOrder($data)
    {
        $invoice = $data['invoice'];
        $query = "update audioWorkOrder set status = 'rejected' where invoice_no = '$invoice' ";
        \Yii::$app->db->createCommand($query)->execute();
        return true;
    }

    public function getAcceptedList()
    {
        $query = "select DISTINCT(invoice_no) as invoice_no,eventName from audioWorkOrder where status = 'accepted'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetails($data)
    {
        $invoice = $data['invoice'];
        $query = "select * from audioWorkOrder WHERE invoice_no = '$invoice' AND status = 'accepted' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetailsForInvoice($data)
    {
        $invoice = $data['invoice'];
        $query = "select * from audioWorkOrder WHERE invoice_no = '$invoice' AND status = 'completed' OR status = 'accepted' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function addReturnedStock($data)
    {
        $invoice = $data['invoice'];
        $query = "select * from audioWorkOrder WHERE invoice_no = '$invoice' AND status = 'accepted' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $id = $value['particular'];
                $quantity = $value['quantity'];

                $bool = (!is_int($id) ? (ctype_digit($id)) : true);
                if (($bool)) {
                    $updateQuery = "update product set stock = stock + '$quantity' WHERE id = '$id' ";
                    \Yii::$app->db->createCommand($updateQuery)->execute();
                }
            }
        }

        $updateWorkOrder = "update audioWorkOrder set status = 'completed' WHERE invoice_no = '$invoice'";
        \Yii::$app->db->createCommand($updateWorkOrder)->execute();

        return true;
    }

    public function updateData($data)
    {
//        AppUtility::dump($data);
        $clientName = $data['cname'];
        $cliendCode = $data['clientcode'];
        $quotationNumber = $data['quotationNumber'];
        $date = $data['date'];
        $address = isset($data['address']) ? $data['address'] : '';
        $eventName = isset($data['eventName']) ? $data['eventName'] : '';
        $eventLocation = isset($data['eventLocation']) ? $data['eventLocation'] : '';
//        $totalBudget = isset($data['totalBudget']) ? $data['totalBudget'] : '';
//        $availableBudget = isset($data['availableBudget']) ? $data['availableBudget'] : '';
        $number = isset($data['number']) ? $data['number'] : '';
        $email = isset($data['email']) ? $data['email'] : '';
        $particular = $data['particularID'];

        foreach ($particular as $key => $value) {
            $quantity = $data['quantity'][$key];
            $existingQuantity = $data['existingQuantity'][$key];
            $rowID = $data['rowID'][$key];
            $updateQuery = "update audioWorkOrder set date = '$date',client_name = '$clientName', clientcode = '$cliendCode',address ='$address',phone_no = '$number',
                            email = '$email',eventName = '$eventName',eventLocation='$eventLocation', quantity = '$quantity' WHERE particular = '$value' AND id = '$rowID' AND invoice_no = '$quotationNumber' ";
            \Yii::$app->db->createCommand($updateQuery)->execute();

            $removeExistingStockValues = "update product set stock = stock + '$existingQuantity' WHERE id = '$value'";
            \Yii::$app->db->createCommand($removeExistingStockValues)->execute();

            $updateStock = "update product set stock = stock - '$quantity' WHERE id = '$value'";
            \Yii::$app->db->createCommand($updateStock)->execute();

        }
        return true;
    }

    public function getCompletedOrders()
    {
        $query = "select DISTINCT(invoice_no) as invoice_no,eventName from audioWorkOrder where status = 'completed' OR status = 'accepted' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDataForBill($invoice)
    {
        $query = "select * from audioWorkOrder where invoice_no = '$invoice' AND status = 'accepted'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }


}