<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 1/12/15
 * Time: 12:52 PM
 */

namespace app\models;


class BalajiVendorStock extends BaseBalajiVendorStock
{
    public function getInStockOrders()
    {
        $query = "select DISTINCT(purchase_order_no) as purchase_order_no,vendorName from balajiVendorStock WHERE status = 'inStock' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetails($data)
    {
        $invoice = $data['invoice'];
        $query = "select * from balajiVendorStock WHERE purchase_order_no = '$invoice' AND status = 'inStock' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function clearOrder($data)
    {
        $purchaseOrder = $data['invoice'];
        $returnedItemsOn = isset($data['returningOn']) ? $data['returningOn'] : '';
        $date = date('Y-m-d', strtotime($returnedItemsOn));
        $query = "update balajiVendorStock set status = 'cleared',returnedItemsOn = '$date'  WHERE purchase_order_no='$purchaseOrder'";
        \Yii::$app->db->createCommand($query)->execute();

        $queryForPO = "update balajiPurchaseOrder set status = 'cleared' WHERE purchase_order_no='$purchaseOrder'";
        \Yii::$app->db->createCommand($queryForPO)->execute();

        $queryToUpdateProduct = "select particular,quantity from balajiVendorStock WHERE purchase_order_no='$purchaseOrder' ";
        $result = \Yii::$app->db->createCommand($queryToUpdateProduct)->queryAll();
        if (!empty($result)) {
            foreach ($result as $value) {
                $id = $value['particular'];
                $quantity = $value['quantity'];

                $updateQuery = "update product set status = 'old',stock = stock - '$quantity'  WHERE id='$id'";
                \Yii::$app->db->createCommand($updateQuery)->execute();
            }
        }
        return true;
    }

    public function getDataForReport($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from balajiPurchaseOrder WHERE date BETWEEN '$fromDate' AND '$toDate' AND status = 'inStock'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getClearedDataForReport($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from balajiVendorStock WHERE date BETWEEN '$fromDate' AND '$toDate' AND status = 'cleared'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }
}