<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 18/10/15
 * Time: 3:00 PM
 */

namespace app\models;


use app\commands\AppUtility;

class DeliveryChallan extends BaseDeliveryChallan
{

    public function getMaxDeliveryNumber()
    {
        $query = "select max(challanNumber) as challanNumber from deliveryChallan";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $challanNumber = isset($result[0]['challanNumber']) ? $result[0]['challanNumber'] : 0;
        $challanNumber = $challanNumber + 1;
        return $challanNumber;
    }

    public function saveData($data)
    {
        $clientName = $data['cname'];
        $cliendCode = $data['clientcode'];
        $challanNumber = isset($data['challanNumber']) ? $data['challanNumber'] : '';
        $yourChallanNumber = isset($data['yourChallanNumber']) ? $data['yourChallanNumber'] : '';
        $poNumber = isset($data['poNumber']) ? $data['poNumber'] : '';
        $date = $data['date'];
        $address = isset($data['address']) ? $data['address'] : '';
        $number = isset($data['number']) ? $data['number'] : '';
        $particular = $data['particular'];
        foreach ($particular as $key => $value) {
            $query = "select name from material where id = '$value'";
            $result = \Yii::$app->db->createCommand($query)->queryAll();
            $name = $result[0]['name'];
            $dimensionOne = isset($data['dimensionOne'][$key]) ? $data['dimensionOne'][$key] : '';
            $dimensionTwo = isset($data['dimensionTwo'][$key]) ? $data['dimensionTwo'][$key] : '';
            $quantity = $data['quantity'][$key];
            $persqft = $data['persqft'][$key];
            $totalsqft = $data['totalsqft'][$key];
            $delivery = new DeliveryChallan();
            $delivery->challanNumber = $challanNumber;
            $delivery->yourChallanNumber = $yourChallanNumber;
            $delivery->poNumber = $poNumber;
            $delivery->date = $date;
            $delivery->clientName = $clientName;
            $delivery->clientCode = $cliendCode;
            $delivery->address = $address;
            $delivery->mobile = $number;
            $delivery->particular = $value;
            $delivery->particularName = $name;
            $delivery->dimensionOne = $dimensionOne;
            $delivery->dimensionTwo = $dimensionTwo;
            $delivery->quantity = $quantity;
            $delivery->persqft = $persqft;
            $delivery->totalsqft = $totalsqft;
            $delivery->save();
        }
        return true;
    }

    public function getDeliveryNumbers()
    {
        $query = "select distinct(challanNumber) as challanNumber,clientName from deliveryChallan";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetails($data)
    {
        $invoiceNumber = $data['selectDeliveryNumber'];
        $query = "select * from deliveryChallan where challanNumber = '$invoiceNumber'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;

    }

    public function updateData($data)
    {
        $clientName = $data['cname'];
        $cliendCode = $data['clientcode'];
        $challanNumber = isset($data['challanNumber']) ? $data['challanNumber'] : '';
        $yourChallanNumber = isset($data['yourChallanNumber']) ? $data['yourChallanNumber'] : '';
        $poNumber = isset($data['poNumber']) ? $data['poNumber'] : '';
        $date = $data['date'];
        $address = isset($data['address']) ? $data['address'] : '';
        $number = isset($data['number']) ? $data['number'] : '';
        $particular = $data['particularID'];

        foreach ($particular as $key => $value) {
            $dimensionOne = isset($data['dimensionOne'][$key]) ? $data['dimensionOne'][$key] : '';
            $dimensionTwo = isset($data['dimensionTwo'][$key]) ? $data['dimensionTwo'][$key] : '';
            $quantity = $data['quantity'][$key];
            $persqft = $data['persqft'][$key];
            $totalsqft = $data['totalsqft'][$key];


            $updateQuery = "update deliveryChallan set date = '$date',poNumber = '$poNumber' ,yourChallanNumber = '$yourChallanNumber',clientName = '$clientName', clientCode = '$cliendCode',address ='$address',mobile = '$number',
                            dimensionOne = '$dimensionOne',dimensionTwo = '$dimensionTwo', quantity = '$quantity', persqft = '$persqft',totalsqft = '$totalsqft'
                            WHERE particular = '$value' AND challanNumber = '$challanNumber' ";
            \Yii::$app->db->createCommand($updateQuery)->execute();
        }
        return true;

    }

    public function getData($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from deliveryChallan WHERE date BETWEEN '$fromDate' AND '$toDate'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }
}