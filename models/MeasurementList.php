<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 18/10/15
 * Time: 4:59 PM
 */

namespace app\models;


class MeasurementList extends BaseMeasurementList
{

    public function getMaxInvoiceNumber()
    {
        $query = "select max(challanNumber) as challanNumber from measurementList";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $challanNumber = isset($result[0]['challanNumber']) ? $result[0]['challanNumber'] : 0;
        $challanNumber = $challanNumber + 1;
        return $challanNumber;
    }

    public function saveData($data)
    {
        $clientName = $data['cname'];
        $cliendCode = $data['clientcode'];
        $challanNumber = isset($data['challanNumber']) ? $data['challanNumber'] : '';
        $yourChallanNumber = isset($data['yourChallanNumber']) ? $data['yourChallanNumber'] : '';
        $poNumber = isset($data['poNumber']) ? $data['poNumber'] : '';
        $date = $data['date'];
        $address = isset($data['address']) ? $data['address'] : '';
        $number = isset($data['number']) ? $data['number'] : '';
        $particular = $data['particular'];
        foreach ($particular as $key => $value) {
            $query = "select name from material where id = '$value'";
            $result = \Yii::$app->db->createCommand($query)->queryAll();
            $name = $result[0]['name'];
            $dimensionOne = isset($data['dimensionOne'][$key]) ? $data['dimensionOne'][$key] : '';
            $dimensionTwo = isset($data['dimensionTwo'][$key]) ? $data['dimensionTwo'][$key] : '';
            $quantity = $data['quantity'][$key];
            $persqft = $data['persqft'][$key];
            $totalsqft = $data['totalsqft'][$key];
            $rft = $data['rft'][$key];
            $measurement = new MeasurementList();
            $measurement->challanNumber = $challanNumber;
            $measurement->yourChallanNumber = $yourChallanNumber;
            $measurement->poNumber = $poNumber;
            $measurement->date = $date;
            $measurement->clientName = $clientName;
            $measurement->clientCode = $cliendCode;
            $measurement->address = $address;
            $measurement->mobile = $number;
            $measurement->particular = $value;
            $measurement->particularName = $name;
            $measurement->dimensionOne = $dimensionOne;
            $measurement->dimensionTwo = $dimensionTwo;
            $measurement->quantity = $quantity;
            $measurement->persqft = $persqft;
            $measurement->totalsqft = $totalsqft;
            $measurement->rft = $rft;
            $measurement->save();
        }
        return true;

    }

    public function getDeliveryNumbers()
    {
        $query = "select distinct(challanNumber) as challanNumber,clientName from measurementList";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetails($data)
    {
        $invoiceNumber = $data['selectDeliveryNumber'];
        $query = "select * from measurementList where challanNumber = '$invoiceNumber'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function updateData($data)
    {
        $clientName = $data['cname'];
        $cliendCode = $data['clientcode'];
        $challanNumber = isset($data['challanNumber']) ? $data['challanNumber'] : '';
        $yourChallanNumber = isset($data['yourChallanNumber']) ? $data['yourChallanNumber'] : '';
        $poNumber = isset($data['poNumber']) ? $data['poNumber'] : '';
        $date = $data['date'];
        $address = isset($data['address']) ? $data['address'] : '';
        $number = isset($data['number']) ? $data['number'] : '';
        $particular = $data['particularID'];

        foreach ($particular as $key => $value) {
            $dimensionOne = isset($data['dimensionOne'][$key]) ? $data['dimensionOne'][$key] : '';
            $dimensionTwo = isset($data['dimensionTwo'][$key]) ? $data['dimensionTwo'][$key] : '';
            $quantity = $data['quantity'][$key];
            $persqft = $data['persqft'][$key];
            $totalsqft = $data['totalsqft'][$key];
            $rft = $data['rft'][$key];

            $updateQuery = "update measurementList set date = '$date',poNumber = '$poNumber' ,yourChallanNumber = '$yourChallanNumber',clientName = '$clientName', clientCode = '$cliendCode',address ='$address',mobile = '$number',
                            dimensionOne = '$dimensionOne',dimensionTwo = '$dimensionTwo', quantity = '$quantity', persqft = '$persqft',rft = '$rft',totalsqft = '$totalsqft'
                            WHERE particular = '$value' AND challanNumber = '$challanNumber' ";
            \Yii::$app->db->createCommand($updateQuery)->execute();
        }
        return true;

    }

}