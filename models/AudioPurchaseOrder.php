<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 21/11/15
 * Time: 4:47 PM
 */

namespace app\models;


use app\commands\AppUtility;

class AudioPurchaseOrder extends BaseAudioPurchaseOrder
{
    public function getMaxOrderNumber()
    {
        $query = "SELECT purchase_order_no FROM audioPurchaseOrder ORDER BY purchase_order_no DESC  ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $orderNumber = isset($result[0]['purchase_order_no']) ? $result[0]['purchase_order_no'] : '';
        $orderNumber = $orderNumber + 1;
        return $orderNumber;
    }

    public function getDataForReport($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from audioPurchaseOrder WHERE date BETWEEN '$fromDate' AND '$toDate'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDataForStockReport($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from audioPurchaseOrder WHERE date BETWEEN '$fromDate' AND '$toDate'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function savePurchaseOrder($data)
    {
        $taxNumber = $data['taxNumber'];
        $clientName = isset($data['cname']) ? $data['cname'] : '';
        $clientCode = isset($data['clientcode']) ? $data['clientcode'] : '';
        $clientAddress = isset($data['address']) ? $data['address'] : '';
        $clientMobileNumber = isset($data['number']) ? $data['number'] : '';
        $clientEmail = isset($data['email']) ? $data['email'] : '';
        $date = isset($data['date']) ? $data['date'] : '';
        $returnDate = isset($data['returnDate']) ? $data['returnDate'] : '';
        $quotationNameCode = isset($data['quotationNameCode']) ? $data['quotationNameCode'] : '';
        $eventName = isset($data['eventName']) ? $data['eventName'] : '';
        $deliveryLocation = isset($data['deliveryLocation']) ? $data['deliveryLocation'] : '';
        $totalAmount = isset($data['totamt']) ? $data['totamt'] : 0;

        $particular = $data['description'];

        foreach ($particular as $key => $value) {
            $particular = $value;
            $query = "select * from product where id = '$particular'";
            $name = \Yii::$app->db->createCommand($query)->queryAll();
            $productName = $name[0]['name'];

            $productQuantity = isset($data['quantity'][$key]) ? $data['quantity'][$key] : '';
            $productRate = isset($data['rate'][$key]) ? $data['rate'][$key] : 0;
            $productAmount = isset($data['amount'][$key]) ? $data['amount'][$key] : 0;

            $purchaseOrder = new AudioPurchaseOrder();
            $purchaseOrder->date = $date;
            $purchaseOrder->purchase_order_no = $taxNumber;
            $purchaseOrder->client_name = $clientName;
            $purchaseOrder->clientcode = $clientCode;
            $purchaseOrder->address = $clientAddress;
            $purchaseOrder->returnDate = $returnDate;
            $purchaseOrder->phone_no = $clientMobileNumber;
            $purchaseOrder->quotationNumber = $quotationNameCode;
            $purchaseOrder->eventName = $eventName;
            $purchaseOrder->email = $clientEmail;
            $purchaseOrder->delivery_location = $deliveryLocation;
            $purchaseOrder->particularName = $productName;
            $purchaseOrder->particular = $value;
            $purchaseOrder->quantity = $productQuantity;
            $purchaseOrder->rate = $productRate;
            $purchaseOrder->amount = $productAmount;
            $purchaseOrder->total_amount = $totalAmount;
            $purchaseOrder->save();
        }
        return true;
    }

    public function getPendingOrders()
    {
        $query = "select DISTINCT(purchase_order_no) as purchase_order_no,client_name from audioPurchaseOrder";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetails($data)
    {
        $purchaseOrderNumber = $data['selectQuotationNumber'];
        $query = "select * from audioPurchaseOrder WHERE purchase_order_no = '$purchaseOrderNumber' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function acceptOrder($data)
    {
        $purchaseOrder = $data['entireData']['purchaseOrder'];
        $total = isset($data['total']) ? $data['total'] : '';
        $particular = $data['particularArray'];
        $returnDate = isset($data['entireData']['returnDate']) ? $data['entireData']['returnDate'] : '';
        if (!empty($returnDate)) {
            $returnDate = date('Y-m-d', strtotime($returnDate));
        }

        foreach ($particular as $key => $value) {
            $quantity = isset($data['quantityArray'][$key]) ? $data['quantityArray'][$key] : '';
            $rate = isset($data['rateArray'][$key]) ? $data['rateArray'][$key] : '';
            $amount = isset($data['amountArray'][$key]) ? $data['amountArray'][$key] : '';
            $particularName = isset($data['particularNameArray'][$key]) ? $data['particularNameArray'][$key] : '';

            $query = "update audioPurchaseOrder set status = 'accepted',rate='$rate',amount='$amount',quantity='$quantity',total_amount='$total',returnDate ='$returnDate' where purchase_order_no = '$purchaseOrder' AND particular = '$value' ";
            \Yii::$app->db->createCommand($query)->execute();

            $updateQuery = "update product set stock = stock + '$quantity',status = 'old + vendor' WHERE id = '$value' ";
            \Yii::$app->db->createCommand($updateQuery)->execute();

            $vendorStock = new AudioVendorStock();
            $vendorStock->purchase_order_no = $purchaseOrder;
            $vendorStock->vendorId = isset($data['entireData']['clientCode']) ? $data['entireData']['clientCode'] : '';
            $vendorStock->vendorName = isset($data['entireData']['clientName']) ? $data['entireData']['clientName'] : '';
            $vendorStock->particular = $value;
            $vendorStock->particularName = $particularName;
            $vendorStock->quantity = $quantity;
            $vendorStock->date = isset($data['entireData']['date']) ? $data['entireData']['date'] : '';
            $vendorStock->returnDate = $returnDate;
            $vendorStock->save();
        }

        date_default_timezone_set("Asia/Calcutta");
        $transaction = new Transactions();
        $transaction->reason = 'Audio Pro Lights Purchase Order Accepted';
        $transaction->amountAdded = 0;
        $transaction->amountDeducted = $total;
        $transaction->date = date('Y-m-d');
        $transaction->time = date("H:i:s");
        $transaction->save();
        return true;
    }

    public function rejectOrder($data)
    {
        $invoice = $data['invoice'];
        $query = "update audioPurchaseOrder set status = 'rejected' where purchase_order_no = '$invoice' ";
        \Yii::$app->db->createCommand($query)->execute();
        return true;
    }

    public function updateData($data)
    {
        $clientName = $data['cname'];
        $cliendCode = $data['clientcode'];
//        $quotationNumber = $data['quotationNumber'];
        $date = $data['date'];
        $returnDate = isset($data['returnDate']) ? $data['returnDate'] : '';
        $address = isset($data['address']) ? $data['address'] : '';
        $number = isset($data['number']) ? $data['number'] : '';
        $quotationNumber = isset($data['quotationNumber']) ? $data['quotationNumber'] : '';
        $eventName = isset($data['eventName']) ? $data['eventName'] : '';
        $deliveryLocation = isset($data['deliveryLocation']) ? $data['deliveryLocation'] : '';
        $email = isset($data['email']) ? $data['email'] : '';
        $particular = $data['particularID'];
        $totalAmount = isset($data['totamt']) ? $data['totamt'] : 0;

        foreach ($particular as $key => $value) {

            $quantity = isset($data['quantity'][$key]) ? $data['quantity'][$key] : 0;
            $rate = isset($data['rate'][$key]) ? $data['rate'][$key] : 0;
            $amount = isset($data['amount'][$key]) ? $data['amount'][$key] : 0;


            $updateQuery = "update audioPurchaseOrder set date = '$date',returnDate = '$returnDate',quotationNumber= '$quotationNumber',eventName= '$eventName',delivery_location = '$deliveryLocation',client_name = '$clientName', clientcode = '$cliendCode',address ='$address',phone_no = '$number',
                            email = '$email', quantity = '$quantity',rate='$rate',amount= '$amount',total_amount = '$totalAmount' WHERE particular = '$value' AND purchase_order_no = '$quotationNumber' ";
            \Yii::$app->db->createCommand($updateQuery)->execute();
        }
        return true;
    }

}