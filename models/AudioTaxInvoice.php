<?php
/**  Sai Fabrication Model
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 19/8/15
 * Time: 10:45 AM
 */

namespace app\models;


use app\commands\AppUtility;

class AudioTaxInvoice extends BaseAudioTaxInvoice
{


    public function getData($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from audioTaxInvoice WHERE date BETWEEN '$fromDate' AND '$toDate' AND status <> 'cancelled' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getTaxNumbersForModification()
    {
        $query = "select DISTINCT (invoice_no),client_name from audioTaxInvoice WHERE taxType = '1' AND status <> 'cancelled'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getTaxNumbers()
    {
        $query = "select DISTINCT (invoice_no),client_name from audioTaxInvoice";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getSimpleTaxNumbersForModification()
    {
        $query = "select DISTINCT (invoice_no),client_name from audioTaxInvoice WHERE taxType = '0' AND status <> 'cancelled'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getSimpleTaxNumbers()
    {
        $query = "select DISTINCT (invoice_no),client_name from audioTaxInvoice WHERE taxType = '0'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getEntireNumbers()
    {
        $query = "select DISTINCT (invoice_no),client_name from audioTaxInvoice WHERE status <> 'cancelled'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;

    }

    public function getDetails($data)
    {
        $taxInvoiceNumber = $data['selectTaxNumber'];
        $query = "select * from audioTaxInvoice where invoice_no = '$taxInvoiceNumber'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;

    }

    public function changeStatus($data)
    {
        $taxInvoiceNumber = $data['taxNumber'];
        $query = "update audioTaxInvoice set status = 'cancelled' WHERE invoice_no = '$taxInvoiceNumber'";
        \Yii::$app->db->createCommand($query)->execute();


        $queryForPendingAmount = "update pendingAmount set status = 'cancelled' WHERE invoice_no = '$taxInvoiceNumber'";
        \Yii::$app->db->createCommand($queryForPendingAmount)->execute();

        $queryForPendingAmountReport = "update pendingAmountReport set status = 'cancelled' WHERE invoice_no = '$taxInvoiceNumber'";
        \Yii::$app->db->createCommand($queryForPendingAmountReport)->execute();
        return true;
    }

    public function modifyData($data)
    {
        $taxInvoiceNumber = $data['taxNumber'];

        $clientName = $data['cname'];
        $clientCode = $data['clientcode'];
        $clientAddress = $data['address'];
        $clientMobileNumber = $data['number'];
        $clientEmail = isset($data['email']) ? $data['email'] : '';
        $date = $data['date'];

        $challanNo = $data['challan_number'];
        $poNo = isset($data['ponumber']) ? $data['ponumber'] : '';
        $vehicalNo = isset($data['vehicalnumber']) ? $data['vehicalnumber'] : '';
        $vat_no = isset($data['vat_no']) ? $data['vat_no'] : '';
        $cst_no = isset($data['cst_no']) ? $data['cst_no'] : '';


        $totalAmount = $data['totamt'];
        $vatAmount = $data['vat'];
        $netAmount = $data['grandtot'];

        $paymentMode = isset($data['PaymentMode']) ? $data['PaymentMode'] : '';
        $creditNumber = isset($data['creditnumber']) ? $data['creditnumber'] : '';

        $particular = $data['perticular'];

        $paymentReceived = isset($data['paymentReceived']) ? $data['paymentReceived'] : '';
        $balanceAmount = isset($data['balanceAmount']) ? $data['balanceAmount'] : '';
        $amountClearanceDate = isset($data['amountClearanceDate']) ? $data['amountClearanceDate'] : '';

        foreach ($particular as $key => $value) {

            $dimensionOne = isset($data['dimensionOne'][$key]) ? $data['dimensionOne'][$key] : 0;
            $dimensionTwo = isset($data['dimensionTwo'][$key]) ? $data['dimensionTwo'][$key] : 0;
            $quantity = $data['quantity'][$key];
            $persqft = isset($data['persqft'][$key]) ? $data['persqft'][$key] : 0;
            $rate = $data['rate'][$key];
            $amount = $data['amount'][$key];

            $updateQuery = "update audioTaxInvoice set date = '$date',client_name='$clientName',clientcode = '$clientCode',
            address = '$clientAddress',phone_no = '$clientMobileNumber',email = '$clientEmail',challan_no = '$challanNo',
            purchase_orderno = '$poNo',vehical_no = '$vehicalNo' , payment_mode = '$paymentMode',credit_no = '$creditNumber',
            dimension_one = '$dimensionOne', dimension_two = '$dimensionTwo', quantity = '$quantity',vat_no = '$vat_no',cst_no = '$cst_no',
            per_squareft = '$persqft',rate = '$rate', amount = '$amount',total_amount = '$totalAmount',
            vat_tax_amount = '$vatAmount',net_amount = '$netAmount',paymentReceived = '$paymentReceived',balanceAmount = '$balanceAmount',clearanceDate = '$amountClearanceDate' where perticular = '$value' AND  invoice_no = '$taxInvoiceNumber' ";
            \Yii::$app->db->createCommand($updateQuery)->execute();
        }

        $query = "update pendingAmount set clearanceDate = '$amountClearanceDate',grandTotal = '$netAmount',paidAmount = '$paymentReceived',remainingAmount = '$balanceAmount' WHERE invoice_no = '$taxInvoiceNumber'";
        \Yii::$app->db->createCommand($query)->execute();

        $queryForReport = "DELETE  from pendingAmountReport where invoice_no = '$taxInvoiceNumber'";
        \Yii::$app->db->createCommand($queryForReport)->execute();

        $pendingReport = new PendingAmountReport();
        $pendingReport->date = $date;
        $pendingReport->clearanceDate = $amountClearanceDate;
        $pendingReport->invoice_no = $taxInvoiceNumber;
        $pendingReport->invoiceType = "Tax Invoice Part Payment";
        $pendingReport->client_name = $clientName;
        $pendingReport->clientcode = $clientCode;
        $pendingReport->number = $clientMobileNumber;
        $pendingReport->grandTotal = $netAmount;
        $pendingReport->paidAmount = $paymentReceived;
        $pendingReport->remainingAmount = $balanceAmount;
        $pendingReport->save();

        if ($vat_no != '' && $cst_no != '') {
            $updateQuery = "update clientdata set vat_no = '$vat_no',cst_no = '$cst_no' WHERE clientcode = '$clientCode'";
            \Yii::$app->db->createCommand($updateQuery)->execute();
        }

        return true;
    }


    public function getAmount($data)
    {
        $taxInvoiceNumber = $data['taxNumber'];
        $query = "select DISTINCT net_amount from audioTaxInvoice where invoice_no = '$taxInvoiceNumber'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();

        $grandTotal = $result[0]['net_amount'];

        $queryForRemainingAmount = "select remainingAmount from advance where invoice_no = '$taxInvoiceNumber' AND invoiceType = 'tax' ORDER BY id DESC limit 1";
        $resultOfQuery = \Yii::$app->db->createCommand($queryForRemainingAmount)->queryAll();

        $remainingAmount = isset($resultOfQuery[0]['remainingAmount']) ? $resultOfQuery[0]['remainingAmount'] : $grandTotal;

        $entireData = ['grandTotal' => $grandTotal, 'remainingAmount' => $remainingAmount];

        return $entireData;
    }

    public function getDescription()
    {
        $query = "select description from audioTaxInvoice";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    /* Below are the functions for Sai Fabrication */

    public function getMaxTaxNumber()
    {
        $query = "SELECT invoice_no FROM audioTaxInvoice ORDER BY invoice_no DESC  ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $taxNumber = isset($result[0]['invoice_no']) ? $result[0]['invoice_no'] : '';
        $taxNumber = $taxNumber + 1;
        return $taxNumber;
    }

    public function saveData($data)
    {
//        AppUtility::dump($data);
        $taxNumber = $data['taxNumber'];
        $clientName = $data['cname'];
        $clientCode = $data['clientcode'];
        $clientAddress = $data['address'];
        $clientMobileNumber = $data['number'];
        $clientEmail = $data['email'];
        $eventName = $data['eventName'];
        $eventLocation = $data['eventLocation'];
        $date = $data['date'];
        $particular = $data['particular'];
        $vat = $data['vat'];
        $vatPercent = $data['selectVAT'];
        $grandTotal = $data['grandtot'];


        foreach ($particular as $key => $value) {
            $particular = $value;
            $query = "select * from product where id = '$particular'";
            $name = \Yii::$app->db->createCommand($query)->queryAll();
            $productName = isset($name[0]['name']) ? $name[0]['name'] : '';

            $productQuantity = $data['quantity'][$key];
            $productRate = $data['rate'][$key];
            $productAmount = $data['amount'][$key];
            $totalAmount = $data['totamt'];


            $tax = new AudioTaxInvoice();
            $tax->date = $date;
            $tax->invoice_no = $taxNumber;
            $tax->client_name = $clientName;
            $tax->clientcode = $clientCode;
            $tax->address = $clientAddress;
            $tax->phone_no = $clientMobileNumber;
            $tax->email = $clientEmail;
            $tax->eventName = $eventName;
            $tax->eventLocation = $eventLocation;
            $tax->particularName = $value;
            $tax->particular = $value;
            $tax->quantity = $productQuantity;
            $tax->rate = $productRate;
            $tax->amount = $productAmount;
            $tax->totalAmount = $totalAmount;
            $tax->vat = $vat;
            $tax->vatPercent = $vatPercent;
            $tax->grandTotal = $grandTotal;
            $tax->save();
        }

        date_default_timezone_set("Asia/Calcutta");
        $transaction = new Transactions();
        $transaction->reason = 'Audio Pro Lights Invoice Accepted';
        $transaction->amountAdded = $data['grandtot'];
        $transaction->amountDeducted = 0;
        $transaction->date = date('Y-m-d');
        $transaction->time = date("H:i:s");
        $transaction->save();

        return true;
    }

    public function getCompletedInvoices()
    {
        $query = "select DISTINCT(invoice_no) as invoice_no,client_name from audioTaxInvoice WHERE status = 'active'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function cancelInvoice($data)
    {
        $invoiceNumber = $data['selectTaxNumber'];
        $query = "update audioTaxInvoice set status = 'cancelled' WHERE invoice_no = '$invoiceNumber' ";
        \Yii::$app->db->createCommand($query)->execute();
        return true;
    }

    public function getDataForReport($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from audioTaxInvoice WHERE date BETWEEN '$fromDate' AND '$toDate' AND status = 'active'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }
}