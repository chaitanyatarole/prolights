<?php
/**
 * Sai Fabrication Model
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 17/10/15
 * Time: 12:24 PM
 */

namespace app\models;


use app\commands\AppUtility;

class Material extends BaseMaterial
{

    public function saveMaterial($data)
    {
        $product = $data['product'];

        foreach ($product as $key => $value) {
            $dimensionOne = $data['dimensionOne'][$key];
            $dimensionTwo = $data['dimensionTwo'][$key];

            $material = new Material();
            $material->name = $value;
            $material->dimensionOne = $dimensionOne;
            $material->dimensionTwo = $dimensionTwo;
            $material->save();

        }
        return true;
    }

    public function getMaterials()
    {
        $query = "select * from material";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function updateMaterial($data)
    {
        $recordId = $data['recordId'];

        foreach ($recordId as $key => $value) {
            $product = $data['product'][$key];
            $dimensionOne = $data['dimensionOne'][$key];
            $dimensionTwo = $data['dimensionTwo'][$key];

            $query = "update material set name = '$product', dimensionOne = '$dimensionOne', dimensionTwo = '$dimensionTwo' WHERE id = '$value'";
            \Yii::$app->db->createCommand($query)->execute();
        }
        return true;
    }

    public function getDetails($data)
    {
        $id = $data['recordID'];
        $query = "select dimensionOne,dimensionTwo from material WHERE id='$id'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

}