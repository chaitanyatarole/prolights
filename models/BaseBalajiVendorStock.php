<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "balajiVendorStock".
 *
 * @property integer $id
 * @property integer $purchase_order_no
 * @property integer $particular
 * @property string $particularName
 * @property double $quantity
 * @property string $vendorId
 * @property string $vendorName
 * @property string $status
 * @property string $date
 * @property string $returnDate
 * @property string $returnedItemsOn
 */
class BaseBalajiVendorStock extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'balajiVendorStock';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['purchase_order_no', 'particular'], 'integer'],
            [['quantity'], 'number'],
            [['date', 'returnDate', 'returnedItemsOn'], 'safe'],
            [['particularName', 'vendorId', 'vendorName', 'status'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'purchase_order_no' => 'Purchase Order No',
            'particular' => 'Particular',
            'particularName' => 'Particular Name',
            'quantity' => 'Quantity',
            'vendorId' => 'Vendor ID',
            'vendorName' => 'Vendor Name',
            'status' => 'Status',
            'date' => 'Date',
            'returnDate' => 'Return Date',
            'returnedItemsOn' => 'Returned Items On',
        ];
    }
}