<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "labourbill".
 *
 * @property integer $id
 * @property string $date
 * @property integer $invoice_no
 * @property string $client_name
 * @property string $clientcode
 * @property string $address
 * @property string $phone_no
 * @property string $email
 * @property integer $perticular
 * @property string $perticular_name
 * @property integer $dimension_one
 * @property integer $dimension_two
 * @property integer $quantity
 * @property double $per_squareft
 * @property double $rate
 * @property double $amount
 * @property double $total_amount
 * @property integer $labourType
 */
class BaseLabourbill extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'labourbill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'invoice_no', 'client_name', 'clientcode', 'perticular', 'perticular_name', 'quantity', 'rate', 'amount', 'total_amount'], 'required'],
            [['date'], 'safe'],
            [['invoice_no', 'perticular', 'dimension_one', 'dimension_two', 'quantity', 'labourType'], 'integer'],
            [['per_squareft', 'rate', 'amount', 'total_amount'], 'number'],
            [['client_name', 'clientcode', 'email', 'perticular_name'], 'string', 'max' => 100],
            [['address'], 'string', 'max' => 500],
            [['phone_no'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'invoice_no' => 'Invoice No',
            'client_name' => 'Client Name',
            'clientcode' => 'Clientcode',
            'address' => 'Address',
            'phone_no' => 'Phone No',
            'email' => 'Email',
            'perticular' => 'Perticular',
            'perticular_name' => 'Perticular Name',
            'dimension_one' => 'Dimension One',
            'dimension_two' => 'Dimension Two',
            'quantity' => 'Quantity',
            'per_squareft' => 'Per Squareft',
            'rate' => 'Rate',
            'amount' => 'Amount',
            'total_amount' => 'Total Amount',
            'labourType' => 'Labour Type',
        ];
    }
}