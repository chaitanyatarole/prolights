<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 23/11/15
 * Time: 12:47 PM
 */

namespace app\models;


class EmployeeData extends BaseEmployeeData
{
    public function getClients()
    {
        $query = "SELECT client_name,clientcode FROM employeeData GROUP BY clientcode";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getMaxClientCode()
    {
        $query = "SELECT ccodeno FROM employeeData ORDER BY ccodeno DESC  ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $clientCode = isset($result[0]['ccodeno']) ? $result[0]['ccodeno'] : 0;
        $clientCode = $clientCode + 1;
        return $clientCode;
    }

    public function getDetails($data)
    {
        $clientCode = $data['clientCode'];
        $query = "SELECT * FROM employeeData where clientcode = '$clientCode'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function saveClient($data)
    {
        $clientName = isset($data['entireData']['employeeName']) ? $data['entireData']['employeeName'] : '';
        $clientCode = isset($data['entireData']['employeeCode']) ? $data['entireData']['employeeCode'] : '';
        $ccode = isset($data['entireData']['ccode']) ? $data['entireData']['ccode'] : '';
        $ccodeNumber = isset($data['entireData']['ccodeno']) ? $data['entireData']['ccodeno'] : '';
        $address = isset($data['entireData']['address']) ? $data['entireData']['address'] : '';
        $number = isset($data['entireData']['number']) ? $data['entireData']['number'] : '';
        $email = isset($data['entireData']['email']) ? $data['entireData']['email'] : '';

        $employeeData = new EmployeeData();
        $employeeData->client_name = $clientName;
        $employeeData->clientcode = $clientCode;
        $employeeData->ccode = $ccode;
        $employeeData->ccodeno = $ccodeNumber;
        $employeeData->address = $address;
        $employeeData->phone_no = $number;
        $employeeData->email = $email;
        $employeeData->save();
        return true;
    }

    public function saveData($data)
    {
        $employeeData = new EmployeeData();
        $employeeData->client_name = $data['cname'];
        $employeeData->clientcode = $data['clientcode'][0];
        $employeeData->ccode = $data['ccode'];
        $employeeData->ccodeno = $data['ccodeno'];
        $employeeData->address = $data['address'];
        $employeeData->phone_no = $data['number'];
        $employeeData->save();
        return true;
    }

    public function getNames()
    {
        $query = "SELECT DISTINCT(clientcode) as clientcode,client_name FROM employeeData";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }
}