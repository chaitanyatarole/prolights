<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "audioWorkOrder".
 *
 * @property integer $id
 * @property string $date
 * @property integer $invoice_no
 * @property integer $quotationNumber
 * @property string $client_name
 * @property string $eventName
 * @property string $eventLocation
 * @property string $clientcode
 * @property string $address
 * @property string $phone_no
 * @property string $email
 * @property double $totalBudget
 * @property double $availableBudget
 * @property string $particular
 * @property string $particularName
 * @property integer $quantity
 * @property double $rate
 * @property double $amount
 * @property double $total_amount
 * @property string $status
 */
class BaseAudioWorkOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audioWorkOrder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'invoice_no', 'clientcode', 'particular', 'quantity'], 'required'],
            [['date'], 'safe'],
            [['invoice_no', 'quotationNumber', 'quantity'], 'integer'],
            [['totalBudget', 'availableBudget', 'rate', 'amount', 'total_amount'], 'number'],
            [['client_name', 'clientcode', 'email', 'particular', 'particularName', 'status'], 'string', 'max' => 100],
            [['eventName', 'eventLocation', 'phone_no'], 'string', 'max' => 200],
            [['address'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'invoice_no' => 'Invoice No',
            'quotationNumber' => 'Quotation Number',
            'client_name' => 'Client Name',
            'eventName' => 'Event Name',
            'eventLocation' => 'Event Location',
            'clientcode' => 'Clientcode',
            'address' => 'Address',
            'phone_no' => 'Phone No',
            'email' => 'Email',
            'totalBudget' => 'Total Budget',
            'availableBudget' => 'Available Budget',
            'particular' => 'Particular',
            'particularName' => 'Particular Name',
            'quantity' => 'Quantity',
            'rate' => 'Rate',
            'amount' => 'Amount',
            'total_amount' => 'Total Amount',
            'status' => 'Status',
        ];
    }
}