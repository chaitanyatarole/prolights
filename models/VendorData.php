<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 21/11/15
 * Time: 4:23 PM
 */

namespace app\models;


class VendorData extends BaseVendorData
{
    public function getClients()
    {
        $query = "SELECT client_name,clientcode FROM vendorData GROUP BY clientcode";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getMaxClientCode()
    {
        $query = "SELECT ccodeno FROM vendorData ORDER BY ccodeno DESC  ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $clientCode = isset($result[0]['ccodeno']) ? $result[0]['ccodeno'] : 0;
        $clientCode = $clientCode + 1;
        return $clientCode;
    }

    public function getDetails($data)
    {
        $clientCode = $data['clientCode'];
        $query = "SELECT * FROM vendorData where clientcode = '$clientCode'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function saveClient($data)
    {
        $clientName = isset($data['cname']) ? $data['cname'] : '';
        $clientCode = isset($data['clientcode']) ? $data['clientcode'] : '';
        $ccode = isset($data['ccode']) ? $data['ccode'] : '';
        $ccodeNumber = isset($data['ccodeno']) ? $data['ccodeno'] : '';
        $address = isset($data['address']) ? $data['address'] : '';
        $number = isset($data['number']) ? $data['number'] : '';
        $email = isset($data['email']) ? $data['email'] : '';

        $vendorData = new VendorData();
        $vendorData->client_name = $clientName;
        $vendorData->clientcode = $clientCode;
        $vendorData->ccode = $ccode;
        $vendorData->ccodeno = $ccodeNumber;
        $vendorData->address = $address;
        $vendorData->phone_no = $number;
        $vendorData->email = $email;
        $vendorData->save();
        return true;
    }

    public function saveData($data)
    {
        $vendorData = new VendorData();
        $vendorData->client_name = $data['cname'];
        $vendorData->clientcode = $data['clientcode'][0];
        $vendorData->ccode = $data['ccode'];
        $vendorData->ccodeno = $data['ccodeno'];
        $vendorData->address = $data['address'];
        $vendorData->phone_no = $data['number'];
        $vendorData->save();
        return true;
    }

}