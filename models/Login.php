<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 17/8/15
 * Time: 11:40 PM
 */

namespace app\models;


use app\commands\AppUtility;

class Login extends BaseLogin
{
    public static function verifyUser($username, $password)
    {
        $query = "select username,password,role from login";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        foreach ($result as $key) {
            if ($key['username'] == $username && $key['password'] == $password) {
                $data = $key['role'];
                return $data;
            }
        }
        return null;
    }

}