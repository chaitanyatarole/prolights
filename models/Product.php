<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 19/8/15
 * Time: 2:57 PM
 */

namespace app\models;


use app\commands\AppUtility;

class Product extends BaseProduct
{
    /* Below are the methods for Sai Fabrication */

    public function saveData($data)
    {
        date_default_timezone_set("Asia/Calcutta");
        $product = $data['item'];
        foreach ($product as $value) {
            if (!empty($value)) {
                $product = new Product();
                $product->category_id = isset($data['category']) ? $data['category'] : '';
                $product->name = $value;
                $product->date = date('Y-m-d');
                $product->save();
            }
        }
        return true;
    }

    public function getProducts()
    {
        $query = "SELECT * FROM product";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function updateProduct($data)
    {
        $recordId = $data['recordId'];
        $category = $data['category'];
        foreach ($recordId as $key => $value) {
            $product = $data['name'][$key];
            if (!empty($product)) {
                $query = "update product set name = '$product' WHERE id = '$value' AND category_id='$category'";
                \Yii::$app->db->createCommand($query)->execute();
            }
        }
        return true;
    }

    public function getItems($data)
    {
        $category = $data['category'];
        $query = "select id,name from product where category_id = '$category'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getItemsWithStock()
    {
        $query = "select * from product where status = 'pending'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function updateStock($data)
    {
        $stock = $data['stock'];

        foreach ($stock as $key => $value) {
            if (!empty($value)) {
                $id = $data['id'][$key];
                $categoryId = $data['category_id'][$key];

                $query = "update product set stock = '$value',status = 'old' WHERE id='$id' AND category_id = '$categoryId'";
                \Yii::$app->db->createCommand($query)->execute();
            }
        }
        return true;
    }

    public function getStock($data)
    {
        $productCode = $data['productCode'];
        $query = "SELECT stock FROM product where id = '$productCode'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $entireData[] = ['stock' => isset($result[0]['stock']) ? $result[0]['stock'] : 0];
        return $entireData;
    }

    public function getCurrentStock()
    {
        $query = "SELECT name,stock FROM product";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;

    }

}