<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "audioTaxInvoice".
 *
 * @property integer $id
 * @property string $date
 * @property integer $invoice_no
 * @property string $client_name
 * @property string $clientcode
 * @property string $address
 * @property string $phone_no
 * @property string $email
 * @property string $eventName
 * @property string $eventLocation
 * @property string $particular
 * @property string $particularName
 * @property double $quantity
 * @property double $rate
 * @property double $amount
 * @property double $totalAmount
 * @property double $vat
 * @property double $vatPercent
 * @property double $grandTotal
 * @property string $status
 */
class BaseAudioTaxInvoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audioTaxInvoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'invoice_no', 'client_name', 'clientcode'], 'required'],
            [['date'], 'safe'],
            [['invoice_no'], 'integer'],
            [['quantity', 'rate', 'amount', 'totalAmount', 'vat', 'vatPercent', 'grandTotal'], 'number'],
            [['client_name', 'clientcode', 'email', 'particular', 'particularName', 'status'], 'string', 'max' => 100],
            [['address', 'eventName', 'eventLocation'], 'string', 'max' => 500],
            [['phone_no'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'invoice_no' => 'Invoice No',
            'client_name' => 'Client Name',
            'clientcode' => 'Clientcode',
            'address' => 'Address',
            'phone_no' => 'Phone No',
            'email' => 'Email',
            'eventName' => 'Event Name',
            'eventLocation' => 'Event Location',
            'particular' => 'Particular',
            'particularName' => 'Particular Name',
            'quantity' => 'Quantity',
            'rate' => 'Rate',
            'amount' => 'Amount',
            'totalAmount' => 'Total Amount',
            'vat' => 'Vat',
            'vatPercent' => 'Vat Percent',
            'grandTotal' => 'Grand Total',
            'status' => 'Status',
        ];
    }
}