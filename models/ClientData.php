<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 18/8/15
 * Time: 12:18 PM
 */

namespace app\models;


use app\commands\AppUtility;

class ClientData extends BaseClientData
{

    public function getClients()
    {
        $query = "SELECT client_name,clientcode FROM clientdata GROUP BY clientcode";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getMaxClientCode()
    {
        $query = "SELECT ccodeno FROM clientdata ORDER BY ccodeno DESC  ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $clientCode = isset($result[0]['ccodeno']) ? $result[0]['ccodeno'] : 0;
        $clientCode = $clientCode + 1;
        return $clientCode;
    }

    public function getDetails($data)
    {
        $clientCode = $data['clientCode'];
        $query = "SELECT * FROM clientdata where clientcode = '$clientCode'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function saveClient($data)
    {
        $clientName = isset($data['cname']) ? $data['cname'] : '';
        $clientCode = isset($data['clientcode']) ? $data['clientcode'] : '';
        $ccode = isset($data['ccode']) ? $data['ccode'] : '';
        $ccodeNumber = isset($data['ccodeno']) ? $data['ccodeno'] : '';
        $address = isset($data['address']) ? $data['address'] : '';
        $number = isset($data['number']) ? $data['number'] : '';
        $email = isset($data['email']) ? $data['email'] : '';
        $vat_no = isset($data['vat_no']) ? $data['vat_no'] : '';
        $cst_no = isset($data['cst_no']) ? $data['cst_no'] : '';

        $clientData = new ClientData();
        $clientData->client_name = $clientName;
        $clientData->clientcode = $clientCode;
        $clientData->ccode = $ccode;
        $clientData->ccodeno = $ccodeNumber;
        $clientData->address = $address;
        $clientData->phone_no = $number;
        $clientData->email = $email;
        $clientData->vat_no = $vat_no;
        $clientData->cst_no = $cst_no;
        $clientData->save();
        return true;
    }

    public function saveData($data)
    {
        $clientData = new ClientData();
        $clientData->client_name = $data['cname'];
        $clientData->clientcode = $data['clientcode'][0];
        $clientData->ccode = $data['ccode'];
        $clientData->ccodeno = $data['ccodeno'];
        $clientData->address = $data['address'];
        $clientData->phone_no = $data['number'];
        $clientData->save();
        return true;
    }

}