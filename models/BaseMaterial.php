<?php

namespace app\models;

use Yii;

/**     Sai Fabrication Model
 * This is the model class for table "material".
 *
 * @property integer $id
 * @property string $name
 * @property integer $dimensionOne
 * @property integer $dimensionTwo
 */
class BaseMaterial extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'material';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['dimensionOne', 'dimensionTwo'], 'integer'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'dimensionOne' => 'Dimension One',
            'dimensionTwo' => 'Dimension Two',
        ];
    }
}