<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 23/10/15
 * Time: 5:46 PM
 */

namespace app\models;


class Receipt extends BaseReceipt
{
    public function getMaxInvoiceNumber()
    {
        $query = "select max(challanNumber) as challanNumber from receipt";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $challanNumber = isset($result[0]['challanNumber']) ? $result[0]['challanNumber'] : 0;
        $challanNumber = $challanNumber + 1;
        return $challanNumber;
    }

    public function getTaxNumbers()
    {
        $query = "select DISTINCT (challanNumber),clientName from receipt";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getData($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from receipt WHERE date BETWEEN '$fromDate' AND '$toDate'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }


    public function getReceiptNumbers()
    {
        $query = "select DISTINCT (challanNumber),clientName from receipt";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function saveData($data)
    {
        $challanNumber = $data['challanNumber'];
        $clientName = $data['cname'];
        $clientCode = $data['clientcode'];
        $clientAddress = $data['address'];
        $clientMobileNumber = $data['number'];
        $clientEmail = $data['email'];
        $date = $data['date'];
        $perticular = $data['particular'];


        foreach ($perticular as $key => $value) {
            $perticular = $value;
            $query = "select * from product where id = '$perticular'";
            $name = \Yii::$app->db->createCommand($query)->queryAll();
            $productName = isset($name[0]['product_name']) ? $name[0]['product_name'] : '';
            if (!empty($productName)) {
                $amount = $data['amount'][$key];
                $totalAmount = $data['totalAmount'][$key];
                $balance = $data['balance'][$key];

                $receipt = new Receipt();
                $receipt->date = $date;
                $receipt->challanNumber = $challanNumber;
                $receipt->clientName = $clientName;
                $receipt->clientCode = $clientCode;
                $receipt->address = $clientAddress;
                $receipt->email = $clientEmail;
                $receipt->mobile = $clientMobileNumber;
                $receipt->particular = $productName;
                $receipt->totalAmount = $totalAmount;
                $receipt->amount = $amount;
                $receipt->balanceAmount = $balance;
                $receipt->save();
            }

        }
        return true;
    }

    public function getDetails($data)
    {
        $taxInvoiceNumber = $data['selectTaxNumber'];
        $query = "select * from receipt where challanNumber = '$taxInvoiceNumber'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function modifyData($data)
    {
        $challanNumber = $data['challanNumber'];
        $clientName = $data['cname'];
        $clientCode = $data['clientcode'];
        $clientAddress = $data['address'];
        $clientMobileNumber = $data['number'];
        $clientEmail = isset($data['email']) ? $data['email'] : '';
        $date = $data['date'];
        $particular = $data['particular'];

        foreach ($particular as $key => $value) {
            $amount = $data['amount'][$key];
            $totalAmount = $data['totalAmount'][$key];
            $balance = $data['balance'][$key];
            $updateQuery = "update receipt set date='$date',clientName='$clientName',clientCode='$clientCode',
            address='$clientAddress',mobile='$clientMobileNumber',email='$clientEmail',totalAmount = '$totalAmount',amount='$amount',balanceAmount ='$balance' WHERE particular = '$value' AND challanNumber = '$challanNumber' ";
            \Yii::$app->db->createCommand($updateQuery)->execute();
        }
        return true;
    }

}