<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 19/10/15
 * Time: 4:39 PM
 */

namespace app\models;


use app\commands\AppUtility;

class PendingAmount extends BasePendingAmount
{
    public function  getPendingNumbers()
    {
        $query = "select distinct(invoice_no) as invoice_no,client_name,clientcode from pendingAmount WHERE status <> 'cleared' AND status <> 'cancelled' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function  getPendingNumbersForReport()
    {
        $query = "select distinct(invoice_no) as invoice_no,client_name,clientcode from pendingAmount WHERE status <> 'cancelled' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetails($combinedData)
    {
        $invoiceNumber = $combinedData[0];
        $clientCode = $combinedData[1];

        $query = "select * from pendingAmount where invoice_no = '$invoiceNumber' AND clientcode = '$clientCode'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function updateAmount($data)
    {
        $combinedData = $data['selectInvoiceNumber'];
        $combinedData = explode(" ", $combinedData);
        $invoiceNumber = $combinedData[0];
        $clientCode = $combinedData[1];
        $receivedAmount = $data['receivedAmount'];
        $remainingAmount = $data['remainingAmount'];
        $paymentMode = isset($data['paymentMode']) ? $data['paymentMode'] : '';
        $date = isset($data['date']) ? $data['date'] : '';
        $referenceNumber = isset($data['creditnumber']) ? $data['creditnumber'] : '';
        $newRemainingAmount = $remainingAmount - $receivedAmount;
        if ($newRemainingAmount == 0) {
            $query = "update pendingAmount set remainingAmount = '$newRemainingAmount',paidAmount = paidAmount + '$receivedAmount' ,status = 'cleared' WHERE invoice_no = '$invoiceNumber' AND clientcode = '$clientCode'";
            \Yii::$app->db->createCommand($query)->execute();
        }
        if ($newRemainingAmount != 0 && $newRemainingAmount > 0) {
            $query = "update pendingAmount set remainingAmount = '$newRemainingAmount',paidAmount = paidAmount + '$receivedAmount' WHERE invoice_no = '$invoiceNumber' AND clientcode = '$clientCode'";
            \Yii::$app->db->createCommand($query)->execute();
        }

        if ($newRemainingAmount == 0) {
            $pendingAmountReport = new PendingAmountReport();
            $pendingAmountReport->date = $date;
            $pendingAmountReport->clearanceDate = $date;
            $pendingAmountReport->invoice_no = $invoiceNumber;
            $pendingAmountReport->invoiceType = 'Tax Invoice Part Payment';
            $pendingAmountReport->client_name = $data['clientName'];
            $pendingAmountReport->clientcode = $clientCode;
            $pendingAmountReport->paymentMode = $paymentMode;
            $pendingAmountReport->referenceNumber = $referenceNumber;
            $pendingAmountReport->grandTotal = $data['grandTotal'];
            $pendingAmountReport->paidAmount = $data['receivedAmount'];
            $pendingAmountReport->remainingAmount = $newRemainingAmount;
            $pendingAmountReport->status = 'cleared';
            $pendingAmountReport->save();
        } else {
            $pendingAmountReport = new PendingAmountReport();
            $pendingAmountReport->date = $date;
            $pendingAmountReport->clearanceDate = $date;
            $pendingAmountReport->invoice_no = $invoiceNumber;
            $pendingAmountReport->invoiceType = 'Tax Invoice Part Payment';
            $pendingAmountReport->client_name = $data['clientName'];
            $pendingAmountReport->clientcode = $clientCode;
            $pendingAmountReport->paymentMode = $paymentMode;
            $pendingAmountReport->referenceNumber = $referenceNumber;
            $pendingAmountReport->grandTotal = $data['grandTotal'];
            $pendingAmountReport->paidAmount = $data['receivedAmount'];
            $pendingAmountReport->remainingAmount = $newRemainingAmount;
            $pendingAmountReport->save();
        }
    }

    public function getPendingClientDetails()
    {
        $date = date('Y-m-d', strtotime('+10 days'));
        $query = "select client_name,clientcode,grandTotal,paidAmount,remainingAmount,CONCAT(invoice_no,' ',clientcode) as combinedData from pendingAmount where clearanceDate < '$date' AND status = 'pending'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }
}