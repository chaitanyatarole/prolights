<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 29/11/15
 * Time: 7:48 PM
 */

namespace app\models;


use app\commands\AppUtility;

class BalajiWorkOrder extends BaseBalajiWorkOrder
{
    public function getMaxOrderNumber()
    {
        $query = "SELECT invoice_no FROM balajiWorkOrder ORDER BY invoice_no DESC  ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $orderNumber = isset($result[0]['invoice_no']) ? $result[0]['invoice_no'] : '';
        $orderNumber = $orderNumber + 1;
        return $orderNumber;
    }

    public function getDataForReport($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from balajiWorkOrder WHERE date BETWEEN '$fromDate' AND '$toDate'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDataForStockReport($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from balajiWorkOrder WHERE date BETWEEN '$fromDate' AND '$toDate' AND status ='completed' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function saveData($data)
    {
//        AppUtility::dump($data);
        $taxNumber = $data['taxNumber'];
        $quotationNumber = $data['quotationList'];
        $clientName = isset($data['cname']) ? $data['cname'] : '';
        $eventName = isset($data['eventName']) ? $data['eventName'] : '';
        $eventLocation = isset($data['eventLocation']) ? $data['eventLocation'] : '';
        $clientCode = isset($data['clientcode']) ? $data['clientcode'] : '';
        $clientAddress = isset($data['address']) ? $data['address'] : '';
        $clientMobileNumber = isset($data['number']) ? $data['number'] : '';
        $clientEmail = isset($data['email']) ? $data['email'] : '';
        $date = isset($data['date']) ? $data['date'] : '';
        $totalBudget = isset($data['totalBudget']) ? $data['totalBudget'] : '';
        $availableBudget = isset($data['availableBudget']) ? $data['availableBudget'] : '';

        $particular = $data['description'];

        foreach ($particular as $key => $value) {

            if (!empty($value)) {

                $particular = $value;
                $query = "select * from product where id = '$particular'";
                $name = \Yii::$app->db->createCommand($query)->queryAll();
                $productName = isset($name[0]['name']) ? $name[0]['name'] : '';

                $productQuantity = isset($data['quantity'][$key]) ? $data['quantity'][$key] : '';
                $productRate = isset($data['rate'][$key]) ? $data['rate'][$key] : '';
                $productAmount = isset($data['amount'][$key]) ? $data['amount'][$key] : '';
                $totalAmount = isset($data['totalAmount'][$key]) ? $data['totalAmount'][$key] : '';

                $workOrder = new BalajiWorkOrder();
                $workOrder->date = $date;
                $workOrder->invoice_no = $taxNumber;
                $workOrder->quotationNumber = $quotationNumber;
                $workOrder->client_name = $clientName;
                $workOrder->clientcode = $clientCode;
                $workOrder->eventName = $eventName;
                $workOrder->eventLocation = $eventLocation;
                $workOrder->address = $clientAddress;
                $workOrder->phone_no = $clientMobileNumber;
                $workOrder->email = $clientEmail;
                $workOrder->totalBudget = $totalBudget;
                $workOrder->availableBudget = $availableBudget;
                if (empty($productName)) {
                    $workOrder->particularName = $value;
                }
                if (!empty($productName)) {
                    $workOrder->particularName = $productName;
                }
                $workOrder->particular = $value;
                $workOrder->quantity = $productQuantity;
                $workOrder->rate = $productRate;
                $workOrder->amount = $productAmount;
//                $workOrder->total_amount = $totalAmount;
                $workOrder->save();
            }
        }

        $quotationNumber = $data['quotationList'];
        $queryForQuotation = "update balajiQuotation set status = 'completed' where quotationNumber = '$quotationNumber' ";
        \Yii::$app->db->createCommand($queryForQuotation)->execute();
        return true;
    }

    public function getPendingList()
    {
        $query = "select DISTINCT(invoice_no) as invoice_no,eventName from balajiWorkOrder where status = 'pending'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getData($data)
    {
        $invoiceNumber = $data['selectQuotationNumber'];
        $query = "select * from balajiWorkOrder WHERE invoice_no = '$invoiceNumber' AND status = 'pending'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function updateOrder($data)
    {
        AppUtility::dump($data);

    }

    public function acceptOrder($data)
    {
        $invoice = $data['invoice'];
        $query = "update balajiWorkOrder set status = 'accepted' where invoice_no = '$invoice' ";
        \Yii::$app->db->createCommand($query)->execute();

        $queryToFetch = "select quantity,particular,total_amount from balajiWorkOrder WHERE invoice_no = '$invoice'";
        $result = \Yii::$app->db->createCommand($queryToFetch)->queryAll();

        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $id = $value['particular'];
                $quantity = $value['quantity'];

                $bool = (!is_int($id) ? (ctype_digit($id)) : true);
                if (($bool)) {
                    $updateQuery = "update product set stock = stock - '$quantity' WHERE id = '$id' ";
                    \Yii::$app->db->createCommand($updateQuery)->execute();
                }
            }
        }
        return true;
    }

    public function rejectOrder($data)
    {
        $invoice = $data['invoice'];
        $query = "update balajiWorkOrder set status = 'rejected' where invoice_no = '$invoice' ";
        \Yii::$app->db->createCommand($query)->execute();
        return true;
    }

    public function getAcceptedList()
    {
        $query = "select DISTINCT(invoice_no) as invoice_no,eventName from balajiWorkOrder where status = 'accepted'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetails($data)
    {
        $invoice = $data['invoice'];
        $query = "select * from balajiWorkOrder WHERE invoice_no = '$invoice' AND status = 'accepted' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetailsForInvoice($data)
    {
        $invoice = $data['invoice'];
        $query = "select quotationNumber,eventName,eventLocation,totalBudget,availableBudget from balajiWorkOrder WHERE invoice_no = '$invoice' AND status = 'completed' OR status = 'accepted' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $quotationNumber = $result[0]['quotationNumber'];
        $eventName = $result[0]['eventName'];
        $eventLocation = $result[0]['eventLocation'];
        $totalBudget = $result[0]['totalBudget'];
        $availableBudget = $result[0]['availableBudget'];
        $queryToGetDetails = "select particular,particularName,quantity,rate,amount,notes,clientName,clientCode,address,mobile,email from balajiQuotation where quotationNumber = '$quotationNumber'";
        $resultOfQuery = \Yii::$app->db->createCommand($queryToGetDetails)->queryAll();
        foreach ($resultOfQuery as $key => $value) {
            $particularName = $value['particularName'];
            $particular = $value['particular'];
            $quantity = $value['quantity'];
            $rate = $value['rate'];
            $clientName = $value['clientName'];
            $clientCode = $value['clientCode'];
            $address = $value['address'];
            $mobile = $value['mobile'];
            $email = $value['email'];
            $amount = $value['amount'];
            $notes = $value['notes'];
            $entireData[] = ['eventName' => $eventName, 'particular' => $particular, 'eventLocation' => $eventLocation, 'totalBudget' => $totalBudget, 'availableBudget' => $availableBudget, 'particularName' => $particularName, 'quantity' => $quantity, 'rate' => $rate, 'amount' => $amount, 'notes' => $notes, 'clientName' => $clientName, 'mobile' => $mobile, 'email' => $email, 'address' => $address, 'clientCode' => $clientCode];
        }
        return $entireData;
    }

    public function addReturnedStock($data)
    {
        $invoice = $data['invoice'];
        $query = "select * from balajiWorkOrder WHERE invoice_no = '$invoice' AND status = 'accepted' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $id = $value['particular'];
                $quantity = $value['quantity'];

                $bool = (!is_int($id) ? (ctype_digit($id)) : true);
                if (($bool)) {
                    $updateQuery = "update product set stock = stock + '$quantity' WHERE id = '$id' ";
                    \Yii::$app->db->createCommand($updateQuery)->execute();
                }
            }
        }

        $updateWorkOrder = "update balajiWorkOrder set status = 'completed' WHERE invoice_no = '$invoice'";
        \Yii::$app->db->createCommand($updateWorkOrder)->execute();

        return true;
    }

    public function updateData($data)
    {
        $clientName = $data['cname'];
        $cliendCode = $data['clientcode'];
        $quotationNumber = $data['quotationNumber'];
        $date = $data['date'];
        $address = isset($data['address']) ? $data['address'] : '';
        $eventName = isset($data['eventName']) ? $data['eventName'] : '';
        $totalBudget = isset($data['totalBudget']) ? $data['totalBudget'] : '';
        $availableBudget = isset($data['availableBudget']) ? $data['availableBudget'] : '';
        $eventLocation = isset($data['eventLocation']) ? $data['eventLocation'] : '';
        $number = isset($data['number']) ? $data['number'] : '';
        $email = isset($data['email']) ? $data['email'] : '';
        $particular = $data['particularID'];

        foreach ($particular as $key => $value) {
            $quantity = $data['quantity'][$key];
            $updateQuery = "update balajiWorkOrder set date = '$date',totalBudget='$totalBudget',availableBudget='$availableBudget',client_name = '$clientName', clientcode = '$cliendCode',address ='$address',phone_no = '$number',
                            email = '$email',eventName = '$eventName',eventLocation='$eventLocation', quantity = '$quantity'  WHERE particular = '$value' AND invoice_no = '$quotationNumber' ";
            \Yii::$app->db->createCommand($updateQuery)->execute();
        }
        return true;
    }

    public function getCompletedOrders()
    {
        $query = "select DISTINCT(invoice_no) as invoice_no,eventName from balajiWorkOrder where status = 'completed' OR status = 'accepted' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDataForBill($invoice)
    {
        $query = "select * from balajiWorkOrder where invoice_no = '$invoice' AND status = 'accepted'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }


}