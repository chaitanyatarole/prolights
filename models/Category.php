<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 19/8/15
 * Time: 12:11 PM
 */

namespace app\models;


use app\commands\AppUtility;

class Category extends BaseCategory
{
    public function getMaxID()
    {
        $query = "SELECT max(id) as id FROM category";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $maxId = isset($result[0]['id']) ? $result[0]['id'] : 0;
        return $maxId;
    }

    public function saveData($data)
    {
        $category = $data['category'];
        foreach ($category as $key => $value) {
            if (!empty($value)) {
                $category = new Category();
                $category->name = $value;
                $category->save();
            }

        }
        return true;
    }

    public function getCategoriesToModify()
    {
        $query = "select * from category";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getData()
    {
        $query = "SELECT * FROM category";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function modifyCategory($data)
    {
        $name = $data['name'];
        foreach ($name as $key => $value) {
            $recordID = $data['recordId'][$key];
            if (!empty($value)) {
                $query = "update category set name = '$value' WHERE id='$recordID'";
                \Yii::$app->db->createCommand($query)->execute();
            }
        }

        return true;
    }

}