<?php
/**
 * Created by PhpStorm.
 * User: chaitanya
 * Date: 18/10/15
 * Time: 8:33 PM
 */

namespace app\models;


use app\commands\AppUtility;

class LabourBill extends BaseLabourbill
{
    public function getMaxInvoiceNumber()
    {
        $query = "select max(invoice_no) as invoice_no from labourbill";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        $challanNumber = isset($result[0]['invoice_no']) ? $result[0]['invoice_no'] : 0;
        $challanNumber = $challanNumber + 1;
        return $challanNumber;
    }

    public function getEntireNumbers()
    {
        $query = "select DISTINCT (invoice_no),client_name from labourbill WHERE status <> 'cancelled'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;

    }

    public function changeStatus($data)
    {
        $taxInvoiceNumber = $data['taxNumber'];
        $query = "update labourbill set status = 'cancelled' WHERE invoice_no = '$taxInvoiceNumber'";
        \Yii::$app->db->createCommand($query)->execute();

        return true;
    }

    public function saveData($data, $labourType)
    {
        $taxNumber = $data['challan_number'];
        $clientName = $data['cname'];
        $clientCode = $data['clientcode'];
        $clientAddress = $data['address'];
        $clientMobileNumber = $data['number'];
        $clientEmail = $data['email'];
        $date = $data['date'];
        $perticular = $data['particular'];


        foreach ($perticular as $key => $value) {
            $perticular = $value;
            $query = "select * from product where id = '$perticular'";
            $name = \Yii::$app->db->createCommand($query)->queryAll();
            $productName = $name[0]['product_name'];

            $productQuantity = $data['quantity'][$key];
            $productRate = $data['rate'][$key];
            $productAmount = $data['amount'][$key];
            $dimOne = isset($data['dimensionOne'][$key]) ? $data['dimensionOne'][$key] : 0;
            $dimTwo = isset($data['dimensionTwo'][$key]) ? $data['dimensionTwo'][$key] : 0;
            $perSquareFt = isset($data['persqft'][$key]) ? $data['persqft'][$key] : 0;
            $totalAmount = $data['totamt'];


            $tax = new LabourBill();
            $tax->date = $date;
            $tax->invoice_no = $taxNumber;
            $tax->client_name = $clientName;
            $tax->clientcode = $clientCode;
            $tax->address = $clientAddress;
            $tax->phone_no = $clientMobileNumber;
            $tax->email = $clientEmail;
            $tax->dimension_one = $dimOne;
            $tax->dimension_two = $dimTwo;
            $tax->per_squareft = $perSquareFt;
            $tax->perticular_name = $productName;
            $tax->perticular = $value;
            $tax->quantity = $productQuantity;
            $tax->rate = $productRate;
            $tax->amount = $productAmount;
            $tax->total_amount = $totalAmount;
            $tax->labourType = $labourType;
            $tax->save();
        }
    }

    public function getTaxNo()
    {
        $query = "select DISTINCT (invoice_no),client_name from labourbill";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getInvoiceNumbersForModification()
    {
        $query = "select distinct(invoice_no) as invoice_no,client_name from labourbill WHERE labourType = '1' AND status <> 'cancelled' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getInvoiceNumbers()
    {
        $query = "select distinct(invoice_no) as invoice_no,client_name from labourbill WHERE labourType = '1'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getSimpleInvoiceNumbersForModification()
    {
        $query = "select distinct(invoice_no) as invoice_no,client_name from labourbill WHERE labourType = '0' AND status <> 'cancelled'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getSimpleInvoiceNumbers()
    {
        $query = "select distinct(invoice_no) as invoice_no,client_name from labourbill WHERE labourType = '0' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function getDetails($data)
    {
        $invoiceNumber = $data['selectDeliveryNumber'];

        $query = "select * from labourbill where invoice_no = '$invoiceNumber'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }

    public function updateData($data)
    {
        $taxNumber = $data['taxNumber'];
        $clientName = $data['cname'];
        $clientCode = $data['clientcode'];
        $clientAddress = $data['address'];
        $clientMobileNumber = $data['number'];
        $clientEmail = $data['email'];
        $date = $data['date'];
        $perticular = $data['perticular'];

        foreach ($perticular as $key => $value) {
            $amount = $data['amount'][$key];
            $productQuantity = $data['quantity'][$key];
            $productRate = $data['rate'][$key];
            $dimOne = isset($data['dimensionOne'][$key]) ? $data['dimensionOne'][$key] : 0;
            $dimTwo = isset($data['dimensionTwo'][$key]) ? $data['dimensionTwo'][$key] : 0;
            $perSquareFt = isset($data['persqft'][$key]) ? $data['persqft'][$key] : 0;
            $totalAmount = $data['totamt'];


            $updateQuery = "update labourbill set date = '$date',client_name = '$clientName', clientcode = '$clientCode',address ='$clientAddress',phone_no = '$clientMobileNumber',email='$clientEmail',
                            dimension_one = '$dimOne',dimension_two = '$dimTwo',quantity = '$productQuantity',per_squareft = '$perSquareFt',rate = '$productRate', amount = '$amount',total_amount = '$totalAmount' WHERE perticular = '$value' AND invoice_no = '$taxNumber' ";
            \Yii::$app->db->createCommand($updateQuery)->execute();
        }
        return true;
    }

    public function getData($data)
    {
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $query = "select * from labourbill WHERE date BETWEEN '$fromDate' AND '$toDate' AND status <> 'cancelled' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        return $result;
    }


}