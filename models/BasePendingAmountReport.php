<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pendingAmountReport".
 *
 * @property integer $id
 * @property string $date
 * @property string $clearanceDate
 * @property string $invoice_no
 * @property string $invoiceType
 * @property string $client_name
 * @property string $clientcode
 * @property string $number
 * @property string $paymentMode
 * @property string $referenceNumber
 * @property double $grandTotal
 * @property double $paidAmount
 * @property double $remainingAmount
 * @property string $status
 */
class BasePendingAmountReport extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pendingAmountReport';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'invoice_no', 'invoiceType', 'client_name', 'clientcode'], 'required'],
            [['date', 'clearanceDate'], 'safe'],
            [['grandTotal', 'paidAmount', 'remainingAmount'], 'number'],
            [['invoice_no', 'clientcode', 'paymentMode', 'referenceNumber', 'status'], 'string', 'max' => 100],
            [['invoiceType', 'client_name'], 'string', 'max' => 200],
            [['number'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'clearanceDate' => 'Clearance Date',
            'invoice_no' => 'Invoice No',
            'invoiceType' => 'Invoice Type',
            'client_name' => 'Client Name',
            'clientcode' => 'Clientcode',
            'number' => 'Number',
            'paymentMode' => 'Payment Mode',
            'referenceNumber' => 'Reference Number',
            'grandTotal' => 'Grand Total',
            'paidAmount' => 'Paid Amount',
            'remainingAmount' => 'Remaining Amount',
            'status' => 'Status',
        ];
    }
}