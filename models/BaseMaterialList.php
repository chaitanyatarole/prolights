<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "materialList".
 *
 * @property integer $id
 * @property integer $invoiceNumber
 * @property string $date
 * @property string $clientName
 * @property string $clientCode
 * @property string $address
 * @property string $mobile
 * @property string $email
 * @property string $particular
 * @property string $particularName
 * @property integer $dimensionOne
 * @property integer $dimensionTwo
 * @property integer $quantity
 * @property double $persqft
 * @property double $rate
 * @property double $amount
 * @property double $totalAmount
 */
class BaseMaterialList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'materialList';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoiceNumber', 'date', 'clientName', 'clientCode', 'particular', 'particularName', 'quantity', 'persqft', 'rate', 'amount', 'totalAmount'], 'required'],
            [['invoiceNumber', 'dimensionOne', 'dimensionTwo', 'quantity'], 'integer'],
            [['date'], 'safe'],
            [['address'], 'string'],
            [['persqft', 'rate', 'amount', 'totalAmount'], 'number'],
            [['clientName', 'clientCode', 'email', 'particular', 'particularName'], 'string', 'max' => 100],
            [['mobile'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoiceNumber' => 'Invoice Number',
            'date' => 'Date',
            'clientName' => 'Client Name',
            'clientCode' => 'Client Code',
            'address' => 'Address',
            'mobile' => 'Mobile',
            'email' => 'Email',
            'particular' => 'Particular',
            'particularName' => 'Particular Name',
            'dimensionOne' => 'Dimension One',
            'dimensionTwo' => 'Dimension Two',
            'quantity' => 'Quantity',
            'persqft' => 'Persqft',
            'rate' => 'Rate',
            'amount' => 'Amount',
            'totalAmount' => 'Total Amount',
        ];
    }
}