<?php

namespace app\controllers;

use app\commands\AppUtility;
use app\models\AudioPurchaseOrder;
use app\models\AudioVendorStock;
use app\models\AudioWorkOrder;
use app\models\BalajiPurchaseOrder;
use app\models\BalajiVendorStock;
use app\models\BalajiWorkOrder;
use app\models\Product;
use app\models\PurchaseOrder;
use app\models\VendorStock;
use app\models\WorkOrder;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Session;

class StockController extends Controller
{
    public function actionNewstock()
    {
        $this->layout = 'stockLayout';
        $product = new Product();
        $getStock = $product->getItemsWithStock();
        return $this->render('newStock', ['entireData' => $getStock]);
    }

    public function actionUpdatestock()
    {
        $data = Yii::$app->request->post();
        $product = new Product();
        $product->updateStock($data);
        return $this->redirect('/prolights/web/stock/newstock');
    }

    public function actionReceiveaudioorder()
    {
        $this->layout = 'stockLayout';
        $order = new AudioWorkOrder();
        $getList = $order->getPendingList();
        return $this->render('receiveAudioOrder', ['entireData' => $getList]);
    }

    public function actionGetaudioorder()
    {
        $this->layout = 'stockLayout';
        $data = Yii::$app->request->post();
        $order = new AudioWorkOrder();
        $getData = $order->getData($data);
        return $this->render('receivedAudioOrder', ['entireData' => $getData]);
    }

    public function actionShowaudioworkorder()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $invoice = $session['audioWorkOrder'];
        $workOrder = new AudioWorkOrder();
        $getData = $workOrder->getDataForBill($invoice);
        return $this->render('generateAudioWorkOrder', ['entireData' => $getData]);
    }

    public function actionReceivebalajiorder()
    {
        $this->layout = 'stockLayout';
        $order = new BalajiWorkOrder();
        $getList = $order->getPendingList();
        return $this->render('receiveBalajiOrder', ['entireData' => $getList]);
    }

    public function actionGetbalajiorder()
    {
        $this->layout = 'stockLayout';
        $data = Yii::$app->request->post();
        $order = new BalajiWorkOrder();
        $getData = $order->getData($data);
        return $this->render('receivedBalajiOrder', ['entireData' => $getData]);
    }

    public function actionShowbalajiworkorder()
    {

        $this->layout = 'printLayout';
        $session = new Session();
        $invoice = $session['balajiWorkOrder'];
        $workOrder = new BalajiWorkOrder();
        $getData = $workOrder->getDataForBill($invoice);
        return $this->render('generateBalajiWorkOrder', ['entireData' => $getData]);
    }

    public function actionUpdateorder()
    {
        $data = Yii::$app->request->post();
        $order = new WorkOrder();
        $order->updateOrder($data);
        return $this->redirect('/prolights/web/stock/receiveorder');
    }

    public function actionAcceptaudioorder()
    {
        $data = Yii::$app->request->post();
        $order = new AudioWorkOrder();
        $order->acceptOrder($data);
        $invoice = $data['invoice'];
        $session = new Session();
        $session['audioWorkOrder'] = $invoice;
        AppUtility::jsonResponse("true");
    }

    public function actionAcceptbalajiorder()
    {
        $data = Yii::$app->request->post();
        $order = new BalajiWorkOrder();
        $order->acceptOrder($data);
        $invoice = $data['invoice'];
        $session = new Session();
        $session['balajiWorkOrder'] = $invoice;
        AppUtility::jsonResponse("true");
    }


    public function actionRejectorder()
    {
        $data = Yii::$app->request->post();
        $order = new AudioWorkOrder();
        $order->rejectOrder($data);
        AppUtility::jsonResponse("true");
    }

    public function actionRejectbalajiorder()
    {
        $data = Yii::$app->request->post();
        $order = new BalajiWorkOrder();
        $order->rejectOrder($data);
        AppUtility::jsonResponse("true");
    }

    public function actionAudiostockreturn()
    {
        $this->layout = 'stockLayout';
        $order = new AudioWorkOrder();
        $getList = $order->getAcceptedList();
        return $this->render('audioStockReturn', ['entireData' => $getList]);
    }

    public function actionGetaudioorderdetails()
    {
        $data = Yii::$app->request->post();
        $order = new AudioWorkOrder();
        $entireData = $order->getDetails($data);
        AppUtility::jsonResponse($entireData);
    }

    public function actionBalajistockreturn()
    {
        $this->layout = 'stockLayout';
        $order = new BalajiWorkOrder();
        $getList = $order->getAcceptedList();
        return $this->render('balajiStockReturn', ['entireData' => $getList]);
    }

    public function actionGetbalajiorderdetails()
    {
        $data = Yii::$app->request->post();
        $order = new BalajiWorkOrder();
        $entireData = $order->getDetails($data);
        AppUtility::jsonResponse($entireData);
    }

    public function actionAddreturnedstock()
    {
        $data = Yii::$app->request->post();
        $order = new AudioWorkOrder();
        $order->addReturnedStock($data);
        AppUtility::jsonResponse("true");
    }

    public function actionAddbalajireturnedstock()
    {
        $data = Yii::$app->request->post();
        $order = new BalajiWorkOrder();
        $order->addReturnedStock($data);
        AppUtility::jsonResponse("true");
    }


    public function actionReceiveaudiopurchaseorder()
    {
        $this->layout = 'stockLayout';
        $purchaseOrder = new AudioPurchaseOrder();
        $getOrders = $purchaseOrder->getPendingOrders();
        return $this->render('receiveAudioPO', ['entireData' => $getOrders]);
    }

    public function actionGetaudiopurchaseorder()
    {
        $this->layout = 'stockLayout';
        $data = Yii::$app->request->post();
        $purchaseOrder = new AudioPurchaseOrder();
        $getData = $purchaseOrder->getDetails($data);
        return $this->render('receivedAudioPO', ['entireData' => $getData]);
    }

    public function actionAcceptaudiopurchaseorder()
    {
        $data = Yii::$app->request->post();
        $order = new AudioPurchaseOrder();
        $order->acceptOrder($data);
        AppUtility::jsonResponse("true");
    }

    public function actionRejectaudiopurchaseorder()
    {
        $data = Yii::$app->request->post();
        $order = new AudioPurchaseOrder();
        $order->rejectOrder($data);
        AppUtility::jsonResponse("true");
    }

    public function actionReceivebalajipurchaseorder()
    {
        $this->layout = 'stockLayout';
        $purchaseOrder = new BalajiPurchaseOrder();
        $getOrders = $purchaseOrder->getPendingOrders();
        return $this->render('receiveBalajiPO', ['entireData' => $getOrders]);
    }

    public function actionGetbalajipurchaseorder()
    {
        $this->layout = 'stockLayout';
        $data = Yii::$app->request->post();
        $purchaseOrder = new BalajiPurchaseOrder();
        $getData = $purchaseOrder->getDetails($data);
        return $this->render('receivedBalajiPO', ['entireData' => $getData]);
    }

    public function actionAcceptbalajipurchaseorder()
    {
        $data = Yii::$app->request->post();
        $order = new BalajiPurchaseOrder();
        $order->acceptOrder($data);
        AppUtility::jsonResponse("true");

    }

    public function actionRejectbalajipurchaseorder()
    {
        $data = Yii::$app->request->post();
        $order = new BalajiPurchaseOrder();
        $order->rejectOrder($data);
        AppUtility::jsonResponse("true");
    }

    public function actionReturnaudiomaterial()
    {
        $this->layout = 'stockLayout';
        $data = Yii::$app->request->post();
        $purchaseOrder = new AudioVendorStock();
        $getData = $purchaseOrder->getInStockOrders($data);
        return $this->render('audioInStockOrders', ['entireData' => $getData]);

    }

    public function actionReturnbalajimaterial()
    {
        $this->layout = 'stockLayout';
        $data = Yii::$app->request->post();
        $purchaseOrder = new BalajiVendorStock();
        $getData = $purchaseOrder->getInStockOrders($data);
        return $this->render('balajiInStockOrders', ['entireData' => $getData]);

    }

    public function actionGetaudiovendorstockdetails()
    {
        $data = Yii::$app->request->post();
        $vendor = new AudioVendorStock();
        $getData = $vendor->getDetails($data);
        AppUtility::jsonResponse($getData);
    }

    public function actionGetbalajivendorstockdetails()
    {
        $data = Yii::$app->request->post();
        $vendor = new BalajiVendorStock();
        $getData = $vendor->getDetails($data);
        AppUtility::jsonResponse($getData);
    }

    public function actionClearaudiovendorstock()
    {
        $data = Yii::$app->request->post();
        $vendor = new AudioVendorStock();
        $vendor->clearOrder($data);
        AppUtility::jsonResponse("true");
    }

    public function actionClearbalajivendorstock()
    {
        $data = Yii::$app->request->post();
        $vendor = new BalajiVendorStock();
        $vendor->clearOrder($data);
        AppUtility::jsonResponse("true");
    }

    public function actionGetpendingworkorderlist()
    {
        $order = new AudioWorkOrder();
        $balajiorder = new BalajiWorkOrder();

        $getList = $order->getPendingList();
        $getBalajiList = $balajiorder->getPendingList();
        $entireData = array_merge(isset($getList) ? $getList : '', isset($getBalajiList) ? $getBalajiList : '');
        AppUtility::jsonResponse($entireData);
    }

    public function actionGetpendingpurchaseorderlist()
    {
        $purchaseOrder = new AudioPurchaseOrder();
        $balajiPurchaseOrder = new BalajiPurchaseOrder();
        $getOrders = $purchaseOrder->getPendingOrders();
        $getBalajiOrders = $balajiPurchaseOrder->getPendingOrders();
        $entireData = array_merge(isset($getOrders) ? $getOrders : '', isset($getBalajiOrders) ? $getBalajiOrders : '');
        AppUtility::jsonResponse($entireData);
    }

    public function actionStockreport()
    {
        $this->layout = 'stockLayout';
        $data = Yii::$app->request->post();
        $purchaseOrder = new VendorStock();
        $getData = $purchaseOrder->getInStockOrders($data);
        return $this->render('inStockOrders', ['entireData' => $getData]);
    }

    public function actionCurrentstock()
    {
        $this->layout = 'stockLayout';
        $product = new Product();
        $getData = $product->getCurrentStock();
        return $this->render('showCurrentStock', ['entireData' => $getData]);
    }

    public function actionVendoraudioinstockreport()
    {
        $this->layout = 'stockLayout';
        return $this->render('vendorAudioInStockReport');
    }

    public function actionGetaudiovendorinstockreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new AudioVendorStock();
        $getData = $tax->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateAudioInStockReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionVendorbalajiinstockreport()
    {
        $this->layout = 'stockLayout';
        return $this->render('vendorBalajiInStockReport');
    }

    public function actionGetbalajivendorinstockreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new BalajiVendorStock();
        $getData = $tax->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateBalajiInStockReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionVendoraudioclearstockreport()
    {
        $this->layout = 'stockLayout';
        return $this->render('vendorAudioClearedStockReport');
    }

    public function actionGetaudiovendorclearedstockreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new AudioVendorStock();
        $getData = $tax->getClearedDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateAudioClearedStockReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);

    }

    public function actionVendorbalajiclearstockreport()
    {
        $this->layout = 'stockLayout';
        return $this->render('vendorBalajiClearedStockReport');
    }

    public function actionGetbalajivendorclearedstockreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new BalajiVendorStock();
        $getData = $tax->getClearedDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateBalajiClearedStockReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionAudioworkorderreports()
    {
        $this->layout = 'stockLayout';
        return $this->render('audioWorkOrderReports');
    }

    public function actionGetaudioworkorderreports()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new AudioWorkOrder();
        $getData = $tax->getDataForStockReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateAudioWorkOrderReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionAudioporeports()
    {
        $this->layout = 'stockLayout';
        return $this->render('audioPOReports');
    }

    public function actionGetaudioporeports()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new AudioPurchaseOrder();
        $getData = $tax->getDataForStockReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateAudioPOReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionBalajiworkorderreports()
    {
        $this->layout = 'stockLayout';
        return $this->render('balajiWorkOrderReports');
    }

    public function actionGetbalajiworkorderreports()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new BalajiWorkOrder();
        $getData = $tax->getDataForStockReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateBalajiWorkOrderReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionBalajiporeports()
    {
        $this->layout = 'stockLayout';
        return $this->render('balajiPOReports');
    }

    public function actionGetbalajiporeports()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new BalajiPurchaseOrder();
        $getData = $tax->getDataForStockReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateBalajiPOReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }


}
