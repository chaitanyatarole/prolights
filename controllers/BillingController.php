<?php

namespace app\controllers;

use app\commands\AppUtility;
use app\models\AudioTaxInvoice;
use app\models\AudioWorkOrder;
use app\models\BalajiTaxInvoice;
use app\models\BalajiWorkOrder;
use app\models\ClientData;
use app\models\Product;
use app\models\TaxInvoice;
use app\models\TaxInvoiceSimple;
use app\models\WorkOrder;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Session;

class BillingController extends Controller
{
    public function actionAudioinvoice()
    {
        $this->layout = 'billingLayout';
        $clientData = new ClientData();
        $tax = new AudioTaxInvoice();
        $audioWorkOrder = new AudioWorkOrder();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxTaxNumber = $tax->getMaxTaxNumber();
        $getCompletedOrders = $audioWorkOrder->getCompletedOrders();
        return $this->render('audioInvoice', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'completedOrders' => $getCompletedOrders]);
    }

    public function actionSaveaudioinvoice()
    {
        $data = Yii::$app->request->post();
        $order = new AudioTaxInvoice();
        $order->saveData($data);
        $session = new Session();
        $session['taxAudioData'] = $data;
        return $this->redirect('/prolights/web/billing/showtaxaudiobill');
    }

    public function actionBalajiinvoice()
    {
        $this->layout = 'billingLayout';
        $clientData = new ClientData();
        $tax = new BalajiTaxInvoice();
        $audioWorkOrder = new BalajiWorkOrder();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxTaxNumber = $tax->getMaxTaxNumber();
        $getCompletedOrders = $audioWorkOrder->getCompletedOrders();
        return $this->render('balajiInvoice', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'completedOrders' => $getCompletedOrders]);
    }

    public function actionSavebalajiinvoice()
    {
        $data = Yii::$app->request->post();
        $order = new BalajiTaxInvoice();
        $order->saveData($data);
        $session = new Session();
        $session['taxBalajiData'] = $data;
        return $this->redirect('/prolights/web/billing/showtaxbalajibill');
    }

    public function actionShowtaxaudiobill()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $data = $session['taxAudioData'];
        return $this->render('audioTaxbill', ['entireData' => $data]);
    }

    public function actionShowtaxbalajibill()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $data = $session['taxBalajiData'];
        return $this->render('balajiTaxbill', ['entireData' => $data]);
    }

    public function actionAudiodupbill()
    {
        $this->layout = 'billingLayout';
        $tax = new AudioTaxInvoice();
        $getTaxNumbers = $tax->getTaxNumbers();
        return $this->render('audioDuplicateBill', ['taxData' => $getTaxNumbers]);
    }

    public function actionShowaudiodupbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $tax = new AudioTaxInvoice();
        $getData = $tax->getDetails($data);
        return $this->render('generateDuplicateAudioBill', ['entireData' => $getData]);
    }

    public function actionBalajidupbill()
    {
        $this->layout = 'billingLayout';
        $tax = new BalajiTaxInvoice();
        $getTaxNumbers = $tax->getTaxNumbers();
        return $this->render('balajiDuplicateBill', ['taxData' => $getTaxNumbers]);
    }

    public function actionShowbalajidupbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $tax = new BalajiTaxInvoice();
        $getData = $tax->getDetails($data);
        return $this->render('generateDuplicateBalajiBill', ['entireData' => $getData]);
    }

    public function actionGetorderdetails()
    {
        $data = Yii::$app->request->post();
        $audioWorkOrder = new AudioWorkOrder();
        $getData = $audioWorkOrder->getDetailsForInvoice($data);
        AppUtility::jsonResponse($getData);
    }

    public function actionGetbalajiorderdetails()
    {
        $data = Yii::$app->request->post();
        $audioWorkOrder = new BalajiWorkOrder();
        $getData = $audioWorkOrder->getDetailsForInvoice($data);
        AppUtility::jsonResponse($getData);
    }

    public function actionCancelaudioinvoice()
    {
        $this->layout = 'billingLayout';
        $tax = new AudioTaxInvoice();
        $getTaxNumbers = $tax->getCompletedInvoices();
        return $this->render('cancelAudioBill', ['taxData' => $getTaxNumbers]);
    }

    public function actionModifyaudioinvoice()
    {
        $data = Yii::$app->request->post();
        $audioInvoice = new AudioTaxInvoice();
        $audioInvoice->cancelInvoice($data);
        return $this->redirect('/prolights/web/billing/cancelaudioinvoice');
    }

    public function actionCancelbalajiinvoice()
    {
        $this->layout = 'billingLayout';
        $tax = new BalajiTaxInvoice();
        $getTaxNumbers = $tax->getCompletedInvoices();
        return $this->render('cancelBalajiBill', ['taxData' => $getTaxNumbers]);
    }

    public function actionModifybalajiinvoice()
    {
        $data = Yii::$app->request->post();
        $audioInvoice = new BalajiTaxInvoice();
        $audioInvoice->cancelInvoice($data);
        return $this->redirect('/prolights/web/billing/cancelbalajiinvoice');
    }

    public function actionAudioreports()
    {
        $this->layout = 'billingLayout';
        return $this->render('audioReports');
    }

    public function actionGetaudioreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new AudioTaxInvoice();
        $getData = $tax->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateAudioInvoiceReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionBalajireports()
    {
        $this->layout = 'billingLayout';
        return $this->render('balajiReports');
    }

    public function actionGetbalajireport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new BalajiTaxInvoice();
        $getData = $tax->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateBalajiInvoiceReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }
}
