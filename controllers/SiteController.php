<?php

namespace app\controllers;

use app\commands\AppUtility;
use app\models\AudioPurchaseOrder;
use app\models\AudioQuotation;
use app\models\AudioTaxInvoice;
use app\models\AudioWorkOrder;
use app\models\BalajiPurchaseOrder;
use app\models\BalajiQuotation;
use app\models\BalajiTaxInvoice;
use app\models\BalajiWorkOrder;
use app\models\Category;
use app\models\ClientData;
use app\models\DeliveryChallan;
use app\models\EmployeeData;
use app\models\EmployeeWages;
use app\models\LabourBill;
use app\models\Login;
use app\models\Material;
use app\models\MaterialList;
use app\models\MeasurementList;
use app\models\PendingAmount;
use app\models\PendingAmountReport;
use app\models\Product;
use app\models\PurchaseOrder;
use app\models\Quotation;
use app\models\Receipt;
use app\models\TaxInvoice;
use app\models\Transactions;
use app\models\VendorData;
use app\models\WorkOrder;
use Yii;
use yii\db\Transaction;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Session;

class SiteController extends Controller
{

    public function actionIndex()
    {
        $this->layout = 'loginLayout';
        $model = new Login();
        if ($model->load(Yii::$app->request->post())) {
            $status = Login::verifyUser($model->username, $model->password);
            if ($status == 'admin') {
                return $this->redirect('/prolights/web/site/cliententry');
            }
            if ($status == 'stock') {
                return $this->redirect('/prolights/web/stock/receiveaudioorder');
            }
            if ($status == 'billing') {
                return $this->redirect('/prolights/web/billing/audioinvoice');
            } else {
                return $this->render('index', ['model' => "Invalid Username and Password"]);
            }
        }
        return $this->render('index');
    }

    public function actionTaxreports()
    {
        $this->layout = 'metronicLayout';
        return $this->render('taxReport');
    }


    public function actionTaxdupbill()
    {
        $this->layout = 'metronicLayout';
        $tax = new TaxInvoice();
        $getTaxNumbers = $tax->getTaxNumbers();
        return $this->render('taxDuplicateBill', ['taxData' => $getTaxNumbers]);
    }


    public function actionGetdetails()
    {
        $data = Yii::$app->request->post();
        $clientData = new ClientData();
        $getDetails = $clientData->getDetails($data);
        AppUtility::jsonResponse($getDetails);
    }

    public function actionGetproducts()
    {
        $product = new Product();
        $getProducts = $product->getProducts();
        AppUtility::jsonResponse($getProducts);
    }

    public function actionGettaxreport()
    {
        $this->layout = 'reportLayout';
        $data = Yii::$app->request->post();
        $tax = new TaxInvoice();
        $getData = $tax->getData($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateTaxReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionGettaxdupbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $tax = new TaxInvoice();
        $getData = $tax->getDetails($data);
        return $this->render('generateDuplicateTaxBill', ['entireData' => $getData]);
    }

    public function actionGettaxpaymentdetails()
    {
        $data = Yii::$app->request->post();
        $taxInvoice = new TaxInvoice();
        $result = $taxInvoice->getAmount($data);
        AppUtility::jsonResponse($result);
    }


    public function actionGettaxdescription()
    {
        $tax = new TaxInvoice();
        $getDescription = $tax->getDescription();
        AppUtility::jsonResponse($getDescription);
    }

    /*   Sai Fabrications methods below   */

    public function actionAddmaterial()
    {
        $this->layout = 'metronicLayout';
        return $this->render('addMaterial');
    }

    public function actionSavematerial()
    {
        $data = Yii::$app->request->post();
        $material = new Material();
        $material->saveMaterial($data);
        return $this->redirect('/prolights/web/site/addmaterial');
    }

    public function actionModifymaterial()
    {
        $this->layout = 'metronicLayout';
        $material = new Material();
        $getMaterials = $material->getMaterials();
        return $this->render('modifyMaterial', ['entireData' => $getMaterials]);
    }

    public function actionUpdatematerial()
    {
        $data = Yii::$app->request->post();
        $material = new Material();
        $material->updateMaterial($data);
        return $this->redirect('/prolights/web/site/modifymaterial');
    }


    public function actionModifyproduct()
    {
        $this->layout = 'metronicLayout';
        $product = new Product();
        $getProducts = $product->getProducts();
        return $this->render('modifyProduct', ['entireData' => $getProducts]);
    }

    public function actionUpdateproduct()
    {
        $data = Yii::$app->request->post();
        $product = new Product();
        $product->updateProduct($data);
        return $this->redirect('/prolights/web/site/modifyproduct');
    }

    public function actionDeliverychallan()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $material = new Material();
        $deliveryChallan = new DeliveryChallan();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaterialList = $material->getMaterials();
        $getMaxChallanNumber = $deliveryChallan->getMaxDeliveryNumber();
        return $this->render('deliveryChallan', ['clientData' => $getClients, 'maxclientCode' => $getMaxClientCode, 'materialList' => $getMaterialList, 'challanNumber' => $getMaxChallanNumber]);
    }

    public function actionSavedeliverychallan()
    {
        $data = Yii::$app->request->post();
        $delivery = new DeliveryChallan();
        $client = new ClientData();
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client->saveClient($data);
        }
        $delivery->saveData($data);
        $session = new Session();
        $session['deliveryData'] = $data;
        return $this->redirect('/prolights/web/site/showdelivery');
    }

    public function actionShowdelivery()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $entireData = $session['deliveryData'];
        return $this->render('showDelivery', ['entireData' => $entireData]);
    }

    public function actionModifydeliverychallan()
    {
        $this->layout = 'metronicLayout';
        $delivery = new DeliveryChallan();
        $getDeliveryNumbers = $delivery->getDeliveryNumbers();
        return $this->render('modifyDelivery', ['deliveryNumbers' => $getDeliveryNumbers]);
    }

    public function actionModifydeliverydata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $delivery = new DeliveryChallan();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getData = $delivery->getDetails($data);
        return $this->render('modifydeliveryData', ['clientData' => $getClients, 'maxclientCode' => $getMaxClientCode, 'entireData' => $getData]);
    }

    public function actionUpdatemodifieddeliverydata()
    {
        $data = Yii::$app->request->post();
        $delivery = new DeliveryChallan();
        $delivery->updateData($data);
        return $this->redirect('/prolights/web/site/modifydeliverychallan');
    }

    public function actionMeasurementlist()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $material = new Material();
        $measurementList = new MeasurementList();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaterialList = $material->getMaterials();
        $getMaxChallanNumber = $measurementList->getMaxInvoiceNumber();
        return $this->render('measurementList', ['clientData' => $getClients, 'maxclientCode' => $getMaxClientCode, 'materialList' => $getMaterialList, 'challanNumber' => $getMaxChallanNumber]);
    }

    public function actionSavemeasurementlist()
    {
        $data = Yii::$app->request->post();
        $delivery = new MeasurementList();
        $client = new ClientData();
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client->saveClient($data);
        }
        $delivery->saveData($data);
        $session = new Session();
        $session['measurementData'] = $data;
        return $this->redirect('/prolights/web/site/showmeasurementlist');
    }

    public function actionShowmeasurementlist()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $entireData = $session['measurementData'];
        return $this->render('showMeasurement', ['entireData' => $entireData]);
    }

    public function actionModifymeasurementlist()
    {
        $this->layout = 'metronicLayout';
        $measurement = new MeasurementList();
        $getDeliveryNumbers = $measurement->getDeliveryNumbers();
        return $this->render('modifyMeasurement', ['deliveryNumbers' => $getDeliveryNumbers]);
    }

    public function actionModifymeasurementdata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $delivery = new MeasurementList();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getData = $delivery->getDetails($data);
        return $this->render('modifyMeasurementData', ['clientData' => $getClients, 'maxclientCode' => $getMaxClientCode, 'entireData' => $getData]);
    }

    public function actionUpdatemodifiedmeasurementdata()
    {
        $data = Yii::$app->request->post();
        $delivery = new MeasurementList();
        $delivery->updateData($data);
        return $this->redirect('/prolights/web/site/modifymeasurementlist');
    }

    public function actionLabourbill()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $tax = new LabourBill();
        $product = new Product();

        $getProducts = $product->getProducts();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxTaxNumber = $tax->getMaxInvoiceNumber();
        return $this->render('labourBill', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts]);
    }

    public function actionSavelabourbill()
    {
        $data = Yii::$app->request->post();
        $labour = new LabourBill();
        $client = new ClientData();
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client->saveClient($data);
        }
        $laboutType = 1;
        $labour->saveData($data, $laboutType);
        $session = new Session();
        $session['labourData'] = $data;
        return $this->redirect('/prolights/web/site/showlabourbill');
    }

    public function actionShowlabourbill()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $entireData = $session['labourData'];
        return $this->render('showLabourBill', ['entireData' => $entireData]);
    }

    public function actionModifylabourbill()
    {
        $this->layout = 'metronicLayout';
        $labour = new LabourBill();
        $getInvoiceNumbers = $labour->getInvoiceNumbersForModification();
        return $this->render('modifyLabourBill', ['invoiceNumbers' => $getInvoiceNumbers]);
    }

    public function actionModifylabourdata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $delivery = new LabourBill();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getData = $delivery->getDetails($data);
        return $this->render('modifyLabourData', ['clientData' => $getClients, 'maxclientCode' => $getMaxClientCode, 'entireData' => $getData]);
    }

    public function actionUpdatemodifiedlabourdata()
    {
        $data = Yii::$app->request->post();
        $labour = new LabourBill();
        $labour->updateData($data);
        return $this->redirect('/prolights/web/site/modifylabourbill');
    }

    public function actionMateriallist()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $material = new Material();
        $materialList = new MaterialList();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaterialList = $material->getMaterials();
        $getMaxChallanNumber = $materialList->getMaxInvoiceNumber();
        return $this->render('materialList', ['clientData' => $getClients, 'maxclientCode' => $getMaxClientCode, 'materialList' => $getMaterialList, 'invoiceNumber' => $getMaxChallanNumber]);
    }

    public function actionSavemateriallist()
    {
        $data = Yii::$app->request->post();
        $materialList = new MaterialList();
        $client = new ClientData();
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client->saveClient($data);
        }
        $materialList->saveData($data);
        $session = new Session();
        $session['materialListData'] = $data;
        return $this->redirect('/prolights/web/site/showmateriallist');
    }

    public function actionShowmateriallist()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $entireData = $session['materialListData'];
        return $this->render('showMaterialList', ['entireData' => $entireData]);
    }

    public function actionModifymateriallist()
    {
        $this->layout = 'metronicLayout';
        $tax = new MaterialList();
        $getInvoiceNumbers = $tax->getInvoiceNumbers();
        return $this->render('modifyMaterialList', ['invoiceNumbers' => $getInvoiceNumbers]);
    }

    public function actionModifymateriallistdata()
    {
        $data = Yii::$app->request->post();

        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $material = new MaterialList();

        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getData = $material->getDetails($data);
        return $this->render('modifyMaterialListData', ['clientData' => $getClients, 'maxclientCode' => $getMaxClientCode, 'entireData' => $getData]);
    }

    public function actionUpdatemodifiedmateriallistdata()
    {
        $data = Yii::$app->request->post();
        $quotation = new MaterialList();
        $quotation->updateData($data);
        return $this->redirect('/prolights/web/site/modifymateriallist');
    }

    /*---------------------- Duplicate bill related methods start here ----------------------*/


    public function actionDeliverybill()
    {
        $this->layout = 'metronicLayout';
        $tax = new DeliveryChallan();
        $getQuotationNumbers = $tax->getDeliveryNumbers();
        return $this->render('deliveryDuplicateBill', ['deliveryNumbers' => $getQuotationNumbers]);
    }

    public function actionGetdeliverybill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $quotation = new DeliveryChallan();
        $getData = $quotation->getDetails($data);
        return $this->render('generateDeliveryBill', ['entireData' => $getData]);
    }

    public function actionMaterialbill()
    {
        $this->layout = 'metronicLayout';
        $tax = new MaterialList();
        $getQuotationNumbers = $tax->getInvoiceNumbers();
        return $this->render('materialListDuplicateBill', ['invoiceNumbers' => $getQuotationNumbers]);
    }

    public function actionGetmaterialbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $material = new MaterialList();
        $getData = $material->getDetails($data);
        return $this->render('generateMaterialBill', ['entireData' => $getData]);
    }

    public function actionMeasurementbill()
    {
        $this->layout = 'metronicLayout';
        $measurement = new MeasurementList();
        $getDeliveryNumbers = $measurement->getDeliveryNumbers();
        return $this->render('measurementDuplicateBill', ['deliveryNumbers' => $getDeliveryNumbers]);
    }

    public function actionGetmeasurementbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $material = new MeasurementList();
        $getData = $material->getDetails($data);
        return $this->render('generateMeasurementBill', ['entireData' => $getData]);
    }

    public function actionLabourduplicatebill()
    {
        $this->layout = 'metronicLayout';
        $labour = new LabourBill();
        $getInvoiceNumbers = $labour->getInvoiceNumbers();
        return $this->render('labourDuplicateBill', ['invoiceNumbers' => $getInvoiceNumbers]);
    }

    public function actionGetlabourbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $material = new LabourBill();
        $getData = $material->getDetails($data);
        return $this->render('generateLabourBill', ['entireData' => $getData]);
    }
    /*---------------------- Duplicate bill related methods end here ---------------------- */


    /*---------------------- Report related methods start here ----------------------*/

    public function actionDeliveryreports()
    {
        $this->layout = 'metronicLayout';
        return $this->render('deliveryReport');
    }

    public function actionGetdeliveryreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new DeliveryChallan();
        $getData = $tax->getData($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateDeliveryReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionLabourbillreport()
    {
        $this->layout = 'metronicLayout';
        return $this->render('labourBillReport');
    }

    public function actionGetlabourreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new LabourBill();
        $getData = $tax->getData($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateLabourReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }
    /*---------------------- Report related methods end here ----------------------*/


    /*---------------------- Tax reltated methods start here----------------------*/

    public function actionTax()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $tax = new TaxInvoice();
        $product = new Product();

        $getProducts = $product->getProducts();

        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();

        $getMaxTaxNumber = $tax->getMaxTaxNumber();
        return $this->render('tax', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts]);
    }

    public function actionSavetax()
    {
        $data = Yii::$app->request->post();
        $tax = new TaxInvoice();
        $taxType = 1;
        $tax->saveData($data, $taxType);
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client = new ClientData();
            $client->saveClient($data);
        }
        $session = new Session();
        $session['taxData'] = $data;
        return $this->redirect('/prolights/web/site/showtaxbill');
    }

    public function actionShowtaxbill()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $data = $session['taxData'];
        return $this->render('taxbill', ['entireData' => $data]);
    }

    public function actionShowtaxdupbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $tax = new TaxInvoice();
        $getData = $tax->getDetails($data);
        return $this->render('generateDuplicateTaxBill', ['entireData' => $getData]);
    }

    public function actionModifytax()
    {
        $this->layout = 'metronicLayout';
        $tax = new TaxInvoice();
        $getTaxNumbers = $tax->getTaxNumbersForModification();
        return $this->render('modifyTax', ['taxData' => $getTaxNumbers]);
    }

    public function actionModifytaxdata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $tax = new TaxInvoice();
        $product = new Product();
        $getProducts = $product->getProducts();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxTaxNumber = $tax->getMaxTaxNumber();
        $getData = $tax->getDetails($data);

        return $this->render('modifyTaxData', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts, 'entireData' => $getData]);
    }

    public function actionUpdatemodifiedtaxdata()
    {
        $data = Yii::$app->request->post();
        $tax = new TaxInvoice();
        $tax->modifyData($data);
        return $this->redirect('/prolights/web/site/modifytax');
    }
    /*---------------------- Tax reltated methods end here----------------------*/


    /*---------------------- Tax without dimension reltated methods start here----------------------*/

    public function actionTaxsimple()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $tax = new TaxInvoice();
        $product = new Product();
        $getProducts = $product->getProducts();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxTaxNumber = $tax->getMaxTaxNumber();
        return $this->render('taxSimple', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts]);
    }

    public function actionSavetaxsimple()
    {
        $data = Yii::$app->request->post();
        $tax = new TaxInvoice();
        $taxType = 0;
        $tax->saveData($data, $taxType);
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client = new ClientData();
            $client->saveClient($data);
        }
        $session = new Session();
        $session['taxData'] = $data;
        return $this->redirect('/prolights/web/site/showsimpletaxbill');
    }

    public function actionShowsimpletaxbill()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $data = $session['taxData'];
        return $this->render('simpleTaxbill', ['entireData' => $data]);
    }

    public function actionSimpletaxdupbill()
    {
        $this->layout = 'metronicLayout';
        $tax = new TaxInvoice();
        $getTaxNumbers = $tax->getSimpleTaxNumbers();
        return $this->render('simpleTaxDuplicateBill', ['taxData' => $getTaxNumbers]);
    }

    public function actionShowsimpletaxdupbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $tax = new TaxInvoice();
        $getData = $tax->getDetails($data);
        return $this->render('generateSimpleDuplicateTaxBill', ['entireData' => $getData]);
    }

    public function actionModifysimpletax()
    {
        $this->layout = 'metronicLayout';
        $tax = new TaxInvoice();
        $getTaxNumbers = $tax->getSimpleTaxNumbersForModification();
        return $this->render('modifySimpleTax', ['taxData' => $getTaxNumbers]);
    }

    public function actionModifysimpletaxdata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $tax = new TaxInvoice();
        $product = new Product();
        $getProducts = $product->getProducts();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxTaxNumber = $tax->getMaxTaxNumber();
        $getData = $tax->getDetails($data);
        return $this->render('modifySimpleTaxData', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts, 'entireData' => $getData]);
    }

    public function actionUpdatemodifiedsimpletaxdata()
    {
        $data = Yii::$app->request->post();
        $tax = new TaxInvoice();
        $tax->modifyData($data);
        return $this->redirect('/prolights/web/site/modifysimpletax');
    }
    /*---------------------- Tax without dimension reltated methods end here----------------------*/


    /*---------------------- Labour Bill without dimension reltated methods end here----------------------*/

    public function actionSimplelabourbill()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $tax = new LabourBill();
        $product = new Product();
        $getProducts = $product->getProducts();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxTaxNumber = $tax->getMaxInvoiceNumber();
        return $this->render('simpleLabourBill', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts]);
    }

    public function actionSavesimplelabourbill()
    {
        $data = Yii::$app->request->post();
        $labour = new LabourBill();
        $client = new ClientData();
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client->saveClient($data);
        }
        $labourType = 0;
        $labour->saveData($data, $labourType);
        $session = new Session();
        $session['labourData'] = $data;
        return $this->redirect('/prolights/web/site/showsimplelabourbill');
    }

    public function actionShowsimplelabourbill()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $entireData = $session['labourData'];
        return $this->render('showSimpleLabourBill', ['entireData' => $entireData]);
    }

    public function actionSimplelabourduplicatebill()
    {
        $this->layout = 'metronicLayout';
        $labour = new LabourBill();
        $getInvoiceNumbers = $labour->getSimpleInvoiceNumbers();
        return $this->render('simpleLabourDuplicateBill', ['invoiceNumbers' => $getInvoiceNumbers]);
    }

    public function actionGetsimplelabourbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $material = new LabourBill();
        $getData = $material->getDetails($data);
        return $this->render('generateSimpleLabourBill', ['entireData' => $getData]);
    }

    public function actionModifysimplelabourbill()
    {
        $this->layout = 'metronicLayout';
        $labour = new LabourBill();
        $getInvoiceNumbers = $labour->getSimpleInvoiceNumbersForModification();
        return $this->render('modifySimpleLabourBill', ['invoiceNumbers' => $getInvoiceNumbers]);

    }

    public function actionModifysimplelabourdata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $delivery = new LabourBill();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getData = $delivery->getDetails($data);
        return $this->render('modifySimpleLabourData', ['clientData' => $getClients, 'maxclientCode' => $getMaxClientCode, 'entireData' => $getData]);
    }

    public function actionUpdatemodifiedsimplelabourdata()
    {
        $data = Yii::$app->request->post();
        $labour = new LabourBill();
        $labour->updateData($data);
        return $this->redirect('/prolights/web/site/modifysimplelabourbill');
    }
    /*---------------------- Labour Bill without dimension reltated methods end here----------------------*/


    /*---------------------- Pending Amount related methods start here----------------------*/

    public function actionClearamount()
    {
        $this->layout = 'metronicLayout';
        $pending = new PendingAmount();
        $getDeliveryNumbers = $pending->getPendingNumbers();
        return $this->render('pendingAmount', ['invoiceNumber' => $getDeliveryNumbers]);
    }

    public function actionUpdatependingamount()
    {
        $data = Yii::$app->request->post();
        $pendingAmount = new PendingAmount();
        $pendingAmount->updateAmount($data);
        return $this->redirect('/prolights/web/site/clearamount');
    }

    public function actionPendingamountreport()
    {
        $this->layout = 'metronicLayout';
        $pending = new PendingAmount();
        $getDeliveryNumbers = $pending->getPendingNumbersForReport();
        return $this->render('pendingAmountReport', ['invoiceNumber' => $getDeliveryNumbers]);
    }

    /*---------------------- Pending Amount related methods end here----------------------*/


    /*---------------------- Cancel Invoice,Labour related methods starts here----------------------*/

    public function actionCanceltax()
    {
        $this->layout = 'metronicLayout';
        $tax = new TaxInvoice();
        $getDetails = $tax->getEntireNumbers();
        return $this->render('cancelTax', ['entireData' => $getDetails]);
    }

    public function actionCanceltaxdata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $tax = new TaxInvoice();
        $product = new Product();
        $getProducts = $product->getProducts();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxTaxNumber = $tax->getMaxTaxNumber();
        $getData = $tax->getDetails($data);
        $taxType = $getData[0]['taxType'];
        if ($taxType == 1) {
            return $this->render('cancelTaxData', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts, 'entireData' => $getData]);
        }
        if ($taxType == 0) {
            return $this->render('cancelSimpleTaxData', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts, 'entireData' => $getData]);
        }
    }

    public function actionChangetaxstatus()
    {
        $data = Yii::$app->request->post();
        $tax = new TaxInvoice();
        $tax->changeStatus($data);
        return $this->redirect('/prolights/web/site/canceltax');
    }

    public function actionCancellabour()
    {
        $this->layout = 'metronicLayout';
        $tax = new LabourBill();
        $getDetails = $tax->getEntireNumbers();
        return $this->render('cancelLabour', ['entireData' => $getDetails]);
    }

    public function actionCancellabourdata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $delivery = new LabourBill();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getData = $delivery->getDetails($data);
        $labourType = $getData[0]['labourType'];

        if ($labourType == 1) {
            return $this->render('cancelLabourData', ['clientData' => $getClients, 'maxclientCode' => $getMaxClientCode, 'entireData' => $getData]);
        }
        if ($labourType == 0) {
            return $this->render('cancelSimpleLabourData', ['clientData' => $getClients, 'maxclientCode' => $getMaxClientCode, 'entireData' => $getData]);
        }
    }

    public function actionChangelabourstatus()
    {
        $data = Yii::$app->request->post();
        $tax = new LabourBill();
        $tax->changeStatus($data);
        return $this->redirect('/prolights/web/site/cancellabour');
    }

    /*---------------------- Cancel Invoice,Labour related methods end here----------------------*/


    /*---------------------- Receipt related methods end here----------------------*/

    public function actionReceipt()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $labourBill = new Receipt();
        $getMaxChallanNumber = $labourBill->getMaxInvoiceNumber();
        $product = new Product();
        $getProducts = $product->getProducts();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        return $this->render('receipt', ['clientData' => $getClients, 'challanNumber' => $getMaxChallanNumber, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts]);
    }

    public function actionSavereceipt()
    {
        $data = Yii::$app->request->post();
        $tax = new Receipt();
        $tax->saveData($data);
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client = new ClientData();
            $client->saveClient($data);
        }
        $session = new Session();
        $session['receiptData'] = $data;
        return $this->redirect('/prolights/web/site/showreceipt');
    }

    public function actionShowreceipt()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $entireData = $session['receiptData'];
        return $this->render('showReceipt', ['entireData' => $entireData]);
    }

    public function actionModifyreceipt()
    {
        $this->layout = 'metronicLayout';
        $receipt = new Receipt();
        $getTaxNumbers = $receipt->getReceiptNumbers();
        return $this->render('modifyReceipt', ['receiptData' => $getTaxNumbers]);
    }

    public function actionModifyreceiptdata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $tax = new Receipt();
        $product = new Product();
        $getProducts = $product->getProducts();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getData = $tax->getDetails($data);

        return $this->render('modifyReceiptData', ['clientData' => $getClients, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts, 'entireData' => $getData]);
    }

    public function actionUpdatemodifiedreceiptdata()
    {
        $data = Yii::$app->request->post();
        $tax = new Receipt();
        $tax->modifyData($data);
        return $this->redirect('/prolights/web/site/modifyreceipt');
    }

    public function actionReceiptduplicatebill()
    {
        $this->layout = 'metronicLayout';
        $tax = new Receipt();
        $getTaxNumbers = $tax->getTaxNumbers();
        return $this->render('receiptDuplicateBill', ['receiptData' => $getTaxNumbers]);
    }

    public function actionShowreceiptdupbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $tax = new Receipt();
        $getData = $tax->getDetails($data);
        return $this->render('generateDuplicateReceiptBill', ['entireData' => $getData]);
    }

    public function actionReceiptreport()
    {
        $this->layout = 'metronicLayout';
        return $this->render('receiptReport');
    }

    public function actionGetreceiptreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new Receipt();
        $getData = $tax->getData($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateReceiptReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }
    /*---------------------- Receipt related methods end here----------------------*/


    /*---------------------- All ajax call methods below ----------------------*/

    public function actionGetmaterials()
    {
        $material = new Material();
        $getMaterials = $material->getMaterials();
        AppUtility::jsonResponse($getMaterials);
    }

    public function actionGetdimension()
    {
        $data = Yii::$app->request->post();
        $material = new Material();
        $getDetails = $material->getDetails($data);
        AppUtility::jsonResponse($getDetails);
    }

    public function actionGetdata()
    {
        $data = Yii::$app->request->post();
        $combinedData = $data['combinedData'];
        $combinedData = explode(" ", $combinedData);
        $pendingAmount = new PendingAmount();
        $getDetails = $pendingAmount->getDetails($combinedData);
        AppUtility::jsonResponse($getDetails);
    }

    public function actionGetpendingamountdata()
    {
        $data = Yii::$app->request->post();
        $combinedData = $data['combinedData'];
        $combinedData = explode(" ", $combinedData);
        $pendingAmount = new PendingAmountReport();
        $getDetails = $pendingAmount->getDetails($combinedData);
        AppUtility::jsonResponse($getDetails);
    }

    public function actionGetpendingbills()
    {
        $pendingAmount = new PendingAmount();
        $getDetails = $pendingAmount->getPendingClientDetails();
        AppUtility::jsonResponse($getDetails);
    }

    public function actionGetreport()
    {
        $this->layout = 'reportLayout';
        $data = $_GET;
        $data = $data['id'];
        $data = explode(" ", $data);
        $pendingReport = new PendingAmountReport();
        $getData = $pendingReport->getDetails($data);
        return $this->render('showReport', ['entireData' => $getData]);
    }

    public function actionGetclientreport()
    {
        $this->layout = 'reportLayout';
        $data = Yii::$app->request->post();
        $pendingReport = new PendingAmountReport();
        $getData = $pendingReport->getClientDetails($data);
        return $this->render('showClientReport', ['entireData' => $getData]);
    }

    public function actionGetitems()
    {
        $data = Yii::$app->request->post();
        $product = new Product();
        $getItems = $product->getItems($data);
        AppUtility::jsonResponse($getItems);
    }

    /*---------------------- All ajax call methods end here----------------------*/

    /*---------------------- All new methods start here----------------------*/

    public function actionGetstock()
    {
        $data = Yii::$app->request->post();
        $product = new Product();
        $getStock = $product->getStock($data);
        AppUtility::jsonResponse($getStock);
    }

    public function actionWages()
    {
        $this->layout = 'metronicLayout';
        $clientData = new EmployeeData();
        $orderNumber = new EmployeeWages();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxTaxNumber = $orderNumber->getMaxOrderNumber();
        return $this->render('wages', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode]);
    }

    public function actionSavewages()
    {
        $data = Yii::$app->request->post();
        $wages = new EmployeeWages();
        $wages->saveData($data);
        $clientNameCode = isset($data['entireData']['clientNameCode']) ? $data['entireData']['clientNameCode'] : '';
        if (empty($clientNameCode)) {
            $client = new EmployeeData();
            $client->saveClient($data);
        }
        AppUtility::jsonResponse("true");
    }

    public function actionGetemployeedetails()
    {
        $data = Yii::$app->request->post();
        $employee = new EmployeeData();
        $getDetails = $employee->getDetails($data);
        AppUtility::jsonResponse($getDetails);
    }

    public function actionPurchaseorderreports()
    {
        $this->layout = 'metronicLayout';
        return $this->render('purchaseOrderReport');
    }

    public function actionGetpurchaseorderreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new PurchaseOrder();
        $getData = $tax->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generatePurchaseOrderReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    /*---------------------- All new methods end here----------------------*/

    /*---------------------- All required methods start here----------------------*/

    /*---------------------- Add Category and Items methods start here----------------------*/

    public function actionAddcategory()
    {
        $this->layout = 'metronicLayout';
        return $this->render('addcategory');
    }

    public function actionSavecategory()
    {
        $data = Yii::$app->request->post();
        $category = new Category();
        $category->saveData($data);
        return $this->redirect('/prolights/web/site/addcategory');
    }

    public function actionModifycategory()
    {
        $this->layout = 'metronicLayout';
        $category = new Category();
        $getCategories = $category->getCategoriesToModify();
        return $this->render('modifyCategory', ['entireData' => $getCategories]);
    }

    public function actionUpdatecategory()
    {
        $data = Yii::$app->request->post();
        $category = new Category();
        $category->modifyCategory($data);
        return $this->redirect('/prolights/web/site/modifycategory');
    }

    public function actionAddproduct()
    {
        $this->layout = 'metronicLayout';
        $category = new Category();
        $getData = $category->getData();
        return $this->render('addproduct', ['categories' => $getData]);
    }

    public function actionSaveproduct()
    {
        $data = Yii::$app->request->post();
        $product = new Product();
        $product->saveData($data);
        return $this->redirect('/prolights/web/site/addproduct');
    }

    public function actionModifyitem()
    {
        $this->layout = 'metronicLayout';
        $category = new Category();
        $getData = $category->getData();
        return $this->render('modifyItem', ['categories' => $getData]);
    }

    public function actionUpdateitem()
    {
        $data = Yii::$app->request->post();
        $product = new Product();
        $product->updateProduct($data);
        return $this->redirect('/prolights/web/site/modifyitem');
    }

    /*---------------------- Add Category and Items methods end here----------------------*/

    /*---------------------- Client Entry methods start here----------------------*/


    public function actionCliententry()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $getMaxClientCode = $clientData->getMaxClientCode();
        return $this->render('clientEntry', ['maxClientCode' => $getMaxClientCode]);
    }

    public function actionSaveclient()
    {
        $data = Yii::$app->request->post();
        $clientData = new ClientData();
        $clientData->saveData($data);
        return $this->redirect('/prolights/web/site/cliententry');
    }
    /*---------------------- Client Entry methods end here----------------------*/

    /* ---------------------- All quotation methods start here---------------------- */

    public function actionAudioquotation()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $product = new Product();
        $quotation = new AudioQuotation();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getProducts = $product->getProducts();
        $getMaxQuotation = $quotation->getMaxQuotationNumber();
        return $this->render('audioQuotation', ['clientData' => $getClients, 'maxclientCode' => $getMaxClientCode, 'materialList' => $getProducts, 'quotationNumber' => $getMaxQuotation]);
    }

    public function actionSaveaudioquotation()
    {
        $data = Yii::$app->request->post();
        $quotation = new AudioQuotation();
        $client = new ClientData();
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client->saveClient($data);
        }
        $quotation->saveData($data);
        $session = new Session();
        $session['quotationData'] = $data;
        return $this->redirect('/prolights/web/site/showaudioquotation');
    }

    public function actionShowaudioquotation()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $entireData = $session['quotationData'];
        return $this->render('showAudioQuotation', ['entireData' => $entireData]);
    }

    public function actionModifyaudioquotation()
    {
        $this->layout = 'metronicLayout';
        $tax = new AudioQuotation();
        $getQuotationNumbers = $tax->getQuotationNumbers();
        return $this->render('modifyAudioQuotation', ['quotationNumbers' => $getQuotationNumbers]);
    }

    public function actionModifyaudioquotationdata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $quotation = new AudioQuotation();

        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();

        $getMaxQuotationNumber = $quotation->getMaxQuotationNumber();
        $getData = $quotation->getDetailsForModification($data);

        return $this->render('modifyAudioQuotationData', ['clientData' => $getClients, 'taxNumber' => $getMaxQuotationNumber, 'maxclientCode' => $getMaxClientCode, 'entireData' => $getData]);
    }

    public function actionUpdatemodifiedaudioquotationdata()
    {
        $data = Yii::$app->request->post();
        $quotation = new AudioQuotation();
        $quotation->updateData($data);
        return $this->redirect('/prolights/web/site/modifyaudioquotation');
    }

    public function actionAudioquotationbill()
    {
        $this->layout = 'metronicLayout';
        $tax = new AudioQuotation();
        $getQuotationNumbers = $tax->getQuotationNumbers();
        return $this->render('audioQuotationDuplicateBill', ['quotationData' => $getQuotationNumbers]);
    }

    public function actionGetaudioquotationbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $quotation = new AudioQuotation();
        $getData = $quotation->getDetails($data);
        return $this->render('generateAudioQuotationBill', ['entireData' => $getData]);
    }

    public function actionAudioquotationreports()
    {
        $this->layout = 'metronicLayout';
        return $this->render('audioQuotationReport');
    }

    public function actionGetaudioquotationreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new AudioQuotation();
        $getData = $tax->getData($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateAudioQuotationReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionBalajiquotation()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $product = new Product();
        $quotation = new BalajiQuotation();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getProducts = $product->getProducts();
        $getMaxQuotation = $quotation->getMaxQuotationNumber();
        return $this->render('balajiQuotation', ['clientData' => $getClients, 'maxclientCode' => $getMaxClientCode, 'materialList' => $getProducts, 'quotationNumber' => $getMaxQuotation]);
    }


    public function actionSavebalajiquotation()
    {
        $data = Yii::$app->request->post();
        $quotation = new BalajiQuotation();
        $client = new ClientData();
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client->saveClient($data);
        }
        $quotation->saveData($data);
        $session = new Session();
        $session['quotationData'] = $data;
        return $this->redirect('/prolights/web/site/showbalajiquotation');
    }

    public function actionShowbalajiquotation()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $entireData = $session['quotationData'];
        return $this->render('showBalajiQuotation', ['entireData' => $entireData]);
    }

    public function actionModifybalajiquotation()
    {
        $this->layout = 'metronicLayout';
        $tax = new BalajiQuotation();
        $getQuotationNumbers = $tax->getQuotationNumbers();
        return $this->render('modifyBalajiQuotation', ['quotationNumbers' => $getQuotationNumbers]);
    }

    public function actionModifybalajiquotationdata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $quotation = new BalajiQuotation();

        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();

        $getMaxQuotationNumber = $quotation->getMaxQuotationNumber();
        $getData = $quotation->getDetailsForModification($data);

        return $this->render('modifyBalajiQuotationData', ['clientData' => $getClients, 'taxNumber' => $getMaxQuotationNumber, 'maxclientCode' => $getMaxClientCode, 'entireData' => $getData]);
    }

    public function actionUpdatemodifiedbalajiquotationdata()
    {
        $data = Yii::$app->request->post();
        $quotation = new BalajiQuotation();
        $quotation->updateData($data);
        return $this->redirect('/prolights/web/site/modifybalajiquotation');
    }

    public function actionBalajiquotationbill()
    {
        $this->layout = 'metronicLayout';
        $tax = new BalajiQuotation();
        $getQuotationNumbers = $tax->getQuotationNumbers();
        return $this->render('balajiQuotationDuplicateBill', ['quotationData' => $getQuotationNumbers]);
    }

    public function actionGetbalajiquotationbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $quotation = new BalajiQuotation();
        $getData = $quotation->getDetails($data);
        return $this->render('generateBalajiQuotationBill', ['entireData' => $getData]);
    }

    public function actionBalajiquotationreports()
    {
        $this->layout = 'metronicLayout';
        return $this->render('balajiQuotationReport');
    }

    public function actionGetbalajiquotationreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new BalajiQuotation();
        $getData = $tax->getData($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateBalajiQuotationReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    /*---------------------- All quotation methods end here----------------------*/

    /*---------------------- All PO methods start here----------------------*/

    public function actionAudiopurchaseorder()
    {
        $this->layout = 'metronicLayout';
        $clientData = new VendorData();
        $orderNumber = new AudioPurchaseOrder();
        $quotation = new AudioQuotation();
        $product = new Product();
        $getProducts = $product->getProducts();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxTaxNumber = $orderNumber->getMaxOrderNumber();
        $quotationList = $quotation->getQuotationNumbers();
        return $this->render('audioPurchaseOrder', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts, 'quotationList' => $quotationList]);
    }

    public function actionSaveaudiopurchaseorder()
    {
        $data = Yii::$app->request->post();
        $purchaseOrder = new AudioPurchaseOrder();
        $purchaseOrder->savePurchaseOrder($data);
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client = new VendorData();
            $client->saveClient($data);
        }
        return $this->redirect('/prolights/web/site/audiopurchaseorder');
    }

    public function actionModifyaudiopurchase()
    {
        $this->layout = 'metronicLayout';
        $tax = new AudioPurchaseOrder();
        $getTaxNumbers = $tax->getPendingOrders();
        return $this->render('modifyAudioPurchase', ['taxData' => $getTaxNumbers]);
    }

    public function actionModifyaudiopurchasedata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $purchase = new AudioPurchaseOrder();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxQuotationNumber = $purchase->getMaxOrderNumber();
        $getData = $purchase->getDetails($data);
        return $this->render('modifyAudioPurchaseData', ['clientData' => $getClients, 'taxNumber' => $getMaxQuotationNumber, 'maxclientCode' => $getMaxClientCode, 'entireData' => $getData]);
    }

    public function actionUpdatemodifiedaudiopurchasedata()
    {
        $data = Yii::$app->request->post();
        $quotation = new AudioPurchaseOrder();
        $quotation->updateData($data);
        return $this->redirect('/prolights/web/site/modifyaudiopurchase');
    }

    public function actionBalajipurchaseorder()
    {
        $this->layout = 'metronicLayout';
        $clientData = new VendorData();
        $orderNumber = new BalajiPurchaseOrder();
        $balajiQuotation = new BalajiQuotation();
        $quotationList = $balajiQuotation->getQuotationNumbers();
        $product = new Product();
        $getProducts = $product->getProducts();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();

        $getMaxTaxNumber = $orderNumber->getMaxOrderNumber();
        return $this->render('balajiPurchaseOrder', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts, 'quotationList' => $quotationList]);
    }

    public function actionSavebalajipurchaseorder()
    {
        $data = Yii::$app->request->post();
        $purchaseOrder = new BalajiPurchaseOrder();
        $purchaseOrder->savePurchaseOrder($data);
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client = new VendorData();
            $client->saveClient($data);
        }
        return $this->redirect('/prolights/web/site/balajipurchaseorder');
    }

    public function actionModifybalajipurchase()
    {
        $this->layout = 'metronicLayout';
        $tax = new BalajiPurchaseOrder();
        $getTaxNumbers = $tax->getPendingOrders();
        return $this->render('modifyBalajiPurchase', ['taxData' => $getTaxNumbers]);
    }

    public function actionModifybalajipurchasedata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $purchase = new BalajiPurchaseOrder();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxQuotationNumber = $purchase->getMaxOrderNumber();
        $getData = $purchase->getDetails($data);
        return $this->render('modifyBalajiPurchaseData', ['clientData' => $getClients, 'taxNumber' => $getMaxQuotationNumber, 'maxclientCode' => $getMaxClientCode, 'entireData' => $getData]);
    }

    public function actionUpdatemodifiedbalajipurchasedata()
    {
        $data = Yii::$app->request->post();
        $quotation = new BalajiPurchaseOrder();
        $quotation->updateData($data);
        return $this->redirect('/prolights/web/site/modifybalajipurchase');
    }

    /*---------------------- All PO methods end here----------------------*/

    /*---------------------- All Work Order methods start here----------------------*/

    public function actionAudioworkorder()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $orderNumber = new AudioWorkOrder();
        $product = new Product();
        $quotation = new AudioQuotation();
        $getProducts = $product->getProducts();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getQuotations = $quotation->getQuotationNumbersForWorkOrder();
        $getMaxTaxNumber = $orderNumber->getMaxOrderNumber();
        return $this->render('audioWorkOrder', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts, 'pendingQuotations' => $getQuotations]);
    }

    public function actionGetorderdetails()
    {
        $data = Yii::$app->request->post();
        $audioQuotation = new AudioQuotation();
        $getData = $audioQuotation->getDetailsForInvoice($data);
        AppUtility::jsonResponse($getData);
    }

    public function actionSaveaudioworkorder()
    {
        $data = Yii::$app->request->post();
        $order = new AudioWorkOrder();
        $order->saveData($data);
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client = new ClientData();
            $client->saveClient($data);
        }
        $session = new Session();
        $session['audioWorkOrder'] = $data;
//        return $this->redirect('/prolights/web/site/showtaxbill');
        return $this->redirect('/prolights/web/site/showaudioworkorder');
    }

    public function actionModifyaudiowork()
    {
        $this->layout = 'metronicLayout';
        $tax = new AudioWorkOrder();
        $getTaxNumbers = $tax->getPendingList();
        return $this->render('modifyAudioWork', ['taxData' => $getTaxNumbers]);
    }

    public function actionModifyaudioworkdata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $purchase = new AudioWorkOrder();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxQuotationNumber = $purchase->getMaxOrderNumber();
        $getData = $purchase->getData($data);
//        AppUtility::dump($getData);
        return $this->render('modifyAudioWorkData', ['clientData' => $getClients, 'taxNumber' => $getMaxQuotationNumber, 'maxclientCode' => $getMaxClientCode, 'entireData' => $getData]);
    }

    public function actionUpdatemodifiedaudioworkdata()
    {
        $data = Yii::$app->request->post();
        $quotation = new AudioWorkOrder();
        $quotation->updateData($data);
        return $this->redirect('/prolights/web/site/modifyaudiowork');
    }

    public function actionAudioworkorderreports()
    {
        $this->layout = 'metronicLayout';
        return $this->render('audioWorkOrderReport');
    }

    public function actionGetaudioworkorderreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new AudioWorkOrder();
        $getData = $tax->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateAudioWorkOrderReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionBalajiworkorder()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $orderNumber = new BalajiWorkOrder();
        $product = new Product();
        $balajiQuotation = new BalajiQuotation();
        $getProducts = $product->getProducts();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getPendingQuotations = $balajiQuotation->getQuotationNumbersForWorkOrder();
        $getMaxTaxNumber = $orderNumber->getMaxOrderNumber();
        return $this->render('balajiWorkOrder', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'productCode' => $getProducts, 'pendingQuotations' => $getPendingQuotations]);
    }

    public function actionGetorderdetailsforbalaji()
    {
        $data = Yii::$app->request->post();
        $balajiQuotation = new BalajiQuotation();
        $getData = $balajiQuotation->getDetailsForInvoice($data);
        AppUtility::jsonResponse($getData);
    }

    public function actionSavebalajiworkorder()
    {
        $data = Yii::$app->request->post();
        $order = new BalajiWorkOrder();
        $order->saveData($data);
        $clientNameCode = $data['clientNameCode'];
        if (empty($clientNameCode)) {
            $client = new ClientData();
            $client->saveClient($data);
        }
        return $this->redirect('/prolights/web/site/balajiworkorder');
    }

    public function actionModifybalajiwork()
    {
        $this->layout = 'metronicLayout';
        $tax = new BalajiWorkOrder();
        $getTaxNumbers = $tax->getPendingList();
        return $this->render('modifyBalajiWork', ['taxData' => $getTaxNumbers]);
    }

    public function actionModifybalajiworkdata()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $purchase = new BalajiWorkOrder();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxQuotationNumber = $purchase->getMaxOrderNumber();
        $getData = $purchase->getData($data);
        return $this->render('modifyBalajiWorkData', ['clientData' => $getClients, 'taxNumber' => $getMaxQuotationNumber, 'maxclientCode' => $getMaxClientCode, 'entireData' => $getData]);
    }

    public function actionUpdatemodifiedbalajiworkdata()
    {
        $data = Yii::$app->request->post();
        $quotation = new BalajiWorkOrder();
        $quotation->updateData($data);
        return $this->redirect('/prolights/web/site/modifybalajiwork');
    }

    public function actionBalajiworkorderreports()
    {
        $this->layout = 'metronicLayout';
        return $this->render('balajiWorkOrderReport');
    }

    public function actionGetbalajiworkorderreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new AudioWorkOrder();
        $getData = $tax->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateBalajiWorkOrderReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }


    /*---------------------- All Work Order methods end here----------------------*/


    public function actionGetvendordetails()
    {
        $data = Yii::$app->request->post();
        $clientData = new VendorData();
        $getDetails = $clientData->getDetails($data);
        AppUtility::jsonResponse($getDetails);
    }

    public function actionWagesreport()
    {
        $this->layout = 'metronicLayout';
        return $this->render('wagesReport');
    }

    public function actionGetwagesreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new EmployeeWages();
        $getData = $tax->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('employeeWagesReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionGraph()
    {
        $this->layout = 'metronicLayout';
        return $this->render('graph');
    }

    public function actionShowgraph()
    {
        $data = Yii::$app->request->post();
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        $session = new Session();
        $session['dates'] = $data;
        return $this->render('demo', ['fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionAmountdeducted()
    {
        $transaction = new Transactions();
        $session = new Session();
        $dates = $session['dates'];
        $getData = $transaction->getAmountDeducted($dates);
        AppUtility::jsonResponse($getData);
    }

    public function actionEmployeereport()
    {
        $this->layout = 'metronicLayout';
        $employeeData = new EmployeeData();
        $getData = $employeeData->getNames();
        return $this->render('employeeReport', ['entireData' => $getData]);
    }

    public function actionGetemployeereport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $employeeWages = new EmployeeWages();
        $getData = $employeeWages->getDataOfEmployee($data);
        return $this->render('showEmployeeReport', ['entireData' => $getData]);
    }

    /*---------------------- All required methods end here----------------------*/


    public function actionReceivebalajipurchaseorder()
    {
        $this->layout = 'metronicLayout';
        $purchaseOrder = new BalajiPurchaseOrder();
        $getOrders = $purchaseOrder->getPendingOrders();
        return $this->render('receiveBalajiPO', ['entireData' => $getOrders]);
    }

    public function actionGetbalajipurchaseorder()
    {
        $this->layout = 'metronicLayout';
        $data = Yii::$app->request->post();
        $purchaseOrder = new BalajiPurchaseOrder();
        $getData = $purchaseOrder->getDetails($data);
        return $this->render('receivedBalajiPO', ['entireData' => $getData]);
    }

    public function actionAcceptbalajipurchaseorder()
    {
        $data = Yii::$app->request->post();
        $order = new BalajiPurchaseOrder();
        $order->acceptOrder($data);
        AppUtility::jsonResponse("true");

    }

    public function actionRejectbalajipurchaseorder()
    {
        $data = Yii::$app->request->post();
        $order = new BalajiPurchaseOrder();
        $order->rejectOrder($data);
        AppUtility::jsonResponse("true");
    }

    public function actionGetquotationeventname()
    {
        $data = Yii::$app->request->post();
        $quotation = new BalajiQuotation();
        $eventName = $quotation->getDetails($data);
        AppUtility::jsonResponse($eventName);

    }

    public function actionGetaudioquotationeventname()
    {
        $data = Yii::$app->request->post();
        $quotation = new AudioQuotation();
        $eventName = $quotation->getDetails($data);
        AppUtility::jsonResponse($eventName);

    }

    public function actionBalajiinvoice()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $tax = new BalajiTaxInvoice();
        $audioWorkOrder = new BalajiWorkOrder();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxTaxNumber = $tax->getMaxTaxNumber();
        $getCompletedOrders = $audioWorkOrder->getCompletedOrders();
        return $this->render('balajiInvoice', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'completedOrders' => $getCompletedOrders]);
    }

    public function actionSavebalajiinvoice()
    {
        $data = Yii::$app->request->post();
        $order = new BalajiTaxInvoice();
        $order->saveData($data);
        $session = new Session();
        $session['taxBalajiData'] = $data;
        return $this->redirect('/prolights/web/site/showtaxbalajibill');
    }

    public function actionShowtaxbalajibill()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $data = $session['taxBalajiData'];
        return $this->render('balajiTaxbill', ['entireData' => $data]);
    }

    public function actionBalajidupbill()
    {
        $this->layout = 'metronicLayout';
        $tax = new BalajiTaxInvoice();
        $getTaxNumbers = $tax->getTaxNumbers();
        return $this->render('balajiDuplicateBill', ['taxData' => $getTaxNumbers]);
    }

    public function actionShowbalajidupbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $tax = new BalajiTaxInvoice();
        $getData = $tax->getDetails($data);
        return $this->render('generateDuplicateBalajiBill', ['entireData' => $getData]);
    }

    public function actionBalajireports()
    {
        $this->layout = 'metronicLayout';
        return $this->render('balajiReports');
    }

    public function actionGetbalajireport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new BalajiTaxInvoice();
        $getData = $tax->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateBalajiInvoiceReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionBalajiporeports()
    {
        $this->layout = 'metronicLayout';
        return $this->render('balajiPOReports');
    }

    public function actionGetbalajiporeport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new BalajiPurchaseOrder();
        $getData = $tax->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateBalajiPOReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionShowaudioworkorder()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $invoice = $session['audioWorkOrder'];
//        AppUtility::dump($invoice);
        $workOrder = new AudioWorkOrder();
//        $getData = $workOrder->getDataForBill($invoice);
        return $this->render('generateAudioWorkOrder', ['entireData' => $invoice]);
    }

    public function actionNewstock()
    {
        $this->layout = 'metronicLayout';
        $product = new Product();
        $getStock = $product->getItemsWithStock();
        return $this->render('newStock', ['entireData' => $getStock]);
    }

    public function actionUpdatestock()
    {
        $data = Yii::$app->request->post();
        $product = new Product();
        $product->updateStock($data);
        return $this->redirect('/prolights/web/site/newstock');
    }

    public function actionCurrentstock()
    {
        $this->layout = 'metronicLayout';
        $product = new Product();
        $getData = $product->getCurrentStock();
        return $this->render('showCurrentStock', ['entireData' => $getData]);
    }

    public function actionAudiostockreturn()
    {
        $this->layout = 'metronicLayout';
        $order = new AudioWorkOrder();
        $getList = $order->getAcceptedList();
        return $this->render('audioStockReturn', ['entireData' => $getList]);
    }

    public function actionGetaudioorderdetails()
    {
        $data = Yii::$app->request->post();
        $order = new AudioWorkOrder();
        $entireData = $order->getDetails($data);
        AppUtility::jsonResponse($entireData);
    }

    public function actionAddreturnedstock()
    {
        $data = Yii::$app->request->post();
        $order = new AudioWorkOrder();
        $order->addReturnedStock($data);
        AppUtility::jsonResponse("true");
    }

    public function actionAudioinvoice()
    {
        $this->layout = 'metronicLayout';
        $clientData = new ClientData();
        $tax = new AudioTaxInvoice();
        $audioWorkOrder = new AudioWorkOrder();
        $getClients = $clientData->getClients();
        $getMaxClientCode = $clientData->getMaxClientCode();
        $getMaxTaxNumber = $tax->getMaxTaxNumber();
        $getCompletedOrders = $audioWorkOrder->getCompletedOrders();
        return $this->render('audioInvoice', ['clientData' => $getClients, 'taxNumber' => $getMaxTaxNumber, 'maxclientCode' => $getMaxClientCode, 'completedOrders' => $getCompletedOrders]);
    }

    public function actionSaveaudioinvoice()
    {
        $data = Yii::$app->request->post();
        $order = new AudioTaxInvoice();
        $order->saveData($data);
        $session = new Session();
        $session['taxAudioData'] = $data;
        return $this->redirect('/prolights/web/site/showtaxaudiobill');
    }

    public function actionShowtaxaudiobill()
    {
        $this->layout = 'printLayout';
        $session = new Session();
        $data = $session['taxAudioData'];
        return $this->render('audioTaxbill', ['entireData' => $data]);
    }

    public function actionAudiodupbill()
    {
        $this->layout = 'metronicLayout';
        $tax = new AudioTaxInvoice();
        $getTaxNumbers = $tax->getTaxNumbers();
        return $this->render('audioDuplicateBill', ['taxData' => $getTaxNumbers]);
    }

    public function actionShowaudiodupbill()
    {
        $data = Yii::$app->request->post();
        $this->layout = 'printLayout';
        $tax = new AudioTaxInvoice();
        $getData = $tax->getDetails($data);
        return $this->render('generateDuplicateAudioBill', ['entireData' => $getData]);
    }

    public function actionAudioreports()
    {
        $this->layout = 'metronicLayout';
        return $this->render('audioReports');
    }

    public function actionGetaudioreport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new AudioTaxInvoice();
        $getData = $tax->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateAudioInvoiceReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionCancelaudioinvoice()
    {
        $this->layout = 'metronicLayout';
        $tax = new AudioTaxInvoice();
        $getTaxNumbers = $tax->getCompletedInvoices();
        return $this->render('cancelAudioBill', ['taxData' => $getTaxNumbers]);
    }

    public function actionModifyaudioinvoice()
    {
        $data = Yii::$app->request->post();
        $audioInvoice = new AudioTaxInvoice();
        $audioInvoice->cancelInvoice($data);
        return $this->redirect('/prolights/web/site/cancelaudioinvoice');
    }

    public function actionCancelbalajiinvoice()
    {
        $this->layout = 'metronicLayout';
        $tax = new BalajiTaxInvoice();
        $getTaxNumbers = $tax->getCompletedInvoices();
        return $this->render('cancelBalajiBill', ['taxData' => $getTaxNumbers]);
    }

    public function actionModifybalajiinvoice()
    {
        $data = Yii::$app->request->post();
        $audioInvoice = new BalajiTaxInvoice();
        $audioInvoice->cancelInvoice($data);
        return $this->redirect('/prolights/web/site/cancelbalajiinvoice');
    }

    public function actionAudioporeports()
    {
        $this->layout = 'metronicLayout';
        return $this->render('audioPOReport');
    }

    public function actionGetaudioporeport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new AudioPurchaseOrder();
        $getData = $tax->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateAudioPOReport', ['entireData' => $getData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionAudiopovsinvoicereports()
    {
        $this->layout = 'metronicLayout';
        return $this->render('audioPOvsInvoiceReports');
    }

    public function actionGetaudiopovsinvoicereport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new AudioTaxInvoice();
        $getData = $tax->getDataForReport($data);
        $poData = new AudioPurchaseOrder();
        $getPOData = $poData->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateAudioPOvsInvoiceReport', ['entireData' => $getData, 'entirePOData' => $getPOData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }

    public function actionBalajipovsinvoicereports()
    {
        $this->layout = 'metronicLayout';
        return $this->render('balajiPOvsInvoiceReports');
    }

    public function actionGetbalajipovsinvoicereport()
    {
        $this->layout = 'printLayout';
        $data = Yii::$app->request->post();
        $tax = new BalajiTaxInvoice();
        $getData = $tax->getDataForReport($data);
        $poData = new BalajiPurchaseOrder();
        $getPOData = $poData->getDataForReport($data);
        $fromDate = $data['fromDate'];
        $toDate = $data['toDate'];
        return $this->render('generateBalajiPOvsInvoiceReport', ['entireData' => $getData, 'entirePOData' => $getPOData, 'fromDate' => $fromDate, 'toDate' => $toDate]);
    }


}


