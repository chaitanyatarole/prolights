<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Audio Pro Lights Invoice Reports</span>
                    </div>
                </div>
                <div class="portlet-body" style="min-height: 350px;">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/prolights/web/billing/getaudioreport"
                              class="form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-2"
                                           style="text-align: right!important;">From
                                        Date</label>

                                    <div class="col-sm-3">
                                        <input type="date" class="form-control" name="fromDate" required="">
                                    </div>
                                    <label class="control-label col-sm-2"
                                           style="text-align: right!important;">To
                                        Date</label>

                                    <div class="col-sm-3">
                                        <input type="date" class="form-control" name="toDate" required="">
                                    </div>
                                </div>
                                <br>
                                <br>
                                <br>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-5"></div>
                                        <div class="col-md-7">
                                            <button type="submit" class="btn green" id="submit">Submit
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>