<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Balaji Events - Tax Invoice</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/prolights/web/site/savebalajiinvoice"
                              class="form-horizontal" ng-controller="balajiInvoice"
                              onsubmit="submitButton.disabled = true; return true;return smoke.alert('Data Saved');">
                            <div class="form-body">
                                <!--                                <div class="form-group">-->
                                <!--                                    <label class="control-label col-sm-2">Select Work Order</label>-->
                                <!---->
                                <!--                                    <div class="col-sm-8">-->
                                <!--                                        <select id="workOrderList" name="workOrderList" ng-model="selectWorkOrder"-->
                                <!--                                                class="form-control input-large select2me select2-offscreen"-->
                                <!--                                                data-placeholder="Select..." tabindex="-1" title=""-->
                                <!--                                                ng-change="getDetails()">-->
                                <!--                                            <option value="">------------------------Select-------------------------->
                                <!--                                            </option>-->
                                <!--                                            --><?php
                                //                                            foreach ($completedOrders as $key) {
                                //                                                $invoiceNumber = $key['invoice_no'];
                                //                                                $eventName = $key['eventName'];
                                //                                                echo "<option value='$invoiceNumber'>$invoiceNumber -- $eventName</option>";
                                //
                                //                                            }
                                //                                            ?>
                                <!--                                        </select>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Existing Clients</label>

                                    <div class="col-sm-8">
                                        <select id="clientNameCode" name="clientNameCode"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($clientData as $key) {
                                                $clientName = $key['client_name'];
                                                $clientCode = $key['clientcode'];
                                                echo "<option value='$clientCode'>$clientCode -- $clientName</option>";

                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>
                                <input type="hidden" class="form-control" name="ccodeno" id="ccodeno"
                                       value="<?php echo $maxclientCode; ?>"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" ng-model="formInfo.clientCode"
                                               name="clientcode" readonly value=""
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" class="form-control" ng-model="formInfo.date" name="date"
                                               value="" id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Invoice Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               ng-init="<?php echo $taxNumber; ?>"
                                               value="<?php echo $taxNumber; ?>"
                                               name="taxNumber" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" ng-model="formInfo.clientName" value=""
                                               onkeyup="gencode(this.value);"
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="address" ng-model="formInfo.address"
                                                  name="address"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" ng-model="formInfo.number" value=""
                                               name="number" id="number">
                                    </div>
                                    <label class="control-label col-sm-2">Email</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" ng-model="formInfo.email" value=""
                                               name="email" id="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Event Name</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" ng-model="formInfo.eventName" value=""
                                               name="eventName"
                                               id="eventName">
                                    </div>
                                    <label class="control-label col-sm-2">Event Location</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" ng-model="formInfo.eventLocation"
                                               value="" name="eventLocation"
                                               id="eventLocation">
                                    </div>
                                </div>
<!--                                <div class="form-group">-->
<!--                                    <label class="control-label col-sm-2">Total Budget</label>-->
<!---->
<!--                                    <div class="col-sm-3">-->
<!--                                        <input type="text" class="form-control" ng-model="formInfo.availableBudget"-->
<!--                                               value=""-->
<!--                                               name="availableBudget"-->
<!--                                               id="availableBudget">-->
<!--                                    </div>-->
<!--                                    <label class="control-label col-sm-2">Available Budget</label>-->
<!---->
<!--                                    <div class="col-sm-3">-->
<!--                                        <input type="text" class="form-control" ng-model="formInfo.totalBudget"-->
<!--                                               value="" name="totalBudget"-->
<!--                                               id="totalBudget">-->
<!--                                    </div>-->
<!--                                </div>-->
                                <!--                                <div ng-bind-html="renderHtml(body)" compile="body">-->
                                <!---->
                                <!---->
                                <!--                                </div>-->
                                <input type="hidden" value="1" name="counter" id="counter">
                                <input type="hidden" value="1" name="counter" id="counter">
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="button" class="btn green" onclick="addRowInTaxSimple();">Add
                                                Row
                                            </button>
                                            <button type="button" class="btn default" onclick="deleteRowInTaxSimple();">
                                                Delete
                                                Row
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="">
                                    <table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        </thead>
                                        <tbody id="taxTable">
                                        <tr>
                                            <td style="width: 60px;">
                                                1
                                            </td>
                                            <td style="width: 400px;">
                                                <input type="text" required id="productCode-1"
                                                       name="particular[]"
                                                       class="form-control" style="width: 400px !important;">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" required
                                                       onkeyup="calculationInTaxSimple(1);calculateRemainingBalance();"
                                                       onkeypress="return isNumber(event);" id="quantity-1"
                                                       name="quantity[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" name="rate[]" id="rate-1" required
                                                       onkeyup="calculationInTaxSimple(1);calculateRemainingBalance();"
                                                       class="form-control"
                                                       onkeypress="return isNumber1(event);">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" readonly id="amount-1" name="amount[]"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Total Amount</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="totamt" id="totamt"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                                SELECT VAT
                                            </td>
                                            <td style="width: 60px;">
                                                <select class="form-control" id="selectVAT" name="selectVAT"
                                                        onchange="calculateTotalInTaxSimple()">
                                                    <option value="0">none</option>
                                                    <option value="5">5%</option>
                                                    <option value="12.5">12.5%</option>
                                                    <option value="14.5">14.5%</option>
                                                </select>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="vat" value="" id="vat"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Grand Total</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="grandtot" id="grandtot"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="button" class="btn green" id="submit" name="submitButton"
                                                    onclick="validateSimpleTaxData();">Submit
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>