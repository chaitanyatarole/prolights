<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Balaji Events Work Order</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/prolights/web/site/savebalajiworkorder"
                              class="form-horizontal" ng-controller="balajiWorkOrder"
                              onsubmit="submitButton.disabled = true; return true;return smoke.alert('Data Saved');">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Select Quotation Number</label>

                                    <div class="col-sm-8">
                                        <select id="quotationList" name="quotationList" ng-model="selectQuotationNumber"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title=""
                                                ng-change="getDetails()">
                                            <option value="">------------------Select Quotation------------------------
                                            </option>
                                            <?php
                                            foreach ($pendingQuotations as $key) {
                                                $invoiceNumber = $key['quotationNumber'];
                                                $clientName = $key['clientName'];
                                                echo "<option value='$invoiceNumber'>$invoiceNumber -- $clientName</option>";

                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Existing Clients</label>

                                    <div class="col-sm-8">
                                        <select id="clientNameCode" name="clientNameCode"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($clientData as $key) {
                                                $clientName = $key['client_name'];
                                                $clientCode = $key['clientcode'];
                                                echo "<option value='$clientCode'>$clientCode -- $clientName</option>";

                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>
                                <input type="hidden" class="form-control" name="ccodeno" id="ccodeno"
                                       value="<?php echo $maxclientCode; ?>"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" ng-model="formInfo.clientCode"
                                               name="clientcode" readonly value=""
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" class="form-control" name="date" value="" id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Invoice Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo $taxNumber; ?>" name="taxNumber" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" ng-model="formInfo.clientName" value=""
                                               onkeyup="gencode(this.value);"
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" ng-model="formInfo.address" id="address"
                                                  name="address"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" ng-model="formInfo.number" value=""
                                               name="number" id="number">
                                    </div>
                                    <label class="control-label col-sm-2">Email</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" ng-model="formInfo.email" value=""
                                               name="email" id="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Event Name</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" ng-model="formInfo.eventName" name="eventName"
                                               id="eventName">
                                    </div>
                                    <label class="control-label col-sm-2">Event Location</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="eventLocation"
                                               id="eventLocation">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Total Budget</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" ng-model="formInfo.totalBudget" name="totalBudget"
                                               onkeypress="return isNumber1(event);"
                                               id="totalBudget">
                                    </div>
                                    <label class="control-label col-sm-2">Available Budget</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="availableBudget"
                                               onkeypress="return isNumber1(event);"
                                               id="availableBudget">
                                    </div>
                                </div>
                                <div ng-bind-html="renderHtml(body)" compile="body"></div>

                                <input type="hidden" value="1" name="counter" id="counter">

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="button" class="btn green" id="submit" name="submitButton"
                                                    onclick="validateOrderData();">Submit
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>