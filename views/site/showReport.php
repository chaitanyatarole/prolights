<script type="text/javascript">
    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }

    function close123() {
        window.location = '/sai/web/';
    }

</script>
<br>
<div class="row">
    <div class="col-xs-12" style="padding-left: 0px;padding-right: 0px;">
        <div class="col-xs-5" style="padding-left: 0px;padding-right: 0px;">
            <p><span style="font-size: 18px;"><strong>SAI FABRICATION</strong></span><br>Vishal Nagar,New D.P. Road,Nandgurde School,<br> Pimple Nilakh,<br>Wakad, Pune - 411027 <br>
                Tel.:020-6500 8835 <br>Mob: 9049058835, 9405601129 <br>
                Email :sai.fabricator3335@gmail.com
            </p>
        </div>
        <div class="col-xs-7" style="padding-left: 0px;padding-right: 0px;">
            <img src="/sai/web/img/sai.png" style="height: 100px;float: right">
        </div>
    </div>
    <br>
    <div class="">
        <table>
            <tr>
                <td><strong>Invoice Number : </strong><?php echo $entireData[0]['invoice_no']; ?></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Client Name: </strong><?php echo $entireData[0]['client_name']; ?></td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Pending Amount Report</strong></td>
            </tr>
        </table>
        <table class="" style="width: 1000px;">
            <thead>
            <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-left: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Sr.No
            </th>
            <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Date
            </th>
            <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Payment Mode
            </th>
            <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Reference No.
            </th>
            <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Mobile
            </th>
            <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Grand Total
            </th>
            <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Amount Paid
            </th>
            <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Amount Remaining
            </th>
            </thead>
            <tbody>
            <?php
            foreach ($entireData as $key => $value) {
                $serialNumber = $key + 1; ?>
                <tr>
                    <td style="border-left:solid 1px #7B7B7B;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $serialNumber; ?></td>
                    <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid"><?php echo date('d-m-Y', strtotime($value['date'])); ?></td>
                    <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid"><?php echo $value['paymentMode']; ?></td>
                    <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid"><?php echo $value['referenceNumber']; ?></td>
                    <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid"><?php echo $value['number']; ?></td>
                    <td style="text-align: right;border-bottom: 1px solid #7B7B7B;border-right: 1px solid"><?php echo $value['grandTotal']; ?></td>
                    <td style="text-align: right;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;border-left: 1px solid #7B7B7B"><?php echo $value['paidAmount']; ?></td>
                    <td style="text-align: right;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['remainingAmount']; ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <p align="center" style='margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                        onclick="printing();" value="Print"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
    </p>
</div>