<?php //\app\commands\AppUtility::dump($productCode); ?>
<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Modify Receipt</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/sai/web/site/updatemodifiedreceiptdata" onsubmit="return $.LoadingOverlay('show');"
                              class="form-horizontal" onsubmit="return smoke.alert('Data Modified');">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Existing Clients</label>

                                    <div class="col-sm-8">
                                        <select id="clientNameCode" name="clientNameCode"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($clientData as $key) {
                                                $clientName = $key['client_name'];
                                                $clientCode = $key['clientcode'];
                                                echo "<option value='$clientCode'>$clientCode -- $clientName</option>";

                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>
                                <input type="hidden" class="form-control" name="ccodeno" id="ccodeno"
                                       value="<?php echo $maxclientCode; ?>"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="clientcode" readonly
                                               value="<?php echo $entireData[0]['clientCode']; ?>"
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" class="form-control" name="date"
                                               value="<?php echo $entireData[0]['date']; ?>" id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Receipt Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo $entireData[0]['challanNumber']; ?>"
                                               name="challanNumber"
                                               id="date">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['clientName']; ?>"
                                               onkeyup=""
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="address"
                                                  name="address"><?php echo $entireData[0]['address']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['mobile']; ?>" name="number"
                                               id="number">
                                    </div>
                                    <label class="control-label col-sm-2">Email</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['email']; ?>" name="email" id="email">
                                    </div>
                                </div>
                                <div class="">
                                    <table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Total Amount
                                        </th>
                                        <th>
                                            Received Amount
                                        </th>
                                        <th>
                                            Balance
                                        </th>
                                        </thead>
                                        <tbody id="receiptTable">
                                        <?php foreach ($entireData as $key => $value) { ?>
                                            <tr>
                                                <td style="width: 60px;">
                                                    <?php echo $key + 1; ?>
                                                </td>
                                                <td style="width: 300px;">
                                                    <input type="text" readonly name="particular[]" id=""
                                                           style="width: 400px;"
                                                           value="<?php echo $value['particular']; ?>"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 150px;padding-right: 10px;">
                                                    <input type="text" name="totalAmount[]"
                                                           onkeyup="calculateBalance(<?php echo $key + 1; ?>);"
                                                           id="totalAmount-<?php echo $key + 1; ?>"
                                                           style="width: 200px;"
                                                           value="<?php echo $value['totalAmount']; ?>"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 150px;padding-right: 10px;">
                                                    <input type="text" name="amount[]"
                                                           onkeyup="calculateBalance(<?php echo $key + 1; ?>);"
                                                           id="amount-<?php echo $key + 1; ?>" style="width: 200px;"
                                                           value="<?php echo $value['amount']; ?>" class="form-control">
                                                </td>
                                                <td style="width: 150px;padding-right: 10px;">
                                                    <input type="text" style="width: 200px;" readonly name="balance[]"
                                                           id="balance-<?php echo $key + 1; ?>"
                                                           value="<?php echo $value['balanceAmount']; ?>"
                                                           class="form-control">
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="submit" class="btn green" id="submit"
                                                    onclick="validateModifyTaxData();">Update
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>