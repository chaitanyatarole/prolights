<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Receipt</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/sai/web/site/savereceipt" onsubmit="return $.LoadingOverlay('show');"
                              class="form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Existing Clients</label>

                                    <div class="col-sm-8">
                                        <select id="clientNameCode" name="clientNameCode"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($clientData as $key) {
                                                $clientName = $key['client_name'];
                                                $clientCode = $key['clientcode'];
                                                echo "<option value='$clientCode'>$clientCode -- $clientName</option>";

                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>
                                <input type="hidden" class="form-control" name="ccodeno" id="ccodeno"
                                       value="<?php echo $maxclientCode; ?>"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="clientcode" readonly value=""
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" required="" class="form-control" name="date" value=""
                                               id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Invoice Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo $challanNumber; ?>" name="challanNumber" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" required class="form-control" value=""
                                               onkeyup="gencode(this.value);"
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" required id="address" name="address">

                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" required class="form-control" value="" name="number"
                                               id="number">
                                    </div>
                                    <label class="control-label col-sm-2">Email</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="email" id="email">
                                    </div>
                                </div>

                                <input type="hidden" id="counter" name="counter" value="1">

                                <div class="">
                                    <table class="mytable" id="receiptDataTable" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Total Amount
                                        </th>
                                        <th>
                                            Received Amount
                                        </th>
                                        <th>
                                            Balance
                                        </th>
                                        </thead>
                                        <tbody id="receiptTable">
                                        <tr>
                                            <td style="width: 60px;">
                                                1
                                            </td>
                                            <td style="width: 300px;">
                                                <select id="productCode-1" name="particular[]" required
                                                        class="form-control select2me select2-offscreen"
                                                        data-placeholder="Select..." onchange=""
                                                        tabindex="-1" title="" style="width: 400px !important;">
                                                    <option value="">
                                                    </option>
                                                    <?php foreach ($productCode as $key => $value) {
                                                        $name = $value['product_name'];
                                                        $id = $value['id'];
                                                        ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td style="width: 150px;padding-right: 10px;">
                                                <input type="text" name="totalAmount[]" required
                                                       id="totalAmount-1" style="width: 200px;" value=""
                                                       onkeyup="calculateBalance(1);" class="form-control">
                                            </td>
                                            <td style="width: 150px;padding-right: 10px;">
                                                <input type="text" name="amount[]" required
                                                       id="amount-1" style="width: 200px;"
                                                       onkeyup="calculateBalance(1);" value="" class="form-control">
                                            </td>
                                            <td style="width: 150px;padding-right: 10px;">
                                                <input type="text" style="width: 200px;" readonly name="balance[]"
                                                       id="balance-1" value=""
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 60px;">
                                                2
                                            </td>
                                            <td style="width: 300px;">
                                                <select id="productCode-1" name="particular[]"
                                                        class="form-control select2me select2-offscreen"
                                                        data-placeholder="Select..." onchange=""
                                                        tabindex="-1" title="" style="width: 400px !important;">
                                                    <option value="">
                                                    </option>
                                                    <?php foreach ($productCode as $key => $value) {
                                                        $name = $value['product_name'];
                                                        $id = $value['id'];
                                                        ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td style="width: 150px;padding-right: 10px;">
                                                <input type="text" name="totalAmount[]"
                                                       id="totalAmount-2" style="width: 200px;"
                                                       onkeyup="calculateBalance(2);" value="" class="form-control">
                                            </td>
                                            <td style="width: 150px;padding-right: 10px;">
                                                <input type="text" name="amount[]"
                                                       id="amount-2" style="width: 200px;"
                                                       onkeyup="calculateBalance(2);" value="" class="form-control">
                                            </td>
                                            <td style="width: 150px;padding-right: 10px;">
                                                <input type="text" style="width: 200px;" readonly name="balance[]"
                                                       id="balance-2" value=""
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="submit" class="btn green" id="submit">Submit
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>