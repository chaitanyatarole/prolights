<script type="text/javascript">
    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }

    function close123() {
        window.location = '/prolights/web/site/wagesreport';
    }

</script>
<div class="container">
    <div class="row">
        <br>

        <div class="col-xs-12" style="padding-left: 0px;padding-right: 0px;">
            <div class="col-xs-5" style="padding-left: 0px;padding-right: 0px;">
                <p>Shop N0.456, Opp.Hemu Kalani Market,
                    Pimpri, Pune - 411017 <br>Mob. 9822666911 Email :audienergynprolights@gmail.com</p>
            </div>
            <div class="col-xs-7" style="padding-left: 0px;padding-right: 0px;">
                <img src="/prolights/web/img/balajilogo.jpg" style="height: 100px;float: right">
            </div>
        </div>
        <br>
        <h4 style="text-align: center">Employee Wages Report</h4>

        <div class="col-xs-12">
            <div class="pull-left">
                From Date : <?php echo date('d-m-Y', strtotime($fromDate)); ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                To Date : <?php echo date('d-m-Y', strtotime($toDate)); ?>
            </div>
        </div>
        <div class="">
            <table class="" style="font-size: 12px;">
                <thead>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-left: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Reference Number
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Date
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Employee Name
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Mobile
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Email
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    WageForMonth
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Remark
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Wage
                </th>
                </thead>
                <tbody>
                <?php $invoiceNumber = '';
                $grandTotal = '';
                foreach ($entireData as $key => $value) {
                    $serialNumber = $key + 1; ?>
                    <tr>
                        <?php if ($invoiceNumber == $value['recordNumber']) {
                            ?>
                            <td style="border-left: 1px solid #7B7B7B;text-align: center;width: 80px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;"></td>
                            <td style="width: 80px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;"></td>
                            <td style="width: 150px;border-right: 1px solid #7B7B7B;border-bottom: 1px solid #7B7B7B"></td>
                            <td style="width: 100px;border-right: 1px solid #7B7B7B;border-bottom: 1px solid #7B7B7B"></td>
                            <td style="width: 300px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;border-left: 1px solid #7B7B7B"><?php ?></td>
                            <td style="text-align: center;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['remark']; ?></td>
                            <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['wageForMonth']; ?></td>
                            <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['wage']; ?></td>
                            <?php
                            $grandTotal += $value['wage'];
                        } else {
                            $date = date('d-m-Y', strtotime($value['wageReceivedOn']));
                            ?>

                            <td style="border-left: 1px solid #7B7B7B;text-align: center;width: 80px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['recordNumber']; ?></td>
                            <td style="text-align: center;width: 150px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $date; ?></td>
                            <td style="width: 150px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['employeeName']; ?></td>
                            <td style="text-align: center;width: 100px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['mobile']; ?></td>
                            <td style="width: 300px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['email']; ?></td>
                            <td style="text-align: center;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['remark']; ?></td>
                            <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['wageForMonth']; ?></td>
                            <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['wage']; ?></td>
                            <?php $invoiceNumber = $value['recordNumber'];
                            $grandTotal += $value['wage'];
                        } ?>
                    </tr>
                <?php } ?>
                <tr>
                    <td style="text-align: center;width: 50px;border-left: solid 1px #7B7B7B;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                    <td style="text-align: center;width: 150px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                    <td style="width: 300px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                    <td style="width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                    <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                    <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                    <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                        <strong>Grand Total</strong>
                    </td>
                    <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                        <strong><?php echo $grandTotal; ?></strong></td>
                </tr>
                </tbody>
            </table>
        </div>
        <p align="center" style='margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                            onclick="printing();" value="Print"/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
        </p>
    </div>
</div>