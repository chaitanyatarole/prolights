<script type="text/javascript">
    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }

    function close123() {
        window.location = '/sai/web/site/taxreports';
    }

</script>
<?php
$fromDate;
$toDate;
$d = date('d-m-Y', strtotime($fromDate));
$dd = date('d-m-Y', strtotime(($toDate)));

?>
<div class="container">
    <div class="row">
        <br>

        <div class="col-xs-12" style="padding-left: 0px;padding-right: 0px;">
            <div class="col-xs-5" style="padding-left: 0px;padding-right: 0px;">
                <p>Vishal Nagar,New D.P. Road,Nandgurde School,<br> Pimple Nilakh,<br>Wakad, Pune - 411027 <br>
                    Tel.:020-6500 8835 <br>Mob: 9049058835, 9405601129 <br>
                    Email :sai.fabricator3335@gmail.com
                </p>
            </div>
            <div class="col-xs-7" style="padding-left: 0px;padding-right: 0px;">
                <img src="/sai/web/img/sai.png" style="height: 100px;float: right">
            </div>
        </div>
        <br>
        <h4 style="text-align: center">Tax Report</h4>

        <div class="col-xs-12">
            <div class="pull-left">
                From Date : <?php echo date('d-m-Y', strtotime($fromDate)); ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                To Date : <?php echo date('d-m-Y', strtotime($toDate)); ?>
            </div>
        </div>
        </table>
        <div align="center">
            <table border="1px;" align="center;" width="1000px;" style="font-size: 15px;">
                <thead>
                <th style="text-align: center">
                    Invoice No
                </th>
                <th style="text-align: center">
                    Date
                </th>
                <th style="text-align: center">
                    Client Name
                </th>
                <th style="text-align: center">
                    Mobile
                </th>
                <th style="text-align: center">
                    Particulars
                </th>
                <th style="text-align: center">
                    Size
                </th>
                <th style="text-align: center">
                    Qty
                </th>
                <th style="text-align: center">
                    Per SqFt
                </th>
                <th style="text-align: center">
                    Rate
                </th>
                <th style="text-align: center">
                    Amount
                </th>
                <th>
                    Total Amt
                </th>
                <th style="text-align: center">
                    V. T.(12.5%)<br>
                    Amt
                </th>
                <th style="text-align: center">
                    Net Amt
                </th>
                </thead>
                <tbody>
                <?php $invoiceNumber = '';
                $grandTotal = '';
                $totalvtAmount = '';
                $totalnetAmount = '';
                foreach ($entireData as $key => $value) {
                    $serialNumber = $key + 1; ?>
                    <tr>
                    <?php if ($invoiceNumber == $value['invoice_no']) {

                        $dimensionOne1 = isset($value['dimension_one']) ? $value['dimension_one'] : '';
                        $dimensionTwo1 = isset($value['dimension_two']) ? $value['dimension_two'] : '';
                        $displayDimension1 = $dimensionOne1 . ' x ' . $dimensionTwo1; ?>


                        <td></td>
                        <td style="text-align: center; width: 100px;"></td>
                        <td></td>
                        <td></td>
                        <td><?php echo $value['perticular_name']; ?></td>
                        <td style=" text-align: center; width: 100px;"><?php echo $displayDimension1; ?></td>
                        <td style=" text-align: right;"><?php echo $value['quantity']; ?></td>
                        <td style=" text-align: right;"><?php echo $value['per_squareft']; ?></td>
                        <td style=" text-align: right; width: 60px;"><?php echo $value['rate']; ?></td>
                        <td style=" text-align: right;"><?php echo $value['amount']; ?></td>
                        <td style=" text-align: right;"><?php ?></td>
                        <td style=" text-align: right;"><?php ?></td>
                        <td style=" text-align: right;"><?php ?></td>
                        </tr>
                        <tr>
                    <?php } else {
                        $date = date('d-m-Y', strtotime($value['date']));
                        $dimensionOne = isset($value['dimension_one']) ? $value['dimension_one'] : '';
                        $dimensionTwo = isset($value['dimension_two']) ? $value['dimension_two'] : '';
                        $displayDimension = $dimensionOne . ' x ' . $dimensionTwo; ?>


                        <td style="text-align: center;border-left: solid 1px #7B7B7B;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;"><?php echo $value['invoice_no']; ?></td>
                        <td style="width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;text-align: center; width: 100px;"><?php echo $date; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['client_name']; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['phone_no']; ?></td>
                        <td style="width: 100px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['perticular_name']; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: center; width: 100px;"><?php echo $displayDimension; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: right;"><?php echo $value['quantity']; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: right;"><?php echo $value['per_squareft']; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: right; width: 60px;"><?php echo $value['rate']; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: right;"><?php echo $value['amount']; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: right;"><?php echo $value['total_amount']; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: right;"><?php echo $value['vat_tax_amount']; ?></td>
                        <td style="border-right:solid 1px #7B7B7B;border-bottom: 1px solid #7B7B7B; text-align: right;"><?php echo $value['net_amount'];

                        $grandTotal += $value['total_amount'];
                        $totalvtAmount += $value['vat_tax_amount'];
                        $totalnetAmount += $value['net_amount'];

                        ?>
                        <?php $invoiceNumber = $value['invoice_no'];
                    } ?>
                    </tr>
                <?php } ?>
                <tr>

                    <td colspan="10" style="text-align:right;"><strong>Grand Total</strong></td>

                    <td style="text-align: right"><strong><?php echo $grandTotal; ?></strong></td>
                    <td style="text-align: right;"><strong><?php echo $totalvtAmount; ?></strong></td>
                    <td style="text-align: right;"><strong><?php echo $totalnetAmount; ?></strong></td>


                </tr>
                </tbody>
            </table>
        </div>
        <br/>
        <br/>
        <?php
        $query = "select * from tax_invoice WHERE date BETWEEN '$fromDate' AND '$toDate' AND status = 'cancelled' ";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        ?>
        <?php if (!empty($result)) { ?>
            <div align="center">
                <h4 style="text-align: center">Cancelled Tax Reports</h4>
                <table border="1px;" align="center;" width="1000px;" style="font-size: 15px;">
                    <thead>
                    <th style="text-align: center">
                        Invoice No
                    </th>
                    <th style="text-align: center">
                        Date
                    </th>
                    <th style="text-align: center">
                        Client Name
                    </th>
                    <th style="text-align: center">
                        Mobile
                    </th>
                    <th style="text-align: center">
                        Particulars
                    </th>
                    <th style="text-align: center">
                        Size
                    </th>
                    <th style="text-align: center">
                        Qty
                    </th>
                    <th style="text-align: center">
                        Per SqFt
                    </th>
                    <th style="text-align: center">
                        Rate
                    </th>
                    <th style="text-align: center">
                        Amount
                    </th>
                    <th>
                        Total Amt
                    </th>
                    <th style="text-align: center">
                        V. T.(12.5%)<br>
                        Amt
                    </th>
                    <th style="text-align: center">
                        Net Amt
                    </th>
                    </thead>
                    <tbody>
                    <?php $invoiceNumber = '';
                    $grandTotal = '';
                    $totalvtAmount = '';
                    $totalnetAmount = '';
                    foreach ($result as $key => $value) {
                        $serialNumber = $key + 1; ?>
                        <tr>
                        <?php if ($invoiceNumber == $value['invoice_no']) {

                            $dimensionOne1 = isset($value['dimension_one']) ? $value['dimension_one'] : '';
                            $dimensionTwo1 = isset($value['dimension_two']) ? $value['dimension_two'] : '';
                            $displayDimension1 = $dimensionOne1 . ' x ' . $dimensionTwo1; ?>


                            <td></td>
                            <td style="text-align: center; width: 100px;"></td>
                            <td></td>
                            <td></td>
                            <td><?php echo $value['perticular_name']; ?></td>
                            <td style=" text-align: center; width: 100px;"><?php echo $displayDimension1; ?></td>
                            <td style=" text-align: right;"><?php echo $value['quantity']; ?></td>
                            <td style=" text-align: right;"><?php echo $value['per_squareft']; ?></td>
                            <td style=" text-align: right; width: 60px;"><?php echo $value['rate']; ?></td>
                            <td style=" text-align: right;"><?php echo $value['amount']; ?></td>
                            <td style=" text-align: right;"><?php ?></td>
                            <td style=" text-align: right;"><?php ?></td>
                            <td style=" text-align: right;"><?php ?></td>
                            </tr>
                            <tr>
                        <?php } else {
                            $date = date('d-m-Y', strtotime($value['date']));
                            $dimensionOne = isset($value['dimension_one']) ? $value['dimension_one'] : '';
                            $dimensionTwo = isset($value['dimension_two']) ? $value['dimension_two'] : '';
                            $displayDimension = $dimensionOne . ' x ' . $dimensionTwo; ?>


                            <td style="text-align: center;border-left: solid 1px #7B7B7B;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;"><?php echo $value['invoice_no']; ?></td>
                            <td style="width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;text-align: center; width: 100px;"><?php echo $date; ?></td>
                            <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['client_name']; ?></td>
                            <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['phone_no']; ?></td>
                            <td style="width: 100px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['perticular_name']; ?></td>
                            <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: center; width: 100px;"><?php echo $displayDimension; ?></td>
                            <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: right;"><?php echo $value['quantity']; ?></td>
                            <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: right;"><?php echo $value['per_squareft']; ?></td>
                            <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: right; width: 60px;"><?php echo $value['rate']; ?></td>
                            <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: right;"><?php echo $value['amount']; ?></td>
                            <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: right;"><?php echo $value['total_amount']; ?></td>
                            <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B; text-align: right;"><?php echo $value['vat_tax_amount']; ?></td>
                            <td style="border-right:solid 1px #7B7B7B;border-bottom: 1px solid #7B7B7B; text-align: right;"><?php echo $value['net_amount'];

                            $grandTotal += $value['total_amount'];
                            $totalvtAmount += $value['vat_tax_amount'];
                            $totalnetAmount += $value['net_amount'];

                            ?>
                            <?php $invoiceNumber = $value['invoice_no'];
                        } ?>
                        </tr>
                    <?php } ?>
                    <tr>

                        <td colspan="10" style="text-align:right;"><strong>Grand Total</strong></td>

                        <td style="text-align: right"><strong><?php echo $grandTotal; ?></strong></td>
                        <td style="text-align: right;"><strong><?php echo $totalvtAmount; ?></strong></td>
                        <td style="text-align: right;"><strong><?php echo $totalnetAmount; ?></strong></td>


                    </tr>
                    </tbody>
                </table>
            </div>
        <?php } ?>

        <p align="center" style='margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                            onclick="printing();" value="Print"/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
        </p>
    </div>
</div>