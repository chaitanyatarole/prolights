<div class="row">

    <div class="row">
        <div class="col-md-12 center" style="height: 50%;">
            <img src="/prolights/web/img/prologo.png" style="height: 130px;">
        </div>
        <br>
        <br>
        <!--/span-->
    </div>
    <!--/row-->
    <div class="row">
        <div class="well col-md-5 center login-box">
            <div class="alert alert-info" id="displaymessage">
                <?php echo isset($model) ? $model : 'Please login with your Username and Password' ?>
            </div>
            <form class="form-horizontal" name="loginForm" action="/prolights/web/site" method="post">
                <div class="input-group input-group-lg">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                    <input type="text" name="Login[username]" class="form-control" required
                           placeholder="Username">
                </div>
                <div class="clearfix"></div>
                <br>

                <div class="input-group input-group-lg">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                    <input type="password" name="Login[password]" class="form-control" required
                           placeholder="Password">
                </div>
                <div class="clearfix"></div>

                <div class="clearfix"></div>

                <p class="center col-md-5">
                    <button type="submit" class="btn btn-primary">Login</button>
                </p>
            </form>
        </div>
        <!--/span-->
    </div>
    <!--/row-->
</div><!--/fluid-row-->
