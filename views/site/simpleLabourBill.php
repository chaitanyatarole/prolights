<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Labour Bill</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/sai/web/site/savesimplelabourbill" onsubmit="return $.LoadingOverlay('show');"
                              class="form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Existing Clients</label>

                                    <div class="col-sm-8">
                                        <select id="clientNameCode" name="clientNameCode"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($clientData as $key) {
                                                $clientName = $key['client_name'];
                                                $clientCode = $key['clientcode'];
                                                echo "<option value='$clientCode'>$clientCode -- $clientName</option>";

                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>
                                <input type="hidden" class="form-control" name="ccodeno" id="ccodeno"
                                       value="<?php echo $maxclientCode; ?>"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="clientcode" readonly value=""
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" required="" class="form-control" name="date" value=""
                                               id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Invoice Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo $taxNumber; ?>" name="challan_number" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" required class="form-control" value=""
                                               onkeyup="gencode(this.value);"
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                    <textarea class="form-control" required id="address" name="address"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" required class="form-control" value="" name="number"
                                               id="number">
                                    </div>
                                    <label class="control-label col-sm-2">Email</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="email" id="email">
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="button" class="btn green" onclick="addRowInTaxSimple();">Add
                                                Row
                                            </button>
                                            <button type="button" class="btn default" onclick="deleteRowInTaxSimple();">
                                                Delete
                                                Row
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="counter" name="counter" value="1">

                                <div class="">
                                    <table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        </thead>
                                        <tbody id="taxTable">
                                        <tr>
                                            <td style="width: 60px;">
                                                1
                                            </td>
                                            <td style="width: 400px;">
                                                <select id="productCode-1" name="particular[]" required
                                                        class="form-control select2me select2-offscreen"
                                                        data-placeholder="Select..." onchange=""
                                                        tabindex="-1" title="" style="width: 400px !important;">
                                                    <option value="">
                                                    </option>
                                                    <?php foreach ($productCode as $key => $value) {
                                                        $name = $value['product_name'];
                                                        $id = $value['id'];
                                                        ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" required
                                                       onkeyup="calculationInTaxSimple(1);calculateRemainingBalance();"
                                                       onkeypress="return isNumber(event);" id="quantity-1"
                                                       name="quantity[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" name="rate[]" id="rate-1" required
                                                       onkeyup="calculationInTaxSimple(1);calculateRemainingBalance();"
                                                       class="form-control"
                                                       onkeypress="return isNumber1(event);">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" readonly id="amount-1" name="amount[]"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Total Amount</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="totamt" id="totamt"
                                                       class="form-control">
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="submit" class="btn green" id="submit">Submit
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>