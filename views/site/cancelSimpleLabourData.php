<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Cancel Labour Bill</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/sai/web/site/changelabourstatus" onsubmit="return $.LoadingOverlay('show');"
                              class="form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
                                </div>
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>
                                <input type="hidden" class="form-control" name="ccodeno" id="ccodeno"
                                       value="<?php echo $maxclientCode; ?>"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="clientcode" readonly
                                               value="<?php echo $entireData[0]['clientcode']; ?>"
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" readonly class="form-control" name="date"
                                               value="<?php echo $entireData[0]['date']; ?>" id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Invoice Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo $entireData[0]['invoice_no']; ?>" name="taxNumber"
                                               id="date">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo $entireData[0]['client_name']; ?>"
                                               onkeyup=""
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" readonly id="address"
                                                  name="address"><?php echo $entireData[0]['address']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" readonly
                                               value="<?php echo $entireData[0]['phone_no']; ?>" name="number"
                                               id="number">
                                    </div>
                                    <label class="control-label col-sm-2">Email</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" readonly
                                               value="<?php echo $entireData[0]['email']; ?>" name="email" id="email">
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                    </div>
                                </div>
                                <div class="">
                                    <table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        </thead>
                                        <tbody id="taxTable">
                                        <?php foreach ($entireData as $key => $value) {
                                            $perticular = $value['perticular'];
                                            $perticular_name = $value['perticular_name'];
                                            $quantity = $value['quantity'];
                                            $persqft = $value['per_squareft'];
                                            $rate = $value['rate'];
                                            $amount = $value['amount'];
                                            $serialno = $key + 1;
                                            ?>
                                            <tr>

                                                <td align="center"><label><?php echo $serialno; ?></label></td>

                                                <td style="width: 400px;">
                                                    <input type="hidden" name="perticular[]"
                                                           onkeyup="calculateRemainingBalance();"
                                                           id="perticular-<?php echo $serialno ?> " readonly
                                                           value="<?php echo $perticular; ?>" class="form-control">
                                                    <input type="text" readonly value="<?php echo $perticular_name; ?>"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 250px;padding-right: 10px;">
                                                    <input type="text" readonly
                                                           onkeyup="calculationInTaxSimple(<?php echo $serialno; ?>);calculateRemainingBalance();"
                                                           value="<?php echo $quantity; ?>"
                                                           onkeypress="return isNumber(event);"
                                                           id="quantity-<?php echo $serialno ?>"
                                                           name="quantity[]"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 250px;;">
                                                    <input type="text" readonly name="rate[]" id="rate-<?php echo $serialno ?>"
                                                           value="<?php echo $rate; ?>"
                                                           onkeyup="calculationInTaxSimple(<?php echo $serialno ?>);calculateRemainingBalance();"
                                                           class="form-control"
                                                           onkeypress="return isNumber(event);">
                                                </td>
                                                <td style="width: 250px;;">
                                                    <input type="text" readonly id="amount-<?php echo $serialno ?>"
                                                           name="amount[]" value="<?php echo $amount; ?>"
                                                           class="form-control">
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Total Amount</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="totamt" id="totamt"
                                                       value="<?php echo $entireData[0]['total_amount']; ?>"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="submit" class="btn green" id="submit">Cancel Invoice
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>