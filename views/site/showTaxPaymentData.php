<?php //\app\commands\AppUtility::dump($entireData); ?>
<script type="text/javascript">
    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }

    function close123() {
        window.location = '/growel/web/site/taxpaymentreport';
    }

</script>
<h4 style="text-align: center">Tax Part Payment Report</h4>
<div class="row">
    <div class="container">
        <div class="">
            <table class="table">
                <thead>
                <th style="border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-left: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Sr.No
                </th>
                <th style="border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Invoice Number
                </th>
                <th style="border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Date
                </th>
                <th style="border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Client Name
                </th>

                <th style="border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Grand Total
                </th>
                <th style="border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Paid Amount
                </th>
                <th style="border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Remaining Amount
                </th>
                </thead>
                <tbody>
                <?php $invoiceNumber = '';
                $totalRemainingAmount = 0;
                foreach ($entireData as $key => $value) {
                    $serialNumber = $key + 1;
                    $date = date('d-m-Y', strtotime($value['date']));
                    if ($invoiceNumber != $value['invoice_no']) {
                        $lastAccessedKey = isset($entireData[$key - 1]['remainingAmount']) ? $entireData[$key - 1]['remainingAmount'] : 0;
                        $totalRemainingAmount += $lastAccessedKey;
                    }
                    ?>
                    <tr>
                        <td style="border-left: solid 1px #7B7B7B;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $serialNumber; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['invoice_no']; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $date; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['client_name']; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['grandTotal']; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['paidAmount']; ?></td>
                        <td style="border-right:solid 1px #7B7B7B;border-bottom: 1px solid #7B7B7B"><?php echo $value['remainingAmount']; ?></td>
                        <?php $invoiceNumber = $value['invoice_no'];
                        if ($value === end($entireData)) {
                            $lastRemainingAmount = $value['remainingAmount'];
                            $totalRemainingAmount += $lastRemainingAmount;
                        }
                        ?>
                    </tr>
                <?php } ?>
                <?php if (!empty($entireData)) { ?>
                    <tr>
                        <td style="border-left: solid 1px #7B7B7B;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $serialNumber + 1; ?></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                        <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">Total Remaining
                            Amount
                        </td>
                        <td style="border-right:solid 1px #7B7B7B;border-bottom: 1px solid #7B7B7B"><?php echo $totalRemainingAmount; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <p align="center" style='margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                            onclick="printing();" value="Print"/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
        </p>
    </div>
</div>