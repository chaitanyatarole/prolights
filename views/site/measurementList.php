<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Measurement List</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/sai/web/site/savemeasurementlist" onsubmit="return $.LoadingOverlay('show');"
                              class="form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Existing Clients</label>

                                    <div class="col-sm-8">
                                        <select id="clientNameCode" name="clientNameCode"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($clientData as $key) {
                                                $clientName = $key['client_name'];
                                                $clientCode = $key['clientcode'];
                                                echo "<option value='$clientCode'>$clientCode -- $clientName</option>";

                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>
                                <input type="hidden" class="form-control" name="ccodeno" id="ccodeno"
                                       value="<?php echo $maxclientCode; ?>"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="clientcode" readonly value=""
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" class="form-control" required name="date" value="" id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Measurement Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo $challanNumber; ?>" name="challanNumber" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" required value=""
                                               onkeyup="gencode(this.value);"
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="address" name="address">

                                        </textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="number" id="number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Your Ch. No.</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="yourChallanNumber"
                                               id="yourChallanNumber">
                                    </div>
                                    <label class="control-label col-sm-2">P.O. Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="poNumber" id="poNumber">
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="button" class="btn green" onclick="addRowInMeasurement();">Add
                                                Row
                                            </button>
                                            <button type="button" class="btn default"
                                                    onclick="deleteRowInMeasurement();">
                                                Delete
                                                Row
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <table class="mytable" id="measurementDataTable" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Size - Dim 1
                                        </th>
                                        <th>
                                            Size - Dim 2
                                        </th>
                                        <th>
                                            Per Sq Ft
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Total Sq Ft
                                        </th>
                                        <th>
                                            RFT
                                        </th>
                                        </thead>
                                        <tbody id="measurementTable">
                                        <tr>
                                            <td style="width: 60px;">
                                                <strong>1</strong>
                                            </td>
                                            <td style="width: 400px;">
                                                <select id="productCode-1" name="particular[]" required
                                                        class="form-control select2me select2-offscreen"
                                                        data-placeholder="Select..." onchange="getDimensions(1);"
                                                        tabindex="-1" title="" style="width: 400px !important;">
                                                    <option value="">
                                                    </option>
                                                    <?php foreach ($materialList as $key => $value) {
                                                        $name = $value['name'];
                                                        $id = $value['id'];
                                                        ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" name="dimensionOne[]" required
                                                       onkeyup="calculatePersqftInDelivery(1);"
                                                       id="dimensionOne-1" value="" class="form-control">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" name="dimensionTwo[]" id="dimensionTwo-1" value="" required
                                                       onkeyup="calculatePersqftInDelivery(1);" class="form-control">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" required onkeyup="" readonly
                                                       onkeypress="return isNumber(event);" id="persqft-1"
                                                       name="persqft[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" required
                                                       onkeypress="return isNumber(event);" id="quantity-1"
                                                       onkeyup="calculatePersqftInDelivery(1);"
                                                       name="quantity[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" required onkeyup="" readonly
                                                       onkeypress="return isNumber(event);" id="totalsqft-1"
                                                       name="totalsqft[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" onkeyup="" required
                                                       onkeypress="return isNumber1(event);" id="rft-1"
                                                       name="rft[]"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <input type="hidden" name="counter" value="1" id="counter">

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="submit" class="btn green" id="submit">Submit
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>