<style type="text/css">

    /* this is the important part (should be used in HTML head): */
    .pagebreak {
        page-break-after: always;
    }

</style>
<script>
    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }

    function close123() {
        window.location = '/sai/web/site/receipt';
    }

</script>
<div class="container" style="float: none">
    <div class="row" style="border: 1px solid;">
        <div class="row">
            <div class="col-xs-2 col-sm-1 text-center" style="padding-right: 0px;padding-left: 0px;">
                <img class="" src="/sai/web/img/sai.png"
                     style="max-height: 100px;margin: 0 auto;margin-top: 4px;margin-left: 20px;">
            </div>
            <div class="col-xs-8 col-sm-9" style="padding-left: 0px;padding-right: 0px;">
                <p style="text-decoration: underline;text-align: center;font-size: 16px;margin-bottom: 0px;"><strong>RECEIPT</strong>
                </p>

                <p style="text-align: center;font-size: 28px;margin-bottom: 0px;"><strong>SAI FABRICATION</strong></p>

                <p style="text-align: center;margin-bottom: 0px;font-size: 12px;"><strong>All Types S.S. Railing,
                        Collapsible
                        Gate, Main Gate,
                        Safety Door,<br>
                        Balcony Door, Balcony Shade, Grill, Window, Rolling Shutter & Fabrication works</strong></p>

                <p style="text-align: center;font-size: 12px;">Vishal Nagar, New D.P Road, Nandgurde School, Pimple
                    Nilakh,
                    Wakad, Pune - 411027 <br>Tel. 020-65008835 Email :sai.fabricator3335@gmail.com</p>
            </div>
            <div class="col-xs-2 col-sm-2"
                 style="padding-right: 0px;padding-left: 0px;position: absolute;right: 0;z-index: 9999;">
                <span style="font-size: 13px;"><strong>Mob: 9049058835<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9405601129</strong></span>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-left" style="width: 50%;padding-bottom: 10px;">
                <table border="1" width="99%" style="height:80px;font-size: 12px">
                    <tr>
                        <td style="border-bottom: hidden">
                            <strong style="padding-left: 4px;">To,</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="400px" rowspan="5" align="left" style="">
                            <label style="padding-left: 20px;">
                                <?php echo $entireData['cname']; ?><br>
                                <?php echo $entireData['address']; ?>
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="pull-right" style="width: 50%;padding-bottom: 10px;">
                <table border="1" width="100%" style="height: 80px;font-size: 12px;">
                    <tr>
                        <td style="border-right: hidden"><strong style="padding-left: 4px;">Receipt
                                No: </strong><?php echo $entireData['challanNumber']; ?></td>
                        <td style=""><strong>Date
                                : </strong><?php echo date('d-m-Y', strtotime($entireData['date'])); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-12" style="float: none">
            <table border='1' class="table" width='100%' align='center' bordercolor='#000000'
                   style='border-collapse:collapse;font-size: 11px;margin-bottom: 0px;'>

                <tr height="20px">
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Sr.No'
                                                                                readonly='true'
                                                                                style='border-style : hidden;text-align: center;width:40px; font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Particular'
                                                                                readonly='true'
                                                                                style='border-style : hidden;text-align: center;width:180px;font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Total Amount'
                                                                                readonly='true'
                                                                                style='border-style : hidden;text-align: center;width:90px;font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Received Amount'
                                                                                readonly='true'
                                                                                style='border-style : hidden;text-align: center;width:100px;font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Balance Amount'
                                                                                readonly='true'
                                                                                style='border-style : hidden;text-align: center;width:90px;font-weight:bold'/>
                    </th>
                </tr>

                <?php $description = $entireData['particular'];
                $counter = '';
                foreach ($description as $key => $value) {
                    $query = "select product_name from product where id = '$value'";
                    $result = Yii::$app->db->createCommand($query)->queryAll();
                    $name = isset($result[0]['product_name']) ? $result[0]['product_name'] : '';
                    $totalAmount = isset($entireData['totalAmount'][$key]) ? $entireData['totalAmount'][$key] : '';
                    $receivedAmount = isset($entireData['amount'][$key]) ? $entireData['amount'][$key] : '';
                    $balanceAmount = isset($entireData['balance'][$key]) ? $entireData['balance'][$key] : '';
                    ?>
                    <tr>
                        <td align="center" style="border-top: 1px solid"><?php echo $key + 1; ?></td>
                        <td style="border-top: 1px solid"><?php echo $name; ?></td>
                        <td align="center" style="border-top: 1px solid">
                            <?php echo $totalAmount; ?>
                        </td>
                        <td align="center" style="border-top: 1px solid">
                            <?php echo $receivedAmount; ?>
                        </td>
                        <td align="center" style="border-top: 1px solid"><input type='text'
                                                                                value='<?php echo $balanceAmount; ?>'
                                                                                readonly="readonly"
                                                                                style='border-style : hidden;text-align: center;width:80px;'/>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <div class="col-sm-12">
            <div class="pull-left" style="width: 69%;border: none">
                <table width="95%" style="height:80px;">

                </table>
            </div>
            <div class="pull-right" style="width: 31%; border: none;">
                <table width="95%" style="height: 80px;">
                    <tr style="height: 40px;">
                        <td style="padding-left: 40px; font-size: 13px;"><strong>For SAI FABRICATION</strong></td>
                    </tr>
                    <tr style="height: 20px;">
                        <td></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 40px; height: 30px;">
                            <strong style="font-size: 13px;">Authorised Signature</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <p align="center" style=' margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                         onclick="printing();" value="Print"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
    </p>
    &nbsp;
    <br/>
    <br/>
</div>

