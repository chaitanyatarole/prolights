<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Audio Pro Lights Purchase Order</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/prolights/web/site/saveaudiopurchaseorder"
                              class="form-horizontal"
                              onsubmit="submitButton.disabled = true; return true;return smoke.alert('Data Saved');">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Existing Vendor</label>

                                    <div class="col-sm-8">
                                        <select id="vendorNameCode" name="clientNameCode"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($clientData as $key) {
                                                $clientName = $key['client_name'];
                                                $clientCode = $key['clientcode'];
                                                echo "<option value='$clientCode'>$clientCode -- $clientName</option>";

                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>
                                <input type="hidden" class="form-control" name="ccodeno" id="ccodeno"
                                       value="<?php echo $maxclientCode; ?>"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Vendor Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="clientcode" readonly value=""
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" class="form-control" name="date" value="" id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Invoice Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo $taxNumber; ?>" name="taxNumber" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Vendor Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" value=""
                                               onkeyup="gencodevendor(this.value);"
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="address" name="address"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="number" id="number">
                                    </div>
                                    <label class="control-label col-sm-2">Email</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="email" id="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Quotation Number</label>

                                    <div class="col-sm-3">
                                        <select id="audioquotationNameCode" name="quotationNameCode"
                                                class="form-control select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="">
                                            <option value="">---------------Select Quotation-------------------
                                            </option>
                                            <?php
                                            foreach ($quotationList as $key) {
                                                $quotationNumber = $key['quotationNumber'];
                                                $clientName = $key['clientName'];
                                                echo "<option value='$quotationNumber'>$quotationNumber  --  $clientName </option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <label class="control-label col-sm-2">Event Name</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="eventName"
                                               id="eventName">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Delivery Location</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="deliveryLocation"
                                               id="deliveryLocation">
                                    </div>
                                    <label class="control-label col-sm-2">Return Date</label>

                                    <div class="col-sm-3">
                                        <input type="date" class="form-control" value="" name="returnDate"
                                               id="returnDate">
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="button" class="btn green"
                                                    onclick="addRowInPurchaseOrderBO();">
                                                Add
                                                Row
                                            </button>
                                            <button type="button" class="btn default" onclick="deleteRowInWorkOrder();">
                                                Delete
                                                Row
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="">
                                    <table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Item
                                        </th>
                                        <th>
                                            Qty.No
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        </thead>
                                        <tbody id="taxTable">
                                        <tr>
                                            <td style="width: 60px;">
                                                1
                                            </td>
                                            <td style="width: 530px;">
                                                <select id="productCode-1" name="description[]"
                                                        class="form-control select2me select2-offscreen"
                                                        data-placeholder="Select..." onchange=""
                                                        tabindex="-1" title="" style="width: 530px !important;">
                                                    <option value="">
                                                    </option>
                                                    <?php foreach ($productCode as $key => $value) {
                                                        $name = $value['name'];
                                                        $id = $value['id'];
                                                        ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td style="width: 300px;padding-right: 10px;">
                                                <input type="text"
                                                       onkeyup=""
                                                       onkeypress="return isNumber(event);" id="quantity-1"
                                                       name="quantity[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 200px;padding-right: 10px;">
                                                <input type="text" onkeyup="calculationInWorkOrder(1);"
                                                       onkeypress="return isNumber(event);" id="rate-1" name="rate[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 200px;;">
                                                <input type="text" readonly id="amount-1" name="amount[]"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Total Amount</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="totamt" id="totamt"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <input type="hidden" value="1" name="counter" id="counter">

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="button" class="btn green" id="submit" name="submitButton"
                                                    onclick="validatePurchaseOrderData();">Submit
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>