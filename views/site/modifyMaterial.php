<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Modify Material</span>
                    </div>
                </div>
                <div class="portlet-body" style="min-height: 350px;">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/sai/web/site/updatematerial" onsubmit="return $.LoadingOverlay('show');"
                              class="form-horizontal">
                            <div class="form-body">
                                <div id="adddynamicitems">
                                    <div class="form-group">

                                        <div class="col-sm-4" style="text-align: center;"><label
                                                class="control-label"><strong>Material</strong></label></div>
                                        <div class="col-sm-4" style="text-align: center;"><label
                                                class="control-label"><strong>Dimension One</strong></label></div>
                                        <div class="col-sm-4" style="text-align: center"><label
                                                class="control-label"><strong>Dimension Two</strong></label></div>
                                        <?php foreach ($entireData as $key => $value) {
                                            $product = $value['name'];
                                            $dimensionOne = $value['dimensionOne'];
                                            $dimensionTwo = $value['dimensionTwo'];
                                            $recordID = $value['id'];
                                            ?>
                                            <div class="form-group">
                                                <div class="col-sm-4"><input type="text" style="width: 100%;"
                                                                             class="form-control"
                                                                             value="<?php echo $product; ?>"
                                                                             name="product[]"></div>
                                                <div class="col-sm-4"><input type="text" style="width: 100%;"
                                                                             class="form-control"
                                                                             value="<?php echo $dimensionOne; ?>"
                                                                             name="dimensionOne[]"></div>
                                                <div class="col-sm-4"><input type="text" style="width: 100%;"
                                                                             class="form-control"
                                                                             value="<?php echo $dimensionTwo; ?>"
                                                                             name="dimensionTwo[]">
                                                </div>
                                                <input type="hidden" value="<?php echo $recordID; ?>" name="recordId[]">
                                            </div>
                                        <?php } ?>

                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <button type="submit" style="float: right;" class="btn green"
                                                            id="submit">Submit
                                                    </button>
                                                </div>
                                                <div class="col-sm-6">
                                                    <button type="button" class="btn default"
                                                            onclick="location.reload();">Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>