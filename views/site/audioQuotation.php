<div class="container-fluid" xmlns="http://www.w3.org/1999/html">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Audio Pro Lights Quotation</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/prolights/web/site/saveaudioquotation"
                              onsubmit="submitButton.disabled = true;return true;return $.LoadingOverlay('show');"
                              class="form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Existing Clients</label>

                                    <div class="col-sm-8">
                                        <select id="clientNameCode" name="clientNameCode"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($clientData as $key) {
                                                $clientName = $key['client_name'];
                                                $clientCode = $key['clientcode'];
                                                echo "<option value='$clientCode'>$clientCode -- $clientName</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>
                                <input type="hidden" class="form-control" name="ccodeno" id="ccodeno"
                                       value="<?php echo $maxclientCode; ?>"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="clientcode" readonly value=""
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" class="form-control" required name="date" value="" id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Quotation Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo $quotationNumber; ?>" name="quotationNumber" id="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" required value=""
                                               onkeyup="gencode(this.value);"
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="address" name="address"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="number" id="number">
                                    </div>
                                    <label class="control-label col-sm-2">Email</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="email" id="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Event Name</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="eventName"
                                               id="eventName">
                                    </div>
                                    <label class="control-label col-sm-2">Event Date</label>

                                    <div class="col-sm-3">
                                        <input type="date" class="form-control" value="" name="eventDate" id="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Event Location</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="eventLocation" id="eventLocation">
                                    </div>
                                    <label class="control-label col-sm-2">Total Budget</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" name="totalBudget"
                                               id="totalBudget">
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="" id="stageandEntry">
                                    <div class="col-sm-12">
                                        <label class="col-sm-2"></label>
                                        <label class="col-sm-1"></label>
                                        <label class="control-label col-sm-1"><strong>Category</strong></label>
                                        <div class="col-sm-4">
                                            <input type="text" name="categoryStage" class="form-control center-block"
                                                   value="Stage and Entry Decor (Reception)">
                                        </div>
                                        <div class="col-sm-4"></div>
                                    </div>
                                    <br>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-9 col-md-9">
                                                <button type="button" class="btn green" onclick="addRowInStage();">
                                                    Add
                                                    Row
                                                </button>
                                                <button type="button" class="btn default"
                                                        onclick="deleteRowInStage();">
                                                    Delete
                                                    Row
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="mytable" id="quotationDataTableStage" cellpadding="10"
                                           cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Notes
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        </thead>
                                        <tbody id="quotationTableStage">
                                        <tr>
                                            <td style="width: 60px;">
                                                1
                                            </td>
                                            <td style="width: 400px;">
                                                <select id="productCodeStage-1" name="particularStage[]"
                                                        class="form-control select2me select2-offscreen"
                                                        data-placeholder="Select..."
                                                        onchange="calculateTotalInStage();"
                                                        tabindex="-1" title="" style="width: 400px !important;">
                                                    <option value="">
                                                    </option>
                                                    <?php foreach ($materialList as $key => $value) {
                                                        $name = $value['name'];
                                                        $id = $value['id'];
                                                        ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" id="notesStage-1"
                                                       name="notesStage[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" required onkeyup="calculationInQuotationStage(1);"
                                                       onkeypress="return isNumber(event);" id="quantityStage-1"
                                                       name="quantityStage[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" required name="rateStage[]" id="rateStage-1"
                                                       onkeyup="calculationInQuotationStage(1);" class="form-control"
                                                       onkeypress="return isNumber1(event);">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" readonly id="amountStage-1" name="amountStage[]"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Total Amount</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="totamtStage" id="totamtstage"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="" id="tentWorks">
                                    <div class="col-sm-12">
                                        <label class="col-sm-2"></label>
                                        <label class="col-sm-1"></label>
                                        <label class="control-label col-sm-1"><strong>Category</strong></label>
                                        <div class="col-sm-4">
                                            <input type="text" name="categoryTent" class="form-control center-block"
                                                   value="Tent Works and sitting arrangements">
                                        </div>
                                        <div class="col-sm-4"></div>
                                    </div>
                                    <br>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-9 col-md-9">
                                                <button type="button" class="btn green" onclick="addRowInTent();">
                                                    Add
                                                    Row
                                                </button>
                                                <button type="button" class="btn default"
                                                        onclick="deleteRowInTent();">
                                                    Delete
                                                    Row
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="mytable" id="quotationDataTableTent" cellpadding="10"
                                           cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Notes
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        </thead>
                                        <tbody id="quotationTableTent">
                                        <tr>
                                            <td style="width: 60px;">
                                                1
                                            </td>
                                            <td style="width: 400px;">
                                                <select id="productCodeTent-1" name="particularTent[]" required
                                                        class="form-control select2me select2-offscreen"
                                                        data-placeholder="Select..."
                                                        onchange="calculateTotalInQuotationTent();"
                                                        tabindex="-1" title="" style="width: 400px !important;">
                                                    <option value="">
                                                    </option>
                                                    <?php foreach ($materialList as $key => $value) {
                                                        $name = $value['name'];
                                                        $id = $value['id'];
                                                        ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" id="notesTent-1"
                                                       name="notesTent[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" required onkeyup="calculationInQuotationTent(1);"
                                                       onkeypress="return isNumber(event);" id="quantityTent-1"
                                                       name="quantityTent[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" required name="rateTent[]" id="rateTent-1"
                                                       onkeyup="calculationInQuotationTent(1);" class="form-control"
                                                       onkeypress="return isNumber1(event);">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" readonly id="amountTent-1" name="amountTent[]"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Total Amount</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="totamtTent" id="totamttent"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="" id="lightsSound">
                                    <div class="col-sm-12">
                                        <label class="col-sm-2"></label>
                                        <label class="col-sm-1"></label>
                                        <label class="control-label col-sm-1"><strong>Category</strong></label>
                                        <div class="col-sm-4">
                                            <input type="text" name="categoryLight" class="form-control center-block"
                                                   value="Lights Sound">
                                        </div>
                                        <div class="col-sm-4"></div>
                                    </div>
                                    <br>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-9 col-md-9">
                                                <button type="button" class="btn green" onclick="addRowInLight();">
                                                    Add
                                                    Row
                                                </button>
                                                <button type="button" class="btn default"
                                                        onclick="deleteRowInLight();">
                                                    Delete
                                                    Row
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="mytable" id="quotationDataTableLight" cellpadding="10"
                                           cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Notes
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        </thead>
                                        <tbody id="quotationTableLight">
                                        <tr>
                                            <td style="width: 60px;">
                                                1
                                            </td>
                                            <td style="width: 400px;">
                                                <select id="productCodeLight-1" name="particularLight[]" required
                                                        class="form-control select2me select2-offscreen"
                                                        data-placeholder="Select..."
                                                        onchange="calculateTotalInQuotationLight();"
                                                        tabindex="-1" title="" style="width: 400px !important;">
                                                    <option value="">
                                                    </option>
                                                    <?php foreach ($materialList as $key => $value) {
                                                        $name = $value['name'];
                                                        $id = $value['id'];
                                                        ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" id="notesLight-1"
                                                       name="notesLight[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" required onkeyup="calculationInQuotationLight(1);"
                                                       onkeypress="return isNumber(event);" id="quantityLight-1"
                                                       name="quantityLight[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" required name="rateLight[]" id="rateLight-1"
                                                       onkeyup="calculationInQuotationLight(1);" class="form-control"
                                                       onkeypress="return isNumber1(event);">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" readonly id="amountLight-1" name="amountLight[]"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Total Amount</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="totamtLight" id="totamtLight"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="" id="technical">
                                    <div class="col-sm-12">
                                        <label class="col-sm-2"></label>
                                        <label class="col-sm-1"></label>
                                        <label class="control-label col-sm-1"><strong>Category</strong></label>
                                        <div class="col-sm-4">
                                            <input type="text" name="categoryTech" class="form-control center-block"
                                                   value="Technical">
                                        </div>
                                        <div class="col-sm-4"></div>
                                    </div>
                                    <br>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-9 col-md-9">
                                                <button type="button" class="btn green" onclick="addRowInTechnical();">
                                                    Add
                                                    Row
                                                </button>
                                                <button type="button" class="btn default"
                                                        onclick="deleteRowInTech();">
                                                    Delete
                                                    Row
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="mytable" id="quotationDataTableTech" cellpadding="10"
                                           cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>Notes</th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        </thead>
                                        <tbody id="quotationTableTech">
                                        <tr>
                                            <td style="width: 60px;">
                                                1
                                            </td>
                                            <td style="width: 400px;">
                                                <select id="productCodeTech-1" name="particularTech[]" required
                                                        class="form-control select2me select2-offscreen"
                                                        data-placeholder="Select..."
                                                        onchange="calculateTotalInTech();"
                                                        tabindex="-1" title="" style="width: 400px !important;">
                                                    <option value="">
                                                    </option>
                                                    <?php foreach ($materialList as $key => $value) {
                                                        $name = $value['name'];
                                                        $id = $value['id'];
                                                        ?>
                                                        <option value="<?php echo $id; ?>"><?php echo $name ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" id="notesTech-1"
                                                       name="notesTech[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" required onkeyup="calculationInQuotationTech(1);"
                                                       onkeypress="return isNumber(event);" id="quantityTech-1"
                                                       name="quantityTech[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" required name="rateTech[]" id="rateTech-1"
                                                       onkeyup="calculationInQuotationTech(1);" class="form-control"
                                                       onkeypress="return isNumber1(event);">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" readonly id="amountTech-1" name="amountTech[]"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Total Amount</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="totamtTech" id="totamtTech"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="" id="specialWork">
                                    <div class="col-sm-12">
                                        <label class="col-sm-2"></label>
                                        <label class="col-sm-1"></label>
                                        <label class="control-label col-sm-1"><strong>Category</strong></label>
                                        <div class="col-sm-4">
                                            <input type="text" name="categorySpc" class="form-control center-block"
                                                   value="Special Work">
                                        </div>
                                        <div class="col-sm-4"></div>
                                    </div>
                                    <br>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-9 col-md-9">
                                                <button type="button" class="btn green" onclick="addRowInSpecial();">
                                                    Add
                                                    Row
                                                </button>
                                                <button type="button" class="btn default"
                                                        onclick="deleteRowInSpc();">
                                                    Delete
                                                    Row
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="mytable" id="quotationDataTableSpc" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Notes
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        </thead>
                                        <tbody id="quotationTableSpc">
                                        <tr>
                                            <td style="width: 60px;">
                                                1
                                            </td>
                                            <td style="width: 400px;">
                                                <!--                                                <select id="productCodeSpc-1" name="particularSpc[]" required-->
                                                <!--                                                        class="form-control select2me select2-offscreen"-->
                                                <!--                                                        data-placeholder="Select..."-->
                                                <!--                                                        onchange="calculateTotalInQuotationSpc();"-->
                                                <!--                                                        tabindex="-1" title="" style="width: 400px !important;">-->
                                                <!--                                                    <option value="">-->
                                                <!--                                                    </option>-->
                                                <!--                                                    --><?php //foreach ($materialList as $key => $value) {
                                                //                                                        $name = $value['name'];
                                                //                                                        $id = $value['id'];
                                                //                                                        ?>
                                                <!--                                                        <option value="-->
                                                <?php //echo $id; ?><!--">--><?php //echo $name ?><!--</option>-->
                                                <!--                                                    --><?php //} ?>
                                                <!--                                                </select>-->
                                                <input type="text" class="form-control" id="productCodeSpc-1"
                                                       name="particularSpc[]" onchange="calculationInQuotationSpc()"
                                                       style="width: 400px;">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" id="notesSpc-1"
                                                       name="notesSpc[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" required onkeyup="calculationInQuotationSpc(1);"
                                                       onkeypress="return isNumber(event);" id="quantitySpc-1"
                                                       name="quantitySpc[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" required name="rateSpc[]" id="rateSpc-1"
                                                       onkeyup="calculationInQuotationSpc(1);" class="form-control"
                                                       onkeypress="return isNumber1(event);">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" readonly id="amountSpc-1" name="amountSpc[]"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Total Amount</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="totamtSpc" id="totamtSpc"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="" id="other">
                                    <div class="col-sm-12">
                                        <label class="col-sm-2"></label>
                                        <label class="col-sm-1"></label>
                                        <label class="control-label col-sm-1"><strong>Category</strong></label>
                                        <div class="col-sm-4">
                                            <input type="text" name="categoryOther" class="form-control center-block"
                                                   value="Other">
                                        </div>
                                        <div class="col-sm-4"></div>
                                    </div>
                                    <br>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-9 col-md-9">
                                                <button type="button" class="btn green" onclick="addRowInOtr();">
                                                    Add
                                                    Row
                                                </button>
                                                <button type="button" class="btn default"
                                                        onclick="deleteRowInOtr();">
                                                    Delete
                                                    Row
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="mytable" id="quotationDataTableOtr" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Notes
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        </thead>
                                        <tbody id="quotationTableOtr">
                                        <tr>
                                            <td style="width: 60px;">
                                                1
                                            </td>
                                            <td style="width: 400px;">
                                                <!--                                                <select id="productCodeOtr-1" name="particularOtr[]" required-->
                                                <!--                                                        class="form-control select2me select2-offscreen"-->
                                                <!--                                                        data-placeholder="Select..."-->
                                                <!--                                                        onchange="calculateTotalInQuotationOtr();"-->
                                                <!--                                                        tabindex="-1" title="" style="width: 400px !important;">-->
                                                <!--                                                    <option value="">-->
                                                <!--                                                    </option>-->
                                                <!--                                                    --><?php //foreach ($materialList as $key => $value) {
                                                //                                                        $name = $value['name'];
                                                //                                                        $id = $value['id'];
                                                //                                                        ?>
                                                <!--                                                        <option value="-->
                                                <?php //echo $id; ?><!--">--><?php //echo $name ?><!--</option>-->
                                                <!--                                                    --><?php //} ?>
                                                <!--                                                </select>-->
                                                <input type="text" class="form-control" id="productCodeOtr-1"
                                                       name="particularOtr[]" onchange="calculationInQuotationOtr()"
                                                       style="width: 400px;">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" id="notesOtr-1"
                                                       name="notesOtr[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" required onkeyup="calculationInQuotationOtr(1);"
                                                       onkeypress="return isNumber(event);" id="quantityOtr-1"
                                                       name="quantityOtr[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" required name="rateOtr[]" id="rateOtr-1"
                                                       onkeyup="calculationInQuotationOtr(1);" class="form-control"
                                                       onkeypress="return isNumber1(event);">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" readonly id="amountOtr-1" name="amountOtr[]"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Total Amount</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="totamtOtr" id="totamtOtr"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="" id="extraWork">
                                    <div class="col-sm-12">
                                        <label class="col-sm-2"></label>
                                        <label class="col-sm-1"></label>
                                        <label class="control-label col-sm-1"><strong>Category</strong></label>
                                        <div class="col-sm-4">
                                            <input type="text" name="categoryExt" class="form-control center-block"
                                                   value="Extra Work Done">
                                        </div>
                                        <div class="col-sm-4"></div>
                                    </div>
                                    <br>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-9 col-md-9">
                                                <button type="button" class="btn green" onclick="addRowInExtra();">
                                                    Add
                                                    Row
                                                </button>
                                                <button type="button" class="btn default"
                                                        onclick="deleteRowInExtra();">
                                                    Delete
                                                    Row
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="mytable" id="quotationDataTableExt" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Notes
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        </thead>
                                        <tbody id="quotationTableExt">
                                        <tr>
                                            <td style="width: 60px;">
                                                1
                                            </td>
                                            <td style="width: 400px;">
                                                <!--                                                <select id="productCodeExt-1" name="particularExt[]" required-->
                                                <!--                                                        class="form-control select2me select2-offscreen"-->
                                                <!--                                                        data-placeholder="Select..."-->
                                                <!--                                                        onchange="calculateTotalInQuotationExt();"-->
                                                <!--                                                        tabindex="-1" title="" style="width: 400px !important;">-->
                                                <!--                                                    <option value="">-->
                                                <!--                                                    </option>-->
                                                <!--                                                    --><?php //foreach ($materialList as $key => $value) {
                                                //                                                        $name = $value['name'];
                                                //                                                        $id = $value['id'];
                                                //                                                        ?>
                                                <!--                                                        <option value="-->
                                                <?php //echo $id; ?><!--">--><?php //echo $name ?><!--</option>-->
                                                <!--                                                    --><?php //} ?>
                                                <!--                                                </select>-->
                                                <input type="text" class="form-control" id="productCodeExt-1"
                                                       name="particularExt[]" onchange="calculationInQuotationExt()"
                                                       style="width: 400px;">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" id="notesExt-1"
                                                       name="notesExt[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;padding-right: 10px;">
                                                <input type="text" required onkeyup="calculationInQuotationExt(1);"
                                                       onkeypress="return isNumber(event);" id="quantityExt-1"
                                                       name="quantityExt[]"
                                                       class="form-control">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" required name="rateExt[]" id="rateExt-1"
                                                       onkeyup="calculationInQuotationExt(1);" class="form-control"
                                                       onkeypress="return isNumber1(event);">
                                            </td>
                                            <td style="width: 250px;;">
                                                <input type="text" readonly id="amountExt-1" name="amountExt[]"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 200px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Total Amount</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="totamtExt" id="totamtExt"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>


                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="submit" name="submitButton" class="btn green" id="submit">
                                                Submit
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="counter" name="counter" value="1">
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>