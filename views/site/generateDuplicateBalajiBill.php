<?php //\app\commands\AppUtility::dump($entireData); ?>
<style type="text/css">

    /* this is the important part (should be used in HTML head): */
    .pagebreak {
        page-break-after: always;
    }

</style>
<script>
    var a = ['', 'One ', 'Two ', 'Three ', 'Four ', 'Five ', 'Six ', 'Seven ', 'Eight ', 'Nine ', 'Ten ', 'Eleven ', 'Twelve ', 'Thirteen ', 'Fourteen ', 'Fifteen ', 'Sixteen ', 'Seventeen ', 'Eighteen ', 'Nineteen '];
    var b = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
    $(document).ready(function () {
        inWords($('#Net_Total').val());

    });
    function numberWithCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        var z = 0;
        var len = String(x1).length;
        var num = parseInt((len / 2) - 1);

        while (rgx.test(x1)) {
            if (z > 0) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            else {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
                rgx = /(\d+)(\d{2})/;
            }
            z++;
            num--;
            if (num == 0) {
                break;
            }
        }
        return x1 + x2;
    }
    function inWords(num) {

        num = parseInt(num);

        if ((num = num.toString()).length > 9) return 'overflow';
        n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
        if (!n) return;
        var str = '';
        str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'Crore ' : '';
        str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Lakh ' : '';
        str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'Thousand ' : '';
        str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'Hundred ' : '';
        str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) : '';
        $('#container3').text(str);
        document.getElementById('Net_Total').value = numberWithCommas(num) + ".00";

    }

    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }

    function close123() {
        window.location = '/prolights/web/site/balajidupbill';
    }

</script>
<div class="container" style="float: none">
    <div class="row" style="border: 1px solid;">
        <div class="row">
            <?= $this->render('addressView'); ?>
        </div>
        <hr style="border-color: orange;margin-top: 0px !important;margin-bottom: 5px !important;">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <label style="font-size: 12px;">To: <?php echo $entireData[0]['client_name']; ?></label>
                </div>
                <div class="col-xs-6">
                    <?php if (!empty($entireData[0]['phone_no'])) { ?>
                        <label class="pull-right" style="font-size: 12px;">Customer Contact
                            No: <?php echo $entireData[0]['phone_no']; ?> </label>
                    <?php } ?>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <label style="font-size: 12px;">Invoice No: <?php echo $entireData[0]['invoice_no']; ?></label>
                </div>
                <div class="col-xs-6">
                    <label class="pull-right"
                           style="font-size: 12px;">Date: <?php echo date('d-m-Y', strtotime($entireData[0]['date'])); ?> </label>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <label style="font-size: 12px;">Event
                        : <?php echo isset($entireData[0]['eventName']) ? $entireData[0]['eventName'] : ''; ?></label>
                </div>
                <div class="col-xs-6">
                </div>
            </div>
        </div>
        <hr style="border-color: orange;margin-top: 0px !important;margin-bottom: 5px !important;">
        <div class="col-xs-12">
            <p style="padding-bottom: 5px;"></p>
        </div>
        <div class="col-xs-12">
            <table class="table" style="font-size: 12px;">
                <thead>
                <th>
                    Description
                </th>
                <!--                <th>-->
                <!--                    Notes-->
                <!--                </th>-->
                <th>
                    Units
                </th>
                <th>
                    Rate
                </th>
                <th>
                    Amount
                </th>
                </thead>
                <tbody>
                <?php
                foreach ($entireData as $key => $value) { ?>
                    <tr>
                        <td><label><?php echo $value['particularName']; ?></label></td>
                        <td><label><?php echo $value['quantity']; ?></label></td>
                        <td><label><?php echo number_format($value['rate'], 2); ?></label></td>
                        <td><label><?php echo number_format($value['amount'], 2); ?></label></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td><label>Total</label></td>
                    <td><label><?php echo number_format($entireData[0]['totalAmount'], 2); ?></label></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><label>Service Tax (<?= $entireData[0]['vatPercent']; ?> %)</label></td>
                    <td><label><?php echo number_format($entireData[0]['vat'], 2); ?></label></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td><label>Net Amount</label></td>
                    <td><label><?php echo number_format($entireData[0]['grandTotal'], 2); ?></label></td>
                </tr>
                </tbody>
            </table>
            <hr style="border-color: orange;margin-top: 0px !important;margin-bottom: 5px !important;">
            <div class="col-sm-12">
                <table width="100%" style="font-size: 10px;">
                    <tr>
                        <td><label style="">Terms & Conditions</label></td>
                    </tr>
                    <tr>
                        <td style="padding-left: 20px;">• Payment on the Event or else interest will be charged @ 18 %
                            p.a.
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 20px;">
                            • The above mentioned item are rented for 4 hrs
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 20px;">
                            • The above rates are inclusive of Transportation
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 20px;">
                            • Additional Sound Output for D.J. System will be charged extra.
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 20px;">
                            • Above mentioned rates are for only regular days.
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 20px;">
                            • Special Rates for Special Days. Urgent order will be charged extra.
                        </td>
                    </tr>
                </table>
                <br>
            </div>
        </div>
    </div>
    &nbsp;
    <br/>
    <br/>

    <p align="center" style='margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                        onclick="printing();" value="Print"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
    </p>
</div>

