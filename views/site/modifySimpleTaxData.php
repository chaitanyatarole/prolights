<?php //\app\commands\AppUtility::dump($productCode); ?>
<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Modify Tax Invoice</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/sai/web/site/updatemodifiedsimpletaxdata" onsubmit="return $.LoadingOverlay('show');"
                              class="form-horizontal" onsubmit="return smoke.alert('Data Modified');">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Existing Clients</label>

                                    <div class="col-sm-8">
                                        <select id="clientNameCode" name="clientNameCode"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($clientData as $key) {
                                                $clientName = $key['client_name'];
                                                $clientCode = $key['clientcode'];
                                                echo "<option value='$clientCode'>$clientCode -- $clientName</option>";

                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>
                                <input type="hidden" class="form-control" name="ccodeno" id="ccodeno"
                                       value="<?php echo $maxclientCode; ?>"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="clientcode" readonly
                                               value="<?php echo $entireData[0]['clientcode']; ?>"
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" class="form-control" name="date"
                                               value="<?php echo $entireData[0]['date']; ?>" id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Invoice Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo $entireData[0]['invoice_no']; ?>" name="taxNumber"
                                               id="date">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['client_name']; ?>"
                                               onkeyup=""
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="address"
                                                  name="address"><?php echo $entireData[0]['address']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['phone_no']; ?>" name="number"
                                               id="number">
                                    </div>
                                    <label class="control-label col-sm-2">Email</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['email']; ?>" name="email" id="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Challan No.</label>

                                    <div class="col-sm-3">
                                        <input type="text" required="" class="form-control"
                                               value="<?php echo $entireData[0]['challan_no']; ?>" name="challan_number"
                                               id="challan_number">
                                    </div>
                                    <label class="control-label col-sm-2">P.O.No.</label>

                                    <div class="col-sm-3">
                                        <input type="text" required class="form-control"
                                               value="<?php echo $entireData[0]['purchase_orderno']; ?>" name="ponumber"
                                               id="ponumber">
                                    </div>


                                </div>
                                <div class="form-group">

                                    <label class="control-label col-sm-2">Vehical No.</label>

                                    <div class="col-sm-3">
                                        <input type="text" required class="form-control"
                                               value="<?php echo $entireData[0]['vehical_no']; ?>" name="vehicalnumber"
                                               id="vehicalnumber">
                                    </div>
                                    <label class="control-label col-sm-2">VAT No.</label>

                                    <div class="col-sm-3">
                                        <input type="text" required class="form-control"
                                               value="<?php echo isset($entireData[0]['vat_no']) ? $entireData[0]['vat_no'] : ''; ?>"
                                               name="vat_no"
                                               id="">
                                    </div>
                                </div>

                                <div class="form-group">

                                    <label class="control-label col-sm-2">CST No.</label>

                                    <div class="col-sm-3">
                                        <input type="text" required class="form-control"
                                               value="<?php echo isset($entireData[0]['cst_no']) ? $entireData[0]['cst_no'] : ''; ?>"
                                               name="cst_no"
                                               id="">
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
<!--                                        <div class="col-md-9 col-md-9">-->
<!--                                            <button type="button" class="btn green" onclick="addRowInTax();">Add-->
<!--                                                Row-->
<!--                                            </button>-->
<!--                                            <button type="button" class="btn default" onclick="deleteRowInTax();">-->
<!--                                                Delete-->
<!--                                                Row-->
<!--                                            </button>-->
<!--                                        </div>-->
                                    </div>
                                </div>
                                <div class="">
                                    <table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        </thead>
                                        <tbody id="taxTable">
                                        <?php foreach ($entireData as $key => $value) {
                                            $perticular = $value['perticular'];
                                            $perticular_name = $value['perticular_name'];
                                            $quantity = $value['quantity'];
                                            $rate = $value['rate'];
                                            $amount = $value['amount'];
                                            $serialno = $key + 1;
                                            ?>
                                            <tr>

                                                <td align="center"><label><?php echo $serialno; ?></label></td>

                                                <td style="width: 400px;">
                                                    <input type="hidden" name="perticular[]"
                                                           onkeyup="calculateRemainingBalance();"
                                                           id="perticular-<?php echo $serialno ?> " readonly
                                                           value="<?php echo $perticular; ?>" class="form-control">
                                                    <input type="text" readonly value="<?php echo $perticular_name; ?>"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 250px;padding-right: 10px;">
                                                    <input type="text"
                                                           onkeyup="calculationInTaxSimple(<?php echo $serialno; ?>);calculateRemainingBalance();"
                                                           value="<?php echo $quantity; ?>"
                                                           onkeypress="return isNumber(event);"
                                                           id="quantity-<?php echo $serialno ?>"
                                                           name="quantity[]"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 250px;;">
                                                    <input type="text" name="rate[]" id="rate-<?php echo $serialno ?>"
                                                           value="<?php echo $rate; ?>"
                                                           onkeyup="calculationInTaxSimple(<?php echo $serialno ?>);calculateRemainingBalance();"
                                                           class="form-control"
                                                           onkeypress="return isNumber1(event);">
                                                </td>
                                                <td style="width: 250px;;">
                                                    <input type="text" readonly id="amount-<?php echo $serialno ?>"
                                                           name="amount[]" value="<?php echo $amount; ?>"
                                                           class="form-control">
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Total Amount</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="totamt" id="totamt"
                                                       value="<?php echo $entireData[0]['total_amount']; ?>"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Vat 12.5%</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="vat" id="vat"
                                                       value="<?php echo $entireData[0]['vat_tax_amount']; ?>"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Grand Total</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly name="grandtot" id="grandtot"
                                                       value="<?php echo $entireData[0]['net_amount']; ?>"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Payment Received</label>

                                    <div class="col-sm-3">
                                        <input type="text" required="" class="form-control"
                                               value="<?php echo $entireData[0]['paymentReceived'] ?>"
                                               onkeypress="return isNumber1(event);"
                                               onkeyup="calculateRemainingBalance();"
                                               name="paymentReceived" id="paymentReceived">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Balance Remaining</label>

                                    <div class="col-sm-3">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo $entireData[0]['balanceAmount']; ?>"
                                               name="balanceAmount"
                                               onkeypress="return isNumber1(event);"
                                               id="balanceAmount">
                                    </div>
                                    <div id="addDatePicker">

                                    </div>
                                </div>
                                <div class="form-group">

                                    <label class="control-label col-sm-2">Payment Mode</label>

                                    <div class="col-sm-3">
                                        <input type="text" required class="form-control"
                                               value="<?php echo $entireData[0]['payment_mode']; ?>" name="PaymentMode"
                                               id="creditnumber">

                                    </div>


                                </div>
                                <div class="form-group">

                                    <label class="control-label col-sm-2">Reciept No.</label>

                                    <div class="col-sm-3">
                                        <input type="text" required class="form-control"
                                               value="<?php echo $entireData[0]['credit_no']; ?>" name="creditnumber"
                                               id="creditnumber">
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="submit" class="btn green" id="submit"
                                                    onclick="validateModifyTaxData();">Update
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>