<script type="text/javascript">
    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }

    function close123() {
        window.location = '/prolights/web/site/balajiporeports';
    }

</script>
<div class="container">
    <div class="row">
        <br>

        <div class="col-xs-12" style="padding-left: 0px;padding-right: 0px;">
            <div class="col-xs-5" style="padding-left: 0px;padding-right: 0px;">
                <p>H B 14/2,Near Sai Chowk,
                    Pimpri, Pune - 411017 <br>Mob. 9822666911 Email :balajieventscreation@gmail.com</p>
            </div>
            <div class="col-xs-7" style="padding-left: 0px;padding-right: 0px;">
                <img src="/prolights/web/img/balajilogo.jpg" style="height: 100px;float: right">
            </div>
        </div>
        <br>
        <h4 style="text-align: center">Balaji Events Purchase Order Report</h4>

        <div class="col-xs-12">
            <div class="pull-left">
                From Date : <?php echo date('d-m-Y', strtotime($fromDate)); ?>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                To Date : <?php echo date('d-m-Y', strtotime($toDate)); ?>
            </div>
        </div>
        <div class="">
            <table class="" style="font-size: 12px;">
                <thead>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-left: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Quotation. No
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Date
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Client Name
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Mobile
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Particular
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Quantity
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Rate
                </th>
                <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                    Amount
                </th>
                </thead>
                <tbody>
                <?php $invoiceNumber = '';
                $grandTotal = '';
                foreach ($entireData as $key => $value) {
                    $serialNumber = $key + 1; ?>
                    <tr>
                        <?php if ($invoiceNumber == $value['purchase_order_no']) {
                            ?>
                            <td style="border-left: 1px solid #7B7B7B;text-align: center;width: 80px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;"></td>
                            <td style="width: 80px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;"></td>
                            <td style="width: 150px;border-right: 1px solid #7B7B7B;border-bottom: 1px solid #7B7B7B"></td>
                            <td style="width: 100px;border-right: 1px solid #7B7B7B;border-bottom: 1px solid #7B7B7B"></td>
                            <td style="width: 300px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;border-left: 1px solid #7B7B7B"><?php echo $value['particularName']; ?></td>
                            <td style="text-align: center;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['quantity']; ?></td>
                            <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['rate']; ?></td>
                            <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['amount']; ?></td>
                            <?php
                            $grandTotal += $value['amount'];
                        } else {
                            $date = date('d-m-Y', strtotime($value['date']));
                            ?>

                            <td style="border-left: 1px solid #7B7B7B;text-align: center;width: 80px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['purchase_order_no']; ?></td>
                            <td style="text-align: center;width: 150px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $date; ?></td>
                            <td style="width: 150px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['client_name']; ?></td>
                            <td style="text-align: center;width: 100px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['phone_no']; ?></td>
                            <td style="width: 300px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['particularName']; ?></td>
                            <td style="text-align: center;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['quantity']; ?></td>
                            <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['rate']; ?></td>
                            <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['amount']; ?></td>
                            <?php $invoiceNumber = $value['purchase_order_no'];
                            $grandTotal += $value['amount'];
                        } ?>
                    </tr>
                <?php } ?>
                <tr>
                    <td style="text-align: center;width: 50px;border-left: solid 1px #7B7B7B;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                    <td style="text-align: center;width: 150px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                    <td style="width: 300px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                    <td style="width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                    <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                    <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                    <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                        <strong>Grand Total</strong>
                    </td>
                    <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                        <strong><?php echo $grandTotal; ?></strong></td>
                </tr>
                </tbody>
            </table>
        </div>
        <br>
        <?php
        $query = "select * from balajiTaxInvoice WHERE date BETWEEN '$fromDate' AND '$toDate' AND status = 'cancelled'";
        $result = \Yii::$app->db->createCommand($query)->queryAll();
        if (!empty($result)) {
            ?>
            <p style="text-align: center">Cancelled Invoices</p>
            <div class="col-xs-12">
                <div class="pull-left">
                    From Date : <?php echo date('d-m-Y', strtotime($fromDate)); ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    To Date : <?php echo date('d-m-Y', strtotime($toDate)); ?>
                </div>
            </div>
            <div class="">
                <table class="" style="font-size: 12px;">
                    <thead>
                    <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-left: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                        Quotation. No
                    </th>
                    <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                        Date
                    </th>
                    <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                        Client Name
                    </th>
                    <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                        Mobile
                    </th>
                    <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                        Particular
                    </th>
                    <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                        Quantity
                    </th>
                    <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                        Rate
                    </th>
                    <th style="text-align: center;border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                        Amount
                    </th>
                    </thead>
                    <tbody>
                    <?php $invoiceNumber = '';
                    $grandTotal = '';
                    foreach ($result as $key => $value) {
                        $serialNumber = $key + 1; ?>
                        <tr>
                            <?php if ($invoiceNumber == $value['purchase_order_no']) {
                                ?>
                                <td style="border-left: 1px solid #7B7B7B;text-align: center;width: 80px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;"></td>
                                <td style="width: 80px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;"></td>
                                <td style="width: 150px;border-right: 1px solid #7B7B7B;border-bottom: 1px solid #7B7B7B"></td>
                                <td style="width: 100px;border-right: 1px solid #7B7B7B;border-bottom: 1px solid #7B7B7B"></td>
                                <td style="width: 300px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B;border-left: 1px solid #7B7B7B"><?php echo $value['particularName']; ?></td>
                                <td style="text-align: center;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['quantity']; ?></td>
                                <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['rate']; ?></td>
                                <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['amount']; ?></td>
                                <?php
                                $grandTotal += $value['amount'];
                            } else {
                                $date = date('d-m-Y', strtotime($value['date']));
                                ?>

                                <td style="border-left: 1px solid #7B7B7B;text-align: center;width: 80px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['purchase_order_no']; ?></td>
                                <td style="text-align: center;width: 150px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $date; ?></td>
                                <td style="width: 150px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['client_name']; ?></td>
                                <td style="text-align: center;width: 100px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['phone_no']; ?></td>
                                <td style="width: 300px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['particularName']; ?></td>
                                <td style="text-align: center;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['quantity']; ?></td>
                                <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['rate']; ?></td>
                                <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['amount']; ?></td>
                                <?php $invoiceNumber = $value['purchase_order_no'];
                                $grandTotal += $value['amount'];
                            } ?>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td style="text-align: center;width: 50px;border-left: solid 1px #7B7B7B;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                        <td style="text-align: center;width: 150px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                        <td style="width: 300px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                        <td style="width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                        <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                        <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"></td>
                        <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                            <strong>Grand Total</strong>
                        </td>
                        <td style="text-align: right;width: 90px;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                            <strong><?php echo $grandTotal; ?></strong></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        <?php } ?>
        <p align="center" style='margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                            onclick="printing();" value="Print"/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
        </p>
    </div>
</div>