<?php //\app\commands\AppUtility::dump($entireData); ?>
<style type="text/css">

    /* this is the important part (should be used in HTML head): */
    .pagebreak {
        page-break-after: always;
    }

</style>
<script>
    var a = ['', 'One ', 'Two ', 'Three ', 'Four ', 'Five ', 'Six ', 'Seven ', 'Eight ', 'Nine ', 'Ten ', 'Eleven ', 'Twelve ', 'Thirteen ', 'Fourteen ', 'Fifteen ', 'Sixteen ', 'Seventeen ', 'Eighteen ', 'Nineteen '];
    var b = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
    $(document).ready(function () {
        inWords($('#Net_Total').val());

    });
    function numberWithCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        var z = 0;
        var len = String(x1).length;
        var num = parseInt((len / 2) - 1);

        while (rgx.test(x1)) {
            if (z > 0) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            else {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
                rgx = /(\d+)(\d{2})/;
            }
            z++;
            num--;
            if (num == 0) {
                break;
            }
        }
        return x1 + x2;
    }
    function inWords(num) {

        num = parseInt(num);

        if ((num = num.toString()).length > 9) return 'overflow';
        n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
        if (!n) return;
        var str = '';
        str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'Crore ' : '';
        str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Lakh ' : '';
        str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'Thousand ' : '';
        str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'Hundred ' : '';
        str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) : '';
        $('#container3').text(str);
        document.getElementById('Net_Total').value = numberWithCommas(num) + ".00";

    }

    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }

    function close123() {
        window.location = '/prolights/web/site/audioquotation';
    }

</script>
<style>
    <!--
    @media screen {
        .bg {
            color: firebrick !important;
        }

        #notesbg {
            background-color: #C0E6AD !important;
        }

        .thbg {
            background-color: #e6afb1 !important;
        }

        .fntclr {
            color: white !important;
        }

        .fntclrbudget {
            color: black !important;
        }

        .table > thead:first-child > tr:first-child > th {
            background-color: #C0E6AD !important;
        }

        .bgforp {
            background-color: black !important;
            color: white !important;
        }

        .boxcol {
            background-color: #C0E6AD !important;
        }

        #totalcol {
            background-color: black !important;
            color: white !important;
        }

        /*p.bodyText {font-family:verdana, arial, sans-serif;}*/
    }

    @media print {
        .bg {
            color: firebrick !important;
        }

        #notesbg {
            background-color: #C0E6AD !important;
        }

        .bgforp {
            background-color: black !important;
            color: white !important;
        }

        .fntclr {
            color: white !important;
        }

        .fntclrbudget {
            color: black !important;
        }

        .thbg {
            background-color: #e6afb1 !important;
        }

        .table > thead:first-child > tr:first-child > th {
            background-color: #C0E6AD !important;
        }

        .boxcol {
            background-color: #C0E6AD !important;
        }

        #totalcol {
            background-color: black !important;
            color: white !important;
        }

        /*p.bodyText {font-family:georgia, times, serif;}*/
    }

    @media screen, print {
        .bg {
            color: firebrick !important;
        }

        #notesbg {
            background-color: #C0E6AD !important;
        }

        .thbg {
            background-color: #e6afb1 !important;
        }

        .bgforp {
            background-color: black !important;
        }

        .fntclr {
            color: white !important;
        }

        .fntclrbudget {
            color: black !important;
        }

        .table > thead:first-child > tr:first-child > th {
            background-color: #C0E6AD !important;
        }

        .boxcol {
            background-color: #C0E6AD !important;
        }

        #totalcol {
            background-color: black !important;
            color: white !important;
        }

        /*p. {font-size:10pt}*/
    }

    -->
</style>
<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        padding: 2px !important;
    }

    .table {
        margin-bottom: 4px !important;
    }
</style>
<div class="container" style="float: none;font-size: 10px;">
    <div class="row" style="border: 1px solid;">
        <div class="row">
            <?= $this->render('addressViewAudio'); ?>
        </div>
        <hr style="border-color: orange;margin-top: 0px !important;margin-bottom: 5px !important;">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <label style="font-size: 12px;" class="bg">To: <?php echo $entireData['cname']; ?></label>
                </div>
                <div class="col-xs-6">
                    <?php if (!empty($entireData['number'])) { ?>
                        <label class="pull-right bg" style="font-size: 12px;">Customer Contact
                            No: <?php echo $entireData['number']; ?> </label>
                    <?php } ?>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <label class="bg" style="font-size: 12px;">Quotation
                        No: <?php echo $entireData['quotationNumber']; ?></label>
                </div>
                <div class="col-xs-6">
                    <label class="pull-right bg"
                           style="font-size: 12px;">Date: <?php echo date('d-m-Y', strtotime($entireData['date'])); ?> </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <label style="font-size: 12px;" class="bg">Event: <?php echo $entireData['eventName']; ?></label>
                </div>
                <div class="col-xs-6">
                    <?php if (!empty($entireData['eventDate'])) { ?>
                        <label class="pull-right bg" style="font-size: 12px;">Event Date
                            No: <?php echo date('d-m-Y',strtotime($entireData['eventDate'])) ; ?> </label>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php
        $totalAmountStage = isset($entireData['totamtStage']) ? $entireData['totamtStage'] : 0;
        $totalAmountTent = isset($entireData['totamtTent']) ? $entireData['totamtTent'] : 0;
        $totalAmountLight = isset($entireData['totamtLight']) ? $entireData['totamtLight'] : 0;
        $totalAmountTech = isset($entireData['totamtTech']) ? $entireData['totamtTech'] : 0;
        $totalAmountSpc = isset($entireData['totamtSpc']) ? $entireData['totamtSpc'] : 0;
        $totalAmountOtr = isset($entireData['totamtOtr']) ? $entireData['totamtOtr'] : 0;
        $totalAmountExt = isset($entireData['totamtExt']) ? $entireData['totamtExt'] : 0;

        $totalBudget = $totalAmountStage + $totalAmountTent + $totalAmountLight + $totalAmountTech + $totalAmountSpc + $totalAmountOtr + $totalAmountExt;

        ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-6 boxcol" style="border: 1px solid;height: 60px;margin-left: 16px;">
                    <label style="font-size: 12px;padding-top: 10px;" class="bg"><span class="fntclrbudget">Total Budget:
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $totalBudget; ?></span></label><br>
                    <label style="font-size: 12px;" class="bg"><span class="fntclrbudget">Available Budget:
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></label>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <p style="padding-bottom: 0px;"></p>
        </div>
        <?php $categoryStage = isset($entireData['particularStage']) ? $entireData['particularStage'] : '';
        if (!empty($categoryStage)) {
            ?>
            <p class="bgforp"
               style="padding-left: 16px;margin-bottom: 0px;margin-top: 8px;width: 258px;margin-left: 15px;">
                <strong class="fntclr" style="font-size: 12px;"><?php echo $entireData['categoryStage']; ?></strong>
            </p>
            <div class="col-sm-12" style="float: none">
                <table border='1' class="table" width='100%' align='center' bordercolor='#000000'
                       style='border-collapse:collapse;font-size: 12px;'>
                    <thead>
                    <th style="border-bottom: 1px solid">
                        Decription
                    </th>
                    <th style="border-bottom: 1px solid">
                        Unit
                    </th>
                    <th style="border-bottom: 1px solid">
                        Rate
                    </th>
                    <th style="border-bottom: 1px solid">
                        Amount
                    </th>
                    <th style="border-bottom: 1px solid">
                        Notes
                    </th>
                    </thead>
                    <?php $description = $entireData['particularStage'];
                    $counter = '';
                    foreach ($description as $key => $value) {
                        $query = "select name from product where id = '$value'";
                        $result = Yii::$app->db->createCommand($query)->queryAll();
                        $name = isset($result[0]['name']) ? $result[0]['name'] : '';
                        ?>
                        <tr>
                            <td style="border-top: 1px solid;width: 300px;"><?php echo $name; ?></td>
                            <td style="border-top: 1px solid;width: 100px;"><?php echo $entireData['quantityStage'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px;"><?php echo $entireData['rateStage'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px;"><?php echo $entireData['amountStage'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 150px;"><?php echo $entireData['notesStage'][$key]; ?></td>
                        </tr>
                        <?php
                    } ?>
                    <tr>
                        <td style="border-top: 1px solid;width: 300px;border-right: hidden"></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                        <td id="totalcol" style="border-top: 1px solid;width: 100px;">Total</td>
                        <td id="totalcol"
                            style="border-top: 1px solid;width: 100px;"><?php echo $entireData['totamtStage']; ?></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
        <?php $categoryTent = isset($entireData['particularTent']) ? $entireData['particularTent'] : '';
        if (!empty($categoryTent)) {
            ?>
            <p style="padding-left: 16px;margin-bottom: 0px;margin-top: 8px;width: 258px;margin-left: 15px;"
               class="bgforp">
                <strong class="fntclr" style="font-size: 12px;"><?php echo $entireData['categoryTent']; ?></strong></p>
            <div class="col-sm-12" style="float: none">
                <table border='1' class="table" width='100%' align='center' bordercolor='#000000'
                       style='border-collapse:collapse;font-size: 12px;'>
                    <thead>
                    <th style="border-bottom: 1px solid">
                        Decription
                    </th>
                    <th style="border-bottom: 1px solid">
                        Unit
                    </th>
                    <th style="border-bottom: 1px solid">
                        Rate
                    </th>
                    <th style="border-bottom: 1px solid">
                        Amount
                    </th>
                    <th style="border-bottom: 1px solid">
                        Notes
                    </th>
                    </thead>
                    <?php $description = $entireData['particularTent'];
                    $counter = '';
                    foreach ($description as $key => $value) {
                        $query = "select name from product where id = '$value'";
                        $result = Yii::$app->db->createCommand($query)->queryAll();
                        $name = isset($result[0]['name']) ? $result[0]['name'] : '';
                        ?>
                        <tr>
                            <td style="border-top: 1px solid;width: 300px"><?php echo $name; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['quantityTent'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['rateTent'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['amountTent'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 150px"><?php echo $entireData['notesTent'][$key]; ?></td>
                        </tr>
                        <?php
                    } ?>
                    <tr>
                        <td style="border-top: 1px solid;width: 300px;border-right: hidden"></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                        <td id="totalcol" style="border-top: 1px solid;width: 100px;">Total</td>
                        <td id="totalcol"
                            style="border-top: 1px solid;width: 100px;"><?php echo $entireData['totamtTent']; ?></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
        <?php $categoryLight = isset($entireData['particularLight']) ? $entireData['particularLight'] : '';
        if (!empty($categoryLight)) {
            ?>
            <p style="padding-left: 16px;margin-bottom: 0px;margin-top: 8px;width: 258px;margin-left: 15px;"
               class="bgforp">
                <strong class="fntclr" style="font-size: 12px;"><?php echo $entireData['categoryLight']; ?></strong></p>
            <div class="col-sm-12" style="float: none">
                <table border='1' class="table" width='100%' align='center' bordercolor='#000000'
                       style='border-collapse:collapse;font-size: 12px;'>
                    <thead>
                    <th style="border-bottom: 1px solid">
                        Decription
                    </th>
                    <th style="border-bottom: 1px solid">
                        Unit
                    </th>
                    <th style="border-bottom: 1px solid">
                        Rate
                    </th>
                    <th style="border-bottom: 1px solid">
                        Amount
                    </th>
                    <th style="border-bottom: 1px solid">
                        Notes
                    </th>
                    </thead>
                    <?php $description = $entireData['particularLight'];
                    $counter = '';
                    foreach ($description as $key => $value) {
                        $query = "select name from product where id = '$value'";
                        $result = Yii::$app->db->createCommand($query)->queryAll();
                        $name = isset($result[0]['name']) ? $result[0]['name'] : '';
                        ?>
                        <tr>
                            <td style="border-top: 1px solid;width: 300px"><?php echo $name; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['quantityLight'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['rateLight'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['amountLight'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 150px"><?php echo $entireData['notesLight'][$key]; ?></td>
                        </tr>
                        <?php
                    } ?>
                    <tr>
                        <td style="border-top: 1px solid;width: 300px;border-right: hidden"></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                        <td id="totalcol" style="border-top: 1px solid;width: 100px;">Total</td>
                        <td id="totalcol"
                            style="border-top: 1px solid;width: 100px;"><?php echo $entireData['totamtLight']; ?></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
        <?php $categoryTech = isset($entireData['particularTech']) ? $entireData['particularTech'] : '';
        if (!empty($categoryTech)) {
            ?>
            <p style="padding-left: 16px;margin-bottom: 0px;margin-top: 8px;width: 258px;margin-left: 15px;"
               class="bgforp">
                <strong class="fntclr" style="font-size: 12px;"><?php echo $entireData['categoryTech']; ?></strong></p>
            <div class="col-sm-12" style="float: none">
                <table border='1' class="table" width='100%' align='center' bordercolor='#000000'
                       style='border-collapse:collapse;font-size: 12px;'>
                    <thead>
                    <th style="border-bottom: 1px solid">
                        Decription
                    </th>
                    <th style="border-bottom: 1px solid">
                        Unit
                    </th>
                    <th style="border-bottom: 1px solid">
                        Rate
                    </th>
                    <th style="border-bottom: 1px solid">
                        Amount
                    </th>
                    <th style="border-bottom: 1px solid">
                        Notes
                    </th>
                    </thead>
                    <?php $description = $entireData['particularTech'];
                    $counter = '';
                    foreach ($description as $key => $value) {
                        $query = "select name from product where id = '$value'";
                        $result = Yii::$app->db->createCommand($query)->queryAll();
                        $name = isset($result[0]['name']) ? $result[0]['name'] : '';
                        ?>
                        <tr>
                            <td style="border-top: 1px solid;width: 300px"><?php echo $name; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['quantityTech'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['rateTech'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['amountTech'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 150px"><?php echo $entireData['notesTech'][$key]; ?></td>
                        </tr>
                        <?php
                    } ?>
                    <tr>
                        <td style="border-top: 1px solid;width: 300px;border-right: hidden"></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                        <td id="totalcol" style="border-top: 1px solid;width: 100px;">Total</td>
                        <td id="totalcol"
                            style="border-top: 1px solid;width: 100px;"><?php echo $entireData['totamtTech']; ?></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
        <?php $categorySpc = isset($entireData['particularSpc']) ? $entireData['particularSpc'] : '';
        if (!empty($categorySpc)) {
            ?>
            <p style="padding-left: 16px;margin-bottom: 0px;margin-top: 8px;width: 258px;margin-left: 15px;"
               class="bgforp">
                <strong class="fntclr" style="font-size: 12px;"><?php echo $entireData['categorySpc']; ?></strong></p>
            <div class="col-sm-12" style="float: none">
                <table border='1' class="table" width='100%' align='center' bordercolor='#000000'
                       style='border-collapse:collapse;font-size: 12px;'>
                    <thead>
                    <th style="border-bottom: 1px solid">
                        Decription
                    </th>
                    <th style="border-bottom: 1px solid">
                        Unit
                    </th>
                    <th style="border-bottom: 1px solid">
                        Rate
                    </th>
                    <th style="border-bottom: 1px solid">
                        Amount
                    </th>
                    <th style="border-bottom: 1px solid">
                        Notes
                    </th>
                    </thead>
                    <?php $description = $entireData['particularSpc'];
                    $counter = '';
                    foreach ($description as $key => $value) {
                        $query = "select name from product where id = '$value'";
                        $result = Yii::$app->db->createCommand($query)->queryAll();
                        $name = isset($result[0]['name']) ? $result[0]['name'] : '';
                        ?>
                        <tr>
                            <td style="border-top: 1px solid;width: 300px"><?php echo $value; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['quantitySpc'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['rateSpc'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['amountSpc'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 150px"><?php echo $entireData['notesSpc'][$key]; ?></td>
                        </tr>
                        <?php
                    } ?>
                    <tr>
                        <td style="border-top: 1px solid;width: 300px;border-right: hidden"></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                        <td id="totalcol" style="border-top: 1px solid;width: 100px;">Total</td>
                        <td id="totalcol"
                            style="border-top: 1px solid;width: 100px;"><?php echo $entireData['totamtSpc']; ?></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
        <?php $categoryOtr = isset($entireData['particularOtr']) ? $entireData['particularOtr'] : '';
        if (!empty($categoryOtr)) {
            ?>
            <p style="padding-left: 16px;margin-bottom: 0px;margin-top: 8px;width: 258px;margin-left: 15px;"
               class="bgforp">
                <strong class="fntclr" style="font-size: 12px;"><?php echo $entireData['categoryOther']; ?></strong></p>
            <div class="col-sm-12" style="float: none">
                <table border='1' class="table" width='100%' align='center' bordercolor='#000000'
                       style='border-collapse:collapse;font-size: 12px;'>
                    <thead>
                    <th style="border-bottom: 1px solid">
                        Decription
                    </th>
                    <th style="border-bottom: 1px solid">
                        Unit
                    </th>
                    <th style="border-bottom: 1px solid">
                        Rate
                    </th>
                    <th style="border-bottom: 1px solid">
                        Amount
                    </th>
                    <th style="border-bottom: 1px solid">
                        Notes
                    </th>
                    </thead>
                    <?php $description = $entireData['particularOtr'];
                    $counter = '';
                    foreach ($description as $key => $value) {
                        $query = "select name from product where id = '$value'";
                        $result = Yii::$app->db->createCommand($query)->queryAll();
                        $name = isset($result[0]['name']) ? $result[0]['name'] : '';
                        ?>
                        <tr>
                            <td style="border-top: 1px solid;width: 300px"><?php echo $value; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['quantityOtr'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['rateOtr'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['amountOtr'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 150px"><?php echo $entireData['notesOtr'][$key]; ?></td>
                        </tr>
                        <?php
                    } ?>
                    <tr>
                        <td style="border-top: 1px solid;width: 300px;border-right: hidden"></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                        <td id="totalcol" style="border-top: 1px solid;width: 100px;">Total</td>
                        <td id="totalcol"
                            style="border-top: 1px solid;width: 100px;"><?php echo $entireData['totamtOtr']; ?></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
        <?php $categoryExt = isset($entireData['particularExt']) ? $entireData['particularExt'] : '';
        if (!empty($categoryExt)) {
            ?>
            <p style="padding-left: 16px;margin-bottom: 0px;margin-top: 8px;width: 258px;margin-left: 15px;"
               class="bgforp">
                <strong class="fntclr" style="font-size: 12px;"><?php echo $entireData['categoryExt']; ?></strong></p>
            <div class="col-sm-12" style="float: none">
                <table border='1' class="table" width='100%' align='center' bordercolor='#000000'
                       style='border-collapse:collapse;font-size: 12px;'>
                    <thead>
                    <th style="border-bottom: 1px solid">
                        Decription
                    </th>
                    <th style="border-bottom: 1px solid">
                        Unit
                    </th>
                    <th style="border-bottom: 1px solid">
                        Rate
                    </th>
                    <th style="border-bottom: 1px solid">
                        Amount
                    </th>
                    <th style="border-bottom: 1px solid">
                        Notes
                    </th>
                    </thead>
                    <?php $description = $entireData['particularExt'];
                    $counter = '';
                    foreach ($description as $key => $value) {
                        $query = "select name from product where id = '$value'";
                        $result = Yii::$app->db->createCommand($query)->queryAll();
                        $name = isset($result[0]['name']) ? $result[0]['name'] : '';
                        ?>
                        <tr>
                            <td style="border-top: 1px solid;width: 300px"><?php echo $value; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['quantityExt'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['rateExt'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $entireData['amountExt'][$key]; ?></td>
                            <td style="border-top: 1px solid;width: 150px"><?php echo $entireData['notesExt'][$key]; ?></td>
                        </tr>
                        <?php
                    } ?>
                    <tr>
                        <td style="border-top: 1px solid;width: 300px;border-right: hidden"></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                        <td id="totalcol" style="border-top: 1px solid;width: 100px;">Total</td>
                        <td id="totalcol"
                            style="border-top: 1px solid;width: 100px;"><?php echo $entireData['totamtExt']; ?></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
        <div class="col-sm-12" style="float: none">
            <table border='1' class="table" width='100%' align='center'
                   style='border-collapse:collapse;font-size: 12px;'>
                <tr>
                    <td id="notesbg"><p style="text-align: center;margin-bottom: 0px;"><strong style="font-size: 12px;">Notes</strong>
                        </p></td>
                </tr>
                <tr>
                    <td style="border-top: 1px solid;width: 300px;">1 Above quote is valid till next 2 days</td>
                </tr>
                <tr>
                    <td style="border-top: 1px solid;width: 300px;">2 Advance is compulsory against Confirmation of the
                        event
                    </td>
                </tr>
                <tr>
                    <td style="border-top: 1px solid;width: 300px;">3 The Balance payment should be done on the day of
                        the Event
                    </td>
                </tr>
                <tr>
                    <td style="border-top: 1px solid;width: 300px;">4 Or else 24$ P.A rate of interest will be charged
                    </td>
                </tr>
                <tr>
                    <td style="border-top: 1px solid;width: 300px;">5 Above listed material is rented for a single event
                        or consider 6 hr
                    </td>
                </tr>
            </table>
        </div>
        <br>
    </div>
    &nbsp;
    <br/>
    <br/>

    <p align="center" style='margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                        onclick="printing();" value="Print"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
    </p>
</div>
<!--    <div class="col-sm-12">-->
<!--        <table width="100%" style="font-size: 12px;">-->
<!--            <tr>-->
<!--                <td><label style="">Terms & Conditions</label></td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td style="padding-left: 20px;">• Payment on the Event or else interest will be charged @ 18 % p.a.-->
<!--                </td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td style="padding-left: 20px;">-->
<!--                    • The above mentioned item are rented for 4 hrs-->
<!--                </td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td style="padding-left: 20px;">-->
<!--                    • The above rates are inclusive of Transportation-->
<!--                </td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td style="padding-left: 20px;">-->
<!--                    • Additional Sound Output for D.J. System will be charged extra.-->
<!--                </td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td style="padding-left: 20px;">-->
<!--                    • Above mentioned rates are for only regular days.-->
<!--                </td>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <td style="padding-left: 20px;">-->
<!--                    • Special Rates for Special Days. Urgent order will be charged extra.-->
<!--                </td>-->
<!--            </tr>-->
<!--                <tr>
<!--                    <td style="float: right;padding-right: 60px;">Your faithfully</td>

<!--                </tr>
<!--                <tr>
<!--                    <td style="float: right;padding-right: 55px;padding-top: 60px;">

<!--                        Sai Fabrication
<!--                    </td>
<!--                </tr>
</table>
<br>
</div>

