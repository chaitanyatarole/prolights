<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Modify Item</span>
                    </div>
                </div>
                <div class="portlet-body" style="min-height: 350px;">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/prolights/web/site/updateitem"
                              class="form-horizontal" ng-controller="modifyItemCtrl" onsubmit="submitButton.disabled = true; return true;return smoke.alert('Data Saved');">
                            <div class="form-body">
                                <div class="form-group">
                                    <label style="text-align: right!important;" class="control-label col-sm-5">Select
                                        Category</label>

                                    <div class="col-sm-3">
                                        <select id="select_category"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="" name="category"
                                                ng-model="formInfo.category" ng-change="getData()">
                                            <option value="select">
                                                ------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($categories as $key) {
                                                $categoryID = $key['id'];
                                                $categoryName = $key['name'];
                                                echo "<option value='$categoryID'>$categoryName</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="adddynamicitems">

                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>