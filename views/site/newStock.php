<?php //\app\commands\AppUtility::dump($entireData); ?>
<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">New Stock Entry</span>
                    </div>
                </div>
                <div class="portlet-body" style="min-height: 350px;">
                    <?php if (!empty($entireData)) { ?>
                        <div class="portlet-body form">
                            <form role="form" method="post" action="/prolights/web/site/updatestock"
                                  class="form-horizontal" onsubmit="submitButton.disabled = true; return true;return smoke.alert('Stock Updated');">
                                <div class="form-body">
                                    <div class="form-group panel-group">
                                        <div class="col-sm-6" style="text-align: center;"><label class="control-label">Product</label>
                                        </div>
                                        <div class="col-sm-6" style="text-align: center;"><label
                                                class="control-label">Stock</label>
                                        </div>
                                        <?php foreach ($entireData as $key => $value) { ?>
                                            <div class="form-group">
                                                <input type="hidden" name="id[]" value="<?php echo $value['id']; ?>">
                                                <input type="hidden" name="category_id[]"
                                                       value="<?php echo $value['category_id']; ?>">

                                                <div class="col-sm-6"><input type="text" style="width: 100%;" readonly
                                                                             class="form-control"
                                                                             value="<?php echo $value['name']; ?>"
                                                                             name="name[]"
                                                                             id="product-"></div>
                                                <div class="col-sm-6"><input type="text" style="width: 100%;"
                                                                             class="form-control"
                                                                             value=""
                                                                             name="stock[]"
                                                                             id="description-"></div>
                                            </div>
                                        <?php } ?>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <button type="submit" name="submitButton" style="float: right;" class="btn green"
                                                            id="">Submit
                                                    </button>
                                                </div>
                                                <div class="col-sm-6">
                                                    <button type="button" class="btn default"
                                                            onclick="location.reload();">
                                                        Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    <?php } else { ?>
                        <div class="portlet-body form">
                                <div class="form-body">
                                    <div class="form-group panel-group">
                                        <div class="col-sm-12" style="text-align: center;"><label class="control-label"><b>No New Item Found</b></label>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>