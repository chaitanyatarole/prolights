<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Received Purchase Order</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/prolights/web/site/updateaudioorder"
                              class="form-horizontal" ng-controller="receivedBalajiPurchaseOrderCtrl">
                            <div class="form-body">
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="clientcode" readonly
                                               ng-model="formInfo.clientCode"
                                               ng-init="formInfo.clientCode='<?php echo isset($entireData[0]['clientcode']) ? $entireData[0]['clientcode'] : '' ?>'"
                                               value="<?php echo isset($entireData[0]['clientcode']) ? $entireData[0]['clientcode'] : '' ?>"
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" class="form-control" name="date" readonly
                                               ng-model="formInfo.date"
                                               ng-init="formInfo.date='<?php echo isset($entireData[0]['date']) ? $entireData[0]['date'] : '' ?>'"
                                               value="<?php echo isset($entireData[0]['date']) ? $entireData[0]['date'] : '' ?>"
                                               id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Invoice Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               ng-model="formInfo.purchaseOrder"
                                               ng-init="formInfo.purchaseOrder='<?php echo isset($entireData[0]['purchase_order_no']) ? $entireData[0]['purchase_order_no'] : ''; ?>'"
                                               value="<?php echo isset($entireData[0]['purchase_order_no']) ? $entireData[0]['purchase_order_no'] : ''; ?>"
                                               name="taxNumber" id="invoiceNumber">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" readonly ng-model="formInfo.clientName"
                                               ng-init="formInfo.clientName='<?php echo isset($entireData[0]['client_name']) ? $entireData[0]['client_name'] : '' ?>'"
                                               value="<?php echo isset($entireData[0]['client_name']) ? $entireData[0]['client_name'] : '' ?>"
                                               onkeyup="gencode(this.value);"
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="address" readonly ng-model="formInfo.address"
                                                  ng-init="formInfo.address='<?php echo isset($entireData[0]['address']) ? $entireData[0]['address'] : '' ?>'"
                                                  name="address"><?php echo isset($entireData[0]['address']) ? $entireData[0]['address'] : '' ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" readonly ng-model="formInfo.mobile"
                                               ng-init="formInfo.mobile='<?php echo isset($entireData[0]['phone_no']) ? $entireData[0]['phone_no'] : '' ?>'"
                                               value="<?php echo isset($entireData[0]['phone_no']) ? $entireData[0]['phone_no'] : '' ?>"
                                               name="number" id="number">
                                    </div>
                                    <label class="control-label col-sm-2">Email</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" readonly ng-model="formInfo.email"
                                               ng-init="formInfo.email='<?php echo isset($entireData[0]['email']) ? $entireData[0]['email'] : '' ?>'"
                                               value="<?php echo isset($entireData[0]['email']) ? $entireData[0]['email'] : '' ?>"
                                               name="email" id="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Delivery Location</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" readonly
                                               ng-model="formInfo.deliveryLocation"
                                               ng-init="formInfo.deliveryLocation='<?php echo isset($entireData[0]['delivery_location']) ? $entireData[0]['delivery_location'] : '' ?>'"
                                               value="<?php echo isset($entireData[0]['delivery_location']) ? $entireData[0]['delivery_location'] : '' ?>"
                                               name="eventName"
                                               id="eventName">
                                    </div>
                                    <label class="control-label col-sm-2">Item Return Date</label>

                                    <div class="col-sm-3">
                                        <input type="date" class="form-control" name="returnDate"
                                               ng-model="formInfo.returnDate"
                                               value=""
                                               id="returnDate">
                                    </div>
                                </div>
                                <div class="form-actions">
                                </div>
                                <div class="">
                                    <table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Item
                                        </th>
                                        <th>
                                            Qty.No
                                        </th>
                                        <th>
                                            Rate
                                        </th>
                                        <th>
                                            Amount
                                        </th>
                                        </thead>
                                        <tbody id="taxTable">
                                        <?php foreach ($entireData as $key => $value) {
                                            $serialNumber = $key + 1; ?>
                                            <tr>
                                                <td style="width: 60px;">
                                                    <?php echo $serialNumber; ?>
                                                </td>
                                                <td style="width: 430px;">
                                                    <input type="text" readonly name="particular[]" id=""
                                                           value="<?php echo isset($value['particularName']) ? $value['particularName'] : '' ?>"
                                                           class="form-control">
                                                    <input type="hidden" readonly name="particularID[]" id=""
                                                           ng-model="itemData.particularID<?php echo $serialNumber ?>"
                                                           ng-init="itemData.particularID<?php echo $serialNumber ?>='<?php echo isset($value['particular']) ? $value['particular'] : '' ?>'"
                                                           value="<?php echo isset($value['particular']) ? $value['particular'] : '' ?>"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 216px;padding-right: 10px;">
                                                    <input type="text"
                                                           onkeyup="calculationInWorkOrder(<?php echo $serialNumber ?>);"
                                                           onkeypress="return isNumber(event);"
                                                           id="quantity-<?php echo $serialNumber; ?>"
                                                           name="quantity[]"
                                                           class="form-control"
                                                           ng-model="quantityData.quantity<?php echo $serialNumber ?>"
                                                           ng-init="quantityData.quantity<?php echo $serialNumber ?>='<?php echo isset($value['quantity']) ? $value['quantity'] : '' ?>'"
                                                           value="<?php echo isset($value['quantity']) ? $value['quantity'] : '' ?>">
                                                </td>
                                                <td style="width: 200px;padding-right: 10px;">
                                                    <input type="text"
                                                           onkeyup="calculationInWorkOrder(<?php echo $serialNumber ?>);"
                                                           onkeypress="return isNumber(event);"
                                                           id="rate-<?php echo $serialNumber; ?>"
                                                           name="rate[]"
                                                           ng-change="calculateTotal(<?php echo $serialNumber ?>)"
                                                           ng-model="rateData.rate<?php echo $serialNumber ?>"
                                                           ng-init="rateData.rate<?php echo $serialNumber ?>='<?php echo isset($value['rate']) ? $value['rate'] : '' ?>'"
                                                           value="<?php echo isset($value['rate']) ? $value['rate'] : '' ?>"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 200px;;">
                                                    <input type="text" readonly
                                                           value="<?php echo isset($value['amount']) ? $value['amount'] : '' ?>"
                                                           id="amount-<?php echo $serialNumber; ?>" name="amount[]"
                                                           ng-model="amountData.amount<?php echo $serialNumber; ?>"
                                                           ng-init="amountData.amount<?php echo $serialNumber; ?>=''"
                                                           class="form-control">
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td style="width: 5px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 10px;">
                                            </td>
                                            <td style="width: 60px;">
                                                <strong>Total Amount</strong>
                                            </td>
                                            <td style="width: 30px;;">
                                                <input type="text" readonly
                                                       value="<?php echo isset($entireData[0]['total_amount']) ? $entireData[0]['total_amount'] : '' ?>"
                                                       name="totamt" id="totamt"
                                                       class="form-control">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <input type="hidden" value="1" name="counter" id="counter">

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="button" class="btn green" ng-click="acceptOrder()"
                                                    id="submit">Accept
                                            </button>
                                            <button type="button" class="btn default" ng-click="cancelOrder()">
                                                Reject
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>