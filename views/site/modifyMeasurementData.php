<?php //\app\commands\AppUtility::dump($entireData); ?>
<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Modify Measurement List</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/sai/web/site/updatemodifiedmeasurementdata" onsubmit="return $.LoadingOverlay('show');"
                              class="form-horizontal" onsubmit="return smoke.alert('Data Modified');">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Existing Clients</label>

                                    <div class="col-sm-8">
                                        <select id="clientNameCode" name="clientNameCode"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($clientData as $key) {
                                                $clientName = $key['client_name'];
                                                $clientCode = $key['clientcode'];
                                                echo "<option value='$clientCode'>$clientCode -- $clientName</option>";

                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>
                                <input type="hidden" class="form-control" name="ccodeno" id="ccodeno"
                                       value="<?php echo $maxclientCode; ?>"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="clientcode" readonly
                                               value="<?php echo $entireData[0]['clientCode']; ?>"
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" class="form-control" name="date"
                                               value="<?php echo $entireData[0]['date']; ?>" id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Measurement Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo $entireData[0]['challanNumber']; ?>"
                                               name="challanNumber"
                                               id="date">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['clientName']; ?>"
                                               onkeyup=""
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="address"
                                                  name="address"><?php echo $entireData[0]['address']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['mobile']; ?>" name="number"
                                               id="number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Your Ch. No.</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo isset($entireData[0]['yourChallanNumber']) ? $entireData[0]['yourChallanNumber'] : '' ?>"
                                               name="yourChallanNumber"
                                               id="yourChallanNumber">
                                    </div>
                                    <label class="control-label col-sm-2">P.O. Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo isset($entireData[0]['poNumber']) ? $entireData[0]['poNumber'] : '' ?>"
                                               name="poNumber"
                                               id="poNumber">
                                    </div>
                                </div>
                                <div class="">
                                    <table class="mytable" id="deliveryDataTable" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Size - Dim 1
                                        </th>
                                        <th>
                                            Size - Dim 2
                                        </th>
                                        <th>
                                            Per Sq Ft
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Total Sq Ft
                                        </th>
                                        <th>
                                            RFT
                                        </th>
                                        </thead>
                                        <tbody id="deliveryTable">
                                        <?php
                                        $counter = '';
                                        foreach ($entireData as $key => $value) {
                                            $serialNumber = $key + 1;
                                            $particular = $value['particular'];
                                            $query = "select name from material where id = '$particular'";
                                            $result = Yii::$app->db->createCommand($query)->queryAll();
                                            $name = $result[0]['name'];
                                            ?>
                                            <tr>
                                                <td style="width: 60px;">
                                                    <?php echo $serialNumber; ?>
                                                </td>
                                                <td style="width: 400px;">
                                                    <input type="hidden" value="<?php echo $particular ?>"
                                                           name="particularID[]">
                                                    <input type="text" readonly name="particular[]" class="form-control"
                                                           value="<?php echo $name; ?>">
                                                </td>
                                                <td style="width: 250px;padding-right: 10px;">
                                                    <input type="text" name="dimensionOne[]" required
                                                           onkeyup="calculatePersqftInDelivery(<?php echo $serialNumber; ?>);"
                                                           id="dimensionOne-<?php echo $serialNumber; ?>"
                                                           value="<?php echo $value['dimensionOne']; ?>"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 250px;padding-right: 10px;">
                                                    <input type="text" name="dimensionTwo[]" required
                                                           id="dimensionTwo-<?php echo $serialNumber; ?>"
                                                           value="<?php echo $value['dimensionTwo']; ?>"
                                                           onkeyup="calculatePersqftInDelivery(<?php echo $serialNumber; ?>);"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 250px;padding-right: 10px;">
                                                    <input type="text" readonly
                                                           onkeypress="return isNumber(event);"
                                                           id="persqft-<?php echo $serialNumber; ?>"
                                                           value="<?php echo $value['persqft']; ?>"
                                                           name="persqft[]"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 250px;padding-right: 10px;">
                                                    <input type="text" required
                                                           onkeypress="return isNumber(event);"
                                                           id="quantity-<?php echo $serialNumber; ?>"
                                                           onkeyup="calculatePersqftInDelivery(<?php echo $serialNumber; ?>);"
                                                           value="<?php echo $value['quantity']; ?>"
                                                           name="quantity[]"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 250px;padding-right: 10px;">
                                                    <input type="text" readonly
                                                           onkeyup=""
                                                           onkeypress="return isNumber(event);"
                                                           id="totalsqft-<?php echo $serialNumber; ?>"
                                                           value="<?php echo $value['totalsqft']; ?>"
                                                           name="totalsqft[]"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 250px;padding-right: 10px;">
                                                    <input type="text" required
                                                           onkeyup=""
                                                           onkeypress="return isNumber(event);"
                                                           id="persqft-<?php echo $serialNumber; ?>"
                                                           value="<?php echo $value['rft']; ?>"
                                                           name="rft[]"
                                                           class="form-control">
                                                </td>
                                            </tr>
                                            <?php $counter = $serialNumber;
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <input type="hidden" id="counter" value="<?php echo $counter; ?>">

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="submit" class="btn green" id="submit">Update
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>