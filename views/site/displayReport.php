<script type="text/javascript">
    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }

    function close123() {
        window.location = '/growel/web/site/stockreport';
    }

</script>
<h4 style="text-align: center">Stock Report</h4>
<div class="row">
    <div class="">
        <table class="table">
            <thead>
            <th style="border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-left: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Sr.No
            </th>
            <th style="border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Entry Number
            </th>
            <th style="border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Entry Type
            </th>
            <th style="border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Date
            </th>
            <th style="border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Product Code
            </th>
            <th style="border-bottom: 1px solid #7B7B7B;border-top: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B">
                Quantity
            </th>
            </thead>
            <tbody>
            <?php $invoiceNumber = '';
            foreach ($entireData as $key => $value) {

                $serialNumber = $key + 1; ?>
                <tr>
                    <?php $date = date('d-m-Y', strtotime($value['date']));
                    ?>

                    <td style="border-left: solid 1px #7B7B7B;border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $serialNumber; ?></td>
                    <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['entryNumber']; ?></td>
                    <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['entryType']; ?></td>
                    <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $date; ?></td>
                    <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['productName']; ?></td>
                    <td style="border-bottom: 1px solid #7B7B7B;border-right: 1px solid #7B7B7B"><?php echo $value['quantity']; ?></td>
                </tr>

            <?php
            } ?>
            </tbody>
        </table>
    </div>
    <p align="center" style='margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                        onclick="printing();" value="Print"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
    </p>
</div>