<script src="/prolights/web/global/plugins/jquery.min.js"></script>
<script src="/prolights/web/global/plugins/highcharts.js"></script>
<script src="/prolights/web/global/plugins/data.js"></script>
<script src="/prolights/web/global/plugins/exporting.js"></script>
<!-- Additional files for the Highslide popup effect-->
<script src="https://www.highcharts.com/samples/static/highslide-full.min.js"></script>
<script src="https://www.highcharts.com/samples/static/highslide.config.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="https://www.highcharts.com/samples/static/highslide.css"/>
<script type="application/javascript">
    $(function () {

        var fromDate = $("#fromDate").val();
        var toDate = $("#toDate").val();
        $.ajax({
            url: 'http://localhost/prolights/web/site/amountdeducted',
            type: 'POST',
            data: {
                fromDate: fromDate,
                toDate: toDate
            },
            dataType: 'json',
            success: function (response) {
                response = JSON.parse(response);
                console.log(response);

                var graphPoints = new Array();

                graphPoints[0] = {
                    "name": "Amount Added",
                    "data": response[0].amountAdded
                };
                graphPoints[1] = {
                    "name": "Amount Deducted",
                    "data": response[0].amountDeducted
                };

                $('#container').highcharts({
                    title: {
                        text: 'ProLights Graphical Display',
                        x: -20 //center
                    },
                    subtitle: {
//                text: 'Source: WorldClimate.com',
//                x: -20
                    },
                    xAxis: {
                        categories: response[0].dates
                    },
                    yAxis: {
                        title: {
                            text: 'Amount(Rupees)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        valueSuffix: 'Rupees'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: graphPoints
                });

            },
            error: function (request, error) {
            }
        })
        ;
    })
    ;
</script>
<div>
    <input type="hidden" value="<?php echo $fromDate; ?>" id="fromDate">
    <input type="hidden" value="<?php echo $toDate; ?>" id="toDate">
</div>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
