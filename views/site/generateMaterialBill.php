<style type="text/css">

    /* this is the important part (should be used in HTML head): */
    .pagebreak {
        page-break-after: always;
    }

</style>
<script>
    document.ready(function () {

    });
    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }

    function close123() {
        window.location = '/sai/web/site/materialbill';
    }

</script>
<div class="container">
    <div class="row" style="border: 1px solid;">
        <div class="row">
            <div class="col-xs-2 col-sm-1 text-center" style="padding-right: 0px;padding-left: 0px;">
                <img class="" src="/sai/web/img/sai.png"
                     style="max-height: 100px;margin: 0 auto;margin-top: 4px;margin-left: 20px;">
            </div>
            <div class="col-xs-8 col-sm-9" style="padding-left: 0px;padding-right: 0px;">
                <p style="text-decoration: underline;text-align: center;font-size: 16px;margin-bottom: 0px;"><strong>MATERIAL
                        LIST</strong>
                </p>

                <p style="text-align: center;font-size: 28px;margin-bottom: 0px;"><strong>SAI FABRICATION</strong></p>

                <p style="text-align: center;margin-bottom: 0px;font-size: 12px;"><strong>All Types S.S. Railing,
                        Collapsible
                        Gate, Main Gate,
                        Safety Door,<br>
                        Balcony Door, Balcony Shade, Grill, Window, Rolling Shutter & Fabrication works</strong></p>

                <p style="text-align: center;font-size: 12px;">Vishal Nagar, New D.P Road, Nandgurde School, Pimple
                    Nilakh,
                    Wakad, Pune - 411027 <br>Tel. 020-65008835 Email :sai.fabricator3335@gmail.com </p>
            </div>
            <div class="col-xs-2 col-sm-2"
                 style="padding-right: 0px;padding-left: 0px;position: absolute;right: 0;z-index: 9999;">
                <span style="font-size: 13px;"><strong>Mob: 9049058835<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9405601129</strong></span>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-left" style="width: 50%">
                <table border="1" width="99%" style="height:100px;font-size: 12px">
                    <tr>
                        <td style="border-bottom: hidden">
                            <strong style="padding-left: 4px;">To,</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="400px" rowspan="5" align="left" style="">
                            <label style="padding-left: 20px;">
                                <?php echo $entireData[0]['clientName']; ?><br>
                                <?php echo $entireData[0]['address']; ?>
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="pull-right" style="width: 50%">
                <table border="1" width="100%" style="height: 100px;font-size: 12px;">
                    <tr>
                        <td style="border-right: hidden"><strong style="padding-left: 4px;">Material
                                No: </strong><?php echo $entireData[0]['invoiceNumber']; ?></td>
                        <td style=""><strong>Date : </strong><?php echo date('d-m-Y', strtotime($entireData[0]['date'])); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-12">
            <p style="text-align: center;padding-top: 2px;font-size: 10px;margin-bottom: 4px;margin-top: 4px;">Please
                receive the
                following goods in good condition and
                order</p>
        </div>
        <div class="col-sm-12">
            <table border='1' class="table" width='100%' height="450" align='center' bordercolor='#000000'
                   style='border-collapse:collapse;font-size: 11px;'>

                <tr height="25">
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Sr.No' readonly='true'
                                                             style='border-style : hidden;text-align: center;width:20px; font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Particular' readonly='true'
                                                             style='border-style : hidden;text-align: center;width:150px;font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Size' readonly='true'
                                                             style='border-style : hidden;text-align: center;width:70px;font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Qty' readonly='true'
                                                             style='border-style : hidden;text-align: center;width:70px;font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Per. Sq Ft' readonly='true'
                                                             style='border-style : hidden;text-align: center;width:70px;font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Rate Rs.' readonly='true'
                                                             style='border-style : hidden;text-align: center;width:70px;font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Amount Rs.' readonly='true'
                                                             style='border-style : hidden;text-align: center;width:70px;font-weight:bold'/>
                    </th>
                </tr>

                <?php
                $grandTotal = '';
                $counter = '';
                foreach ($entireData as $key => $value) {
                    $dimensionOne = isset($value['dimensionOne']) ? $value['dimensionOne'] : '';
                    $dimensionTwo = isset($value['dimensionTwo']) ? $value['dimensionTwo'] : '';
                    $displayDimension = $dimensionOne . ' x ' . $dimensionTwo;
                    if ($key == 18) { ?>
                        <tr style="page-break-after:always;" height="28px">
                        </tr>
                        <tr style="height: 20px;"></tr>
                        <tr height="25">
                            <th style="border-top: 1px solid"><input type='text' value='Sr.No' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:30px; font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Particular' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:150px;font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Size' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:70px;font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Qty' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:70px;font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Per. Sq Ft' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:70px;font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Rate Rs.' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:70px;font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Amount Rs.' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:70px;font-weight:bold'/>
                            </th>
                        </tr>
                    <?php }
                    if ($key == 36) { ?>
                        <tr style="page-break-after:always;" height="28px">
                        </tr>
                    <?php }
                    if ($key < 18) { ?>
                        <tr style="" height="28px">
                    <?php } ?>
                    <td align="center"><input type="text" readonly style="border-style: hidden;width: 20px;"
                                              value="<?php echo $key + 1; ?>"></td>
                    <td><input type="text" value="<?php echo $value['particularName']; ?>"
                               style="border-style: hidden;width: 150px;">
                    </td>
                    <td align="center" style="width: 70px;"><?php echo $displayDimension; ?>
                    </td>
                    <td align="center"><input type='text' value='<?php echo $value['quantity']; ?>'
                                              readonly="readonly"
                                              style='border-style : hidden;text-align: center;width:70px;'/></td>
                    <td align="center"><input type='text' value='<?php echo $value['persqft']; ?>'
                                              readonly="readonly"
                                              style='border-style : hidden;text-align: center;width:70px;'/></td>
                    <td align="center"><input type='text' value='<?php echo $value['rate']; ?>'
                                              readonly="readonly"
                                              style='border-style : hidden;text-align: center;width:70px;'/></td>
                    <td align="right"><input type='text' value='<?php echo $value['amount']; ?>'
                                             readonly="readonly"
                                             style='border-style : hidden;text-align: right;width:70px;'/></td>
                    </tr>
                    <?php $counter = $key + 1;
                    $grandTotal += $value['persqft'];
                }
                for ($i = $counter; $i < 10; $i++) { ?>
                    <tr style="" height="25">
                        <td align="center"><input type="text" readonly style="border-style: hidden;width: 20px;"
                                                  value="<?php echo $i + 1; ?>"></td>
                        <td><label></td>
                        <td align="center">
                        </td>
                        <td align="center"></td>
                        <td align="center"></td>
                        <td align="center"></td>
                        <td align="right"></td>
                    </tr>
                <?php } ?>
                <tr height="25">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><strong>Total Sq Ft</strong></td>
                    <td style="text-align: center"><strong><?php echo $grandTotal; ?></strong></td>
                    <td><strong>Net Amount...</strong></td>
                    <td align="right"><input type='text' value='<?php echo $entireData[0]['totalAmount']; ?>'
                                             name='Net_Total'
                                             readonly="readonly"
                                             id='Net_Total'
                                             style='border-style : hidden;text-align: right; font-weight:bold;width:70px;'/>
                    </td>
                </tr>

            </table>
        </div>
        <div class="col-sm-12">
            <div class="pull-left" style="width: 75%">
                <table width="100%" style="height:80px;">
                    <tr>
                        <td style="border-bottom: hidden">
                            <strong style="padding-left: 4px;">Please return the duplicate copy duly stamped &
                                signed.</strong>
                        </td>
                    </tr>
                    <tr style="height: 80px;">
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <strong style="padding-left: 4px;">Receiver's Signature</strong>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="pull-right" style="width: 25%">
                <table width="100%" style="height: 80px;">
                    <tr>
                        <td><strong>For SAI FABRICATION</strong></td>
                    </tr>
                    <tr style="height: 80px;">
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Authorised Signature</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        &nbsp;
        <br/>
    </div>
    <br/>

    <p align="center" style='margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                        onclick="printing();" value="Print"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
    </p>
</div>