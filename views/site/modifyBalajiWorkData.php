<?php //\app\commands\AppUtility::dump($entireData); ?>
<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span
                            class="caption-subject theme-font bold uppercase">Modify Balaji Events Work Order Data</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/prolights/web/site/updatemodifiedbalajiworkdata"
                              class="form-horizontal"
                              onsubmit="submitButton.disabled = true; return true;return smoke.alert('Data Saved');">
                            <div class="form-body">
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>
                                <input type="hidden" class="form-control" name="ccodeno" id="ccodeno"
                                       value="<?php echo $maxclientCode; ?>"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="clientcode" readonly
                                               value="<?php echo $entireData[0]['clientcode']; ?>"
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" class="form-control" name="date"
                                               value="<?php echo $entireData[0]['date']; ?>" id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Invoice Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo $entireData[0]['invoice_no']; ?>"
                                               name="quotationNumber"
                                               id="date">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['client_name']; ?>"
                                               onkeyup=""
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="address"
                                                  name="address"><?php echo $entireData[0]['address']; ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['phone_no']; ?>" name="number"
                                               id="number">
                                    </div>
                                    <label class="control-label col-sm-2">Email</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['email']; ?>" name="email" id="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Event Name</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['eventName']; ?>" name="eventName"
                                               id="eventName">
                                    </div>
                                    <label class="control-label col-sm-2">Event Location</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['eventLocation']; ?>"
                                               name="eventLocation" id="eventLocation">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Total Budget</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['totalBudget']; ?>" name="totalBudget"
                                               id="totalBudget">
                                    </div>
                                    <label class="control-label col-sm-2">Available Budget</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo $entireData[0]['availableBudget']; ?>"
                                               name="availableBudget" id="availableBudget">
                                    </div>
                                </div>
                                <div class="">
                                    <table class="mytable" id="quotationDataTable" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Particulars
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        </thead>
                                        <tbody id="quotationTable">
                                        <?php
                                        $counter = '';
                                        foreach ($entireData as $key => $value) {
                                            $serialNumber = $key + 1;
                                            $particular = $value['particular'];
                                            $query = "select name from product where id = '$particular'";
                                            $result = Yii::$app->db->createCommand($query)->queryAll();
                                            $name = $result[0]['name'];
                                            ?>
                                            <tr>
                                                <td style="width: 60px;">
                                                    <?php echo $serialNumber; ?>
                                                </td>
                                                <td style="width: 400px;">
                                                    <input type="hidden" value="<?php echo $particular ?>"
                                                           name="particularID[]">
                                                    <input type="text" readonly name="particular[]" class="form-control"
                                                           value="<?php echo $name; ?>">
                                                </td>
                                                <td style="width: 250px;padding-right: 10px;">
                                                    <input type="text" required
                                                           onkeyup="calculationInQuotation(<?php echo $serialNumber; ?>);"
                                                           onkeypress="return isNumber(event);"
                                                           id="quantity-<?php echo $serialNumber; ?>"
                                                           value="<?php echo $value['quantity']; ?>"
                                                           name="quantity[]"
                                                           class="form-control">
                                                </td>
                                            </tr>
                                            <?php $counter = $serialNumber;
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <input type="hidden" id="counter" value="<?php echo $counter; ?>">

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="submit" name="submitButton" class="btn green" id="submit">
                                                Update
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>