<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Modify Product</span>
                    </div>
                </div>
                <div class="portlet-body" style="min-height: 350px;">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/prolights/web/site/updatecategory"
                              class="form-horizontal"
                              onsubmit="submitButton.disabled = true; return true;return smoke.alert('Category Modified');">
                            <div class="form-body">
                                <div class="form-group">
                                    <div class="col-sm-12" style="text-align: center;"><label class="control-label">Category</label>
                                    </div>
                                    <?php foreach ($entireData as $key => $value) { ?>
                                        <div class="form-group">
                                            <input type="hidden" name="recordId[]" value="<?php echo $value['id']; ?>">

                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-4"><input type="text" style="width: 100%;"
                                                                         class="form-control"
                                                                         value="<?php echo $value['name']; ?>"
                                                                         name="name[]"
                                                                         id="product-"></div>
                                        </div>
                                    <?php } ?>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <button type="submit" name="submitButton" style="float: right;" class="btn green"
                                                        id="">Submit
                                                </button>
                                            </div>
                                            <div class="col-sm-6">
                                                <button type="button" class="btn default" onclick="location.reload();">
                                                    Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>