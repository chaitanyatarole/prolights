<?php //\app\commands\AppUtility::dump($entireData); ?>
<style type="text/css">

    /* this is the important part (should be used in HTML head): */
    .pagebreak {
        page-break-after: always;
    }

</style>
<style>
    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        padding: 2px !important;
    }

    .table {
        margin-bottom: 4px !important;
    }
</style>
<script>
    var a = ['', 'One ', 'Two ', 'Three ', 'Four ', 'Five ', 'Six ', 'Seven ', 'Eight ', 'Nine ', 'Ten ', 'Eleven ', 'Twelve ', 'Thirteen ', 'Fourteen ', 'Fifteen ', 'Sixteen ', 'Seventeen ', 'Eighteen ', 'Nineteen '];
    var b = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
    $(document).ready(function () {
        inWords($('#Net_Total').val());

    });
    function numberWithCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        var z = 0;
        var len = String(x1).length;
        var num = parseInt((len / 2) - 1);

        while (rgx.test(x1)) {
            if (z > 0) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            else {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
                rgx = /(\d+)(\d{2})/;
            }
            z++;
            num--;
            if (num == 0) {
                break;
            }
        }
        return x1 + x2;
    }
    function inWords(num) {

        num = parseInt(num);

        if ((num = num.toString()).length > 9) return 'overflow';
        n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
        if (!n) return;
        var str = '';
        str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'Crore ' : '';
        str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Lakh ' : '';
        str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'Thousand ' : '';
        str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'Hundred ' : '';
        str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) : '';
        $('#container3').text(str);
        document.getElementById('Net_Total').value = numberWithCommas(num) + ".00";

    }

    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }

    function close123() {
        window.location = '/prolights/web/site/balajiquotationbill';
    }

</script>
<style>
    <!--
    @media screen {
        .bg {
            color: firebrick !important;
        }

        .thbg {
            background-color: #e6afb1 !important;
        }

        #notesbg {
            background-color: #e6afb1 !important;
        }

        .fntclr {
            color: white !important;
        }

        .table > thead:first-child > tr:first-child > th {
            background-color: #e6afb1 !important;
        }

        .bgforp {
            background-color: black !important;
            color: white !important;
        }

        #totalcol {
            background-color: black !important;
            color: white !important;
        }

        .fntclrbudget {
            color: black !important;
        }

        .boxcol {
            background-color: pink !important;
        }

        /*p.bodyText {font-family:verdana, arial, sans-serif;}*/
    }

    @media print {
        .bg {
            color: firebrick !important;
        }

        #notesbg {
            background-color: #e6afb1 !important;
        }

        .bgforp {
            background-color: black !important;
            color: white !important;
        }

        .fntclrbudget {
            color: black !important;
        }

        .fntclr {
            color: white !important;
        }

        .thbg {
            background-color: #e6afb1 !important;
        }

        .table > thead:first-child > tr:first-child > th {
            background-color: #e6afb1 !important;
        }

        #totalcol {
            background-color: black !important;
            color: white !important;
        }

        .boxcol {
            background-color: pink !important;
        }

        /*p.bodyText {font-family:georgia, times, serif;}*/
    }

    @media screen, print {
        .bg {
            color: firebrick !important;
        }

        #notesbg {
            background-color: #e6afb1 !important;
        }

        #totalcol {
            background-color: black !important;
            color: white !important;
        }

        .fntclrbudget {
            color: black !important;
        }

        .thbg {
            background-color: #e6afb1 !important;
        }

        .bgforp {
            background-color: black !important;
        }

        .fntclr {
            color: white !important;
        }

        .table > thead:first-child > tr:first-child > th {
            background-color: #e6afb1 !important;
        }

        .boxcol {
            background-color: pink !important;
        }

        /*p. {font-size:10pt}*/
    }

    -->
</style>
<div class="container" style="float: none;font-size: 10px;">
    <div class="row" style="border: 1px solid;">
        <div class="row">
            <?= $this->render('addressView') ?>
        </div>
        <hr style="border-color: firebrick;margin-top: 0px !important;margin-bottom: 5px !important;">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <label style="font-size: 12px;" class="bg">To: <?php echo $entireData[0]['clientName']; ?></label>
                </div>
                <div class="col-xs-6">
                    <?php if (!empty($entireData[0]['number'])) { ?>
                        <label class="pull-right bg" style="font-size: 12px;">Customer Contact
                            No: <?php echo $entireData[0]['number']; ?> </label>
                    <?php } ?>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <label style="font-size: 12px;" class="bg">Quotation
                        No: <?php echo $entireData[0]['quotationNumber']; ?></label>
                </div>
                <div class="col-xs-6">
                    <label class="pull-right bg"
                           style="font-size: 12px;">Date: <?php echo date('d-m-Y', strtotime($entireData[0]['date'])); ?> </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-6">
                    <label style="font-size: 12px;" class="bg">Event: <?php echo $entireData[0]['eventName']; ?></label>
                </div>
                <div class="col-xs-6">
                    <?php if (!empty($entireData[0]['eventDate'])) { ?>
                        <label class="pull-right bg" style="font-size: 12px;">Event Date
                            No: <?php echo $entireData[0]['eventDate']; ?> </label>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php
        $quotationNumberForTotal = $entireData[0]['quotationNumber'];
        foreach ($entireData as $value) {
            $categoryList = $value['category'];
            $query = "select amount from balajiQuotation WHERE quotationNumber = '$quotationNumberForTotal'";
            $result = Yii::$app->db->createCommand($query)->queryAll();
            ?>
            <?php $totalBudgetOfItems = 0;
            foreach ($result as $subkey => $subvalue) {
                $totalBudgetOfItems += $subvalue['amount'];
            } ?>
        <?php } ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-6 boxcol" style="border: 1px solid;height: 60px;margin-left: 16px;">
                    <label style="font-size: 12px;padding-top: 10px;" class="bg"><span class="fntclrbudget">Total Budget:
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo isset($entireData[0]['totalBudget']) ? $entireData[0]['totalBudget'] : $totalBudgetOfItems; ?></span></label><br>
                    <label style="font-size: 12px;" class="bg"><span class="fntclrbudget">Available Budget:
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></label>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <p style="padding-bottom: 0px;"></p>
        </div>
        <?php
        $quotationNumber = $entireData[0]['quotationNumber'];
        foreach ($entireData as $value) {
            $categoryList = $value['category'];
            $query = "select particularName,quantity,rate,amount,totalAmount,notes from balajiQuotation WHERE quotationNumber = '$quotationNumber' AND category = '$categoryList' ";
            $result = Yii::$app->db->createCommand($query)->queryAll();
            ?>
            <p style="padding-left: 16px;margin-bottom: 0px;margin-top: 8px;width: 258px;margin-left: 15px;"
               class="bgforp">
                <strong class="fntclr" style="font-size: 12px;"><?php echo $categoryList; ?></strong></p>
            <div class="col-sm-12" style="float: none">
                <table border='1' class="table" width='100%' align='center'
                       style='border-collapse:collapse;font-size: 12px;'>
                    <thead class="thbg">
                    <th style="border-bottom: 1px solid">
                        Decription
                    </th>
                    <th style="border-bottom: 1px solid">
                        Unit
                    </th>
                    <th style="border-bottom: 1px solid">
                        Rate
                    </th>
                    <th style="border-bottom: 1px solid">
                        Amount
                    </th>
                    <th style="border-bottom: 1px solid">
                        Notes
                    </th>
                    </thead>
                    <?php
                    $totalAmount = 0;
                    foreach ($result as $subkey => $subvalue) { ?>
                        <tr style="background-color: floralwhite">
                            <td style="border-top: 1px solid;width: 300px"><?php echo $subvalue['particularName']; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $subvalue['quantity']; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $subvalue['rate']; ?></td>
                            <td style="border-top: 1px solid;width: 100px"><?php echo $subvalue['amount']; ?></td>
                            <td style="border-top: 1px solid;width: 150px"><?php echo $subvalue['notes']; ?></td>
                        </tr>
                        <?php
                        $totalAmount += $subvalue['amount'];
                    } ?>
                    <tr>
                        <td style="border-top: 1px solid;width: 300px;border-right: hidden"></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                        <td id="totalcol" style="border-top: 1px solid;width: 100px;">Total</td>
                        <td id="totalcol" style="border-top: 1px solid;width: 100px;"><?php echo $totalAmount; ?></td>
                        <td style="border-top: 1px solid;width: 100px;"></td>
                    </tr>
                </table>
            </div>
        <?php } ?>
        <div class="col-sm-12" style="float: none">
            <table border='1' class="table" width='100%' align='center'
                   style='border-collapse:collapse;font-size: 12px;'>
                <tr>
                    <td id="notesbg"><p style="text-align: center;margin-bottom: 0px;"><strong style="font-size: 12px;">Notes</strong>
                        </p></td>
                </tr>
                <tr>
                    <td style="border-top: 1px solid;width: 300px;">1 Above quote is valid till next 2 days</td>
                </tr>
                <tr>
                    <td style="border-top: 1px solid;width: 300px;">2 Advance is compulsory against Confirmation of the
                        event
                    </td>
                </tr>
                <tr>
                    <td style="border-top: 1px solid;width: 300px;">3 The Balance payment should be done on the day of
                        the Event
                    </td>
                </tr>
                <tr>
                    <td style="border-top: 1px solid;width: 300px;">4 Or else 24$ P.A rate of interest will be charged
                    </td>
                </tr>
                <tr>
                    <td style="border-top: 1px solid;width: 300px;">5 Above listed material is rented for a single event
                        or consider 6 hr
                    </td>
                </tr>
            </table>
        </div>
        <br>
    </div>
    &nbsp;
    <br/>
    <br/>

    <p align="center" style='margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                        onclick="printing();" value="Print"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
    </p>
</div>

