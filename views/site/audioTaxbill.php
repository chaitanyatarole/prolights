<?php //\app\commands\AppUtility::dump($entireData); ?>
<style type="text/css">

    /* this is the important part (should be used in HTML head): */
    .pagebreak {
        page-break-after: always;
    }

</style>
<script>
    var a = ['', 'One ', 'Two ', 'Three ', 'Four ', 'Five ', 'Six ', 'Seven ', 'Eight ', 'Nine ', 'Ten ', 'Eleven ', 'Twelve ', 'Thirteen ', 'Fourteen ', 'Fifteen ', 'Sixteen ', 'Seventeen ', 'Eighteen ', 'Nineteen '];
    var b = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
    $(document).ready(function () {
        inWords($('#Net_Total').val());

    });
    function numberWithCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        var z = 0;
        var len = String(x1).length;
        var num = parseInt((len / 2) - 1);

        while (rgx.test(x1)) {
            if (z > 0) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            else {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
                rgx = /(\d+)(\d{2})/;
            }
            z++;
            num--;
            if (num == 0) {
                break;
            }
        }
        return x1 + x2;
    }
    function inWords(num) {

        num = parseInt(num);

        if ((num = num.toString()).length > 9) return 'overflow';
        n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
        if (!n) return;
        var str = '';
        str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'Crore ' : '';
        str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Lakh ' : '';
        str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'Thousand ' : '';
        str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'Hundred ' : '';
        str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) : '';
        $('#container3').text(str);
        document.getElementById('Net_Total').value = numberWithCommas(num) + ".00";

    }

    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }

    function close123() {
        window.location = '/prolights/web/site/audioinvoice';
    }

</script>
<div class="container" style="float: none">
    <div class="row" style="border: 1px solid;">
        <div class="row">
            <div class="col-xs-2 col-sm-1 text-center" style="padding-right: 0px;padding-left: 0px;">
                <img class="" src="/prolights/web/img/billLogo.png"
                     style="max-height: 100px;margin: 0 auto;margin-top: 4px;margin-left: 20px;width: 150%">
            </div>
            <div class="col-xs-8 col-sm-9" style="padding-left: 0px;padding-right: 0px;">
                <p style="text-decoration: underline;text-align: center;font-size: 16px;margin-bottom: 0px;"><strong>Tax
                        Invoice</strong>
                </p>

                <p style="text-align: center;font-size: 28px;margin-bottom: 0px;"><strong>Audio Energy and Pro
                        Lights</strong><br>D.J.Sunny</p>

                <p style="text-align: center;font-size: 12px;">Shop N0.456, Opp.Hemu Kalani Market,
                    Pimpri, Pune - 411017 <br>Mob. 9822666911 Email :audienergynprolights@gmail.com</p>
            </div>
            <div class="col-xs-2 col-sm-2"
                 style="padding-right: 0px;padding-left: 0px;position: absolute;right: 0;z-index: 9999;">
                <!--                <span style="font-size: 13px;"><strong>Mob: 9049058835<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9405601129</strong></span>-->
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-left" style="width: 50%">
                <table border="1" width="99%" style="height:80px;font-size: 12px">
                    <tr>
                        <td width="400px" rowspan="5" align="left" style="">
                            <label style="padding-left: 20px;">
                                To : <?php echo $entireData['cname']; ?><br>
                                <?php echo $entireData['address']; ?>
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="pull-right" style="width: 50%">
                <table border="1" width="100%" style="height: 80px;font-size: 12px;">
                    <tr>
                        <td style="border-right: hidden"><strong style="padding-left: 4px;">Invoice
                                No: </strong><?php echo $entireData['taxNumber']; ?></td>
                        <td style=""><strong>Date
                                : </strong><?php echo date('d-m-Y', strtotime($entireData['date'])); ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-xs-12">
            <p style="padding-bottom: 5px;"></p>
        </div>
        <div class="col-sm-12" style="float: none">
            <table border='1' class="table" width='100%' height="450" align='center' bordercolor='#000000'
                   style='border-collapse:collapse;font-size: 13px;'>

                <tr height="28px">
                    <th style="text-align:center;border-top: 1px solid"><input type='text' value='Sr.No' readonly='true'
                                                                               style='border-style : hidden;text-align: center;width:40px; font-weight:bold'/>
                    </th>
                    <th style="text-align:center;border-top: 1px solid"><input type='text' value='Particular'
                                                                               readonly='true'
                                                                               style='border-style : hidden;text-align: center;width:150px;font-weight:bold'/>
                    </th>
                    <th style="text-align:center;border-top: 1px solid"><input type='text' value='Qty' readonly='true'
                                                                               style='border-style : hidden;text-align: center;width:60px;font-weight:bold'/>
                    </th>
                    <th style="text-align:center;border-top: 1px solid"><input type='text' value='Rate' readonly='true'
                                                                               style='border-style : hidden;text-align: center;width:60px;font-weight:bold'/>
                    <th style="text-align:center;border-top: 1px solid"><input type='text' value='Amount'
                                                                               readonly='true'
                                                                               style='border-style : hidden;text-align: center;width:60px;font-weight:bold'/>
                    </th>
                </tr>

                <?php $description = $entireData['particular'];
                $counter = '';
                foreach ($description as $key => $value) {
                    $query = "select name from product where id = '$value'";
                    $result = Yii::$app->db->createCommand($query)->queryAll();
                    $name = isset($result[0]['name']) ? $result[0]['name'] : '';
                    if ($key == 20) { ?>
                        <tr style="page-break-after:always;" height="28px">
                        </tr>
                        <tr style="height: 20px;"></tr>
                        <tr>
                            <th style="border-top: 1px solid"><input type='text' value='Sr.No' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:40px; font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Particular' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:150px;font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Qty' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:60px;font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Rate' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:60px;font-weight:bold'/>
                            <th style="border-top: 1px solid"><input type='text' value='Amount' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:60px;font-weight:bold'/>
                            </th>
                        </tr>
                    <?php }
                    if ($key == 36) { ?>
                        <tr style="page-break-after:always;" height="28px">
                        </tr>
                        <tr style="height: 20px;"></tr>
                    <?php }
                    if ($key < 18) { ?>
                        <tr style="" height="28px">
                    <?php } ?>
                    <td align="center" style="border-top: 1px solid"><?php echo $key + 1; ?></td>
                    <td style="border-top: 1px solid"><?php echo $value; ?></td>
                    <td align="center" style="border-top: 1px solid"><input type='text'
                                                                            value='<?php echo $entireData['quantity'][$key]; ?>'
                                                                            readonly="readonly"
                                                                            style='border-style : hidden;text-align: center;width:60px;'/>
                    </td>
                    <td align="center" style="border-top: 1px solid"><input type='text'
                                                                            value='<?php echo $entireData['rate'][$key];; ?>'
                                                                            readonly="readonly"
                                                                            style='border-style : hidden;text-align: center;width:60px;'/>
                    </td>
                    <td align="right" style="border-top: 1px solid"><input type='text'
                                                                           value='<?php echo $entireData['amount'][$key];; ?>'
                                                                           readonly="readonly"
                                                                           style='border-style : hidden;text-align: center;width:60px;'/>
                    </td>
                    </tr>
                    <?php $counter = $key + 1;
                }
                for ($i = $counter; $i < 10; $i++) { ?>
                    <tr style="" height="10">
                        <td align="center"><?php echo $i + 1; ?></td>
                        <td></td>
                        <td align="center"></td>
                        <td align="center"></td>
                        <td align="center"></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td style="border-right: hidden"></td>
                    <td style="border-right: hidden"></td>
                    <td style="border-right: hidden"></td>
                    <td><strong><span style="font-size: 12px">Total</span></strong></td>
                    <td align="right"><input type='text' value='<?php echo $entireData['totamt']; ?>'
                                             name='Net_Total'
                                             readonly="readonly"
                                             id='Net_Total'
                                             style='border-style : hidden;text-align: right; font-weight:bold;width:70px;'/>
                    </td>
                </tr>
                <tr>
                    <td style="border-right: hidden"></td>
                    <td style="border-right: hidden"></td>
                    <td style="border-right: hidden"></td>
                    <td><strong><span style="font-size: 12px">Service Tax (<?= $entireData['selectVAT']?>%)</span></strong></td>
                    <td align="right"><input type='text' value='<?php echo $entireData['vat']; ?>'
                                             name='Net_Total'
                                             readonly="readonly"
                                             id='Net_Total'
                                             style='border-style : hidden;text-align: right; font-weight:bold;width:70px;'/>
                    </td>
                </tr>
                <tr>
                    <td style="border-right: hidden"></td>
                    <td style="border-right: hidden"></td>
                    <td style="border-right: hidden"></td>
                    <td><strong><span style="font-size: 12px">Net Amount</span></strong></td>
                    <td align="right"><input type='text' value='<?php echo $entireData['grandtot']; ?>'
                                             name='Net_Total'
                                             readonly="readonly"
                                             id='Net_Total'
                                             style='border-style : hidden;text-align: right; font-weight:bold;width:70px;'/>
                    </td>
                </tr>
                <tr style="text-align: left">
                    <td colspan="7" style="text-align: left;border-left: ;border-top: "> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>
                            <span style="font-size: 14px;">Amount in words : </span><span id="container3"
                                                                                          style="font-size:14px;font-weight:bold"></span><span
                                style="font-size: 13px;"> Only</span>
                        </strong></td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12">
            <table width="100%" style="font-size: 12px;">
                <tr>
                    <td><label style="">Terms & Conditions</label></td>
                </tr>
                <tr>
                    <td style="padding-left: 20px;">• Payment on the Event or else interest will be charged @ 18 % p.a.
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 20px;">
                        • The above mentioned item are rented for 4 hrs
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 20px;">
                        • The above rates are inclusive of Transportation
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 20px;">
                        • Additional Sound Output for D.J. System will be charged extra.
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 20px;">
                        • Above mentioned rates are for only regular days.
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 20px;">
                        • Special Rates for Special Days. Urgent order will be charged extra.
                    </td>
                </tr>
                <!--                <tr>-->
                <!--                    <td style="float: right;padding-right: 60px;">Your faithfully</td>-->
                <!--                </tr>-->
                <!--                <tr>-->
                <!--                    <td style="float: right;padding-right: 55px;padding-top: 60px;">-->
                <!--                        Sai Fabrication-->
                <!--                    </td>-->
                <!--                </tr>-->
            </table>
            <br>
        </div>
    </div>
    &nbsp;
    <br/>
    <br/>

    <p align="center" style='margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                        onclick="printing();" value="Print"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
    </p>
</div>

