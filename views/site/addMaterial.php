<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Add Item</span>
                    </div>
                </div>
                <div class="portlet-body" style="min-height: 350px;">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/sai/web/site/savematerial"
                              onsubmit="return $.LoadingOverlay('show');"
                              class="form-horizontal">
                            <div class="form-body">
                                <div class="form-group">
                                    <label style="text-align: right!important;" class="control-label col-sm-5"><strong>Select
                                            Number of Items to Add</strong></label>

                                    <div class="col-sm-4">
                                        <select id="number_of_materials"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="">
                                            <option value="select">
                                                ------------------------Select------------------------
                                            </option>
                                            <?php
                                            for ($i = 0; $i < 10; $i++) {
                                                $serial = $i + 1;
                                                echo "<option value='$serial'>$serial</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="adddynamicitems">

                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>