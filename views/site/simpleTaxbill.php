<script>
    var a = ['', 'One ', 'Two ', 'Three ', 'Four ', 'Five ', 'Six ', 'Seven ', 'Eight ', 'Nine ', 'Ten ', 'Eleven ', 'Twelve ', 'Thirteen ', 'Fourteen ', 'Fifteen ', 'Sixteen ', 'Seventeen ', 'Eighteen ', 'Nineteen '];
    var b = ['', '', 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
    $(document).ready(function () {
        inWords($('#Net_Total').val());

    });
    function numberWithCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        var z = 0;
        var len = String(x1).length;
        var num = parseInt((len / 2) - 1);

        while (rgx.test(x1)) {
            if (z > 0) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            else {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
                rgx = /(\d+)(\d{2})/;
            }
            z++;
            num--;
            if (num == 0) {
                break;
            }
        }
        return x1 + x2;
    }
    function inWords(num) {

        num = parseInt(num);

        if ((num = num.toString()).length > 9) return 'overflow';
        n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
        if (!n) return;
        var str = '';
        str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'Crore ' : '';
        str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'Lakh ' : '';
        str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'Thousand ' : '';
        str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'Hundred ' : '';
        str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) : '';
        $('#container3').text(str);
        document.getElementById('Net_Total').value = numberWithCommas(num) + ".00";

    }
    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }
    function close123() {
        window.location = '/sai/web/site/taxsimple';
    }

</script>

<?php $d = $entireData['date'];
$date = date('d-m-Y', strtotime($d));
?>

<div class="container">
    <div class="row" style="border: 1px solid;">
        <div class="row">
            <div class="col-xs-2 col-sm-1 text-center" style="padding-right: 0px;padding-left: 0px;">
                <img class="" src="/sai/web/img/sai.png"
                     style="max-height: 100px;margin: 0 auto;margin-top: 4px;margin-left: 20px;">
            </div>
            <div class="col-xs-8 col-sm-9" style="padding-left: 0px;padding-right: 0px;">
                <p style="text-decoration: underline;text-align: center;font-size: 16px;margin-bottom: 0px;"><strong>TAX BILL</strong>
                </p>

                <p style="text-align: center;font-size: 28px;margin-bottom: 0px;"><strong>SAI FABRICATION</strong></p>

                <p style="text-align: center;margin-bottom: 0px;font-size: 12px;"><strong>All Types S.S. Railing,
                        Collapsible
                        Gate, Main Gate,
                        Safety Door,<br>
                        Balcony Door, Balcony Shade, Grill, Window, Rolling Shutter & Fabrication works</strong></p>

                <p style="text-align: center;font-size: 12px;">Vishal Nagar, New D.P Road, Nandgurde School, Pimple
                    Nilakh,
                    Wakad, Pune - 411027 <br>Tel. 020-65008835 Email :sai.fabricator3335@gmail.com </p>
            </div>
            <div class="col-xs-2 col-sm-2"
                 style="padding-right: 0px;padding-left: 0px;position: absolute;right: 0;z-index: 9999;">
                <span style="font-size: 13px;"><strong>Mob: 9049058835<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9405601129</strong></span>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-left" style="width: 50%">
                <table border="1" width="99%" style="height:100px;font-size: 12px">
                    <tr>
                        <td style="border-bottom: hidden">
                            <strong style="padding-left: 4px;">To,</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="400px" rowspan="5" align="left" style="font-size: 13px;">
                            <label style="padding-left: 20px;">
                                <?php echo $entireData['cname']; ?><br>
                                <?php echo $entireData['address']; ?>
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="pull-right" style="width: 50%">
                <table border="1" width="100%" style="height: 100px;">
                    <tr>
                        <td style="border-bottom: hidden;border-right: hidden"><strong
                                style="padding-left: 4px; font-size: 13px;">Invoice
                                No: </strong><?php echo $entireData['taxNumber']; ?></td>
                        <td style="border-bottom: hidden"><strong>Date : </strong><?php echo $date; ?></td>
                    </tr>
                    <tr>
                        <td style="border-bottom: hidden;border-right: hidden"><strong
                                style="padding-left: 4px; font-size: 13px;">Challan
                                No: </strong><?php echo $entireData['challan_number']; ?></td>
                        <td style="border-bottom: hidden"><strong>Date :</strong>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: hidden;border-right: hidden"><strong
                                style="padding-left: 4px; font-size: 13px;">P.O.
                                No: </strong><?php echo $entireData['ponumber']; ?></td>
                        <td style="border-bottom: hidden"><strong>VAT NO</strong>
                            : <?php echo isset($entireData['vat_no']) ? $entireData['vat_no'] : ''; ?></td>

                    </tr>
                    <tr>
                        <td style="border-right: hidden"><strong style="padding-left: 4px; font-size: 13px;">Vehicle
                                No: </strong><?php echo $entireData['vehicalnumber']; ?></td>
                        <td><strong>CST NO : </strong><?php echo isset($entireData['cst_no']) ? $entireData['cst_no'] : ''; ?></td>

                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-12" style="border-bottom: hidden;">
            <p style="text-align: center;padding-top: 2px;">Please receive the following goods in good condition and
                order</p>
        </div>

        <div class="col-sm-12">
            <table border='1' width='100%' height="300px;" align='center' bordercolor='#000000'
                   style='font-size: 14px;'>


                <tr height="10px;">
                    <th style="text-align: center">Sr. No</th>
                    <th style="text-align: center"><input type='text' value='Particular' readonly='true'
                                                          style='border-style : hidden;text-align: center;width:100px;font-weight:bold'/></th>
                    <th style="text-align: center"><input type='text' value='Qty' readonly='true'
                                                          style='border-style : hidden;text-align: center; width:70px;font-weight:bold'/></th>
                    <th style="text-align: center"><input type='text' value='Rate Rs.' readonly='true'
                                                          style='border-style : hidden;text-align: center;width:70px;font-weight:bold'/></th>
                    <th style="text-align: center"><input type='text' value='Amount Rs.' readonly='true'
                                                          style='border-style : hidden;text-align: center;width:80px;font-weight:bold'/></th>
                </tr>

                <?php $particular = $entireData['particular'];
                $counter = '';
                foreach ($particular as $key => $value) {
                    $query = "select * from product where id = '$value'";
                    $result = Yii::$app->db->createCommand($query)->queryAll();
                    if (!empty ($result)) {
                        $productName = $result[0]['product_name'];?>

                        <tr style="border-bottom:hidden;" height="20">
                            <td align="center" style="width: 40px;"><label><?php echo $key + 1; ?></label></td>
                            <td style="width: 150px;"><label><?php echo $productName; ?></label></td>

                            <td align="center"><input type='text' value='<?php echo $entireData['quantity'][$key]; ?>'
                                                      readonly="readonly"
                                                      style='border-style : hidden;text-align: center;width:70px;'/>
                            </td>
                            </td>
                            <td align="center"><input type='text' value='<?php echo $entireData['rate'][$key]; ?>'
                                                      readonly="readonly"
                                                      style='border-style : hidden;text-align: center;width:50px;'/>
                            </td>
                            <td align="right"><input type='text' value='<?php echo $entireData['amount'][$key]; ?>'
                                                     readonly="readonly"
                                                     style='border-style : hidden;text-align: right;width:70px;'/></td>
                        </tr>
                        <?php $counter = $key + 1;
                    }
                }
                for ($i = $counter; $i < 10; $i++) { ?>
                    <tr style="border-bottom:hidden;" height="20px;">
                        <td align="center" style="width: 40px;"></td>
                        <td style='width:150px;'><label></td>
                        <td align="center" style='width:90px;'></td>
                        <td align="center" style='width:70px;'></td>
                        <td align="right" style='width:70px;'></td>
                    </tr>
                <?php } ?>

                <tr>
                    <td style="width: 40px;">&nbsp;</td>
                    <td style='width:150px;'></td>
                    <td style='width:70px;'></td>
                    <td style='width:70px;'></td>
                    <td style='width:70px;'></td>
                </tr>

                <tr height="20px;">
                    <td colspan="3" align="center" style="border-bottom: hidden"></td>
                    <td><strong>Total</strong></td>
                    <td align="right"><input type='text' value='<?php echo $entireData['totamt']; ?>' name='tot'
                                             id='tot'
                                             readonly="readonly"
                                             style='border-style : hidden;text-align: right; font-weight:bold;width:70px;'/>
                    </td>
                </tr>

                <tr height="20px;">
                    <td colspan="3" rowspan="2" align="right"></td>
                    <td align="left"><strong>VAT 12.5%</strong></td>
                    <td align="right"><input type='text' value='<?php echo $entireData['vat']; ?>' name='vat'
                                             id='vat'
                                             readonly="readonly"
                                             style='border-style : hidden;text-align: right;width:70px;font-weight: bold'/>
                    </td>
                </tr>


                <tr height="20px;">

                    <td><strong><span style="font-size: 12px"> Net Amount...</span></strong></td>
                    <td align="right"><input type='text' value='<?php echo $entireData['grandtot']; ?>'
                                             name='Net_Total'
                                             readonly="readonly"
                                             id='Net_Total'
                                             style='border-style : hidden;text-align: right; font-weight:bold;width:80px;'/>
                    </td>
                </tr>


                <tr style="text-align: left">
                    <td colspan="7" style="text-align: left;border-left: ;border-top: "> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>
                            <span style="font-size: 13px;">Amount in words : </span><span id="container3"
                                                                                          style="font-size:14px;font-weight:bold"></span><span
                                style="font-size: 13px;"> Only</span>
                        </strong></td>
                </tr>
            </table>
            &nbsp;&nbsp;&nbsp;

        </div>

        <div class="col-sm-12">
            <div class="pull-left" style="width: 69%;border: 1px solid;">
                <table width="95%" style="height:80px;">
                    <tr>
                        <td style="border-bottom: hidden">
                            <strong style="padding-left: 4px;font-size: 12px;">VAT TIN No.:27870934016 V w.e.f.
                                15/09/2012</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong style="font-size: 12px;">PCMC-LBT-0012069</strong>

                        </td>

                    </tr>
                    <tr>
                        <td style="border-bottom: hidden">
                            <strong style="padding-left: 4px;font-size: 12px;">CST TIN No.:27870934016 C w.e.f.
                                15/09/2012</strong>
                        </td>
                    </tr>
                    <br/>
                    <tr style="height: 80px; font-size: 10px;">
                        <td>"I/We heareby certify that my/our Registration Certificate under the Maharashtra Value<br/>
                            Added Tax Act,2002 is in Force on the Date on which the Sales of the goods specified in<br/>
                            this Tax Invoice is made by me/us & that the transaction of the sales covered by this
                            Tax<br/>
                            Invoice has been effected by me/us & shall be accounted for in the turnover of sales
                            while<br/>
                            filling of return and the due tax if any payable on the sales has been paid or shall be
                            paid."
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: hidden"><br/>
                            <strong style="padding-left: 1px;font-size: 14px;">PAN No.:BVIPS4890J</strong>
                        </td>
                    </tr>

                </table>
            </div>
            <div class="pull-right" style="width: 31%; border: 1px solid; border-left: hidden;">
                <table width="95%" style="height: 80px;">
                    <tr style="height: 80px;">
                        <td style="padding-left: 40px; font-size: 13px;"><strong>For SAI FABRICATION</strong></td>
                    </tr>
                    <tr style="height: 20px;">
                        <td></td>
                    </tr>

                    <tr>
                        <td style="padding-left: 40px; height: 80px;">
                            <strong style="font-size: 13px;">Authorised Signature</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        &nbsp;
        <br/>
    </div>
    <br/>

    <div class="col-sm-10">
        <p align="center" style='margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                            onclick="printing();" value="Print"/>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
        </p>
    </div>
</div>
