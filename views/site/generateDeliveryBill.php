<style type="text/css">

    /* this is the important part (should be used in HTML head): */
    .pagebreak {
        page-break-after: always;
    }

</style>
<script>
    document.ready(function () {

    });
    function printing() {
        var printButton = document.getElementById("bprint");
        var close = document.getElementById("close");
        printButton.style.visibility = 'hidden';
        close.style.visibility = 'hidden';
        window.print();

        printButton.style.visibility = 'visible';
        close.style.visibility = 'visible';
    }

    function close123() {
        window.location = '/sai/web/site/deliverybill';
    }

</script>
<div class="container">
    <div class="row" style="border: 1px solid;">
        <div class="row">
            <div class="col-xs-2 col-sm-1 text-center" style="padding-right: 0px;padding-left: 0px;">
                <img class="" src="/sai/web/img/sai.png"
                     style="max-height: 100px;margin: 0 auto;margin-top: 4px;margin-left: 20px;">
            </div>
            <div class="col-xs-8 col-sm-9" style="padding-left: 0px;padding-right: 0px;">
                <p style="text-decoration: underline;text-align: center;font-size: 16px;margin-bottom: 0px;"><strong>DELIVERY
                        CHALLAN</strong>
                </p>

                <p style="text-align: center;font-size: 28px;margin-bottom: 0px;"><strong>SAI FABRICATION</strong></p>

                <p style="text-align: center;margin-bottom: 0px;font-size: 12px;"><strong>All Types S.S. Railing,
                        Collapsible
                        Gate, Main Gate,
                        Safety Door,<br>
                        Balcony Door, Balcony Shade, Grill, Window, Rolling Shutter & Fabrication works</strong></p>

                <p style="text-align: center;font-size: 12px;">Vishal Nagar, New D.P Road, Nandgurde School, Pimple
                    Nilakh,
                    Wakad, Pune - 411027 <br>Tel. 020-65008835 Email :sai.fabricator3335@gmail.com </p>
            </div>
            <div class="col-xs-2 col-sm-2"
                 style="padding-right: 0px;padding-left: 0px;position: absolute;right: 0;z-index: 9999;">
                <span style="font-size: 13px;"><strong>Mob: 9049058835<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;9405601129</strong></span>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="pull-left" style="width: 50%">
                <table border="1" width="99%" style="height:100px;font-size: 12px">
                    <tr>
                        <td style="border-bottom: hidden">
                            <strong style="padding-left: 4px;">To,</strong>
                        </td>
                    </tr>
                    <tr>
                        <td width="400px" rowspan="5" align="left" style="">
                            <label style="padding-left: 20px;">
                                <?php echo $entireData[0]['clientName']; ?><br>
                                <?php echo $entireData[0]['address']; ?>
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="pull-right" style="width: 50%">
                <table border="1" width="100%" style="height: 100px;font-size: 12px;">
                    <tr>
                        <td style="border-bottom: hidden;border-right: hidden"><strong style="padding-left: 4px;">Challan
                                No: </strong><?php echo $entireData[0]['challanNumber']; ?></td>
                        <td style="border-bottom: hidden"><strong>Date
                            : </strong><?php echo date('d-m-Y', strtotime($entireData[0]['date'])); ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: hidden;border-right: hidden"><strong style="padding-left: 4px;">P.O.
                                No: </strong><?php echo $entireData[0]['poNumber']; ?></td>
                        <td style="border-bottom: hidden"><strong>Date :</strong></td>

                    </tr>
                    <tr>
                        <td style="border-right: hidden"><strong style="padding-left: 4px;">Your Ch.
                                No: </strong><?php echo $entireData[0]['yourChallanNumber']; ?></td>
                        <td><strong>Date :</strong></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-12">
            <p style="text-align: center;padding-top: 2px;font-size: 10px;margin-bottom: 4px;margin-top: 4px;">Please
                receive the
                following goods in good condition and
                order</p>
        </div>
        <div class="col-sm-12">
            <table border='1' class="table" width='100%' height="450" align='center' bordercolor='#000000'
                   style='border-collapse:collapse;font-size: 11px;'>

                <tr height="25">
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Sr.No' readonly='true'
                                                             style='border-style : hidden;text-align: center;width:40px; font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Particular' readonly='true'
                                                             style='border-style : hidden;text-align: center;width:180px;font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Size' readonly='true'
                                                             style='border-style : hidden;text-align: center;width:80px;font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Per. Sq Ft' readonly='true'
                                                             style='border-style : hidden;text-align: center;width:80px;font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Qty' readonly='true'
                                                             style='border-style : hidden;text-align: center;width:80px;font-weight:bold'/>
                    </th>
                    <th style="text-align: center;border-top: 1px solid"><input type='text' value='Total. Sq Ft' readonly='true'
                                                             style='border-style : hidden;text-align: center;width:80px;font-weight:bold'/>
                    </th>
                </tr>

                <?php
                $counter = '';
                foreach ($entireData as $key => $value) {
                    $dimensionOne = isset($value['dimensionOne']) ? $value['dimensionOne'] : '';
                    $dimensionTwo = isset($value['dimensionTwo']) ? $value['dimensionTwo'] : '';
                    $displayDimension = $dimensionOne . ' x ' . $dimensionTwo;
                    if ($key == 18) { ?>
                        <tr style="page-break-after:always;" height="25">
                        </tr>
                        <tr style="" height="25">
                        </tr>
                        <tr>
                            <th style="border-top: 1px solid"><input type='text' value='Sr.No' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:40px; font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Particular' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:180px;font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Size' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:80px;font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Per. Sq Ft' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:80px;font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Qty' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:80px;font-weight:bold'/>
                            </th>
                            <th style="border-top: 1px solid"><input type='text' value='Total. Sq Ft' readonly='true'
                                                                     style='border-style : hidden;text-align: center;width:80px;font-weight:bold'/>
                            </th>
                        </tr>
                    <?php }
                    if ($key == 36) { ?>
                        <tr style="page-break-after:always;" height="25">
                        </tr>
                    <?php }
                    if ($key < 18) { ?>
                        <tr style="" height="25">
                    <?php } ?>
                    <td align="center"><label><?php echo $key + 1; ?></label></td>
                    <td><?php echo $value['particularName']; ?></td>
                    <td align="center">
                        <label><?php echo $displayDimension; ?></label>
                    </td>
                    <td align="center"><input type='text' value='<?php echo $value['persqft']; ?>'
                                              readonly="readonly"
                                              style='border-style : hidden;text-align: center;width:80px;'/></td>
                    <td align="center"><input type='text' value='<?php echo $value['quantity']; ?>'
                                              readonly="readonly"
                                              style='border-style : hidden;text-align: center;width:80px;'/></td>
                    <td align="center"><input type='text' value='<?php echo $value['totalsqft']; ?>'
                                              readonly="readonly"
                                              style='border-style : hidden;text-align: center;width:80px;'/></td>

                    </tr>
                    <?php $counter = $key + 1;
                }
                for ($i = $counter; $i < 10; $i++) { ?>
                    <tr style="" height="10">
                        <td align="center"><label><?php echo $i + 1; ?></label></td>
                        <td><label></td>
                        <td align="center">
                        </td>
                        <td align="center"></td>
                        <td align="center"></td>
                        <td align="right"></td>
                    </tr>
                <?php } ?>
                <tr>
                    <td style="border-right: hidden"></td>
                    <td style="border-right: hidden"><strong>PCMC-LBT-0012069</strong></td>
                    <td style="border-right: hidden"></td>
                    <td style="border-right: hidden"></td>
                    <td style="border-right: hidden"></td>
                    <td></td>
                </tr>
            </table>
        </div>
        <div class="col-sm-12">
            <div class="pull-left" style="width: 69%;border: 1px solid;">
                <table width="95%" style="height:80px;">
                    <tr>
                        <td style="border-bottom: hidden">
                            <strong style="padding-left: 4px;font-size: 12px;">VAT TIN No.:27870934016 V w.e.f.
                                15/09/2012</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong style="font-size: 12px;">PCMC-LBT-0012069</strong>

                        </td>

                    </tr>
                    <tr>
                        <td style="border-bottom: hidden">
                            <strong style="padding-left: 4px;font-size: 12px;">CST TIN No.:27870934016 C w.e.f.
                                15/09/2012</strong>
                        </td>
                    </tr>
                    <br/>
                    <tr style="height: 80px; font-size: 10px;">
                        <td>"I/We heareby certify that my/our Registration Certificate under the Maharashtra Value<br/>
                            Added Tax Act,2002 is in Force on the Date on which the Sales of the goods specified in<br/>
                            this Tax Invoice is made by me/us & that the transaction of the sales covered by this
                            Tax<br/>
                            Invoice has been effected by me/us & shall be accounted for in the turnover of sales
                            while<br/>
                            filling of return and the due tax if any payable on the sales has been paid or shall be
                            paid."
                        </td>
                    </tr>
                    <tr>
                        <td style="border-bottom: hidden"><br/>
                            <strong style="padding-left: 1px;font-size: 14px;">PAN No.:BVIPS4890J</strong>
                        </td>
                    </tr>

                </table>
            </div>
            <div class="pull-right" style="width: 31%; border: 1px solid; border-left: hidden;">
                <table width="95%" style="height: 80px;">
                    <tr style="height: 80px;">
                        <td style="padding-left: 40px; font-size: 13px;"><strong>For SAI FABRICATION</strong></td>
                    </tr>
                    <tr style="height: 20px;">
                        <td></td>
                    </tr>

                    <tr>
                        <td style="padding-left: 40px; height: 80px;">
                            <strong style="font-size: 13px;">Authorised Signature</strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        &nbsp;
        <br/>
    </div>
    <br/>

    <p align="center" style='margin-bottom:1px;margin-top:10px;'><input id="bprint" type="button" name="Submit"
                                                                        onclick="printing();" value="Print"/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input id="close" type="button" name="close" onclick="close123();" value="Close"/>
    </p>
</div>

