<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Employee Wages</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/prolights/web/site/savewages"
                              class="form-horizontal" ng-controller="wagesController">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Existing Employee</label>

                                    <div class="col-sm-8">
                                        <select id="" name="clientNameCode" ng-model="wages.clientNameCode"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="" ng-change="getDetails()">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($clientData as $key) {
                                                $clientName = $key['client_name'];
                                                $clientCode = $key['clientcode'];
                                                echo "<option value='$clientCode'>$clientCode -- $clientName</option>";

                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" ng-model="wages.ccode" name="ccode" id="ccode"/>
                                <input type="hidden" class="form-control" ng-init="<?php echo $maxclientCode; ?>" ng-model="wages.ccodeno" name="ccodeno" id="ccodeno"
                                       value="<?php echo $maxclientCode; ?>"/>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Employee Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" ng-model="wages.employeeCode" name="clientcode" readonly value=""
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" class="form-control" ng-model="wages.date" name="date" value="" id="date">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Employee Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" ng-model="wages.employeeName" value="" ng-keyup="generateCode()"
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" ng-model="wages.address" id="address" name="address"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" ng-model="wages.number" name="number" id="number">
                                    </div>
                                    <label class="control-label col-sm-2">Email</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" value="" ng-model="wages.email" name="email" id="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Remark</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="remark" ng-model="wages.remark" name="remark"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Wage For Month</label>

                                    <div class="col-sm-2">
                                        <select id="" name="clientNameCode" ng-model="wages.month"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select..." tabindex="-1" title="">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <option value="jan">Janaury</option>
                                            <option value="feb">February</option>
                                            <option value="march">March</option>
                                            <option value="april">April</option>
                                            <option value="may">May</option>
                                            <option value="june">June</option>
                                            <option value="july">July</option>
                                            <option value="aug">August</option>
                                            <option value="sep">September</option>
                                            <option value="oct">October</option>
                                            <option value="nov">November</option>
                                            <option value="dec">December</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Enter Wage</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" ng-model="wages.wage" value="" onkeypress="return isNumber1(event);" name="wages" id="wages">

                                    </div>
                                </div>
<!--                                <div class="form-actions">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-9 col-md-9">-->
<!--                                            <button type="button" class="btn green" onclick="addRowInPurchaseOrder();">-->
<!--                                                Add-->
<!--                                                Row-->
<!--                                            </button>-->
<!--                                            <button type="button" class="btn default" onclick="deleteRowInWorkOrder();">-->
<!--                                                Delete-->
<!--                                                Row-->
<!--                                            </button>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="button" class="btn green" id="submit" ng-disabled="isDisabled"
                                                    ng-click="saveData()">Submit
                                            </button>
                                            <button type="button" class="btn default" onclick="location.reload();">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>