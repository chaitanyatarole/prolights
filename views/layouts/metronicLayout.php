<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
Version: 3.2.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Pro Lights</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="/prolights/web/global/plugins/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/global/plugins/simple-line-icons.min.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/global/plugins/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/global/plugins/uniform.default.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/global/plugins/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/global/plugins/select2.css" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="/prolights/web/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css">
    <link href="/prolights/web/global/css/plugins-md.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/layout3/css/layout.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
    <link href="/prolights/web/layout3/css/custom.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/css/smoke.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/css/purchase.css" rel="stylesheet" type="text/css">
    <!-- END THEME STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<body ng-app="proLights">
<input type="hidden" id="baseUrl" value="<?php echo Yii::$app->request->baseUrl; ?>">
<!-- BEGIN HEADER -->
<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <img src="/prolights/web/img/prologo.png" style="height: 75px;">

            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <li class="droddown dropdown-separator">
                        <span class="separator"></span>
                    </li>
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <!--                            <img alt="" class="img-circle" src="/sai/web/img/photo(1).jpg">-->
                            <span class="username username-hide-mobile">Admin</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="<?php echo Yii::$app->request->baseUrl ?>">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->
    <!-- BEGIN HEADER MENU -->
    <div class="page-header-menu">
        <div class="container">
            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu ">
                <ul class="nav navbar-nav">
                    <!--                    <li>-->
                    <!--                        <a href="-->
                    <?php //echo Yii::$app->request->baseUrl ?><!--/site/cliententry">Client Code Entry</a>-->
                    <!--                    </li>-->
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Audio Prolights<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class="dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/cliententry"><i
                                        class="icon-pencil"></i> Client Code
                                    Entry</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/audioquotation">
                                    <i class="icon-pencil"></i>
                                    Quotation</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/audiopurchaseorder">
                                    <i class="icon-pencil"></i>
                                    Purchase Order</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/audioworkorder">
                                    <i class="icon-pencil"></i>
                                    Work Order</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/audioinvoice">
                                    <i class="icon-pencil"></i>
                                    Tax Invoice</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/modifyaudioquotation">
                                    <i class="icon-pencil"></i>
                                    Modify Quotation</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/modifyaudiopurchase">
                                    <i class="icon-pencil"></i>
                                    Modify Purchase Order</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/modifyaudiowork">
                                    <i class="icon-pencil"></i>
                                    Modify Work Order</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/cancelaudioinvoice">
                                    <i class="icon-pencil"></i>
                                    Cancel Tax Invoice</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Audio Prolights Bill / Reports<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <!--                            <li class=" dropdown">-->
                            <!--                                <!--                                <a href="-->
                            <!--                                -->
                            <?php ////echo Yii::$app->request->baseUrl ?><!--<!--/site/taxdupbill">-->
                            <!--                                <a href="#">-->
                            <!--                                    <i class="icon-tag"></i>-->
                            <!--                                    Purchase Order Bill</a>-->
                            <!--                            </li>-->
                            <!--                            <li class=" dropdown">-->
                            <!--                                <!--                                <a href="-->
                            <!--                                -->
                            <?php ////echo Yii::$app->request->baseUrl ?><!--<!--/site/simpletaxdupbill">-->
                            <!--                                <a href="#">-->
                            <!--                                    <i class="icon-tag"></i>-->
                            <!--                                    Work Order Bill</a>-->
                            <!--                            </li>-->
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/audioquotationbill">
                                    <i class="icon-tag"></i>
                                    Quotation Duplicate Bill</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/audiodupbill">
                                    <i class="icon-tag"></i>
                                    Tax Invoice Duplicate Bill</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/audioquotationreports">
                                    <i class="icon-tag"></i>
                                    Quotation Report</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/audioporeports">
                                    <i class="icon-tag"></i>
                                    Purchase Order Report</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/audioworkorderreports">
                                    <i class="icon-tag"></i>
                                    Work Order Report</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/audioreports">
                                    <i class="icon-tag"></i>
                                    Tax Invoice Reports</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/audiopovsinvoicereports">
                                    <i class="icon-tag"></i>
                                    Purchase vs Invoice Reports</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/graph">
                                    <i class="icon-tag"></i>
                                    Graph</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Balaji Events<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class="dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/cliententry"><i
                                        class="icon-tag"></i> Client Code
                                    Entry</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/balajiquotation">
                                    <i class="icon-tag"></i>
                                    Quotation</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/balajipurchaseorder">
                                    <i class="icon-tag"></i>
                                    Purchase Order</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/balajiinvoice">
                                    <i class="icon-tag"></i>
                                    Tax Invoice</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/modifybalajiquotation">
                                    <i class="icon-tag"></i>
                                    Modify Quotation</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/modifybalajipurchase">
                                    <i class="icon-tag"></i>
                                    Modify Purchase Order</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/cancelbalajiinvoice">
                                    <i class="icon-tag"></i>
                                    Cancel Tax Invoice</a>
                            </li>

                            <!--                            <li class=" dropdown">-->
                            <!--                                <a href="-->
                            <?php //echo Yii::$app->request->baseUrl ?><!--/site/receivebalajipurchaseorder">-->
                            <!--                                    <i class="icon-tag"></i>-->
                            <!--                                    Receive Balaji Evts. P.O</a>-->
                            <!--                            </li>-->
                            <!--                            <li class=" dropdown">-->
                            <!--                                <a href="-->
                            <?php //echo Yii::$app->request->baseUrl ?><!--/site/balajiworkorder">-->
                            <!--                                    <i class="icon-tag"></i>-->
                            <!--                                    Balaji Evts. Work Order</a>-->
                            <!--                            </li>-->
                        </ul>
                    </li>
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Balaji Events Bill / Reports<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <!--                            <li class=" dropdown">-->
                            <!--                                <!--                                <a href="-->
                            <!--                                -->
                            <?php ////echo Yii::$app->request->baseUrl ?><!--<!--/site/taxdupbill">-->
                            <!--                                <a href="#">-->
                            <!--                                    <i class="icon-tag"></i>-->
                            <!--                                    Purchase Order Bill</a>-->
                            <!--                            </li>-->
                            <!--                            <li class=" dropdown">-->
                            <!--                                <!--                                <a href="-->
                            <!--                                -->
                            <?php ////echo Yii::$app->request->baseUrl ?><!--<!--/site/simpletaxdupbill">-->
                            <!--                                <a href="#">-->
                            <!--                                    <i class="icon-tag"></i>-->
                            <!--                                    Work Order Bill</a>-->
                            <!--                            </li>-->
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/balajiquotationbill">
                                    <i class="icon-tag"></i>
                                    Quotation Duplicate Bill</a>
                            </li>
                            <li class=" dropdown">
                                <a href="
                                <?php echo Yii::$app->request->baseUrl ?>/site/balajidupbill">
                                    <i class="icon-tag"></i>
                                    Tax Invoice Duplicate Bill</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/balajiquotationreports">
                                    <i class="icon-tag"></i>
                                    Quotation Reports</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/balajiporeports">
                                    <i class="icon-tag"></i>
                                    Purchase Order Reports</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/balajireports">
                                    <i class="icon-tag"></i>
                                    Tax Invoice Reports</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/balajipovsinvoicereports">
                                    <i class="icon-tag"></i>
                                    Purchase vs Invoice Reports</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/graph">
                                    <i class="icon-tag"></i>
                                    Graph</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Items<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/addcategory">
                                    <i class="icon-tag"></i>
                                    Add Category</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/addproduct">
                                    <i class="icon-tag"></i>
                                    Add Items</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/modifycategory">
                                    <i class="icon-tag"></i>
                                    Modify Category</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/modifyitem">
                                    <i class="icon-tag"></i>
                                    Modify Items</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Employee Management<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/wages">
                                    <i class="icon-tag"></i>
                                    Employee Wages</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/wagesreport">
                                    <i class="icon-tag"></i>
                                    Wages Report</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/employeereport">
                                    <i class="icon-tag"></i>
                                    Employee Report</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Stock<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/newstock">
                                    <i class="icon-tag"></i>
                                    New Stock Entry</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/currentstock">
                                    <i class="icon-tag"></i>
                                    Current Stock</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/site/audiostockreturn">
                                    <i class="icon-tag"></i>
                                    Stock Return</a>
                            </li>
                        </ul>
                    </li>
                    <!--                    <li class="menu-dropdown classic-menu-dropdown ">-->
                    <!--                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"-->
                    <!--                           href="javascript:;">-->
                    <!--                            Reports<i class="fa fa-angle-down"></i>-->
                    <!--                        </a>-->
                    <!--                        <ul class="dropdown-menu pull-left">-->
                    <!--                            <!--                            <li class=" dropdown">-->
                    <!--                            <!--                                <a href="-->
                    <!--                            -->
                    <?php ////echo Yii::$app->request->baseUrl ?><!--<!--/site/purchaseorderreports">-->
                    <!--                            <!--                                    <i class="icon-tag"></i>-->

                    <!--                            <!--                                    Purch. Order Reports</a>-->
                    <!--                            <!--                            </li>-->
                    <!--                            <!--                            <li class=" dropdown">-->
                    <!--                            <!--                                <a href="-->
                    <!--                            -->
                    <?php ////echo Yii::$app->request->baseUrl ?><!--<!--/site/balajiworkorderreports">-->
                    <!--                            <!--                                    <i class="icon-tag"></i>-->

                    <!--                            <!--                                    Balaji Evts. Work Order</a>-->

                    <!--                            <!--                            </li>-->
                    <!--                            <li class=" dropdown">-->
                    <!--                                <a href="-->
                    <?php //echo Yii::$app->request->baseUrl ?><!--/site/balajiquotationreports">-->
                    <!--                                    <i class="icon-tag"></i>-->
                    <!--                                    Balaji Events Quotation</a>-->
                    <!--                            </li>-->
                    <!--                            <li class=" dropdown">-->
                    <!--                                <a href="-->
                    <?php //echo Yii::$app->request->baseUrl ?><!--/site/graph">-->
                    <!--                                    <i class="icon-tag"></i>-->
                    <!--                                    Graph</a>-->
                    <!--                            </li>-->
                    <!--                        </ul>-->
                    <!--                    </li>-->
                </ul>
            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
    <?= $content ?>
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="container">
        2016 &copy; Developed by InsproIT.
    </div>
</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="/prolights/web/global/plugins/respond.min.js"></script>
<script src="/prolights/web/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="/prolights/web/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/prolights/web/global/plugins/jquery-ui.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/bootstrap.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/jquery.uniform.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/select2.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/bootstrap-select.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="/prolights/web/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/prolights/web/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="/prolights/web/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="/prolights/web/smoke.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/angular.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/angular-sanitize.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/angular-messages.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/angular-route.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/angular-ui-notification.min.js" type="text/javascript"></script>
<script src="/prolights/web/js/app.js" type="text/javascript"></script>
<!--<script src="/prolights/web/js/stock.js" type="text/javascript"></script>-->
<script src="/prolights/web/js/loadingoverlay.js" type="text/javascript"></script>
<script src="/prolights/web/js/purchase.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>