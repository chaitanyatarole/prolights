<!DOCTYPE html>
<html lang="en">
<head>
    <!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
    <meta charset="utf-8">
    <title>ProLights</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Muhammad Usman">


    <link href="/prolights/web/global/css/charisma-app.css" rel="stylesheet">
    <link href='/prolights/web/global/plugins/bootstrap.min.css' rel='stylesheet'>
    <link href='/prolights/web/css/animate.css' rel='stylesheet'>
    <!-- jQuery -->
    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- The fav icon -->
    <!--    <link rel="shortcut icon" href="img/favicon.ico">-->

</head>
<body background="/prolights/web/img/prolightsbg.jpg">

<!--<body background="/prolights/web/img/SaiBG.jpg">-->

<input type="hidden" id="baseUrl" value="<?php echo Yii::$app->request->baseUrl; ?>">

<div class="ch-container">
    <?= $content ?>
</div>
<!--/.fluid-container-->
<!-- external javascript -->
<script src="/prolights/web/global/plugins/jquery.min.js"></script>
<script src="/prolights/web/global/plugins/bootstrap.min.js"></script>
<script src="/prolights/web/jquery.noty.packaged.js"></script>
<script src="/prolights/web/js/login.js"></script>
</body>
</html>
