<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Invoice</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Customer Invoice">
    <meta name="author" content="5marks">

    <link rel="stylesheet" href="/prolights/web/global/plugins/bootstrap.min.css">

    <script src="/prolights/web/global/plugins/jquery.min.js"></script>
</head>

<body>
<div class="container">
    <?= $content; ?>
</div>
</body>
</html>