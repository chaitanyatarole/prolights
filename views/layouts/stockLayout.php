<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.2
Version: 3.2.0
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Pro Lights</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="/prolights/web/global/plugins/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/global/plugins/simple-line-icons.min.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/global/plugins/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/global/plugins/angular-ui-notification.min.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/global/plugins/uniform.default.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/global/plugins/jquery-ui.min.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/global/plugins/select2.css" rel="stylesheet"
    <link href="/prolights/web/global/plugins/select2.css" rel="stylesheet"
          type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME STYLES -->
    <link href="/prolights/web/global/css/components-md.css" id="style_components" rel="stylesheet" type="text/css">
    <link href="/prolights/web/global/css/plugins-md.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/layout3/css/layout.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color">
    <link href="/prolights/web/layout3/css/custom.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/css/smoke.css" rel="stylesheet" type="text/css">
    <link href="/prolights/web/css/purchase.css" rel="stylesheet" type="text/css">
    <!-- END THEME STYLES -->
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-menu-fixed" class to set the mega menu fixed  -->
<!-- DOC: Apply "page-header-top-fixed" class to set the top menu fixed  -->
<body ng-app="proLightStock">
<input type="hidden" id="baseUrl" value="<?php echo Yii::$app->request->baseUrl; ?>">
<!-- BEGIN HEADER -->
<div class="page-header">
    <!-- BEGIN HEADER TOP -->
    <div class="page-header-top">
        <div class="container">
            <!-- BEGIN LOGO -->
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler"></a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <img src="/prolights/web/img/prologo.png" style="height: 75px;">

            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <li class="droddown dropdown-separator">
                        <span class="separator"></span>
                    </li>
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <!--                            <img alt="" class="img-circle" src="/sai/web/img/photo(1).jpg">-->
                            <span class="username username-hide-mobile">Stock</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="<?php echo Yii::$app->request->baseUrl ?>">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- END HEADER TOP -->
    <!-- BEGIN HEADER MENU -->
    <div class="page-header-menu">
        <div class="container">
            <!-- BEGIN MEGA MENU -->
            <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
            <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
            <div class="hor-menu ">
                <ul class="nav navbar-nav">
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Stock<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/newstock">
                                    <i class="icon-tag"></i>
                                    New Stock Entry</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/currentstock">
                                    <i class="icon-tag"></i>
                                    Current Stock</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Stock Return<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/audiostockreturn">
                                    <i class="icon-tag"></i>
                                    Audio Pro Lights</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/balajistockreturn">
                                    <i class="icon-tag"></i>
                                    Balaji Events</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Audio Pro Lights Orders<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/receiveaudioorder">
                                    <i class="icon-tag"></i>
                                    Work Order</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/receiveaudiopurchaseorder">
                                    <i class="icon-tag"></i>
                                    Purchase Order</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Balaji Events Orders<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/receivebalajiorder">
                                    <i class="icon-tag"></i>
                                    Work Order</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/receivebalajipurchaseorder">
                                    <i class="icon-tag"></i>
                                    Purchase Order</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Vendor Material<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/returnaudiomaterial">
                                    <i class="icon-tag"></i>
                                    Return Audio Pro Lights Material</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/returnbalajimaterial">
                                    <i class="icon-tag"></i>
                                    Return Balaji Events Material</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Reports<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class=" dropdown">
                                <a href="
                                <?php echo Yii::$app->request->baseUrl ?>/stock/audioworkorderreports">
                                    <i class="icon-tag"></i>
                                    Audio. Work Order</a>
                            </li>
                            <li class=" dropdown">
                                <a href="
                                <?php echo Yii::$app->request->baseUrl ?>/stock/audioporeports">
                                    <i class="icon-tag"></i>
                                    Audio. P.O</a>
                            </li>
                            <li class=" dropdown">
                                <a href="
                                <?php echo Yii::$app->request->baseUrl ?>/stock/balajiworkorderreports">
                                    <i class="icon-tag"></i>
                                    Balaji. Work Order</a>
                            </li>
                            <li class=" dropdown">
                                <a href="
                                <?php echo Yii::$app->request->baseUrl ?>/stock/balajiporeports">
                                    <i class="icon-tag"></i>
                                    Balaji. P.O</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu-dropdown classic-menu-dropdown ">
                        <a data-hover="megamenu-dropdown" data-close-others="true" data-toggle="dropdown"
                           href="javascript:;">
                            Vendor Reports<i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-left">
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/vendoraudioinstockreport">
                                    <i class="icon-tag"></i>
                                    Audio Pro Lights In Stock</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/vendorbalajiinstockreport">
                                    <i class="icon-tag"></i>
                                    Balaji Events In Stock</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/vendoraudioclearstockreport">
                                    <i class="icon-tag"></i>
                                    Audio Pro Lights Cleared Stock</a>
                            </li>
                            <li class=" dropdown">
                                <a href="<?php echo Yii::$app->request->baseUrl ?>/stock/vendorbalajiclearstockreport">
                                    <i class="icon-tag"></i>
                                    Balaji Events Cleared Stock</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- END MEGA MENU -->
        </div>
    </div>
    <!-- END HEADER MENU -->
</div>
<!-- END HEADER -->
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container" ng-controller="notificationController">
    <?= $content ?>
</div>
<!-- END PAGE CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="container">
        2015 &copy; Developed by InsproIT.
    </div>
</div>
<div class="scroll-to-top">
    <i class="icon-arrow-up"></i>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="/prolights/web/global/plugins/respond.min.js"></script>
<script src="/prolights/web/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="/prolights/web/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="/prolights/web/global/plugins/jquery-ui.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/bootstrap.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/jquery.uniform.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/select2.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/bootstrap-select.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<script src="/prolights/web/global/scripts/metronic.js" type="text/javascript"></script>
<script src="/prolights/web/layout3/scripts/layout.js" type="text/javascript"></script>
<script src="/prolights/web/layout3/scripts/demo.js" type="text/javascript"></script>
<script src="/prolights/web/smoke.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/angular.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/angular-sanitize.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/angular-messages.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/angular-route.min.js" type="text/javascript"></script>
<script src="/prolights/web/global/plugins/angular-ui-notification.min.js" type="text/javascript"></script>
<!--<script src="/prolights/web/global/plugins/angular-initial-value.js" type="text/javascript"></script>-->
<script src="/prolights/web/js/stock.js" type="text/javascript"></script>
<script src="/prolights/web/js/loadingoverlay.js" type="text/javascript"></script>
<script src="/prolights/web/js/purchase.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        Demo.init(); // init demo features
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>