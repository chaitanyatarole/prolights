<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span
                            class="caption-subject theme-font bold uppercase">Audio Pro Lights - Return Vendor Items</span>
                    </div>
                </div>
                <div class="portlet-body" style="min-height: 350px;">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/prolights/web/stock/getpurchaseorder"
                              class="form-horizontal" ng-controller="returnVendorItems">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" style="text-align: right !important;">Select
                                        Purchase Order Number</label>

                                    <div class="col-sm-6">
                                        <select id="selectTaxNumber" name="selectQuotationNumber"
                                                class="form-control input-large select2me select2-offscreen"
                                                ng-model="invoiceNumber"
                                                data-placeholder="Select Invoice Number" tabindex="-1"
                                                ng-change="getData()"
                                                title="">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($entireData as $key) {
                                                $invoiceNumber = $key['purchase_order_no'];
                                                $clientName = $key['vendorName'];
                                                echo "<option value='$invoiceNumber'>$clientName -- $invoiceNumber</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <br>

                                <div ng-bind-html="renderHtml(body)" compile="body"></div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>