<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Audio Pro Lights - Received Work Order</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/prolights/web/stock/acceptaudiopurchaseorder"
                              class="form-horizontal" ng-controller="receivedOrderCtrl">
                            <div class="form-body">
                                <input type="hidden" class="form-control" name="ccode" id="ccode"/>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Code</label>

                                    <div class="col-sm-1">
                                        <input type="text" class="form-control" name="clientcode" readonly
                                               value="<?php echo isset($entireData[0]['clientcode']) ? $entireData[0]['clientcode'] : '' ?>"
                                               id="clientcode">
                                    </div>
                                    <label class="control-label col-sm-1">Date</label>

                                    <div class="col-sm-2">
                                        <input type="date" class="form-control" name="date"
                                               value="<?php echo isset($entireData[0]['date']) ? $entireData[0]['date'] : '' ?>"
                                               id="date">
                                    </div>
                                    <label class="control-label col-sm-2">Invoice Number</label>

                                    <div class="col-sm-2">
                                        <input type="text" readonly class="form-control"
                                               value="<?php echo isset($entireData[0]['invoice_no']) ? $entireData[0]['invoice_no'] : ''; ?>"
                                               name="taxNumber" id="invoiceNumber">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Client Name</label>

                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"
                                               value="<?php echo isset($entireData[0]['client_name']) ? $entireData[0]['client_name'] : '' ?>"
                                               onkeyup="gencode(this.value);"
                                               name="cname" id="cname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Address</label>

                                    <div class="col-sm-6">
                                        <textarea class="form-control" id="address"
                                                  name="address"><?php echo isset($entireData[0]['address']) ? $entireData[0]['address'] : '' ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mobile Number</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo isset($entireData[0]['phone_no']) ? $entireData[0]['phone_no'] : '' ?>"
                                               name="number" id="number">
                                    </div>
                                    <label class="control-label col-sm-2">Email</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo isset($entireData[0]['email']) ? $entireData[0]['email'] : '' ?>"
                                               name="email" id="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Event Name</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" readonly
                                               value="<?php echo isset($entireData[0]['eventName']) ? $entireData[0]['eventName'] : '' ?>"
                                               name="eventName"
                                               id="eventName">
                                    </div>
                                    <label class="control-label col-sm-2">Event Location</label>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control"
                                               value="<?php echo isset($entireData[0]['eventLocation']) ? $entireData[0]['eventLocation'] : '' ?>"
                                               name="eventLocation"
                                               id="eventLocation">
                                    </div>
                                </div>
                                <div class="form-actions">

                                </div>
                                <div class="">
                                    <table class="mytable" id="taxDataTable" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Item
                                        </th>
                                        <th>
                                            Available Stock
                                        </th>
                                        <th>
                                            Qty.No
                                        </th>
<!--                                        <th>-->
<!--                                            Rate-->
<!--                                        </th>-->
<!--                                        <th>-->
<!--                                            Amount-->
<!--                                        </th>-->
                                        </thead>
                                        <tbody id="taxTable">
                                        <?php foreach ($entireData as $key => $value) {
                                            $serialNumber = $key + 1; ?>
                                            <tr>
                                                <td style="width: 60px;">
                                                    <?php echo $serialNumber; ?>
                                                </td>
                                                <td style="width: 430px;">
                                                    <input type="text" readonly name="particular[]" id=""
                                                           value="<?php echo isset($value['particularName']) ? $value['particularName'] : '' ?>"
                                                           class="form-control">
                                                    <input type="hidden" readonly name="particularID[]" id=""
                                                           value="<?php echo isset($value['particular']) ? $value['particular'] : '' ?>"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 202px;padding-right: 10px;">
                                                    <?php $id = isset($value['particular']) ? $value['particular'] : '';
                                                    $query = "select stock from product where id = '$id' ";
                                                    $result = Yii::$app->db->createCommand($query)->queryAll();
                                                    $stock = isset($result[0]['stock']) ? $result[0]['stock'] : '';
                                                    ?>
                                                    <input type="text" readonly name="stock[]"
                                                           value="<?php echo isset($stock) ? $stock : ''; ?>"
                                                           id="stock-1"
                                                           class="form-control">
                                                </td>
                                                <td style="width: 216px;padding-right: 10px;">
                                                    <input type="text"
                                                           onkeyup="calculationInWorkOrder(1);checkEnteredValue(1);"
                                                           onkeypress="return isNumber(event);" id="quantity-1"
                                                           name="quantity[]"
                                                           class="form-control"
                                                           value="<?php echo isset($value['quantity']) ? $value['quantity'] : '' ?>">
                                                </td>
<!--                                                <td style="width: 200px;padding-right: 10px;">-->
<!--                                                    <input type="text" onkeyup="calculationInWorkOrder(1);"-->
<!--                                                           onkeypress="return isNumber(event);" id="rate-1"-->
<!--                                                           name="rate[]"-->
<!--                                                           value="--><?php //echo isset($value['rate']) ? $value['rate'] : '' ?><!--"-->
<!--                                                           class="form-control">-->
<!--                                                </td>-->
<!--                                                <td style="width: 200px;;">-->
<!--                                                    <input type="text" readonly-->
<!--                                                           value="--><?php //echo isset($value['amount']) ? $value['amount'] : '' ?><!--"-->
<!--                                                           id="amount-1" name="amount[]"-->
<!--                                                           class="form-control">-->
<!--                                                </td>-->
                                            </tr>
                                        <?php } ?>
                                        </tbody>
<!--                                        <tbody>-->
<!--                                        <tr>-->
<!--                                            <td style="width: 5px;">-->
<!--                                            </td>-->
<!--                                            <td style="width: 200px;">-->
<!--                                            </td>-->
<!--                                            <td style="width: 10px;">-->
<!--                                            </td>-->
<!--                                            <td style="width: 10px;">-->
<!--                                            </td>-->
<!--                                            <td style="width: 60px;">-->
<!--                                                <strong>Total Amount</strong>-->
<!--                                            </td>-->
<!--                                            <td style="width: 30px;;">-->
<!--                                                <input type="text" readonly-->
<!--                                                       value="--><?php //echo isset($entireData[0]['total_amount']) ? $entireData[0]['total_amount'] : '' ?><!--"-->
<!--                                                       name="totamt" id="totamt"-->
<!--                                                       class="form-control">-->
<!--                                            </td>-->
<!--                                        </tr>-->
<!--                                        </tbody>-->
                                    </table>
                                </div>
                                <input type="hidden" value="1" name="counter" id="counter">

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="button" ng-disabled="isDisabled" class="btn green" ng-click="acceptOrder()"
                                                    id="submit">Accept
                                            </button>
                                            <button type="button" class="btn default" ng-click="cancelOrder()">
                                                Reject
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>