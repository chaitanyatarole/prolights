<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span
                            class="caption-subject theme-font bold uppercase">Stock Return from Audio Pro Lights Event</span>
                    </div>
                </div>
                <div class="portlet-body" style="min-height: 350px;">
                    <div class="portlet-body form">
                        <form role="form" method="post" action="/prolights/web/stock/showorder"
                              class="form-horizontal" ng-controller="stockReturnCtrl">
                            <div class="form-body" compile="details">
                                <div class="form-group">
                                    <label class="control-label col-sm-4" style="text-align: right !important;">Select
                                        Event Name / Order Number</label>

                                    <div class="col-sm-6">
                                        <select id="selectTaxNumber" name="selectQuotationNumber"
                                                class="form-control input-large select2me select2-offscreen"
                                                data-placeholder="Select Invoice Number" tabindex="-1"
                                                ng-model="formInfo.invoiceNumber" ng-change="getDetails()"
                                                title="">
                                            <option value="">------------------------Select------------------------
                                            </option>
                                            <?php
                                            foreach ($entireData as $key) {
                                                $invoiceNumber = $key['invoice_no'];
                                                $eventName = $key['eventName'];
                                                echo "<option value='$invoiceNumber'>$eventName -- $invoiceNumber</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <br>

                                <div ng-bind-html="renderHtml(body)" compile="body"></div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT INNER -->
</div>