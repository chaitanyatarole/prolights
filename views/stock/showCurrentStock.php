<div class="container-fluid">
    <!-- BEGIN PAGE CONTENT INNER -->
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption caption-md">
                        <i class="icon-bar-chart theme-font hide"></i>
                        <span class="caption-subject theme-font bold uppercase">Current Stock</span>
                    </div>
                </div>
                <div class="portlet-body" style="min-height: 350px;">
                    <div class="portlet-body form">
                        <form role="form" method="post" action=""
                              class="form-horizontal">
                            <div class="form-body">
                                <div class="">
                                    <table class="mytable" id="quotationDataTable" cellpadding="10" cellspacing="10">
                                        <thead>
                                        <th>
                                            Sr.No
                                        </th>
                                        <th>
                                            Item
                                        </th>
                                        <th>
                                            Stock
                                        </th>
                                        </thead>
                                        <tbody id="quotationTable">
                                        <?php
                                        $counter = '';
                                        foreach ($entireData as $key => $value) {
                                            $serialNumber = $key + 1; ?>
                                            <tr>
                                                <td style="width: 60px;">
                                                    <?php echo $serialNumber; ?>
                                                </td>
                                                <td style="width: 400px;">
                                                    <input type="text" readonly name="particular[]" class="form-control"
                                                           value="<?php echo $value['name']; ?>">
                                                </td>
                                                <td style="width: 250px;padding-right: 10px;">
                                                    <input type="text" readonly
                                                           value="<?php echo $value['stock']; ?>"
                                                           class="form-control">
                                                </td>
                                            </tr>
                                            <?php $counter = $serialNumber;
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-9 col-md-9">
                                            <button type="button" class="btn green" id="submit"
                                                    onclick="location.href=('http://localhost/prolights/web/stock/receiveaudioorder')">
                                                Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>