-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1build0.15.04.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 08, 2016 at 08:23 PM
-- Server version: 5.6.27-0ubuntu0.15.04.1
-- PHP Version: 5.6.4-4ubuntu6.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `prolights`
--

-- --------------------------------------------------------

--
-- Table structure for table `audioPurchaseOrder`
--

CREATE TABLE IF NOT EXISTS `audioPurchaseOrder` (
`id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  `returnDate` date DEFAULT NULL,
  `purchase_order_no` int(100) NOT NULL,
  `quotationNumber` int(50) DEFAULT NULL,
  `eventName` varchar(100) DEFAULT NULL,
  `client_name` varchar(100) NOT NULL,
  `clientcode` varchar(100) NOT NULL,
  `address` varchar(500) DEFAULT NULL,
  `phone_no` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `particular` varchar(300) DEFAULT NULL,
  `particularName` varchar(300) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `delivery_location` varchar(300) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `total_amount` float DEFAULT NULL,
  `status` varchar(100) DEFAULT 'completed'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `audioQuotation`
--

CREATE TABLE IF NOT EXISTS `audioQuotation` (
`id` bigint(20) NOT NULL,
  `quotationNumber` int(20) NOT NULL,
  `date` date NOT NULL,
  `clientName` varchar(100) NOT NULL,
  `clientCode` varchar(100) NOT NULL,
  `address` varchar(600) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `eventName` varchar(500) DEFAULT NULL,
  `eventLocation` varchar(500) DEFAULT NULL,
  `eventDate` date DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `category` varchar(1000) DEFAULT NULL,
  `particular` varchar(100) NOT NULL,
  `particularName` varchar(300) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `totalAmount` float DEFAULT NULL,
  `totalBudget` float DEFAULT NULL,
  `availableBudget` float DEFAULT NULL,
  `status` varchar(100) DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `audioTaxInvoice`
--

CREATE TABLE IF NOT EXISTS `audioTaxInvoice` (
`id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  `invoice_no` int(100) NOT NULL,
  `client_name` varchar(100) NOT NULL,
  `clientcode` varchar(100) NOT NULL,
  `address` varchar(500) DEFAULT NULL,
  `phone_no` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `eventName` varchar(500) DEFAULT NULL,
  `eventLocation` varchar(500) DEFAULT NULL,
  `particular` varchar(100) DEFAULT NULL,
  `particularName` varchar(100) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `totalAmount` float DEFAULT NULL,
  `vat` float DEFAULT NULL,
  `vatPercent` float DEFAULT NULL,
  `grandTotal` float DEFAULT NULL,
  `status` varchar(100) DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `audioVendorStock`
--

CREATE TABLE IF NOT EXISTS `audioVendorStock` (
`id` bigint(20) NOT NULL,
  `purchase_order_no` int(20) DEFAULT NULL,
  `particular` int(100) DEFAULT NULL,
  `particularName` varchar(300) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `vendorId` varchar(300) DEFAULT NULL,
  `vendorName` varchar(300) DEFAULT NULL,
  `status` varchar(300) DEFAULT 'inStock',
  `date` date DEFAULT NULL,
  `returnDate` date DEFAULT NULL,
  `returnedItemsOn` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `audioWorkOrder`
--

CREATE TABLE IF NOT EXISTS `audioWorkOrder` (
`id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  `invoice_no` int(100) NOT NULL,
  `quotationNumber` bigint(50) DEFAULT NULL,
  `client_name` varchar(100) DEFAULT NULL,
  `eventName` varchar(200) DEFAULT NULL,
  `eventLocation` varchar(200) DEFAULT NULL,
  `clientcode` varchar(100) NOT NULL,
  `address` varchar(500) DEFAULT NULL,
  `phone_no` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `totalBudget` float DEFAULT NULL,
  `availableBudget` float DEFAULT NULL,
  `particular` varchar(100) NOT NULL,
  `particularName` varchar(100) DEFAULT NULL,
  `quantity` int(100) NOT NULL,
  `rate` float DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `total_amount` float DEFAULT NULL,
  `status` varchar(100) DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `balajiPurchaseOrder`
--

CREATE TABLE IF NOT EXISTS `balajiPurchaseOrder` (
`id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  `returnDate` date DEFAULT NULL,
  `purchase_order_no` int(100) NOT NULL,
  `quotationNumber` int(50) DEFAULT NULL,
  `eventName` varchar(100) DEFAULT NULL,
  `client_name` varchar(100) NOT NULL,
  `clientcode` varchar(100) NOT NULL,
  `address` varchar(500) DEFAULT NULL,
  `phone_no` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `particular` varchar(300) DEFAULT NULL,
  `particularName` varchar(300) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `delivery_location` varchar(300) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `total_amount` float DEFAULT NULL,
  `status` varchar(100) DEFAULT 'completed'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `balajiQuotation`
--

CREATE TABLE IF NOT EXISTS `balajiQuotation` (
`id` bigint(20) NOT NULL,
  `quotationNumber` int(20) NOT NULL,
  `date` date NOT NULL,
  `clientName` varchar(100) NOT NULL,
  `clientCode` varchar(100) NOT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `address` varchar(600) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `eventName` varchar(500) DEFAULT NULL,
  `eventLocation` varchar(500) DEFAULT NULL,
  `eventDate` date DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `category` varchar(1000) DEFAULT NULL,
  `particular` varchar(100) NOT NULL,
  `particularName` varchar(300) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `totalAmount` float DEFAULT NULL,
  `totalBudget` float DEFAULT NULL,
  `availableBudget` float DEFAULT NULL,
  `status` varchar(100) DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `balajiTaxInvoice`
--

CREATE TABLE IF NOT EXISTS `balajiTaxInvoice` (
`id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  `invoice_no` int(100) NOT NULL,
  `client_name` varchar(100) NOT NULL,
  `clientcode` varchar(100) NOT NULL,
  `address` varchar(500) DEFAULT NULL,
  `phone_no` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `eventName` varchar(500) DEFAULT NULL,
  `eventLocation` varchar(500) DEFAULT NULL,
  `particular` varchar(100) DEFAULT NULL,
  `particularName` varchar(100) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `totalAmount` float DEFAULT NULL,
  `vat` float DEFAULT NULL,
  `vatPercent` float DEFAULT NULL,
  `grandTotal` float DEFAULT NULL,
  `status` varchar(100) DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `balajiVendorStock`
--

CREATE TABLE IF NOT EXISTS `balajiVendorStock` (
`id` bigint(20) NOT NULL,
  `purchase_order_no` int(20) DEFAULT NULL,
  `particular` int(100) DEFAULT NULL,
  `particularName` varchar(300) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `vendorId` varchar(300) DEFAULT NULL,
  `vendorName` varchar(300) DEFAULT NULL,
  `status` varchar(300) DEFAULT 'inStock',
  `date` date DEFAULT NULL,
  `returnDate` date DEFAULT NULL,
  `returnedItemsOn` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `balajiWorkOrder`
--

CREATE TABLE IF NOT EXISTS `balajiWorkOrder` (
`id` bigint(20) NOT NULL,
  `date` date NOT NULL,
  `invoice_no` int(100) NOT NULL,
  `quotationNumber` bigint(50) DEFAULT NULL,
  `client_name` varchar(100) DEFAULT NULL,
  `eventName` varchar(200) DEFAULT NULL,
  `eventLocation` varchar(200) DEFAULT NULL,
  `clientcode` varchar(100) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `phone_no` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `totalBudget` float DEFAULT NULL,
  `availableBudget` float DEFAULT NULL,
  `particular` varchar(100) NOT NULL,
  `particularName` varchar(100) DEFAULT NULL,
  `quantity` int(100) NOT NULL,
  `rate` float DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `total_amount` float DEFAULT NULL,
  `status` varchar(100) DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Stage'),
(2, 'Lights'),
(3, 'Sound'),
(4, 'Technical');

-- --------------------------------------------------------

--
-- Table structure for table `clientdata`
--

CREATE TABLE IF NOT EXISTS `clientdata` (
  `client_name` varchar(100) NOT NULL,
  `clientcode` varchar(100) NOT NULL,
  `ccode` varchar(100) NOT NULL,
  `ccodeno` int(100) NOT NULL,
  `address` varchar(500) DEFAULT NULL,
  `phone_no` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `vat_no` varchar(100) DEFAULT NULL,
  `cst_no` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientdata`
--

INSERT INTO `clientdata` (`client_name`, `clientcode`, `ccode`, `ccodeno`, `address`, `phone_no`, `email`, `vat_no`, `cst_no`) VALUES
('Chaitanya Tarole', 'CT01', 'CT', 1, 'Akola', '8087562015', NULL, NULL, NULL),
('Akshay', 'A02', 'A', 2, 'asas', 'sasasas', NULL, NULL, NULL),
('prahtam', 'P03', 'P', 3, 'qjkqh', 'j', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employeeData`
--

CREATE TABLE IF NOT EXISTS `employeeData` (
  `client_name` varchar(100) NOT NULL,
  `clientcode` varchar(100) NOT NULL,
  `ccode` varchar(100) NOT NULL,
  `ccodeno` int(100) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone_no` varchar(200) NOT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employeeWages`
--

CREATE TABLE IF NOT EXISTS `employeeWages` (
`id` bigint(20) NOT NULL,
  `recordNumber` bigint(50) DEFAULT NULL,
  `employeeId` varchar(200) DEFAULT NULL,
  `employeeName` varchar(200) DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `wage` float DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `wageForMonth` varchar(100) DEFAULT NULL,
  `wageReceivedOn` date DEFAULT NULL,
  `status` varchar(100) DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
`id` bigint(20) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`, `name`, `role`) VALUES
(1, 'admin', 'admin', 'admin', 'admin'),
(2, 'stock', 'stock', 'stock', 'stock'),
(3, 'billing', 'billing', 'billing', 'billing');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
`id` bigint(20) NOT NULL,
  `category_id` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `stock` float DEFAULT '0',
  `date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE IF NOT EXISTS `transactions` (
`id` bigint(20) NOT NULL,
  `reason` varchar(1000) DEFAULT NULL,
  `amountAdded` float DEFAULT NULL,
  `amountDeducted` float DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendorData`
--

CREATE TABLE IF NOT EXISTS `vendorData` (
  `client_name` varchar(100) NOT NULL,
  `clientcode` varchar(100) NOT NULL,
  `ccode` varchar(100) NOT NULL,
  `ccodeno` int(100) NOT NULL,
  `address` varchar(500) DEFAULT NULL,
  `phone_no` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audioPurchaseOrder`
--
ALTER TABLE `audioPurchaseOrder`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audioQuotation`
--
ALTER TABLE `audioQuotation`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audioTaxInvoice`
--
ALTER TABLE `audioTaxInvoice`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audioVendorStock`
--
ALTER TABLE `audioVendorStock`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `audioWorkOrder`
--
ALTER TABLE `audioWorkOrder`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `balajiPurchaseOrder`
--
ALTER TABLE `balajiPurchaseOrder`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `balajiQuotation`
--
ALTER TABLE `balajiQuotation`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `balajiTaxInvoice`
--
ALTER TABLE `balajiTaxInvoice`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `balajiVendorStock`
--
ALTER TABLE `balajiVendorStock`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `balajiWorkOrder`
--
ALTER TABLE `balajiWorkOrder`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clientdata`
--
ALTER TABLE `clientdata`
 ADD PRIMARY KEY (`ccodeno`);

--
-- Indexes for table `employeeData`
--
ALTER TABLE `employeeData`
 ADD PRIMARY KEY (`ccodeno`);

--
-- Indexes for table `employeeWages`
--
ALTER TABLE `employeeWages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendorData`
--
ALTER TABLE `vendorData`
 ADD PRIMARY KEY (`client_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audioPurchaseOrder`
--
ALTER TABLE `audioPurchaseOrder`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `audioQuotation`
--
ALTER TABLE `audioQuotation`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `audioTaxInvoice`
--
ALTER TABLE `audioTaxInvoice`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `audioVendorStock`
--
ALTER TABLE `audioVendorStock`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `audioWorkOrder`
--
ALTER TABLE `audioWorkOrder`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `balajiPurchaseOrder`
--
ALTER TABLE `balajiPurchaseOrder`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `balajiQuotation`
--
ALTER TABLE `balajiQuotation`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `balajiTaxInvoice`
--
ALTER TABLE `balajiTaxInvoice`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `balajiVendorStock`
--
ALTER TABLE `balajiVendorStock`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `balajiWorkOrder`
--
ALTER TABLE `balajiWorkOrder`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `employeeWages`
--
ALTER TABLE `employeeWages`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
